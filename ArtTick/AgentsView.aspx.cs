﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ARTQM
{
    public partial class AgentsView : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();

                DataTable aux = Lists.GetAUXList();
                Session["AUXList"] = aux;
            }
        }

        public void LoadData()
        {

            Session["Groups"] = Lists.GetGroups(CurrentUser.GroupIDs);


            drpGroup.DataSource = (DataTable)Session["Groups"];
            drpGroup.DataTextField = "GroupName";
            drpGroup.DataValueField = "GroupID";
            drpGroup.DataBind();
            drpGroup_SelectedIndexChanged(null, null);


        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string ucid = UCID;

            //string ani = ANI;

            //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            //try
            //{
            //    conn.Open();
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception("Bildirim Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            //}
            //string name = txtUnitName.Text;
            
            //try
            //{
            //    SqlCommand cmd = new SqlCommand("insert into Units (DepartmentID, ParentUnitID, UnitName, IsActive) values (@p1,@p2, @p3, 1)", conn);
            //    cmd.Parameters.Add(new SqlParameter("@p1", SqlDbType.Int)).Value = drpDept.SelectedValue;
            //    cmd.Parameters.Add(new SqlParameter("@p2", SqlDbType.Int)).Value = drpParent.SelectedValue;
            //    cmd.Parameters.Add(new SqlParameter("@p3", SqlDbType.NVarChar, 250)).Value = name;
            //    cmd.CommandType = CommandType.Text;
            //    int result = cmd.ExecuteNonQuery();

            //    if (result == 1)
            //    {
            //        hResult.InnerHtml = "Kayıt Başarılı";
            //        bResult.InnerHtml = "Birim kaydı başarıyla alınmıştır.";
            //        divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
            //        LoadData();

            //    }
            //    else
            //    {
            //        hResult.InnerHtml = "Hata Oluştu!";
            //        bResult.InnerHtml = "Birim kaydı gerçekleştirilemedi.";
            //        divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";

            //    }


            //}
            //catch (Exception ex)
            //{
            //    hResult.InnerHtml = "Hata Oluştu!";
            //    bResult.InnerHtml = "Birim kaydı gerçekleştirilemedi. Hata Mesajı: " + ex.Message;
            //    divResult.InnerHtml = "<script>$('#myModal').modal();  </script>";


            //}
            //finally
            //{
            //    conn.Close();
            //}
        }

        protected void grdData_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            //try
            //{
            //    conn.Open();
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception("Bildirim Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            //}
            //try
            //{ 
            //    string id = grdData.SelectedDataKey.Value.ToString();
            //SqlCommand cmd = new SqlCommand("update Units set IsActive = IsActive ^ 1 where UnitID= @p1", conn);
            //cmd.Parameters.Add(new SqlParameter("@p1", SqlDbType.NVarChar, 500)).Value = id;
            
            //cmd.CommandType = CommandType.Text;
            //int result = cmd.ExecuteNonQuery();

            //if (result == 1)
            //{
            //    hResult.InnerHtml = "Kayıt Başarılı";
            //    bResult.InnerHtml = "Birim kaydı başarıyla güncellendi.";
            //    divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
            //    LoadData();

            //}
            //else
            //{
            //    hResult.InnerHtml = "Hata Oluştu!";
            //    bResult.InnerHtml = "Birim kaydı güncellenmedi.";
            //    divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";

            //}

            //}
            //catch (Exception ex)
            //{
            //    hResult.InnerHtml = "Hata Oluştu!";
            //    bResult.InnerHtml = "Birim kaydı güncellenmedi. Hata Mesajı: " + ex.Message;
            //    divResult.InnerHtml = "<script>$('#myModal').modal();  </script>";


            //}
            //finally
            //{
            //    conn.Close();
            //}
        }

        protected void drpGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadAgents();
            
        }
        public void LoadAgents()
        {



            //DataTable agents = Lists.GetAgentsList(int.Parse(drpGroup.SelectedValue));
            //grdData.DataSource = agents;
            //grdData.DataBind();


            //if (agents.Rows.Count > 0)
            //{
            //    DataView vi = new DataView(agents);
            //    vi.Sort = "StatusCode asc, Status desc";
            //    DataTable stats = vi.ToTable(true, new string[] { "Status", "StatusCode" });

            //    DataTable auxlist = (DataTable)Session["AUXList"];
            //    DataTable result = new DataTable();
            //    result.Columns.Add("StatusName");
            //    result.Columns.Add("Status");
            //    result.Columns.Add("StatusCount");

            //    DataRow total = result.NewRow();
            //    foreach (DataRow dr in stats.Rows)
            //    {

            //        DataRow nrow = result.NewRow();
            //        nrow["Status"] = dr["Status"].ToString();
            //        nrow["StatusName"] = GetCardStatus(dr["Status"].ToString(), dr["StatusCode"].ToString());
            //        DataRow[] _res = agents.Select("Status='" + dr["Status"].ToString() + "' and StatusCode=" + dr["StatusCode"].ToString() + "");
            //        nrow["StatusCount"] = _res.Length.ToString();

            //        result.Rows.Add(nrow);
            //    }
            //    total[0] = "TOPLAM";
            //    total[1] = "TOPLAM";
            //    total[2] = agents.Rows.Count;

            //    result.Rows.InsertAt(total, 0);
            //    lstSummary.DataSource = result;
            //    lstSummary.DataBind();

            //    divResult.InnerHtml = "";

            //}
            //else
            //{
            //    hResult.InnerHtml = "Uyarı!";
            //    bResult.InnerHtml = "Gruba kayıtlı operatör bulunmamaktadır!";
            //    divResult.InnerHtml = "<script>$('#myModal').modal();  </script>";


            //}

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            //ExportToExcel(grdData, "Birimler");
        }

        public string GetCardStyle(string Status)
        {
            //return "col-sm-12 card card-inverse bg-success";

            switch (Status)
            {
                case "RING":
                case "BUSY":
                    return "col-sm-12 card card-inverse bg-warning";
                    break;
                case "ACW":
                    return "col-sm-12 card card-inverse bg-primary";
                    break;
                case "AUX":
                    return "col-sm-12 card card-inverse bg-danger";
                    break;
                case "LOGIN":
                    return "col-sm-12 card card-inverse bg-gray";
                    break;
                case "AVAIL":
                     return "col-sm-12 card card-inverse bg-success";
                    break;
                case "OFFLINE":
                case "LOGOFF":
                default: 
                return "col-sm-12 card card-inverse bg-gray";
                
            }
        }

        public string GetCardStatus(string Status, string StatusCode)
        {

            DataTable auxList = (DataTable)Session["AUXList"];


            switch (Status)
            {
                case "BUSY":
                    return "ÇAĞRIDA";
                    break;
                case "ACW":
                    return "ACW";
                    break;
                case "AUX":
                       try
                       {
                          return  auxList.Select("SettingName='AUX_" + StatusCode.Trim().PadLeft(2, '0') + "'")[0]["DefaultValue"].ToString();
                       }
                    catch
                       {
                        return Status;
                    }
                    break;
                case "AVAIL":
                    return "HAZIR";
                    break;
                case "OFFLINE":
                case "LOGOFF":
                    return "OFFLINE";
                    break;
                case "RING":
                    return "RING";
                    break;
                default: 
                    return "N/A";

            }

        }

        protected void tmView_Tick(object sender, EventArgs e)
        {
            LoadAgents();
        }
    }
}