﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ARTQM
{
    public partial class MTitles : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string tanimlamalar = Session["TANIMLAMALAR"].ToString();

            if (tanimlamalar.Equals("0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                        "alert('Yetkiniz Yok'); window.location='" + Request.UrlReferrer.ToString() + "';", true);
                return;
            }
            if (!IsPostBack)
            {
                Session["SelectedID"] = null;
             
                LoadForms();
                if (string.IsNullOrEmpty(Request.QueryString["formno"]))
                {

                    drpForm.SelectedIndex = 0;
                    LoadData();

                }
                else 
                {
                    drpForm.SelectedValue = Request.QueryString["formno"];
                    LoadData();
                }
            }
        }

        public void LoadForms()
        {
            Session["Forms"] = Lists.GetForms();

            
            drpForm.DataSource = (DataTable)Session["Forms"];
            drpForm.DataValueField = "PROJENO";
            drpForm.DataTextField= "PROJEADI";
            drpForm.DataBind();

            drpForm1.DataSource = (DataTable)Session["Forms"];
            drpForm1.DataValueField = "PROJENO";
            drpForm1.DataTextField = "PROJEADI";
            drpForm1.DataBind();
    


        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
           
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Bildirim Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            string name = txtFormName.Text;
            
            try
            {
                SqlCommand cmd = new SqlCommand("insert into KRITERLER (PROJENO, KRITER, KRITERNO, MAKSSKOR) values (@PROJENO, @KRITER, @KRITERNO, @MAKSSKOR)", conn);

               int result = 0;
               if (Session["SelectedID"] == null)
               {
                   cmd.CommandType = CommandType.Text;
                   cmd.Parameters.Add("@KRITER", SqlDbType.NVarChar, 255).Value = txtFormName.Text ;
                   cmd.Parameters.Add("@PROJENO", SqlDbType.Int).Value = int.Parse(drpForm1.SelectedValue);
                   cmd.Parameters.Add("@KRITERNO", SqlDbType.Int).Value = int.Parse(txtOrder.Text.Trim());
                   cmd.Parameters.Add("@MAKSSKOR", SqlDbType.Int).Value = int.Parse(txtMaksSkor.Text.Trim());
                   result = cmd.ExecuteNonQuery();
               }
               else
               {
                   cmd.CommandText = "UPDATE KRITERLER SET KRITER=@KRITER, KRITERNO=@KRITERNO, MAKSSKOR=@MAKSSKOR where DUMMYID=@DUMMYID";
                   cmd.CommandType = CommandType.Text;
                   cmd.Parameters.Add("@DUMMYID", SqlDbType.Int).Value = Session["SelectedID"].ToString();
                   cmd.Parameters.Add("@KRITER", SqlDbType.NVarChar, 255).Value = txtFormName.Text;
                   cmd.Parameters.Add("@KRITERNO", SqlDbType.Int).Value = int.Parse(txtOrder.Text.Trim());
                   cmd.Parameters.Add("@MAKSSKOR", SqlDbType.Int).Value = int.Parse(txtMaksSkor.Text.Trim());
                   result = cmd.ExecuteNonQuery();
               }

                   hResult.InnerHtml = "Kayıt Başarılı";
                    bResult.InnerHtml = "Kırılım kaydı başarıyla alınmıştır.";
                    divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                  
                    LoadData();

            }
            catch (Exception ex)
            {
                hResult.InnerHtml = "Hata Oluştu!";
                bResult.InnerHtml = "Kırılım kaydı gerçekleştirilemedi. Hata Mesajı: " + ex.Message;
                divResult.InnerHtml = "<script>$('#myModal').modal();  </script>";


            }
            finally
            {
                conn.Close();
                Session["SelectedID"] = null;
            }
        }
        
     
        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    Session["SelectedID"] = null;
        //    ExportToExcel(grdData, drpForm.SelectedItem.Text.Replace(" ", "_").Replace("-", "_").Replace(".", "_").Replace("/", "_").Replace("\\", "_") + "_KIRILIMLAR");
        //}

        protected void lnkNew_Click(object sender, EventArgs e)
        {
            try
            {
                Session["SelectedID"] = null;
                string FormNo = Request.QueryString["formno"];
                drpForm1.SelectedValue = FormNo;
                txtFormName.Text = "";
                txtOrder.Text = "0";
                txtMaksSkor.Text = "0";
                divResult.InnerHtml = "<script>$('#divUpdate').modal(); </script>";
            }
            catch
            {
            }
        }

        protected void grdData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "editt":
                    try
                    {
                        Session["SelectedID"] = null;
                        DataTable groups = (DataTable)Session["Titles"];
                        string id = e.CommandArgument.ToString();
                        Session["SelectedID"] = id;
                        DataRow dr = groups.Select("DUMMYID=" + id)[0];
                        txtFormName.Text = dr["KRITER"].ToString();
                        drpForm1.SelectedValue = dr["PROJENO"].ToString();
                        txtOrder.Text = dr["KRITERNO"].ToString();
                        txtMaksSkor.Text = dr["MAKSSKOR"].ToString();
                        divResult.InnerHtml = "<script>$('#divUpdate').modal(); </script>";

                    }
                    catch
                    {
                    }
                    break;

                case "questions":
                    try
                    {
                        string id = e.CommandArgument.ToString();
                        Session["SelectedID"] = null;
                        Response.Redirect("MQuestions.aspx?formno=" + id, true);

                    }
                    catch
                    {
                    }
                    break;
                default: break;
              
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("MTitles.aspx?formno=" + drpForm.SelectedValue, true);
                
        }
        public void LoadData()
        {
            try
            {
                int formno = int.Parse(drpForm.SelectedValue);
                Session["Titles"] = Lists.GetTitles(formno);
                grdData.DataSource = (DataTable)Session["Titles"];
                grdData.DataKeyNames = new string[] { "DUMMYID" };
                grdData.DataBind();
            }
            catch(Exception ex)
            {

            }
           
        }
    }
}