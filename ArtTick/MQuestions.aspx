﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="MQuestions.aspx.cs" Inherits="ARTQM.MQuestions" ValidateRequest="false" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="form-horizontal">
<fieldset>
     <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" runat="server" id="hResult"></h4>
        </div>
        <div class="modal-body" runat="server" id="bResult">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
        </div>
      </div>
      
    </div>
  </div>
     <div class="modal fade" id="divUpdate" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" runat="server" id="hResult2">Yeni Soru Kartı</h4>
        </div>
        <div class="modal-body" runat="server" id="bResult2">
    <div class="form-group">
  
  <div class="col-sm-12">     
    <div class="form-group">
    
     <label class="col-sm-3 control-label" for="drpTitle">Kırılım</label>
    
  <div class="col-sm-9">
  	<asp:DropDownList runat="server" id="drpTitle" ClientIDMode="Static" name="drpTitle" class="form-control input-sm"/>
  
      
  </div>
        </div>
        
        
 <div class="form-group">
       <label class="col-sm-3 control-label" for="txtOrder">Sıra Numarası</label>
    
      <div class="col-sm-9">
  	    <asp:TextBox runat="server" id="txtOrder" ClientIDMode="Static" name="txtOrder" placeholder="Sıra No" class="form-control input-sm" TextMode="Number"/>
    </div>
  

</div>
      
      <div class="form-group">
    
     <label class="col-sm-3 control-label" for="txtSORUMETNI">Soru Metni</label>
    
      <div class="col-sm-9">
  	    <asp:TextBox runat="server" id="txtSORUMETNI" ClientIDMode="Static" name="txtSORUMETNI" placeholder="Açık Soru Metni" TextMode="MultiLine" Rows="2" class="form-control input-sm"/>
    </div>
  
 
</div>
    <div class="form-group">
    
     <label class="col-sm-3 control-label" for="txtKISAETIKET">Kısa Metin</label>
    
      <div class="col-sm-9">
  	    <asp:TextBox runat="server" id="txtKISAETIKET" ClientIDMode="Static" name="txtKISAETIKET" placeholder="Kısa Soru Metni (Rapor tablolarında kullanılacaktır)" class="form-control input-sm"/>
    </div>
  
 
</div>
       <div class="form-group">
    
     <label class="col-sm-3 control-label" for="drpSORUTIPI">Soru Tipi</label>
    
  <div class="col-sm-9">
  	<asp:DropDownList runat="server" id="drpSORUTIPI" ClientIDMode="Static" name="drpSORUTIPI" class="form-control input-sm"/>
  
      
  </div>
        </div>

      <div class="form-group">
    
     <label class="col-sm-12 control-label" for="">SEÇENEK/PUANLAMA AYARLARI</label>
    
 
        </div>   
      <asp:UpdatePanel ID="pnlTemplate" runat="server" UpdateMode="Conditional">
          <ContentTemplate>
       <div class="form-group">
    
     <label class="col-sm-3 control-label" for="drpTemplate">Taslaklar</label>
    
  <div class="col-sm-9">
  	<asp:DropDownList runat="server" id="drpTemplate" ClientIDMode="Static" name="drpTemplate" class="form-control input-sm" AutoPostBack="True" OnSelectedIndexChanged="drpTemplate_SelectedIndexChanged"/>
  
      
  </div>
        </div>

       <div class="form-group">
    
     <label class="col-sm-3 control-label" for="txtSECENEKLER">Seçenekler</label>
    
      <div class="col-sm-9">
  	    <asp:TextBox runat="server" id="txtSECENEKLER" ClientIDMode="Static" name="txtSECENEKLER" placeholder="Değer ve seçenekler listesi" class="form-control input-sm"/>
    </div>
  
 
</div>


       <div class="form-group">
    
     <label class="col-sm-3 control-label" for="txtPUANLAMA">Puanlama</label>
    
      <div class="col-sm-9">
  	    <asp:TextBox runat="server" id="txtPUANLAMA" ClientIDMode="Static" name="txtPUANLAMA" placeholder="Değer ve puan listesi" class="form-control input-sm"/>
    </div>
  
 
</div>
</ContentTemplate>
          <Triggers>
              <asp:AsyncPostBackTrigger ControlID="drpTemplate" EventName="SelectedIndexChanged" />
          </Triggers>
      </asp:UpdatePanel>
          <div class="form-group">
    
     <label class="col-sm-3 control-label" for="txtAciklama">Açıklama</label>
    
      <div class="col-sm-9">
  	    <asp:TextBox runat="server" id="txtAciklama" ClientIDMode="Static" name="txtAciklama" placeholder="Soruya ilişkin açıklamalar" class="form-control input-sm" Rows="2" TextMode="MultiLine"/>
    </div>
  
 
</div>
    <div class="form-group col-sm-12">
  <label class="col-sm-10 control-label" for="btnSave"></label>
  <div class="col-sm-2 pull-right">
    <asp:Button id="btnSave" runat="server" class="btn btn-success" Text="Kaydet" OnClick="btnSave_Click" />
  </div>
</div>
        </div>
        </div></div>
      
      </div>
      
    </div>
  </div>
   


    <div runat="server" id="divResult"></div>
    


   

    


   
    <legend>TANIMLI SORULAR</legend>

      <div class="row">

     <div class="form-group">
    
     <label class="col-sm-1 control-label" for="drpForm">Form Seçiniz</label>
    
  <div class="col-sm-3">
  	<asp:DropDownList runat="server" id="drpForm" ClientIDMode="Static" name="drpForm" class="form-control input-sm"/>
  
      
  </div>
         <div class="col-sm-1" style="margin-top:-2px;">
  <asp:Button id="btnSearch" runat="server" class="btn btn-success" Text="LİSTELE" OnClick="btnSearch_Click" />
  </div>
 
</div><div class="col-sm-6"><hr /></div></div>
  <div class="form-group col-sm-12">   <div class="col-sm-3 btn-group" style="margin-left:-15px;">
     <asp:LinkButton ID="lnkNew" runat="server" class="btn btn-danger"  data-toggle="tooltip" data-placement="bottom" title="Yeni Soru" OnClick="lnkNew_Click" ><i class="glyphicon glyphicon-plus" aria-hidden="true"></i> Yeni Soru</asp:LinkButton>
     <%--<asp:LinkButton ID="Button1" runat="server" CssClass="btn btn-danger"  OnClick="Button1_Click" data-toggle="tooltip" data-placement="bottom" title="Excel' e Aktar"><i class="fa fa-file-excel-o" aria-hidden="true"></i>Excel'e Aktar</asp:LinkButton>--%>
            </div>
    <div class="col-sm-9">
        
        <span class="label label-default" id="spanInfo" runat="server"></span>
        </div>
      </div>
  <div class="form-group">
  
  <div class="col-sm-12">
      <br />
 <asp:GridView ID="grdData" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-responsive table-striped" 
     RowStyle-HorizontalAlign="Center"
     HeaderStyle-HorizontalAlign="Center" OnRowCommand="grdData_RowCommand">
     <Columns>
        <asp:BoundField HeaderText="Ana Kırılım"  DataField="KRITER" ItemStyle-CssClass="col-sm-1" ItemStyle-HorizontalAlign="Left">
<ItemStyle HorizontalAlign="Left" CssClass="col-sm-1"></ItemStyle>
         </asp:BoundField>    <asp:TemplateField HeaderText="Soru"  ItemStyle-CssClass="col-sm-4" >
             <ItemStyle CssClass="col-sm-4" HorizontalAlign="Left"></ItemStyle>
             <ItemTemplate>
                 <%# Eval("SORUSIRA").ToString() + ". "+ Eval("SORUMETNI").ToString()  %>
        </ItemTemplate>
                 </asp:TemplateField>

         
          <asp:BoundField HeaderText="Kısa Etiket"  DataField="KISAETIKET" ItemStyle-CssClass="col-sm-1" ItemStyle-HorizontalAlign="Left">
<ItemStyle HorizontalAlign="Left" CssClass="col-sm-1"></ItemStyle>
         </asp:BoundField>
         
      
           
       <asp:BoundField HeaderText="Soru Tipi"  DataField="SORUTIPI2" ItemStyle-CssClass="col-sm-1" ItemStyle-HorizontalAlign="Left">
<ItemStyle HorizontalAlign="Left" CssClass="col-sm-1"></ItemStyle>
         </asp:BoundField>
    
       <asp:BoundField HeaderText="Seçenekler"  DataField="SECENEKLER" ItemStyle-CssClass="col-sm-3" ItemStyle-HorizontalAlign="Left">
<ItemStyle HorizontalAlign="Left" CssClass="col-sm-1"></ItemStyle>
         </asp:BoundField>
      <asp:BoundField HeaderText="Puanlama"  DataField="SECENEKLERPUAN" ItemStyle-CssClass="col-sm-3" ItemStyle-HorizontalAlign="Left">
<ItemStyle HorizontalAlign="Left" CssClass="col-sm-1"></ItemStyle>
         </asp:BoundField>
         <asp:TemplateField HeaderText="İşlemler">
             <ItemStyle CssClass="col-sm-1" HorizontalAlign="Left"></ItemStyle>
             <ItemTemplate>
                   <asp:LinkButton  runat="server" ID="btnEdit" CommandName="editt" CommandArgument='<%#Eval("SORUNO").ToString() %>'  Text="<span data-toggle='tooltip' title='Düzenle' ><span class='glyphicon glyphicon-edit'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
           
                 
                 
             </ItemTemplate>
         </asp:TemplateField>


     </Columns>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<RowStyle HorizontalAlign="Center"></RowStyle>
      </asp:GridView>
    
  </div>

     
</div>


    </fieldset>
          </div>
 </asp:Content>
