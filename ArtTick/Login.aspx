﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="ARTQM.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=10" />
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css" media="screen" />

    <link rel="stylesheet" href="css/bootstrap-theme.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/bootstrap-ie7.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="css/common.css" type="text/css" media="screen" />
    <link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="js/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.1.custom.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/daterangepicker/moment.min.js"></script>
    <script type="text/javascript" src="js/daterangepicker/daterangepicker.js"></script>
    <link rel="stylesheet" href="js/daterangepicker/daterangepicker.css" type="text/css" media="screen" />
    

    <style>
        .cd-main-header {
            position: absolute;
            z-index: 2;
            top: 0;
            left: 0;
            height: 45px;
            width: 100%;
            background: #3e454c;
            box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

            .cd-main-header::after {
                clear: both;
                content: "";
                display: table;
            }

        @media only screen and (min-width: 768px) {
            .cd-main-header {
                position: fixed;
            }
        }

        .cd-logo {
            float: left;
            display: block;
            margin: 11px 0 0 5%;
        }

            .cd-logo img {
                display: block;
            }

        @media only screen and (min-width: 768px) {
            .cd-logo {
                margin: 16px 0 0 36px;
            }
        }

        .form-group {
            margin-bottom: 6px;
        }

        .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
            position: relative;
            min-height: 1px;
            padding-right: 3px;
            padding-left: 15px;
        }

        .wrapper {
            margin-top: 240px;
            margin-bottom: 20px;
        }

        .form-signin {
            max-width: 420px;
            padding: 30px 38px 66px;
            margin: 0 auto;
            background-color: #e5e5e5;
            border: 3px dotted rgba(0,0,0,0.1);
        }

        .form-signin-heading {
            text-align: center;
            margin-bottom: 30px;
        }

        .form-control {
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
        }

        input[type="text"] {
            margin-bottom: 0px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        .colorgraph {
            height: 7px;
            border-top: 0;
            background: #c4e17f;
            border-radius: 5px;
            background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
            background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
            background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
            background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
        }
    </style>

</head>
<body>
    <nav class="navbar navbar-custom navbar-fixed-top">
        <div class="container">
            <div class="navbar-header cd-main-header">

                <a class="navbar-brand" href="http://artpro.com.tr/wpr/" style="color: #fff;">
                    <span class="glyphicon glyphicon-pushpin"></span>ART<span style="color: #F15854;">QM</span></a>


            </div>
        </div>

    </nav>
    <div class="container">
        <div class="wrapper">

            <form action="" method="post" name="Login_Form" class="form-signin" runat="server">
                <h3 class="form-signin-heading">KULLANICI GİRİŞİ</h3>
                <hr class="colorgraph" />
                <br />

                <input type="text" class="form-control" runat="server" id="txtUser" name="Username" placeholder="Kullanıcı Adı" required="" autofocus="" />
                <input type="password" class="form-control" runat="server" id="txtPPass" name="Password" placeholder="Şifre" required="" />



                <asp:Button CssClass="btn btn-lg btn-block btn-danger " runat="server" ID="Submit" Text="Giriş" OnClick="Submit_Click"></asp:Button>
                <div id="divResult" runat="server">&nbsp;</div>
            </form>
        </div>

    </div>
    <img src="images/logo4.png" alt="powered by ARTPRO TEKNOLOJI" width="150" height:"150" style="position:fixed;bottom:0;right:0; background:transparent"  />
</body>
</html>
