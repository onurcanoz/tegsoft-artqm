﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;

namespace ARTQM
{
    public class DataProvider
    {
        string responseFromServer = string.Empty;
        public string GetData(string sql)
        {
            WebRequest request = WebRequest.Create("http://192.168.1.220/Tobe/app/ApplicationServlet?xmlService=DataProvider&serviceType=DataProvider&usercode=artpro.admin&clearpassword=Artpro12!&method=getData&sqlQuery=" + sql);

            WebResponse response = request.GetResponse();

            using (Stream dataStream = response.GetResponseStream())
            {

                StreamReader reader = new StreamReader(dataStream);

                responseFromServer = reader.ReadToEnd();

                //responseFromServer = responseFromServer.Replace("&lt;", "<")
                //                                   .Replace("&amp;", "&")
                //                                   .Replace("&gt;", ">")
                //                                   .Replace("&quot;", "\"")
                //                                   .Replace("&apos;", "'");
            }
            response.Close();

            return responseFromServer;
        }
        public DataTable GetCallRec(string sql)
        {
            string Result = EverythingBetween(GetData(sql), "<Rows>", "</Rows>");

            XmlDocument xml = new XmlDocument();
            xml.LoadXml("<root>" + Result + "</root>");

            XmlNodeList xnList = xml.SelectNodes("/root/Row");

            DataTable callRec = new DataTable();
            callRec.Columns.Add("Callid");
            callRec.Columns.Add("Startdate");
            callRec.Columns.Add("Partynames");
            //callRec.Columns.Add("dialedNum");
            callRec.Columns.Add("Duration");
            callRec.Columns.Add("Direction");
            callRec.Columns.Add("Partyagentnames");
            callRec.Columns.Add("disposition");
            callRec.Columns.Add("audioFileName");
            callRec.Columns.Add("Skillset");
            callRec.Columns.Add("DONEM");
            callRec.Columns.Add("UID");
            callRec.Columns.Add("Skill");

            foreach (XmlNode xn in xnList)
            {
                if (GetXmlAttribute("AUDIOFILENAME", xn) != string.Empty)
                {
                    DataRow dr = callRec.NewRow();

                    dr["Callid"] = GetXmlAttribute("UNIQUEID", xn);
                    dr["Startdate"] = GetXmlAttribute("CALLDATE", xn);
                    dr["Partynames"] = GetXmlAttribute("SRC", xn) + ", " + GetXmlAttribute("DST", xn);
                    //dr["dialedNum"] = GetXmlAttribute("DST", xn);
                    dr["Duration"] = GetXmlAttribute("DURATION", xn);

                    if (GetXmlAttribute("USERFIELD", xn) == "INBOUND")
                        dr["Direction"] = "1";
                    else if (GetXmlAttribute("USERFIELD", xn) == "OUTBOUND")
                        dr["Direction"] = "2";
                    else
                        dr["Direction"] = "3";

                    dr["Partyagentnames"] = GetXmlAttribute("USERCODE", xn);
                    dr["disposition"] = GetXmlAttribute("DISPOSITION", xn);
                    dr["audioFileName"] = GetXmlAttribute("AUDIOFILENAME", xn);
                    dr["Skillset"] = GetXmlAttribute("SKILL", xn);

                    string[] Donem = (dr["Startdate"].ToString().Split(' ')[0]).Split('/');
                    dr["DONEM"] = Donem[2] + Donem[1] + Donem[0];
                    dr["UID"] = GetXmlAttribute("UID", xn);
                    dr["Skill"] = GetXmlAttribute("SKILLID", xn);

                    callRec.Rows.Add(dr);
                }
            }
            return callRec;
        }

        public DataTable EditEvalGetList(string sql)
        {
            string Result = EverythingBetween(GetData(sql), "<Rows>", "</Rows>");

            XmlDocument xml = new XmlDocument();
            xml.LoadXml("<root>" + Result + "</root>");

            XmlNodeList xnList = xml.SelectNodes("/root/Row");

            DataTable dt = new DataTable();
            dt.Columns.Add("inum");
            dt.Columns.Add("startedat");
            dt.Columns.Add("duration2");
            dt.Columns.Add("src");
            dt.Columns.Add("dst");
            dt.Columns.Add("userfield");
            dt.Columns.Add("usercode");

            foreach (XmlNode xn in xnList)
            {
                DataRow dr = dt.NewRow();

                dr["inum"] = GetXmlAttribute("UNIQUEID", xn);
                dr["startedat"] = GetXmlAttribute("CALLDATE", xn);
                dr["duration2"] = GetXmlAttribute("DURATION", xn);
                dr["src"] = GetXmlAttribute("SRC", xn) + ", " + GetXmlAttribute("DST", xn);
                if (GetXmlAttribute("USERFIELD", xn) == "INBOUND")
                        dr["userfield"] = "1";
                    else if (GetXmlAttribute("USERFIELD", xn) == "OUTBOUND")
                        dr["userfield"] = "2";
                    else
                        dr["userfield"] = "3";
                dr["usercode"] = GetXmlAttribute("USERCODE", xn);

                dt.Rows.Add(dr);
            }
            return dt;
        }
        public DataTable QueryEvalSummary(string sql)
        {
            string Result = EverythingBetween(GetData(sql), "<Rows>", "</Rows>");

            XmlDocument xml = new XmlDocument();
            xml.LoadXml("<root>" + Result + "</root>");

            XmlNodeList xnList = xml.SelectNodes("/root/Row");

            DataTable dt = new DataTable();
            dt.Columns.Add("REPORTDATE");
            dt.Columns.Add("ACDGROUP");
            dt.Columns.Add("AGENTS");
            dt.Columns.Add("CAGRI");
            dt.Columns.Add("DEGERLENDIRME");

            foreach (XmlNode xn in xnList)
            {
                DataRow dr = dt.NewRow();

                dr["REPORTDATE"] = GetXmlAttribute("REPORTDATE", xn);
                dr["ACDGROUP"] = GetXmlAttribute("ACDGROUP", xn);
                dr["AGENTS"] = GetXmlAttribute("AGENTS", xn);
                dr["CAGRI"] = GetXmlAttribute("CAGRI", xn);
                dr["DEGERLENDIRME"] = GetXmlAttribute("DEGERLENDIRME", xn);

                dt.Rows.Add(dr);
                
            }
            return dt;
        }
        public DataTable getUserList(string sql)
        {
            string Result = EverythingBetween(GetData(sql), "<Rows>", "</Rows>");

            XmlDocument xml = new XmlDocument();
            xml.LoadXml("<root>" + Result + "</root>");

            XmlNodeList xnList = xml.SelectNodes("/root/Row");

            DataTable dt = new DataTable();
            dt.Columns.Add("LOGID");
            dt.Columns.Add("AD");

            foreach (XmlNode xn in xnList)
            {
                DataRow dr = dt.NewRow();

                dr["LOGID"] = GetXmlAttribute("LOGID", xn);
                dr["AD"] = GetXmlAttribute("AD", xn);

                dt.Rows.Add(dr);

            }
            return dt;
        }
        public DataTable GetList(string sql, string param1, string param2, string param3, string param4)
        {
            string Result = EverythingBetween(GetData(sql), "<Rows>", "</Rows>");

            XmlDocument xml = new XmlDocument();
            xml.LoadXml("<root>" + Result + "</root>");

            XmlNodeList xnList = xml.SelectNodes("/root/Row");

            DataTable agentList = new DataTable();
            agentList.Columns.Add(param3);
            agentList.Columns.Add(param4);

            foreach (XmlNode xn in xnList)
            {
                DataRow dr = agentList.NewRow();

                dr[param3] = GetXmlAttribute(param1, xn);
                dr[param4] = GetXmlAttribute(param2, xn);

                agentList.Rows.Add(dr);
            }
            return agentList;

        }
        public DataTable GetAgentData(string sql)
        {
            string Result = EverythingBetween(GetData(sql), "<Rows>", "</Rows>");

            XmlDocument xml = new XmlDocument();
            xml.LoadXml("<root>" + Result + "</root>");

            XmlNodeList xnList = xml.SelectNodes("/root/Row");

            DataTable dt = new DataTable();
            dt.Columns.Add("UID");
            dt.Columns.Add("Pass");
            dt.Columns.Add("Name");
            dt.Columns.Add("Usercode");
            dt.Columns.Add("Eposta");

            foreach (XmlNode xn in xnList)
            {
                DataRow dr = dt.NewRow();

                dr["UID"] = GetXmlAttribute("UID", xn);
                dr["Name"] = GetXmlAttribute("USERNAME", xn) + " " + GetXmlAttribute("SURNAME", xn);
                dr["Usercode"] = GetXmlAttribute("USERCODE", xn);
                dr["Pass"] = GetXmlAttribute("PASS", xn);

                dt.Rows.Add(dr);
            }
            return dt;
        }
        public string GetXmlAttribute(string Attribute, XmlNode xn)
        {
            string Result = string.Empty;
            try
            {
                Result = xn[Attribute].InnerText.Trim().ToString();
            }
            catch (Exception Ex)
            {
               
            }
            return Result;
        }
        public string EverythingBetween(string source, string start, string end)
        {
            var results = string.Empty;

            string pattern = string.Format(
                "{0}({1}){2}",
                Regex.Escape(start),
                ".+?",
                 Regex.Escape(end));

            foreach (Match m in Regex.Matches(source, pattern))
            {
                results = m.Groups[1].Value;
            }

            return results;
        }
    }
}