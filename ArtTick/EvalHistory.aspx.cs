﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
namespace ARTQM
{
    public partial class EvalHistory : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string degerlendirmeler = Session["DEGERLENDIRMELER"].ToString();

            if (degerlendirmeler.Equals("0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                        "alert('Yetkiniz Yok'); window.location='" + Request.UrlReferrer.ToString() + "';", true);
                return;
            }

            if (!IsPostBack)
            {
                DateTime date = DateTime.Today;
                var firstDayOfMonth = DateTime.Today.AddDays(-2);
                var lastDayOfMonth = DateTime.Today;
                //txtRange.Text = firstDayOfMonth.ToString("dd.MM.yyyy") + " - " + lastDayOfMonth.ToString("dd.MM.yyyy");
                txtEvalRange.Text = firstDayOfMonth.ToString("dd.MM.yyyy") + " - " + lastDayOfMonth.ToString("dd.MM.yyyy");
                LoadGroups();
            }
        }
        public void LoadGroups()
        {
            DataTable tmp = null;
            DataTable tmp2 = null;

            if (!Session["UserAgents"].ToString().Equals("0"))
                tmp = ((DataTable)Session["AGENTS"]).Select("AgentID IN (" + Session["UserAgents"] + ")").CopyToDataTable();
            else
                tmp = (DataTable)Session["AGENTS"];
            drpGroup.DataSource = tmp;
            drpGroup.DataTextField = "AGENT";
            drpGroup.DataValueField = "AgentID";
            drpGroup.DataBind();
            drpGroup.Items.Insert(0, new ListItem("Tümü", "0"));

            if (!Session["UserSkills"].ToString().Equals(""))
                tmp2 = ((DataTable)Session["SKILLS"]).Select("SkillID IN (" + Session["UserSkills"] + ")").CopyToDataTable();
            else
                tmp2 = (DataTable)Session["SKILLS"];

            drpSkill.DataSource = tmp2;
            drpSkill.DataTextField = "Skill";
            drpSkill.DataValueField = "SkillID";
            drpSkill.DataBind();
            drpSkill.Items.Insert(0, new ListItem("Tümü", "0"));
            drpSkill.SelectedIndex = 0;



            drpUsers.DataSource = Lists.GetUsers();
            drpUsers.DataTextField = "KULLANICI";
            drpUsers.DataValueField = "KULLANICI";
            drpUsers.DataBind();
            drpUsers.Items.Insert(0, new ListItem("Tümü", "0"));



            drpForm.DataSource = Lists.GetForms();
            drpForm.DataValueField = "PROJENO";
            drpForm.DataTextField = "PROJEADI";
            drpForm.DataBind();
            drpForm.Items.Insert(0, new ListItem("Tümü", "0"));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            divScript.InnerHtml = "";


            WFOUtils ws = new WFOUtils();

            int durStart = -1;
            int durEnd = -1;

            try
            {
                durStart = int.Parse(txtStart.Text.Trim());
            }
            catch
            {
                durStart = -1;
                txtStart.Text = durStart.ToString();
            }

            try
            {
                durEnd = int.Parse(txtEnd.Text.Trim());
            }
            catch
            {
                durEnd = -1;
                txtEnd.Text = durEnd.ToString();
            }


            //DateTime callStart = DateTime.Now;
            //DateTime callEnd= DateTime.Now;
            //DateTime evalStart = DateTime.Now;
            //DateTime evalEnd = DateTime.Now;

            string evalDateStart = string.Empty;
            string evalDateEnd = string.Empty;

            try
            {

                //callStart = DateTime.ParseExact(txtRange.Text.Split('-')[0].Trim() + " 00:00:00", "dd.MM.yyyy HH:mm:ss", null);
                evalDateStart = DateTime.Parse(txtEvalRange.Text.Split('-')[0].Trim()).ToString("yyyy-MM-dd 00:00:00");
            }
            catch (Exception)
            {

                //callStart = DateTime.MinValue;

                evalDateStart = DateTime.MinValue.ToString("yyyy-MM-dd");
            }

            try
            {
                //callEnd = DateTime.ParseExact(txtRange.Text.Split('-')[1].Trim() + " 23:59:59", "dd.MM.yyyy HH:mm:ss", null);
                evalDateEnd = DateTime.Parse(txtEvalRange.Text.Split('-')[1].Trim()).ToString("yyyy-MM-dd 23:59:59");
            }
            catch (Exception)
            {

                //callEnd = DateTime.MaxValue;
                evalDateEnd = DateTime.MaxValue.ToString("yyyy-MM-dd");
            }

            //try
            //{
            //    evalStart = DateTime.ParseExact(txtEvalRange.Text.Split('-')[0].Trim() + " 00:00:00", "dd.MM.yyyy HH:mm:ss", null);
            //}
            //catch (Exception)
            //{

            //    evalStart = DateTime.MinValue;
            //}

            //try
            //{

            //    evalEnd =  DateTime.ParseExact(txtEvalRange.Text.Split('-')[1].Trim() + " 23:59:59", "dd.MM.yyyy HH:mm:ss", null);
            //}
            //catch (Exception)
            //{
                
            //    evalEnd = DateTime.MaxValue;
            //}

            DataTable calls = ws.QueryEvals(evalDateStart, evalDateEnd, drpDirection.SelectedValue, durStart, durEnd, txtTelefon.Text.Trim(), drpSkill.SelectedValue == "0" ? Session["UserSkills"].ToString() : drpSkill.SelectedValue, drpGroup.SelectedValue == "0" ? Session["UserAgents"].ToString() : "'"+drpGroup.SelectedValue+"'", drpForm.SelectedValue, drpUsers.SelectedValue == "0" ? "" : drpUsers.SelectedValue);
      
            grdData.DataSource = calls;
            grdData.DataBind();

            string drpUser = drpUsers.SelectedValue;
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            ExportToExcel(grdData, "DEGERLENDIRME_RAPORU_" + txtEvalRange.Text.Replace(" ", "").Replace("-", "_").Replace(".", "_")+"_"+ drpGroup.SelectedItem.Text.Replace(" ","_").Replace("-","_"));
        }

        protected void grdData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {

                case "details":
                    divScript.InnerHtml = "<script> popitup('CallDetail.aspx?callid=" + e.CommandArgument+"')</script>;";
                    break;
                case "view":
                    string id = e.CommandArgument.ToString();
                    string startdate = DateTime.Parse(id.Split('?')[3]).ToString("yyyy-MM-dd HH:mm:ss");
                    divScript.InnerHtml = "<script> popitup('ViewEval.aspx?anket=&callid=" + id.Split('?')[0] + "&donem=" + id.Split('?')[1] + "&projeno="+id.Split('?')[2] +"&startdate=" + startdate + "')</script>;";
                    break;
                default: break;
            }
        }
       

       
    }
}