﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ARTQM
{
    public partial class ObjectionEvals : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!(Convert.ToInt32(Session["YETKİ"].ToString()) > 1))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Yetkiniz Yok'); window.location='Login.aspx';", true);
                return;
            }

            if (!IsPostBack)
            {
                LoadData();
            }
        }
        protected void LoadData()
        {
            txtStartDate.Text = DateTime.Now.AddDays(-1).ToString("dd.MM.yyyy") + " - " + DateTime.Now.ToString("dd.MM.yyyy");

            DataTable tmp = null;

            if (!Session["UserAgents"].ToString().Equals("0"))
                tmp = ((DataTable)Session["AGENTS"]).Select("AgentID IN (" + Session["UserAgents"] + ")").CopyToDataTable();
            else
                tmp = (DataTable)Session["AGENTS"];

            drpGroup.DataSource = tmp;
            drpGroup.DataTextField = "AGENT";
            drpGroup.DataValueField = "AgentID";
            drpGroup.DataBind();
            drpGroup.Items.Insert(0, new ListItem("Tümü", "0"));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            divScript.InnerHtml = string.Empty;
            string startDate = DateTime.Parse(txtStartDate.Text.Split('-')[0].Trim()).ToString("yyyy-MM-dd 00:00:00");
            string endDate = DateTime.Parse(txtStartDate.Text.Split('-')[1].Trim()).ToString("yyyy-MM-dd 23:59:59");

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);

            string sql = "SELECT a.ANKETID, a.ANKETNO, a.SKILLSET as ACDGROUP, a.PARTYAGENTNAMES as AGENTS, a.DEGERLENDIRMETARIHI, a.DEGERLENDIREN, P.PROJEADI as FORM, P.TOPLAMPUAN, a.GENELCSI as PUAN, P.GECERPUAN, a.CALLDATE as GORUSMETARIHI, a.YON, a.SURE, PARTYNAMES as KATILIMCILAR, a.DATAYIL, a.DATAAY, P.PROJENO from ANKETLER as a inner join PROJELER P on P.PROJENO = a.PROJENO inner join ITIRAZ as i on a.ANKETNO = i.ANKETNO WHERE i.DEGERLENDIRMETARIHI BETWEEN '" + startDate + "' AND '" + endDate + "'";

            if (drpDirection.SelectedValue != "0")
            {
                sql += " AND a.YON = '" + drpDirection.SelectedValue + "' ";
            }
            if (drpGroup.SelectedValue != "0")
            {
                sql += " AND a.LOGID IN ('" + drpGroup.SelectedValue + "')";
            }

            try
            {
                con.Open();
                SqlCommand comm = new SqlCommand(sql, con);
                comm.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.SelectCommand.CommandTimeout = 600000;
                DataTable summ = new DataTable("EVALS");
                da.Fill(summ);

                grdData.DataSource = summ;
                grdData.DataBind();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                con.Close();

            }
        }
        protected void ButtonExcel_Click(object sender, EventArgs e)
        {
            ExportToExcel(grdData, "ITIRAZ_RAPORU_" + txtStartDate.Text.Replace(" ", "").Replace("-", "_").Replace(".", "_") + "_" + drpGroup.SelectedItem.Text.Replace(" ", "_").Replace("-", "_"));
        }
        protected void grdData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {

                case "details":
                    divScript.InnerHtml = "<script> popitup('CallDetail.aspx?callid=" + e.CommandArgument + "')</script>;";
                    break;
                case "view":
                    string id = e.CommandArgument.ToString();
                    string startdate = DateTime.Parse(id.Split('?')[3]).ToString("yyyy-MM-dd HH:mm:ss");
                    divScript.InnerHtml = "<script> popitup('ViewEval.aspx?anket=&callid=" + id.Split('?')[0] + "&donem=" + id.Split('?')[1] + "&projeno=" + id.Split('?')[2] + "&startdate=" + startdate + "')</script>;";
                    break;
                default: break;
            }
        }
    }
}