﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ARTQM
{
    public partial class MForms : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string tanimlamalar = Session["TANIMLAMALAR"].ToString();

            if (tanimlamalar.Equals("0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                        "alert('Yetkiniz Yok'); window.location='" + Request.UrlReferrer.ToString() + "';", true);
                return;
            }
            if (!IsPostBack)
            {
                Session["SelectedID"] = null;
             
                LoadData();
            }
        }

        public void LoadData()
        {
            Session["Forms"] = Lists.GetForms();
            grdData.DataSource = (DataTable)Session["Forms"];
            grdData.DataKeyNames = new string[] { "PROJENO" };
            grdData.DataBind();
    
            spanInfo.InnerHtml = "Toplam " + grdData.Rows.Count.ToString() + " kayıt bulundu.";


        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
           
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Bildirim Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            string name = txtFormName.Text;
            
            try
            {
                SqlCommand cmd = new SqlCommand("insert into PROJELER (PROJEADI, GECERPUAN, TOPLAMPUAN, AKTIF) values (@FORMNAME, @GECERPUAN, @TOPLAMPUAN, 1)", conn);

               int result = 0;
               if (Session["SelectedID"] == null)
               {
                   cmd.CommandType = CommandType.Text;
                   cmd.Parameters.Add("@FORMNAME", SqlDbType.NVarChar, 100).Value = name;
                   cmd.Parameters.Add("@GECERPUAN", SqlDbType.Int).Value = int.Parse(txtGecer.Text.Trim());
                   cmd.Parameters.Add("@TOPLAMPUAN", SqlDbType.Int).Value = int.Parse(txtToplam.Text.Trim());
                   result = cmd.ExecuteNonQuery();
               }
               else
               {
                   cmd.CommandText = "UPDATE PROJELER SET PROJEADI=@FORMNAME, GECERPUAN=@GECERPUAN, TOPLAMPUAN=@TOPLAMPUAN where PROJENO=@PROJENO";
                   cmd.CommandType = CommandType.Text;
                   cmd.Parameters.Add("@PROJENO", SqlDbType.Int).Value = Session["SelectedID"].ToString();
                   cmd.Parameters.Add("@FORMNAME", SqlDbType.NVarChar, 100).Value = name;
                   cmd.Parameters.Add("@GECERPUAN", SqlDbType.Int).Value = int.Parse(txtGecer.Text.Trim());
                   cmd.Parameters.Add("@TOPLAMPUAN", SqlDbType.Int).Value = int.Parse(txtToplam.Text.Trim());
                   result = cmd.ExecuteNonQuery();
               }

                   hResult.InnerHtml = "Kayıt Başarılı";
                   bResult.InnerHtml = "Form kaydı başarıyla alınmıştır.";
                   divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                  
                    LoadData();

            }
            catch (Exception ex)
            {
                hResult.InnerHtml = "Hata Oluştu!";
                bResult.InnerHtml = "Form kaydı gerçekleştirilemedi. Hata Mesajı: " + ex.Message;
                divResult.InnerHtml = "<script>$('#myModal').modal();  </script>";


            }
            finally
            {
                conn.Close();
                Session["SelectedID"] = null;
            }
        }
        
        protected void lnkNew_Click(object sender, EventArgs e)
        {
            try
            {
                Session["SelectedID"] = null;
                txtFormName.Text = "";
                txtToplam.Text = "0";
                txtGecer.Text = "0";
                divResult.InnerHtml = "<script>$('#divUpdate').modal(); </script>";
            }
            catch
            {
            }
        }

        protected void grdData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "editt":
                    try
                    {
                        Session["SelectedID"] = null;
                        DataTable groups = (DataTable)Session["Forms"];
                        string id = e.CommandArgument.ToString();
                        Session["SelectedID"] = id;
                        DataRow dr = groups.Select("PROJENO=" + id)[0];
                        txtFormName.Text = dr["PROJEADI"].ToString();
                        txtToplam.Text = dr["TOPLAMPUAN"].ToString();
                        txtGecer.Text = dr["GECERPUAN"].ToString();
                        divResult.InnerHtml = "<script>$('#divUpdate').modal(); </script>";

                    }
                    catch
                    {
                    }
                    break;
              case "switch":
                      SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
                      try
                      {
                          conn.Open();
                      }
                      catch (Exception ex)
                      {
                          throw new Exception("Form Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

                      }

                       
                        try
                        { 
                            string id = e.CommandArgument.ToString();
                        SqlCommand cmd = new SqlCommand("update PROJELER set AKTIF = AKTIF ^ 1 where PROJENO = @p1", conn);
                        cmd.Parameters.Add(new SqlParameter("@p1", SqlDbType.Int)).Value = id;
            
                        cmd.CommandType = CommandType.Text;
                        int result = cmd.ExecuteNonQuery();
                        LoadData();
                        }
                        catch (Exception ex)
                        {
                            throw ex;

                        }
                        finally
                        {
                            conn.Close();
                        }
                    break;
                case "titles":
                    try
                    {
                        string id = e.CommandArgument.ToString();
                        Session["SelectedID"] = null;
                        Response.Redirect("MTitles.aspx?formno=" + id, true);

                    }
                    catch
                    {
                    }
                    break;

                case "questions":
                    try
                    {
                        string id = e.CommandArgument.ToString();
                        Session["SelectedID"] = null;
                        Response.Redirect("MQuestions.aspx?formno=" + id, true);

                    }
                    catch
                    {
                    }
                    break;
            }
        }
    }
}