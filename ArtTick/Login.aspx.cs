﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ARTQM
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Submit.Visible = true;
            
            }
           
        }

        protected void Submit_Click(object sender, EventArgs e)
        {


            //FormsAuthentication.SetAuthCookie(
            //        "admin", false);

            //FormsAuthenticationTicket ticket1 =
            //   new FormsAuthenticationTicket(
            //        1,                                   // version
            //        "admin",   // get username  from the form
            //        DateTime.Now,                        // issue time is now
            //        DateTime.Now.AddMinutes(60),         // expires in 10 minutes
            //        false,      // cookie is not persistent

            //        ""                              // role assignment is stored
            //    // in userData
            //        );
            //HttpCookie cookie1 = new HttpCookie(
            //  FormsAuthentication.FormsCookieName,
            //  FormsAuthentication.Encrypt(ticket1));
            //Response.Cookies.Add(cookie1);
            //ICUser currentuser = new ICUser();

            //currentuser.UserName = "admin";
            //currentuser.UserID = 1;
            //currentuser.UserType = UserTypes.ADMIN;

            //Session["CurrentUser"] = currentuser;


            //Response.Redirect("Dashboard.aspx", true);


            int UserID = 0;
            string UserAgents = null;
            string UserSkills = null;
            string UserPrefixes = null;
            string SEVIYEKOD = null;
            try
            {
                ICUser currentuser = null;

                if (UserFunctions.LoginUser(txtUser.Value, txtPPass.Value, out UserID, out UserAgents,out UserSkills,out UserPrefixes, out SEVIYEKOD))
                {

                    Submit.Visible = true;
                    currentuser = UserFunctions.GetUserObject(Convert.ToInt32(SEVIYEKOD));

                    if (currentuser == null) throw new Exception("Kullanıcı bilgisi bulunamadı");
                    if (UserAgents.Equals("YOK")) throw new Exception("Kullanıcıya Bağlı Agentlar Bulunamadı.");

                    currentuser.GroupIDs = "";

                    UserFunctions.getPermissions(currentuser.UserID);
                    Session["YETKİ"] = UserID;
                    Session["CurrentUser"] = currentuser;
                    Session["UserAgents"] = UserFunctions.Concat(UserAgents);
                    Session["UserSkills"] = UserFunctions.Concat(UserSkills);
                    Session["UserPrefixes"] = UserFunctions.Concat(UserPrefixes);

                    FormsAuthentication.SetAuthCookie(
                    currentuser.UserName, false);

                    FormsAuthenticationTicket ticket1 =
                       new FormsAuthenticationTicket(
                            1,                                   // version
                            currentuser.UserName,   // get username  from the form
                            DateTime.Now,                        // issue time is now
                            DateTime.Now.AddMinutes(60),         // expires in 10 minutes
                            false,      // cookie is not persistent

                            ""                              // role assignment is stored
                        // in userData
                            );
                    HttpCookie cookie1 = new HttpCookie(
                      FormsAuthentication.FormsCookieName,
                      FormsAuthentication.Encrypt(ticket1));
                    Response.Cookies.Add(cookie1);

                    // 4. Do the redirect. 
                    String returnUrl1;
                    // the login is successful
                    if (Request.QueryString["ReturnUrl"] == null)
                    {

                        returnUrl1 = "Calls.aspx";
                    }
                    //login not unsuccessful 
                    else
                    {
                        returnUrl1 = Request.QueryString["ReturnUrl"];
                        if (!returnUrl1.Contains(".aspx"))
                            returnUrl1 = "Calls.aspx";

                    }
                    Response.Redirect(returnUrl1, false);

                }

            }
            catch (ThreadAbortException)
            {

            }
            catch (Exception ex)
            {
                divResult.InnerHtml = ex.Message;
            }
            }


        
    

    }
}