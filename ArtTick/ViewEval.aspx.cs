﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ARTQM
{
    public partial class ViewEval : PageBase 
    {
        DataProvider obj = new DataProvider();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["YETKİ"].ToString() == "1")
            {
                EditButton.Visible = false;
                ItirazButton.Visible = ItirazKontrol();
            }
            if (!IsPostBack)
            {
                if (Request.QueryString["loadall"] == "ok")
                {
                    LoadAnket2();
                }
                else
                {
                        Session["ANKETNO"] = Request.QueryString["anket"];
                        LoadAnket();
                   
                }
            }
            loadTextBox();
            LoadMediaPlayer();
        }
        protected void LoadMediaPlayer()
        {
            DataTable segments = obj.EditEvalGetList("SELECT UNIQUEID, CALLDATE, DURATION FROM TBLCCCDR WHERE UNIQUEID = " + Request.QueryString["callid"]);
            //DataTable segments = ws.GetCallSegments(Request.QueryString["callid"]);
            if (segments != null)
            {
                grdSegments.DataSource = segments;
                grdSegments.DataKeyNames = new string[] { "inum" };
                grdSegments.DataBind();
            }
        }
        protected void loadTextBox()
        {
            DataTable dt = Lists.GetFirstCallNotes(Request.QueryString["callid"]);
            if (dt.Rows.Count > 0)
            {
                txtNot.Text = dt.Rows[0]["NOTE"].ToString();
            }
            txtNot.Attributes.Add("readonly", "readonly");
        }
        public void LoadAnket()
        {
            divScript.InnerHtml = "";
            DataTable anket = null;
            try
            {
                anket = Lists.GetEval(Request.QueryString["callid"], int.Parse(Request.QueryString["projeno"]));
                if (anket.Rows.Count == 0)
                    throw new Exception("Anket bulunamadı.");
            }
            catch (Exception)
            {

                // anket = new Entity.Anket();
                Response.Redirect("EditEval.aspx?anket=&callid=" + Request.QueryString["callid"] + "&projeno=" + Request.QueryString["projeno"] + "&agent=" + Request.QueryString["agent"] + "&donem=" + Request.QueryString["donem"] + "&direction=" + Request.QueryString["direction"] + "&duration=" + Request.QueryString["duration"] + "&skill=" + Request.QueryString["skill"] + "&partynames=" + Request.QueryString["partynames"] + "&startdate=" + Request.QueryString["startdate"] + "&skillset=" + Request.QueryString["skillset"] + "&partyagentnames=" + Request.QueryString["partyagentnames"], false);
            }

            try
            {
                string agent = "";

                Session["ANKET"] = anket;
                DataTable detaylar = Lists.GetEvalDetails(anket.Rows[0]["ANKETID"].ToString());
                DataTable kriterler = Lists.GetTitles(int.Parse(Request.QueryString["projeno"]));
                DataTable sorular = Lists.GetQuestions2(int.Parse(Request.QueryString["projeno"]),"0");
                lblProje.Text = Lists.GetEvalInfo(Request.QueryString["callid"], Request.QueryString["projeno"], out agent);
                Session["AGENT"] = agent;
                DataTable ust_bilgi = new DataTable();
                ust_bilgi.Columns.Add("SORUNO");
                ust_bilgi.Columns.Add("ETIKET");
                ust_bilgi.Columns.Add("YANIT");


                string[] info = Lists.GetEvalInfo2(Request.QueryString["callid"], Request.QueryString["projeno"], out agent);
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                DataTable kriter_sum = new DataTable();
                if (con.State == ConnectionState.Closed)
                    con.Open();
                SqlDataAdapter da = new SqlDataAdapter("SELECT KRITERNO, VERI AS SKOR FROM CARSAF_KRITER WHERE ANKETNO = " + anket.Rows[0]["ANKETID"].ToString() + "", con);
                da.Fill(kriter_sum);

                object genelcsi = anket.Rows[0]["GENELCSII"];

                da = new SqlDataAdapter("SELECT SUM(VERI) AS VERI  FROM [ARTQM].[dbo].[CARSAF_KRITER] WHERE ANKETNO = '" + anket.Rows[0]["ANKETID"] + "'", con);
                DataTable toplamVeri = new DataTable();
                da.Fill(toplamVeri);

                da = new SqlDataAdapter("SELECT SUM(MAKSSKOR) AS MAKSSKOR FROM KRITERLER WHERE PROJENO = '"+ anket.Rows[0]["PROJENO"].ToString()  + "'", con);
                DataTable maxskor = new DataTable();
                da.Fill(maxskor);

                double yuzdeSkor = Math.Round((Convert.ToDouble(toplamVeri.Rows[0]["VERI"].ToString()) / Convert.ToDouble(maxskor.Rows[0]["MAKSSKOR"].ToString())) * Convert.ToDouble(100));

                //toplam işlemlerinin yapıldığı bölüm
                //lblYuzde.Text = String.Format("{0:0}", anket.Rows[0]["GENELCSII"]);
                lblYuzde.Text = String.Format("{0:0}", yuzdeSkor) + "%";
                //lblToplam.Text = String.Format("{0:0}", anket.Rows[0]["GENELCSI"]);
                lblToplam.Text = String.Format("{0:0}", toplamVeri.Rows[0]["VERI"]);

                DataTable dt = new DataTable();
                dt.Columns.Add("SORUNO");
                dt.Columns.Add("SORUSIRA");
                dt.Columns.Add("EVETHAYIR");
                dt.Columns.Add("SKORADAHIL");
                dt.Columns.Add("SECENEKLER");
                dt.Columns.Add("ETIKET");
                dt.Columns.Add("YANIT");
                dt.Columns.Add("YANITNO");
                dt.Columns.Add("MAKSSKOR");
                dt.Columns.Add("PUAN");
                dt.Columns.Add("TOPLANIR");
                dt.Columns.Add("ONCELIK");

                // lblSaat.Text = anket.BASLANGICSAAT;
                // lblGizliMusteri.Text = anket.GIZLI;

                //lblTarih.Text = anket.KATILIMTARIH.ToString("dd.MM.yyyy");
                string prevkriter = "";

                DataRow dr;

                Dictionary<string, string> _ustkisim = new Dictionary<string, string>();
                foreach (DataRow soru  in sorular.Rows)
                {


                    if (prevkriter != soru["KRITERNO"].ToString())
                    {
                        dr = dt.NewRow();
                        dr["SORUNO"] = 0;
                        try
                        {
                            DataRow var = kriterler.Select("KRITERNO="+ soru["KRITERNO"].ToString())[0];
                                
                            dr["SORUSIRA"] = "";
                            dr["ETIKET"] = var["KRITER"].ToString();
                            dr["MAKSSKOR"] = var["MAKSSKOR"];

                        }
                        catch (Exception)
                        {


                        }
                        try
                        {
                            dr["PUAN"] = kriter_sum.Select("KRITERNO=" + soru["KRITERNO"].ToString())[0]["SKOR"].ToString();
                        }
                        catch
                        {

                            dr["PUAN"] = "";
                        }
                        prevkriter = soru["KRITERNO"].ToString();
                        dt.Rows.Add(dr);

                    }
                    dr = dt.NewRow();
                    dr["SORUNO"] = soru["SORUNO"];
                    dr["SORUSIRA"] = soru["SORUSIRA"];
                    dr["TOPLANIR"] = soru["TOPLANIR"];
                    if (soru["EVETHAYIR"].ToString() == "-2")
                        dr["ETIKET"] = soru["SORUMETNI"]+(soru["ACIKLAMA"].ToString() == "" ? "" : "<br/>" + soru["ACIKLAMA"].ToString());
                    else
                        dr["ETIKET"] = soru["SORUMETNI"];


                    dr["SECENEKLER"] = soru["SECENEKLER"];
                    dr["EVETHAYIR"] = (soru["GRUP"].ToString() == "YORUM" ? "-2" : soru["EVETHAYIR"].ToString());
                    dr["SKORADAHIL"] = 1;
                    dr["ONCELIK"] = soru["SORUSIRA"];
                    dr["MAKSSKOR"] = soru["MAKSSKOR"];

                    try
                    {
                        DataRow _y = detaylar.Select("SORUNO=" + soru["SORUNO"].ToString())[0];

                        if (int.Parse(dr["EVETHAYIR"].ToString()) < 0)
                        {
                            try
                            {
                                dr["ETIKET"] = dr["ETIKET"].ToString() + "<br/><font style='color:#880000;'>" + _y["YANIT"].ToString()+"</font>";
                            }
                            catch
                            {
                            }

                        }
                   
                        try
                        {
                            dr["YANITNO"] = _y["YANITNO"].ToString();
                            dr["YANIT"] = EtiketBul2(soru["SECENEKLER"].ToString(), ((int)parse2Double(_y["DEGER"].ToString())).ToString(), soru["EVETHAYIR"].ToString());
                        }
                        catch
                        {
                        }
                        try
                        {
                            dr["PUAN"] = _y["YANITR"].ToString();
                        }
                        catch
                        {
                            dr["PUAN"] = "";
                        }
                    }
                    catch
                    {
                    }

                    //TOPLANAN /OR ILE ISLENEN SUTUNLARIN PUANI BURAYA

                    dt.Rows.Add(dr);
                    if (soru["EVETHAYIR"].ToString() =="0")
                    {
                        //tüm alt secenekler yüksek puanlıdan baslayarak satır satır eklenecek
                        if (soru["SECENEKLERPUAN"].ToString() == "") continue;
                        string[] puanlar = soru["SECENEKLERPUAN"].ToString().Split(Properties.Settings.Default.FIELDSEP1);
                        foreach (string puan in puanlar)
                        {
                            dr = dt.NewRow();
                            dr["SORUNO"] = soru["SORUNO"].ToString();
                            dr["SORUSIRA"] = puan.Split(Properties.Settings.Default.FIELDSEP2)[0] + ">";
                            dr["MAKSSKOR"] = puan.Split(Properties.Settings.Default.FIELDSEP2)[1];
                            dr["ETIKET"] = EtiketBul2(soru["SECENEKLER"].ToString(), puan.Split(Properties.Settings.Default.FIELDSEP2)[0], soru["EVETHAYIR"].ToString());
                            dt.Rows.Add(dr);
                        }
                    }

                }

                foreach(DataRow item in dt.Rows)
                {
                    if (item["SORUNO"].ToString() == "0")
                    {
                        item["YANIT"] = item["PUAN"].ToString();
                    }                    
                }



                DataRow toplam = dt.NewRow();
                lstKarne.DataSource = dt;
                lstKarne.DataKeyNames = new string[] { "YANITNO", "SORUNO", "YANIT", "SECENEKLER", "EVETHAYIR" };
                lstKarne.DataBind();






                string filename = anket.Rows[0]["LOGID"].ToString() + "_" + anket.Rows[0]["ANKETNO"].ToString() + "_" + anket.Rows[0]["DEGERLENDIRMETARIHI"].ToString().Replace("-","_").Substring(0,10) +".xls";
                filename = filename.Replace("__", "_").Replace("__", "_");

                //catch e düşüyor

                string temp = new ARTQM.ExcelConvertor().ConvertKarne(dt, Server.MapPath("temp"), filename, info[0] + " | " + info[1], info[2], info[3], info[4]);

                Session["FILENAME"] = "temp/" + filename;


            }
            catch(Exception Ex)
            {
                
            }
        }

        public void LoadAnket2()
        {
        }
        public string GetRowClass(string soruno, string sorusira, string evethayir)
        {
            string result = "";
            if (soruno == "0") //kriter
                result = "kriter_baslik";
            else if (evethayir == "-2")
                result = "yorum";
            else if (sorusira.Contains("_") || sorusira.Contains(">")) // soru baslıgı degil saga yasla
                result = "kriter_secenek";

            else result = "kriter_satir";
            return result;
        }

        protected void Unnamed1_Click(object sender, ImageClickEventArgs e)
        {

            DataTable temp = (DataTable)Session["ANKET"];

            Response.Redirect("EditEval.aspx?anket=" + temp.Rows[0]["ANKETID"].ToString() + "&callid=" + Request.QueryString["callid"] + "&projeno=" + Request.QueryString["projeno"] + "&agent=" + Request.QueryString["agent"] + "&donem=" + Request.QueryString["donem"], true);


        }

        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            DataTable temp = (DataTable)Session["ANKET"];

            //Response.Redirect("EditEval.aspx?anket=" + temp.Rows[0]["ANKETID"].ToString() + "&callid=" + Request.QueryString["callid"] + "&projeno=" + Request.QueryString["projeno"] + "&agent=" + Request.QueryString["agent"] + "&donem=" + Request.QueryString["donem"] + "&startdate=" + Request.QueryString["startdate"], true);
            Response.Redirect("EditEval.aspx?anket=" + temp.Rows[0]["ANKETID"].ToString() + "&callid=" + Request.QueryString["callid"] + "&projeno=" + Request.QueryString["projeno"] + "&agent=" + Request.QueryString["agent"] + "&donem=" + Request.QueryString["donem"] + "&startdate=" + Request.QueryString["startdate"] + "&duration=" + temp.Rows[0]["SURE"] + "&direction=" + temp.Rows[0]["YON"] + "&partynames=" + temp.Rows[0]["PARTYNAMES"] + "&partyagentnames=" + temp.Rows[0]["PARTYAGENTNAMES"], true);

        }

        protected void lnkDownload_Click(object sender, EventArgs e)
        {
            divScript.InnerHtml = "<script>popitupxs('Download.aspx');</script>";

        }
        protected void lnkItiraz_Click(object sender, EventArgs e)
        {
            Tools T = new Tools();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);

            try
            {
                con.Open();
                SqlDataAdapter da = new SqlDataAdapter("SELECT a.DEGERLENDIRMETARIHI, a.DEGERLENDIREN, a.PARTYAGENTNAMES, a.ANKETNO, a.GUNCELLEME, a.GUNCELLEYEN, t.EPOSTA AS EPOSTA, t.NAME AS NAME, a.LOGID as LOGID FROM ANKETLER AS a LEFT JOIN TEAMLEADERS AS t ON t.USERCODE = a.DEGERLENDIREN WHERE ANKETNO = '" + Request.QueryString["callid"].ToString().Trim() + "'", con);
                DataTable call_rec = new DataTable();
                da.Fill(call_rec);
                SqlCommand cmd = new SqlCommand("INSERT INTO ITIRAZ VALUES('" + call_rec.Rows[0]["ANKETNO"].ToString() + "', '" + DateTime.Parse(call_rec.Rows[0]["DEGERLENDIRMETARIHI"].ToString()).ToString("yyyy-MM-dd HH:mm:ss") + "', '" + call_rec.Rows[0]["LOGID"].ToString() + "', '" + Request.QueryString["DONEM"] + "')", con);
                cmd.ExecuteNonQuery();
                string MailBody = "Merhabalar Sayın " + call_rec.Rows[0]["NAME"].ToString() + ",\n\n " + call_rec.Rows[0]["DEGERLENDIRMETARIHI"].ToString() + " tarihinde değerlendirmiş olduğunuz " + call_rec.Rows[0]["ANKETNO"].ToString() + " nolu çağrı kaydına " + call_rec.Rows[0]["PARTYAGENTNAMES"].ToString() + " tarafından itiraz edilmiştir. \n\nİyi Çalışmalar";
                T.SendMail(MailBody, "ARTQM İtiraz Maili", call_rec.Rows[0]["EPOSTA"].ToString());
                ItirazButton.Visible = false;

                con.Close();

                hResult.InnerHtml = "Başarılı!";
                bResult.InnerHtml = "İtiraz maili başarılı bir şekilde gönderilmiştir.";
                divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";

            }
            catch (Exception Ex)
            {
                hResult.InnerHtml = "Hata Oluştu!";
                bResult.InnerHtml = "İtiraz maili gönderilemedi.";
                divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
            }
        }
        protected bool ItirazKontrol()
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            con.Open();
            SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM ITIRAZ WHERE ANKETNO = '" + Request.QueryString["callid"] + "' AND DONEM = '" + Request.QueryString["donem"] + "'", con);
            int Result = Convert.ToInt32(cmd.ExecuteScalar().ToString());
            con.Close();
            if (Result > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

            string inum = grdSegments.SelectedDataKey.Value.ToString();
            //frmPlayer.Attributes["src"] = "Player.aspx?inum=" + inum;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://176.88.216.226/Tobe/app/ApplicationServlet?externalService=downloadRecording&usercode=artpro.admin&clearpassword=Artpro12!&uniqueid=" + inum);
            //string authInfo = ARTQM.Properties.Settings.Default.ACRAPIUSER + ":" + ARTQM.Properties.Settings.Default.ACRAPIPWD;
            //authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            //request.Headers.Add("Authorization", "Basic " + authInfo);
            //request.Credentials = new NetworkCredential(ARTQM.Properties.Settings.Default.ACRAPIUSER, ARTQM.Properties.Settings.Default.ACRAPIPWD);
            request.AllowAutoRedirect = true;
            request.Proxy = null;
            request.MaximumAutomaticRedirections = 1;

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            if (response.ContentType == "audio/mpeg;charset=UTF-8")
            {
                MemoryStream ms = new MemoryStream();
                response.GetResponseStream().CopyTo(ms);
                ms.Position = 0;
                FileStream sw = new FileStream(Server.MapPath("temp") + "\\" + inum + ".mp3", FileMode.OpenOrCreate);
                ms.WriteTo(sw);
                ms.Close();
                sw.Close();

                Player.Attributes["src"] = "temp\\" + inum + ".mp3";
            }
            else if (response.ContentType == "application/zip;charset=UTF-8")
            {
                DataProvider obj = new DataProvider();

                DataTable dt = obj.GetCallRec("SELECT AUDIOFILENAME, CALLDATE FROM TBLCCCDR WHERE UNIQUEID = '" + inum + "'");

                if (!File.Exists(Server.MapPath("temp") + "\\" + dt.Rows[0]["audioFileName"].ToString().Trim() + ".mp3"))
                {
                    MemoryStream ms = new MemoryStream();
                    response.GetResponseStream().CopyTo(ms);
                    ms.Position = 0;

                    //FileStream sw = new FileStream(@"C:\Users\lorta\Desktop\TegsoftSesKayit\TegsoftSesKayit\zip\20190503-1556892009.300.zip", FileMode.OpenOrCreate);
                    FileStream sw = new FileStream(Server.MapPath("zip") + "\\" + inum + ".zip", FileMode.OpenOrCreate);
                    ms.WriteTo(sw);
                    ms.Close();
                    sw.Close();

                    //ZipFile.ExtractToDirectory(@"C:\Users\lorta\Desktop\TegsoftSesKayit\TegsoftSesKayit\zip\20190503-1556892009.300.zip", @"C:\Users\lorta\Desktop\TegsoftSesKayit\TegsoftSesKayit\temp");



                    ZipFile.ExtractToDirectory(Server.MapPath("zip") + "\\" + inum + ".zip", Server.MapPath("temp"));
                }
                Player.Attributes["src"] = "temp\\" + dt.Rows[0]["audioFileName"].ToString().Trim() + ".mp3";
            }

        }
    }
}