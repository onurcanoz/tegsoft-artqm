﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ARTQM
{
    public partial class Agents : PageBase
    {
        DataProvider obj = new DataProvider();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            string yonetim = Session["YONETIM"].ToString();

            if (yonetim.Equals("0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                        "alert('Yetkiniz Yok'); window.location='Login.aspx';", true);
                return;
            }
            if (!IsPostBack)
            {
                Session["SelectedID"] = null;

                LoadData();
            }
        }
        protected void LoadData()
        {
            try
            {
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM AGENTS ORDER BY NAME DESC", conn);
                DataTable TotalAgents = new DataTable();
                da.Fill(TotalAgents);
                TotalAgents.Columns.Add("Index");
                int index = 1;
                foreach(DataRow dr in TotalAgents.Rows)
                {
                    dr["Index"] = index;
                    index++;
                }
                grdData.DataSource = TotalAgents;
                grdData.DataBind();

                spanInfo.InnerHtml = "Toplam " + grdData.Rows.Count.ToString() + " kayıt bulundu.";
            }
            catch(Exception Ex)
            {
                hResult.InnerHtml = "Hata Oluştu!";
                bResult.InnerHtml = "Sistemsel bir hata oluşmuştur. Lütfen bağlantılarınızı kontrol edeniz.";
                divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                conn.Close();
            }
        }
        protected void grdData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int Result = 0;
            switch (e.CommandName)
            {
                case "Refresh_Pwd":
                    try
                    {
                        string[] id = e.CommandArgument.ToString().Trim().Split(':');
                        DataTable AgentInfo = new DataTable();
                        AgentInfo = obj.GetAgentData("SELECT UID, USERNAME, SURNAME, USERCODE, PASS FROM TBLUSERS WHERE UID = '" + id[0] + "'");
                        if(AgentInfo.Rows.Count == 1)
                        {
                            SqlCommand cmd = new SqlCommand("UPDATE AGENTS SET NAME = '" + AgentInfo.Rows[0]["Name"] + "', USERCODE = '" + AgentInfo.Rows[0]["Usercode"] + "' WHERE LOGID = '" + id[0] + "'", conn);
                            conn.Open();
                            cmd.ExecuteNonQuery();

                            cmd = new SqlCommand("UPDATE KULLANICILAR SET KULLANICIKOD = '"+AgentInfo.Rows[0]["Usercode"]+"', SIFRE = '"+HashPassword(AgentInfo.Rows[0]["Pass"].ToString().Trim())+"' WHERE KULLANICIKOD = '" + id[1] + "'", conn);
                            cmd.ExecuteNonQuery();

                            conn.Close();

                            Response.Redirect("Agents.aspx", false);
                        }
                        else if(AgentInfo.Rows.Count == 0)
                        {
                            hResult.InnerHtml = "Hata Oluştu!";
                            bResult.InnerHtml = "Belirtilen kullanıcı adına ait müşteri temsilcisi bulunamamıştır.";
                            divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                        }
                        
                    }
                    catch(Exception Ex)
                    {
                        hResult.InnerHtml = "Hata Oluştu!";
                        bResult.InnerHtml = "Sistemsel bir hata oluşmuştur. Lütfen bağlantılarınızı kontrol edeniz.";
                        divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                        conn.Close();
                    }
                    break;
                case "Delete_Agent":
                    try
                    {
                        string[] Params = e.CommandArgument.ToString().Trim().Split(':');
                        conn.Open();
                        SqlCommand cmd = new SqlCommand("DELETE FROM AGENTS WHERE LOGID = '" + Params[0] + "'", conn);
                        Result = cmd.ExecuteNonQuery();
                        if(Result == 1)
                        {
                            hResult.InnerHtml = "Başarılı İşlem!";
                            bResult.InnerHtml = "Seçilen müşteri temsilcisi sistemden silinmiştir.";
                            divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                        }
                        cmd = new SqlCommand("DELETE FROM KULLANICILAR WHERE KULLANICIKOD = '" + Params[1] + "'", conn);
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                    catch (Exception Ex)
                    {
                        hResult.InnerHtml = "Hata Oluştu!";
                        bResult.InnerHtml = "Sistemsel bir hata oluşmuştur. Lütfen bağlantılarınızı kontrol edeniz.";
                        divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                        conn.Close();
                    }
                    Response.Redirect("Agents.aspx");
                    break;
                case "Edit_Agent":
                    try
                    {
                        string[] Params = e.CommandArgument.ToString().Trim().Split(':');
                        TxtEditUsercode.Text = Params[1];
                        TxtEditUsercode.Attributes.Add("readonly", "readonly");
                        TxtEditEposta.Text = Params[2];
                        TxtEditName.Text = Params[3];
                        TxtEditName.Attributes.Add("readonly", "readonly");
                        divResult.InnerHtml = "<script>$('#divEdit').modal(); </script>";
                    }
                    catch (Exception Ex)
                    {
                        hResult.InnerHtml = "Hata Oluştu!";
                        bResult.InnerHtml = "Sistemsel bir hata oluşmuştur. Lütfen bağlantılarınızı kontrol edeniz.";
                        divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                        conn.Close();
                    }
                    break;
            }

        }
        protected void BtnEditSave_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("UPDATE AGENTS SET EPOSTA = '"+ TxtEditEposta.Text.Trim() +"' WHERE USERCODE = '" + TxtEditUsercode.Text.Trim() + "'", conn);
                int Result = cmd.ExecuteNonQuery();
                if(Result == 1)
                {
                    Response.Redirect("Agents.aspx");
                }
            }
            catch(Exception Ex)
            {
                hResult.InnerHtml = "Hata Oluştu!";
                bResult.InnerHtml = "Sistemsel bir hata oluşmuştur. Lütfen bağlantılarınızı kontrol edeniz.";
                divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                conn.Close();
            }

            
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM AGENTS", conn);
                int Counter = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                if(Counter <= 76)
                {
                    try
                    {
                        DataTable AgentInfo = new DataTable();
                        AgentInfo = obj.GetAgentData("SELECT UID, USERNAME, SURNAME, USERCODE, PASS FROM TBLUSERS WHERE USERCODE = '" + txtUsercode.Text.Trim() + "'");

                        if (AgentInfo.Rows.Count == 1)
                        {
                            AgentInfo.Rows[0]["Eposta"] = txtEposta.Text.Trim();
                            cmd = new SqlCommand("SELECT COUNT(*) FROM AGENTS WHERE LOGID = '" + AgentInfo.Rows[0]["UID"] + "'", conn);
                            int Result = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                            if (Result == 0)
                            {
                                cmd = new SqlCommand("INSERT INTO AGENTS(LOGID, NAME, USERCODE, EPOSTA) VALUES(@LOGID, @NAME, @USERCODE, @EPOSTA)", conn);
                                cmd.Parameters.AddWithValue("@LOGID", AgentInfo.Rows[0]["UID"]);
                                cmd.Parameters.AddWithValue("@NAME", AgentInfo.Rows[0]["Name"]);
                                cmd.Parameters.AddWithValue("@USERCODE", AgentInfo.Rows[0]["Usercode"]);
                                cmd.Parameters.AddWithValue("@EPOSTA", AgentInfo.Rows[0]["Eposta"]);
                                cmd.ExecuteNonQuery();

                                cmd = new SqlCommand("INSERT INTO KULLANICILAR VALUES(@KULLANICIKOD, @SIFRE, 'AGENT', 'AGENT', '1', '1', '1', '0')", conn);
                                cmd.Parameters.AddWithValue("@KULLANICIKOD", AgentInfo.Rows[0]["Usercode"]);
                                cmd.Parameters.AddWithValue("@SIFRE", HashPassword(AgentInfo.Rows[0]["Pass"].ToString()));
                                cmd.ExecuteNonQuery();

                                hResult.InnerHtml = "Kayıt Başarılı";
                                bResult.InnerHtml = "Yeni kayıt başarıyla alınmıştır.";
                                divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";

                                Response.Redirect("Agents.aspx", false);
                            }
                            else
                            {
                                hResult.InnerHtml = "Hata Oluştu!";
                                bResult.InnerHtml = "Aynı kullanıcı adlı bir müşteri temsilcisi sistemde bulunmaktadır.";
                                divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                                conn.Close();
                            }
                        }
                        else if (AgentInfo.Rows.Count == 0)
                        {
                            hResult.InnerHtml = "Hata Oluştu!";
                            bResult.InnerHtml = "Belirtilen kullanıcı adına ait müşteri temsilcisi bulunamamıştır.";
                            divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                            conn.Close();
                        }
                        else
                        {
                            hResult.InnerHtml = "Hata Oluştu!";
                            bResult.InnerHtml = "Belirtilen kullanıcı adına ait Tegsoft'da birden fazla müşteri temsilcisi bulunmaktadır.";
                            divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                            conn.Close();
                        }
                    }
                    catch (Exception Ex)
                    {
                        hResult.InnerHtml = "Hata Oluştu!";
                        bResult.InnerHtml = "Sistemsel bir hata oluşmuştur. Lütfen bağlantılarınızı kontrol edeniz.";
                        divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                        conn.Close();
                    }
                }
                else
                {
                    hResult.InnerHtml = "Uyarı!";
                    bResult.InnerHtml = "Maksimum müşteri temsilcisi sayısına ulaştınız. Daha fazla yeni kayıt oluşturamazsınız.";
                    divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                    conn.Close();
                }
            }
            catch(Exception Ex)
            {
                hResult.InnerHtml = "Hata Oluştu!";
                bResult.InnerHtml = "Sistemsel bir hata oluşmuştur. Lütfen bağlantılarınızı kontrol edeniz.";
                divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                conn.Close();
            }    
        }
        protected void lnkNew_Click(object sender, EventArgs e)
        {
            try
            {
                txtUsercode.Text = "";
                divResult.InnerHtml = "<script>$('#divUpdate').modal(); </script>";
            }
            catch
            {
            }
        }
        public static string HashPassword(string password)
        {
            var bytes = new UTF8Encoding().GetBytes(password);
            var hashBytes = System.Security.Cryptography.MD5.Create().ComputeHash(bytes);
            return Convert.ToBase64String(hashBytes);
        }
    }
}