﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ARTQM
{
    public partial class UserMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    string cagriKayitlari = Session["CAGRIKAYITLARI"].ToString();
                    string degerlendirmeler = Session["DEGERLENDIRMELER"].ToString();
                    string raporlar = Session["RAPORLAR"].ToString();
                    string tanimlamalar = Session["TANIMLAMALAR"].ToString();
                    string yonetim = Session["YONETIM"].ToString();

                    if (cagriKayitlari.Equals("1"))
                    {
                        liCagriKayitlari.Visible = true;
                    }
                    if (degerlendirmeler.Equals("1"))
                    {
                        liDegerlendirmeler.Visible = true;
                    }
                    if (raporlar.Equals("1"))
                    {
                        liRaporlar.Visible = true;
                    }
                    if (tanimlamalar.Equals("1"))
                    {
                        liTanimlamalar.Visible = true;
                    }
                    if (yonetim.Equals("1"))
                    {
                        liYonetim.Visible = true;
                    }
                    object obj = Session["CurrentUser"];
                    //username.InnerText = ((ARTQM.ICUser)obj).UserName;
                    username.InnerText = "Çıkış";
                }
                catch
                {
                    Response.Redirect("Tickets.aspx", true);
                }
            }
        }

        protected void lnkLogout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            Response.Redirect("Login.aspx", true);
        }
    }
}