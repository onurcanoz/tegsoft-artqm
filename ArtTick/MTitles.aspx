﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="MTitles.aspx.cs" Inherits="ARTQM.MTitles" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="form-horizontal">
<fieldset>
     <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" runat="server" id="hResult"></h4>
        </div>
        <div class="modal-body" runat="server" id="bResult">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
        </div>
      </div>
      
    </div>
  </div>
     <div class="modal fade" id="divUpdate" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" runat="server" id="hResult2">Yeni Kırılım Kartı</h4>
        </div>
        <div class="modal-body" runat="server" id="bResult2">
    <div class="form-group">
  
  <div class="col-sm-12">     
    <div class="form-group">
    
     <label class="col-sm-3 control-label" for="drpForm1">Form</label>
    
  <div class="col-sm-3">
  	<asp:DropDownList runat="server" id="drpForm1" ClientIDMode="Static" name="drpForm1" class="form-control input-sm"/>
  
      
  </div>
        </div>
        
        
          <div class="form-group">
    
     <label class="col-sm-3 control-label" for="txtFormName">Kırılım Adı</label>
    
      <div class="col-sm-9">
  	    <asp:TextBox runat="server" id="txtFormName" ClientIDMode="Static" name="txtFormName" placeholder="Form Adı" class="form-control input-sm"/>
    </div>
  
 
</div>
<div class="form-group">
       <label class="col-sm-3 control-label" for="txtOrder">Sıra Numarası</label>
    
      <div class="col-sm-9">
  	    <asp:TextBox runat="server" id="txtOrder" ClientIDMode="Static" name="txtOrder" placeholder="Sıra No" class="form-control input-sm" TextMode="Number"/>
    </div>
  

</div>
<div class="form-group">
       <label class="col-sm-3 control-label" for="txtMaksSkor">Maks Skor</label>
    
      <div class="col-sm-9">
  	    <asp:TextBox runat="server" id="txtMaksSkor" ClientIDMode="Static" name="txtMaksSkor" placeholder="Kırılımın maksimum skoru" class="form-control input-sm" TextMode="Number"/>
    </div>
  

</div>


    <div class="form-group col-sm-12" style="float:right;">
  <label class="col-sm-10 control-label" for="btnSave"></label>
  <div class="col-sm-2 pull-right">
    <asp:Button id="btnSave" runat="server" class="btn btn-success" Text="Kaydet" OnClick="btnSave_Click" />
  </div>
</div>
        </div>
        </div></div>
      
      </div>
      
    </div>
  </div>
   


    <div runat="server" id="divResult"></div>
    


   

    


   
    <legend>TANIMLI KIRILIMLAR</legend>

      <div class="row">

     <div class="form-group">
    
     <label class="col-sm-1 control-label" for="drpForm">Form Seçiniz</label>
    
  <div class="col-sm-3">
  	<asp:DropDownList runat="server" id="drpForm" ClientIDMode="Static" name="drpForm" class="form-control input-sm"/>
  
      
  </div>
         <div class="col-sm-1" style="margin-top:-2px;">
  <asp:Button id="btnSearch" runat="server" class="btn btn-success" Text="LİSTELE" OnClick="btnSearch_Click" />
  </div>
 
</div><div class="col-sm-6"><hr /></div></div>
  <div class="form-group col-sm-12">   <div class="col-sm-3 btn-group" style="margin-left:-15px;">
     <asp:LinkButton ID="lnkNew" runat="server" class="btn btn-danger"  data-toggle="tooltip" data-placement="bottom" title="Yeni Kırılım" OnClick="lnkNew_Click" ><i class="glyphicon glyphicon-plus" aria-hidden="true"></i> Yeni Kırılım</asp:LinkButton>
     <%--<asp:LinkButton ID="Button1" runat="server" CssClass="btn btn-danger"  OnClick="Button1_Click" data-toggle="tooltip" data-placement="bottom" title="Excel' e Aktar"><i class="fa fa-file-excel-o" aria-hidden="true"></i>Excel'e Aktar</asp:LinkButton>--%>
            </div>
    <div class="col-sm-9">
        
        <span class="label label-default" id="spanInfo" runat="server"></span>
        </div>
      </div>
  <div class="form-group">
  
  <div class="col-sm-6">
      <br />
 <asp:GridView ID="grdData" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-responsive table-striped" 
     RowStyle-HorizontalAlign="Center"
     HeaderStyle-HorizontalAlign="Center" OnRowCommand="grdData_RowCommand">
     <Columns>

      <asp:BoundField HeaderText="Sıra No"  DataField="KRITERNO" ItemStyle-CssClass="col-sm-1" HeaderStyle-HorizontalAlign="Center">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle CssClass="col-sm-1"></ItemStyle>
         </asp:BoundField>
         <asp:BoundField HeaderText="Kırılım Adı"  DataField="KRITER" ItemStyle-CssClass="col-sm-8" ItemStyle-HorizontalAlign="Left">
<ItemStyle HorizontalAlign="Left" CssClass="col-sm-8"></ItemStyle>
         </asp:BoundField>
       
       <asp:BoundField HeaderText="Maks Puan"  DataField="MAKSSKOR" ItemStyle-CssClass="col-sm-1" ItemStyle-HorizontalAlign="Left">
<ItemStyle HorizontalAlign="Left" CssClass="col-sm-1"></ItemStyle>
         </asp:BoundField>
       
         <asp:TemplateField HeaderText="İşlemler">
             <ItemStyle CssClass="col-sm-1" HorizontalAlign="Left"></ItemStyle>
             <ItemTemplate>
                   <asp:LinkButton  runat="server" ID="btnEdit" CommandName="editt" CommandArgument='<%#Eval("DUMMYID").ToString() %>'  Text="<span data-toggle='tooltip' title='Düzenle' ><span class='glyphicon glyphicon-edit'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
               <asp:LinkButton  runat="server" ID="btnQuestions" CommandName="questions" CommandArgument='<%#Eval("PROJENO").ToString() %>'  Text="<span data-toggle='tooltip' title='Sorular' ><span class='glyphicon glyphicon-question-sign'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
         
                 
                 
             </ItemTemplate>
         </asp:TemplateField>


     </Columns>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<RowStyle HorizontalAlign="Center"></RowStyle>
      </asp:GridView>
    
  </div>

</div>


    </fieldset>
          </div>
 </asp:Content>
