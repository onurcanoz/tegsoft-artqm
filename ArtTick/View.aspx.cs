﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ARTQM
{
    public partial class View : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lnkEdit.Visible = false;
         
            if (!IsPostBack)
            {
                if (Request.QueryString["loadall"] == "ok")
                {
                    LoadAnket2();
                }
                else
                {
                    Session["ANKETNO"] = Request.QueryString["anket"];
                    LoadAnket();

                }
            }

        }
        public void LoadAnket()
        {
            divScript.InnerHtml = "";
            DataTable anket = null;
            try
            {
                anket = Lists.GetEval(Request.QueryString["callid"], int.Parse(Request.QueryString["projeno"]));
                if (anket.Rows.Count == 0)
                    throw new Exception("Anket bulunamadı.");
            }
            catch (Exception)
            {

                // anket = new Entity.Anket();
                Response.Redirect("EditEval.aspx?anket=&callid=" + Request.QueryString["callid"] + "&projeno=" + Request.QueryString["projeno"] + "&agent=" + Request.QueryString["agent"] + "&donem=" + Request.QueryString["donem"] + "&direction=" + Request.QueryString["direction"] + "&duration=" + Request.QueryString["duration"] + "&skill=" + Request.QueryString["skill"] + "&partynames=" + Request.QueryString["partynames"] + "&startdate=" + Request.QueryString["startdate"] + "&skillset=" + Request.QueryString["skillset"] + "&partyagentnames=" + Request.QueryString["partyagentnames"], true);
            }

            try
            {
                string agent = "";

                Session["ANKET"] = anket;
                DataTable detaylar = Lists.GetEvalDetails(anket.Rows[0]["ANKETID"].ToString());
                DataTable kriterler = Lists.GetTitles(int.Parse(Request.QueryString["projeno"]));
                DataTable sorular = Lists.GetQuestions2(int.Parse(Request.QueryString["projeno"]), "0");
                lblProje.Text = Lists.GetEvalInfo(Request.QueryString["callid"], Request.QueryString["projeno"], out agent);
                Session["AGENT"] = agent;
                DataTable ust_bilgi = new DataTable();
                ust_bilgi.Columns.Add("SORUNO");
                ust_bilgi.Columns.Add("ETIKET");
                ust_bilgi.Columns.Add("YANIT");


                string[] info = Lists.GetEvalInfo2(Request.QueryString["callid"], Request.QueryString["projeno"], out agent);
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);

                if (con.State == ConnectionState.Closed)
                    con.Open();

                DataTable kriter_sum = new DataTable();
                if (con.State == ConnectionState.Closed)
                    con.Open();
                SqlDataAdapter da = new SqlDataAdapter("SELECT KRITERNO, VERI AS SKOR FROM CARSAF_KRITER WHERE ANKETNO = " + anket.Rows[0]["ANKETID"].ToString() + "", con);
                da.Fill(kriter_sum);

                object genelcsi = anket.Rows[0]["GENELCSII"];


                lblYuzde.Text = String.Format("{0:0}", anket.Rows[0]["GENELCSII"]);
                lblToplam.Text = String.Format("{0:0}", anket.Rows[0]["GENELCSI"]);

                DataTable dt = new DataTable();
                dt.Columns.Add("SORUNO");
                dt.Columns.Add("SORUSIRA");
                dt.Columns.Add("EVETHAYIR");
                dt.Columns.Add("SKORADAHIL");
                dt.Columns.Add("SECENEKLER");
                dt.Columns.Add("ETIKET");
                dt.Columns.Add("YANIT");
                dt.Columns.Add("YANITNO");
                dt.Columns.Add("MAKSSKOR");
                dt.Columns.Add("PUAN");
                dt.Columns.Add("TOPLANIR");
                dt.Columns.Add("ONCELIK");

                // lblSaat.Text = anket.BASLANGICSAAT;
                // lblGizliMusteri.Text = anket.GIZLI;

                //lblTarih.Text = anket.KATILIMTARIH.ToString("dd.MM.yyyy");
                string prevkriter = "";

                DataRow dr;

                Dictionary<string, string> _ustkisim = new Dictionary<string, string>();
                foreach (DataRow soru in sorular.Rows)
                {


                    if (prevkriter != soru["KRITERNO"].ToString())
                    {
                        dr = dt.NewRow();
                        dr["SORUNO"] = 0;
                        try
                        {
                            DataRow var = kriterler.Select("KRITERNO=" + soru["KRITERNO"].ToString())[0];

                            dr["SORUSIRA"] = "";
                            dr["ETIKET"] = var["KRITER"].ToString();
                            dr["MAKSSKOR"] = var["MAKSSKOR"];

                        }
                        catch (Exception)
                        {


                        }
                        try
                        {
                            dr["PUAN"] = kriter_sum.Select("KRITERNO=" + soru["KRITERNO"].ToString())[0]["SKOR"].ToString();
                        }
                        catch
                        {

                            dr["PUAN"] = "";
                        }
                        prevkriter = soru["KRITERNO"].ToString();
                        dt.Rows.Add(dr);

                    }
                    dr = dt.NewRow();
                    dr["SORUNO"] = soru["SORUNO"];
                    dr["SORUSIRA"] = soru["SORUSIRA"];
                    dr["TOPLANIR"] = soru["TOPLANIR"];
                    if (soru["EVETHAYIR"].ToString() == "-2")
                        dr["ETIKET"] = soru["SORUMETNI"] + (soru["ACIKLAMA"].ToString() == "" ? "" : "<br/>" + soru["ACIKLAMA"].ToString());
                    else
                        dr["ETIKET"] = soru["SORUMETNI"];


                    dr["SECENEKLER"] = soru["SECENEKLER"];
                    dr["EVETHAYIR"] = (soru["GRUP"].ToString() == "YORUM" ? "-2" : soru["EVETHAYIR"].ToString());
                    dr["SKORADAHIL"] = 1;
                    dr["ONCELIK"] = soru["SORUSIRA"];
                    dr["MAKSSKOR"] = soru["MAKSSKOR"];

                    try
                    {
                        DataRow _y = detaylar.Select("SORUNO=" + soru["SORUNO"].ToString())[0];

                        if (int.Parse(dr["EVETHAYIR"].ToString()) < 0)
                        {
                            try
                            {
                                dr["ETIKET"] = dr["ETIKET"].ToString() + "<br/><font style='color:#880000;'>" + _y["YANIT"].ToString() + "</font>";
                            }
                            catch
                            {
                            }

                        }

                        try
                        {
                            dr["YANITNO"] = _y["YANITNO"].ToString();
                            dr["YANIT"] = EtiketBul2(soru["SECENEKLER"].ToString(), ((int)parse2Double(_y["DEGER"].ToString())).ToString(), soru["EVETHAYIR"].ToString());
                        }
                        catch
                        {
                        }
                        try
                        {
                            dr["PUAN"] = _y["YANITR"].ToString();
                        }
                        catch
                        {
                            dr["PUAN"] = "";
                        }
                    }
                    catch
                    {
                    }

                    //TOPLANAN /OR ILE ISLENEN SUTUNLARIN PUANI BURAYA

                    dt.Rows.Add(dr);
                    if (soru["EVETHAYIR"].ToString() == "0")
                    {
                        //tüm alt secenekler yüksek puanlıdan baslayarak satır satır eklenecek
                        if (soru["SECENEKLERPUAN"].ToString() == "") continue;
                        string[] puanlar = soru["SECENEKLERPUAN"].ToString().Split(Properties.Settings.Default.FIELDSEP1);
                        foreach (string puan in puanlar)
                        {
                            dr = dt.NewRow();
                            dr["SORUNO"] = soru["SORUNO"].ToString();
                            dr["SORUSIRA"] = puan.Split(Properties.Settings.Default.FIELDSEP2)[0] + ">";
                            dr["MAKSSKOR"] = puan.Split(Properties.Settings.Default.FIELDSEP2)[1];
                            dr["ETIKET"] = EtiketBul2(soru["SECENEKLER"].ToString(), puan.Split(Properties.Settings.Default.FIELDSEP2)[0], soru["EVETHAYIR"].ToString());
                            dt.Rows.Add(dr);
                        }
                    }

                }



                DataRow toplam = dt.NewRow();
                lstKarne.DataSource = dt;
                lstKarne.DataKeyNames = new string[] { "YANITNO", "SORUNO", "YANIT", "SECENEKLER", "EVETHAYIR" };
                lstKarne.DataBind();






                string filename = anket.Rows[0]["LOGID"].ToString() + "_" + anket.Rows[0]["ANKETNO"].ToString() + "_" + anket.Rows[0]["DEGERLENDIRMETARIHI"].ToString().Replace("-", "_").Substring(0, 10) + ".xls";
                filename = filename.Replace("__", "_").Replace("__", "_");

                //catch e düşüyor

                string temp = new ARTQM.ExcelConvertor().ConvertKarne(dt, Server.MapPath("temp"), filename, info[0] + " | " + info[1], info[2], info[3], info[4]);

                Session["FILENAME"] = "temp/" + filename;


            }
            catch (Exception Ex)
            {

            }
        }

        public void LoadAnket2()
        {
        }
        public string GetRowClass(string soruno, string sorusira, string evethayir)
        {
            string result = "";
            if (soruno == "0") //kriter
                result = "kriter_baslik";
            else if (evethayir == "-2")
                result = "yorum";
            else if (sorusira.Contains("_") || sorusira.Contains(">")) // soru baslıgı degil saga yasla
                result = "kriter_secenek";

            else result = "kriter_satir";
            return result;
        }

        protected void Unnamed1_Click(object sender, ImageClickEventArgs e)
        {

            DataTable temp = (DataTable)Session["ANKET"];

            Response.Redirect("EditEval.aspx?anket=" + temp.Rows[0]["ANKETID"].ToString() + "&callid=" + Request.QueryString["callid"] + "&projeno=" + Request.QueryString["projeno"] + "&agent=" + Request.QueryString["agent"] + "&donem=" + Request.QueryString["donem"], true);


        }

        protected void lnkEdit_Click(object sender, EventArgs e)
        {
            DataTable temp = (DataTable)Session["ANKET"];

            Response.Redirect("EditEval.aspx?anket=" + temp.Rows[0]["ANKETID"].ToString() + "&callid=" + Request.QueryString["callid"] + "&projeno=" + Request.QueryString["projeno"] + "&agent=" + Request.QueryString["agent"] + "&donem=" + Request.QueryString["donem"], true);

        }

        protected void lnkDownload_Click(object sender, EventArgs e)
        {
            divScript.InnerHtml = "<script>popitupxs('Download.aspx');</script>";

        }
    }
}