﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
using System.Data.SqlClient;
using System.Configuration;

namespace ARTQM
{
    public partial class Calls : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string cagriKayitlari = Session["CAGRIKAYITLARI"].ToString();

            if (cagriKayitlari.Equals("0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                        "alert('Yetkiniz Yok'); window.location='" + Request.UrlReferrer.ToString() + "';", true);
                return;
            }
            
            if (!IsPostBack)
            {
                DateTime date = DateTime.Today;
                var firstDayOfMonth = DateTime.Today.AddDays(-10);
                var lastDayOfMonth = DateTime.Today;
                drpRandomCall.SelectedValue = "1";
                txtRange.Text = firstDayOfMonth.ToString("dd.MM.yyyy") + " - " + lastDayOfMonth.ToString("dd.MM.yyyy");
                LoadGroups();
                

                //btnSearch_Click(null, null);
            }
        }
        public void LoadGroups()
        {
            DataTable tmp = null;
            DataTable tmp2 = null;

            if (!Session["UserAgents"].ToString().Equals("0"))
                //tmp = ((DataTable)Session["AGENTS"]).Select("AgentID IN (" + Session["UserAgents"] + ")").CopyToDataTable();
                tmp = ((DataTable)Session["AGENTS"]).Select("AgentID IN ("+ Session["UserAgents"] + ")").CopyToDataTable();
            else
                tmp = (DataTable)Session["AGENTS"];
            drpGroup.DataSource = tmp;
            drpGroup.DataTextField = "AGENT";
            drpGroup.DataValueField = "AgentID";
            drpGroup.DataBind();
            drpGroup.Items.Insert(0, new ListItem("Tümü", "0"));

            if (!Session["UserSkills"].ToString().Equals(""))
                tmp2 = ((DataTable)Session["SKILLS"]).Select("SkillID IN (" + Session["UserSkills"] + ")").CopyToDataTable();
            else
                tmp2 = (DataTable)Session["SKILLS"];

            drpSkill.DataSource = tmp2;
            drpSkill.DataTextField = "Skill";
            drpSkill.DataValueField = "SkillID";
            drpSkill.DataBind();
            drpSkill.Items.Insert(0, new ListItem("Tümü", "0"));
            drpSkill.SelectedIndex = 0;


            //drpUsers.DataSource = Lists.GetUsers();
            //drpUsers.DataTextField = "KULLANICI";
            //drpUsers.DataValueField = "KULLANICI";
            //drpUsers.DataBind();
            //drpUsers.Items.Insert(0, new ListItem("Tümü", "0"));



            DataTable forms = Lists.GetActiveForms();

            drpForms.DataSource = forms;
            drpForms.DataValueField = "PROJENO";
            drpForms.DataTextField = "PROJEADI";
            drpForms.DataBind();

        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            divScript.InnerHtml = "";
            //WFOUtils ws = new WFOUtils();
            //DataProvider obj = new DataProvider();
            Utils obj = new Utils();

            int durStart = -1;
            int durEnd = -1;

            try
            {
                durStart = int.Parse(txtStart.Text.Trim());
            }
            catch
            { durStart = -1; }

            try
            {
                durEnd = int.Parse(txtEnd.Text.Trim());
            }
            catch
            { durEnd = -1; }


            //DateTime callStart = DateTime.Now;
            //DateTime callEnd = DateTime.Now;
            //DateTime evalStart = DateTime.MinValue;
            //DateTime evalEnd = DateTime.MaxValue;

            String callStart = string.Empty;
            String callEnd = string.Empty;

            try
            {

                //callStart = DateTime.ParseExact(txtRange.Text.Split('-')[0].Trim(), "yyyy-MM-dd hh:mm:ss", null);
                callStart = DateTime.Parse(txtRange.Text.Split('-')[0].Trim()).ToString("yyyy-MM-dd 00:00:00");
                //sqlCallStart = DateTime.Today.AddDays(-2).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

            }
            catch (Exception)
            {

                callStart = DateTime.MinValue.ToString("yyyy-MM-dd");
            }
            try
            {
                //callEnd = DateTime.ParseExact(txtRange.Text.Split('-')[1].Trim(), "yyyy-MM-dd", null);
                callEnd = DateTime.Parse(txtRange.Text.Split('-')[1].Trim()).ToString("yyyy-MM-dd 23:59:59");
            }
            catch (Exception)
            {

                callEnd = DateTime.MaxValue.ToString("yyyy-MM-dd");
            }

            //try
            //{

            //    evalStart = DateTime.ParseExact(txtEvalRange.Text.Split('-')[0].Trim() + " 00:00:00", "dd.MM.yyyy HH:mm:ss", null);
            //}
            //catch (Exception)
            //{

            //    evalStart = DateTime.MinValue;
            //}
            //try
            //{
            //    evalEnd = DateTime.ParseExact(txtEvalRange.Text.Split('-')[1].Trim() + " 23:59:59", "dd.MM.yyyy HH:mm:ss", null);
            //}
            //catch (Exception)
            //{

            //    evalEnd = DateTime.MaxValue;
            //}




            //DataTable calls = ws.QueryCalls2("", drpGroup.SelectedValue == "0" ? "" : drpGroup.SelectedValue, drpSkill.SelectedValue == "0" ? Session["UserSkills"].ToString() : drpSkill.SelectedValue, callStart, callEnd, durStart, durEnd, evalStart, evalEnd, "", "", txtTelefon.Text.Trim().Replace("'", ""), "", drpDirection.SelectedValue, "", "", "");
            //DataTable calls = obj.GetCallRec("SELECT * FROM TBLCCCDR WHERE UNITUID = {UNITUID} AND DISPOSITION = 'ANSWERED' AND (SKILLEVENT = 'COMPLETECALLER' OR SKILLEVENT = 'COMPLETEAGENT') AND USERFIELD = 'INBOUND' AND CALLDATE BETWEEN '" + callStart + "' AND '" + callEnd + "'");
            //DataTable calls = obj.QueryCalls(callStart, callEnd, drpDirection.SelectedValue, txtTelefon.Text.Trim(), durStart, durEnd, drpGroup.SelectedValue == "0" ? "" : drpGroup.SelectedValue, drpSkill.SelectedValue == "0" ? Session["UserSkills"].ToString() : drpSkill.SelectedValue);
            DataTable calls = obj.QueryCalls(callStart, callEnd, drpDirection.SelectedValue, txtTelefon.Text.Trim(), durStart, durEnd, drpGroup.SelectedValue == "0" ? "" : drpGroup.SelectedValue, drpSkill.SelectedValue == "0" ? Session["UserSkills"].ToString() : drpSkill.SelectedValue, Session["UserAgents"].ToString(), drpRandomCall.SelectedValue);

            calls.Columns.Add("EditEval");

            string ANKETNO = string.Empty;
            foreach (DataRow dr in calls.Rows)
            {
                ANKETNO += "'" + dr["Callid"].ToString() + "',";
                dr["EditEval"] = "0";
            }
            if (ANKETNO.Length > 0)
            {
                ANKETNO = ANKETNO.Remove(ANKETNO.Length - 1, 1);

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
                DataTable anketNo = new DataTable();
                try
                {
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter("SELECT ANKETNO FROM ANKETLER WHERE CALLDATE BETWEEN '" + callStart + "' AND '" + callEnd + "' AND ANKETNO IN (" + ANKETNO + ")", conn);
                    da.Fill(anketNo);
                    conn.Close();
                }
                catch (Exception Ex) { }

                foreach (DataRow dr in anketNo.Rows)
                {
                    foreach (DataRow call in calls.Rows)
                    {
                        if (dr["ANKETNO"].ToString() == call["Callid"].ToString())
                        {
                            call["EditEval"] = "1";
                        }
                    }
                }
            }
            grdData.DataSource = calls;
            grdData.DataBind();

            Session["grdData"] = new DataTable();
            Session["grdData"] = calls;

            //TextBox1.Text = drpGroup.SelectedValue;

        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            ExportToExcel(grdData, "DEGERLENDIRME_RAPORU_" + txtRange.Text.Replace(" ", "").Replace("-", "_").Replace(".", "_") + "_" + drpGroup.SelectedItem.Text.Replace(" ", "_").Replace("-", "_"));
        }

        protected void grdData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {


                case "details":
                    divScript.InnerHtml = "<script> popitup('CallDetail.aspx?callid=" + e.CommandArgument + "')</script>;";
                    break;
                case "eval":
                    Session["SelectedID"] = e.CommandArgument.ToString();
                    divScript.InnerHtml = "<script>$('#divUpdate').modal(); </script>";
                    break;
                default: break;
            }
        }

        protected void btnContinue_Click(object sender, EventArgs e)
        {

            try
            {
                string id = Session["SelectedID"].ToString();
                Session["PROJEADI"] = drpForms.SelectedItem.Text;
                Session["PROJENO"] = drpForms.SelectedValue;
                divScript.InnerHtml = "<script> popitup('ViewEval.aspx?anket=&callid=" + id.Split('?')[0] + "&donem=" + id.Split('?')[1] + "&projeno=" + drpForms.SelectedValue + "&agent=" + id.Split('?')[2] + "&direction=" + id.Split('?')[3] + "&duration=" + id.Split('?')[4] + "&skill=" + id.Split('?')[5] + "&partynames=" + id.Split('?')[6] + "&startdate=" + id.Split('?')[7] + "&skillset=" + id.Split('?')[8] + "&partyagentnames=" + id.Split('?')[9] + "')</script>;";
            }
            catch
            {
            }


        }

        protected void grdData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            divScript.InnerHtml = string.Empty;
            grdData.PageIndex = e.NewPageIndex;
            //btnSearch_Click(null, null);
            grdData.DataSource = Session["grdData"];
            grdData.DataBind();
        }



    }
}