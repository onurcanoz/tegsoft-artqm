﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ARTQM
{
    public partial class CallDetail : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                LoadNotes();
                LoadDetails();

            }

        }
        public void LoadNotes()
        {
            string callID = Request.QueryString["callid"].Split('?')[6];
            grdData.DataSource = Lists.GetCallNotes(callID);
            grdData.DataBind();


            grdLog.DataSource = Lists.GetCallLog(callID);
            grdLog.DataBind();
            //txtNot.Text = "";

            //ÇAĞRI NOTLARININ VE İŞLEM GEÇMİŞİNİN BASILDIĞI YER
        }
        public void LoadDetails()
        {
            string[] args = Request.QueryString["callid"].ToString().Split('?');
            try
            {
                //DataTable dt = Lists.GetCallDetails(Request.QueryString["callid"]);
                //lblAgent.Text = dt.Rows[0]["AGENTS"].ToString();
                //lblAgent.Text = Request.QueryString["callid"];
                //lblDuration.Text = dt.Rows[0]["DURATION"].ToString();
                //lblDirection.Text = dt.Rows[0]["DIRECTION"].ToString();
                //lblParties.Text = dt.Rows[0]["PARTIES"].ToString();
                //lblGroup.Text = dt.Rows[0]["SKILLSET"].ToString();
                //lblStartDate.Text = dt.Rows[0]["STARTDATE"].ToString();

                

                lblAgent.Text = args[0].Trim();
                lblDuration.Text = Seconds2Span(args[5].Trim().ToString());
                lblDirection.Text = args[3].Trim() == "1" ? "Gelen" : "Giden";
                lblParties.Text = args[4].Trim();
                lblGroup.Text = args[1].Trim();
                lblStartDate.Text = args[2].Trim();

            }
            catch
            {
            }


            //WFOUtils ws = new WFOUtils();
            //DataTable segments = ws.GetCallSegments(Request.QueryString["callid"]);
            //if (segments != null){
            DataTable audioFile = new DataTable();
            audioFile.Columns.Add("inum");
            audioFile.Columns.Add("startedat");
            audioFile.Columns.Add("duration2");
            DataRow audioFileRow = audioFile.NewRow();

            audioFileRow["inum"] = args[6];
            audioFileRow["startedat"] = args[2];
            audioFileRow["duration2"] = args[5];
            audioFile.Rows.Add(audioFileRow);

            grdSegments.DataSource = audioFile;
            grdSegments.DataKeyNames = new string[] { "inum" };
            grdSegments.DataBind();
            }
        //}

        //protected void btnSave_Click(object sender, EventArgs e)
        //{
        //    string callID = Request.QueryString["callid"].Split('?')[6];
        
        //    try
        //    {
        //        Lists.AddNote(callID, txtNot.Text, CurrentUser.UserName);
        //        LoadNotes();
        //    }
        //    catch
        //    {
        //    }
        //}

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string inum = grdSegments.SelectedDataKey.Value.ToString();
            //frmPlayer.Attributes["src"] = "Player.aspx?inum=" + inum;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://176.88.216.226/Tobe/app/ApplicationServlet?externalService=downloadRecording&usercode=artpro.admin&clearpassword=Artpro12!&uniqueid=" + inum);
            //string authInfo = ARTQM.Properties.Settings.Default.ACRAPIUSER + ":" + ARTQM.Properties.Settings.Default.ACRAPIPWD;
            //authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            //request.Headers.Add("Authorization", "Basic " + authInfo);
            //request.Credentials = new NetworkCredential(ARTQM.Properties.Settings.Default.ACRAPIUSER, ARTQM.Properties.Settings.Default.ACRAPIPWD);
            request.AllowAutoRedirect = true;
            request.Proxy = null;
            request.MaximumAutomaticRedirections = 1;

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            if (response.ContentType == "audio/mpeg;charset=UTF-8")
            {
                MemoryStream ms = new MemoryStream();
                response.GetResponseStream().CopyTo(ms);
                ms.Position = 0;
                FileStream sw = new FileStream(Server.MapPath("temp") + "\\" + inum + ".mp3", FileMode.OpenOrCreate);
                ms.WriteTo(sw);
                ms.Close();
                sw.Close();

                Player.Attributes["src"] = "temp\\" + inum + ".mp3";
            }
            else if(response.ContentType == "application/zip;charset=UTF-8")
            {
                DataProvider obj = new DataProvider();

                DataTable dt = obj.GetCallRec("SELECT AUDIOFILENAME, CALLDATE FROM TBLCCCDR WHERE UNIQUEID = '" + inum + "'");

                if (!File.Exists(Server.MapPath("temp") + "\\" + dt.Rows[0]["audioFileName"].ToString().Trim() + ".mp3"))
                {
                    MemoryStream ms = new MemoryStream();
                    response.GetResponseStream().CopyTo(ms);
                    ms.Position = 0;

                    //FileStream sw = new FileStream(@"C:\Users\lorta\Desktop\TegsoftSesKayit\TegsoftSesKayit\zip\20190503-1556892009.300.zip", FileMode.OpenOrCreate);
                    FileStream sw = new FileStream(Server.MapPath("zip") + "\\" + inum + ".zip", FileMode.OpenOrCreate);
                    ms.WriteTo(sw);
                    ms.Close();
                    sw.Close();

                    //ZipFile.ExtractToDirectory(@"C:\Users\lorta\Desktop\TegsoftSesKayit\TegsoftSesKayit\zip\20190503-1556892009.300.zip", @"C:\Users\lorta\Desktop\TegsoftSesKayit\TegsoftSesKayit\temp");



                    ZipFile.ExtractToDirectory(Server.MapPath("zip") + "\\" + inum + ".zip", Server.MapPath("temp"));     
                }
                Player.Attributes["src"] = "temp\\" + dt.Rows[0]["audioFileName"].ToString().Trim() + ".mp3";
            }

            
            //divPlayer.InnerHtml = "<embed src=\"temp\\" + inum + ".wav" + "\" autoplay=\"false\" autostart=\"false\" type=\"audio/mpeg\" loop=\"true\" controller=\"true\" width=\"100%\" height=\"45\"></embed>";


        }
    }
}