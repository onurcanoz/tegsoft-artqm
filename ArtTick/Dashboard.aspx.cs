﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ARTQM
{
    public partial class Dashboard : PageBase 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadGroups();
            }
            
        }


        public void LoadGroups()
        {
            drpYIL.Items.Clear();
            drpYIL2.Items.Clear();
            for (int i = 2016; i <= DateTime.Now.Year; i++)
            {
                drpYIL.Items.Add(i.ToString());
                drpYIL2.Items.Add(i.ToString());
            }
            

            drpAY.Items.Clear();
            drpAY2.Items.Clear();
            for (int i = 1; i <= 12; i++)
            {
                drpAY.Items.Add(i.ToString().PadLeft(2,'0'));
                drpAY2.Items.Add(i.ToString().PadLeft(2,'0'));
            }

            drpYIL.SelectedValue = drpYIL2.SelectedValue = DateTime.Now.Year.ToString();
            drpAY.SelectedValue = drpAY2.SelectedValue = DateTime.Now.Month.ToString();
            drpForm.DataSource = Lists.GetForms();
            drpForm.DataValueField = "PROJENO";
            drpForm.DataTextField = "PROJEADI";
            drpForm.DataBind();
            drpForm.Items.Insert(0, new ListItem("Tümü", "0"));
           
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
           // return;

            DataSet set = Reports.DashboardTotals(int.Parse(drpYIL.SelectedValue), int.Parse(drpAY.SelectedValue), int.Parse(drpYIL2.SelectedValue), int.Parse(drpAY2.SelectedValue), drpForm.SelectedValue);



            string serie1 = "var data1 = [";
            DataTable evals = set.Tables["EVALS"];
            foreach (DataRow dr in evals.Rows) {
            
                serie1+= "['"+dr["LOGID"].ToString()+"', "+dr["EVALS"].ToString()+"],";
            }
            serie1 = serie1.Remove(serie1.Length - 1);
            serie1 += "];";
            
            
            string serie2 = "var data2 = [ ";
            string serie3 = "var data3 = [ ";
            int max = 0;
            DataTable iocalls = set.Tables["IOCALLS"];
            foreach (DataRow dr in iocalls.Rows)
            {
                serie2 += "['" + dr["Partyagentnames"].ToString()+ "', " + dr["CALLS"].ToString() + "],";
               
                max = Math.Max( int.Parse(dr["CALLS"].ToString()), max );
            }
            serie2 = serie2.Remove(serie2.Length - 1);
            serie2 += "];";

            serie3 = serie3.Remove(serie3.Length - 1);
            serie3 += "];";

            serie1 += serie2 + " var dataset = [ { label: 'Gelen+Giden', data: data2,  bars: { show: true, barWidth: 0.6, align: \"center\" } } "
                +(evals.Rows.Count == 0 ? "" :  ", { label: 'Değerlendirme', data: data1 , points: { symbol: 'triangle', fillColor: '#0062FF', show: true }, lines: {show:true}}")  +"];";


            divScriptLeft.InnerHtml = "<script>" + serie1 + " var plot= $.plot('#placeholder', dataset, { " +
                "  yaxes: [{ position: 'left', max: "+max.ToString() +", color: 'black', axisLabel: 'Adet(N)',        axisLabelUseCanvas: true, axisLabelFontSizePixels: 12, axisLabelFontFamily: 'Verdana, Arial', axisLabelPadding: 3 }], xaxis: { mode: \"categories\", tickLength: 0,  axisLabelUseCanvas: true, labelAngle: -90 }});</script>";
            


        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}