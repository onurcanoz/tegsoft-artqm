﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ARTQM
{
    public class Utils
    {
        DataProvider obj = new DataProvider();
        public DataTable QueryCalls(string callStart, string callEnd, string direction, string telefonNo, int durationFrom, int durationTo, string agentID, string skill, string agentList, string randomCallsCount)
        {
            if (direction == "0")
            {
                string q1 = "SELECT cdr.UNIQUEID, cdr.CALLDATE, cdr.SRC, cdr.DST, cdr.DURATION, cdr.USERFIELD, cdr.CLID, cdr.DISPOSITION, cdr.AUDIOFILENAME, s.NAME AS SKILL, s.SKILL AS SKILLID, cdr.UID, u.USERCODE AS USERCODE FROM TBLCCCDR AS cdr LEFT JOIN TBLCCSKILLS AS s ON cdr.SKILL = s.SKILL LEFT JOIN TBLUSERS AS u ON cdr.UID = u.UID WHERE cdr.DISPOSITION = 'ANSWERED' AND (cdr.SKILLEVENT = 'COMPLETECALLER' OR cdr.SKILLEVENT = 'COMPLETEAGENT') AND cdr.USERFIELD = 'INBOUND' AND cdr.CALLDATE BETWEEN '" + callStart + "' AND '" + callEnd + "'";
                string q2 = "SELECT cdr.UNIQUEID, cdr.CALLDATE, cdr.SRC, cdr.DST, cdr.DURATION, cdr.USERFIELD, cdr.CLID, cdr.DISPOSITION, cdr.AUDIOFILENAME, cdr.UID, u.USERCODE AS USERCODE FROM TBLCCCDR AS cdr LEFT JOIN TBLUSERS AS u ON cdr.UID = u.UID WHERE DISPOSITION = 'ANSWERED' AND USERFIELD = 'OUTBOUND' AND CALLDATE BETWEEN '" + callStart + "' AND '" + callEnd + "'";

                if (telefonNo != "")
                {
                    q1 += " AND (cdr.SRC = '" + telefonNo + "' OR cdr.DST = '" + telefonNo + "')";
                    q2 += " AND (cdr.SRC = '" + telefonNo + "' OR cdr.DST = '" + telefonNo + "')";
                }
                if (durationFrom != -1)
                {
                    q1 += " AND cdr.DURATION >='" + durationFrom + "'";
                    q2 += " AND cdr.DURATION >='" + durationFrom + "'";
                }
                if (durationTo != -1)
                {
                    q1 += " AND cdr.DURATION <='" + durationTo + "'";
                    q2 += " AND cdr.DURATION <='" + durationTo + "'";
                }
                if (agentID != "")
                {
                    q1 += " AND cdr.UID = '" + agentID + "'";
                    q2 += " AND cdr.UID = '" + agentID + "'";
                }
                if (skill != "")
                {
                    q1 += " AND cdr.SKILL IN (" + skill + ")";
                }
                if (agentList != "0")
                {
                    q1 += " AND cdr.UID IN (" + agentList + ")";
                    q2 += " AND cdr.UID IN (" + agentList + ")";
                }
                if(agentList == "0")
                {
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
                    DataTable dt = new DataTable();
                    try
                    {
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter("SELECT LOGID FROM AGENTS", conn);
                        da.Fill(dt);
                        conn.Close();
                        string UID = string.Empty;
                        foreach(DataRow dr in dt.Rows)
                        {
                            UID += "'" + dr["LOGID"] + "',";
                        }
                        UID = UID.Remove(UID.Length - 1, 1);

                        q1 += " AND cdr.UID IN (" + UID + ")";
                        q2 += " AND cdr.UID IN (" + UID + ")";

                    }
                    catch(Exception Ex)
                    {

                    }
                }
                if(randomCallsCount == "1")
                {
                    q1 += " ORDER BY rand() FETCH FIRST 4 ROWS ONLY";
                    q2 += " ORDER BY rand() FETCH FIRST 1 ROWS ONLY";
                }
                else if(randomCallsCount == "2")
                {
                    q1 += " ORDER BY rand() FETCH FIRST 5 ROWS ONLY";
                    q2 += " ORDER BY rand() FETCH FIRST 5 ROWS ONLY";
                }
                else if (randomCallsCount == "3")
                {
                    q1 += " FETCH FIRST 100 ROWS ONLY";
                    q2 += " FETCH FIRST 100 ROWS ONLY";
                }
                else if(randomCallsCount == "0")
                {
                    q1 += " FETCH FIRST 100 ROWS ONLY";
                    q2 += " FETCH FIRST 100 ROWS ONLY";
                }
                DataTable Inbound = obj.GetCallRec(q1);
                DataTable Outbound = obj.GetCallRec(q2);
                Inbound.Merge(Outbound);
                Inbound.DefaultView.Sort = "Callid DESC";
                return Inbound;
            }
            else if(direction == "1")
            {
                string q = "SELECT cdr.UNIQUEID, cdr.CALLDATE, cdr.SRC, cdr.DST, cdr.DURATION, cdr.USERFIELD, cdr.CLID, cdr.DISPOSITION, cdr.AUDIOFILENAME, s.NAME AS SKILL, s.SKILL AS SKILLID, cdr.UID, u.USERCODE AS USERCODE FROM TBLCCCDR AS cdr LEFT JOIN TBLCCSKILLS AS s ON cdr.SKILL = s.SKILL LEFT JOIN TBLUSERS AS u ON cdr.UID = u.UID WHERE cdr.DISPOSITION = 'ANSWERED' AND (cdr.SKILLEVENT = 'COMPLETECALLER' OR cdr.SKILLEVENT = 'COMPLETEAGENT') AND cdr.USERFIELD = 'INBOUND' AND cdr.CALLDATE BETWEEN '" + callStart + "' AND '" + callEnd + "'";

                if(telefonNo != "")
                {
                    q += " AND (cdr.SRC = '" + telefonNo + "' OR cdr.DST = '" + telefonNo + "')";
                }
                if(durationFrom != -1)
                {
                    q += " AND cdr.DURATION >='" + durationFrom + "'";
                }
                if(durationTo != -1)
                {
                    q += " AND cdr.DURATION <='" + durationTo + "'";
                }
                if(agentID != "")
                {
                    q += " AND cdr.UID = '" + agentID+"'";
                }
                if(skill != "")
                {
                    q += " AND cdr.SKILL IN (" + skill + ")";
                }
                if(agentList != "0")
                {
                    q += " AND cdr.UID IN (" + agentList + ")";
                }
                if (agentList == "0")
                {
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
                    DataTable dt = new DataTable();
                    try
                    {
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter("SELECT LOGID FROM AGENTS", conn);
                        da.Fill(dt);
                        conn.Close();
                        string UID = string.Empty;
                        foreach (DataRow dr in dt.Rows)
                        {
                            UID += "'" + dr["LOGID"] + "',";
                        }
                        UID = UID.Remove(UID.Length - 1, 1);

                        q += " AND cdr.UID IN (" + UID + ")";
                    }
                    catch (Exception Ex)
                    {

                    }
                }
                if (randomCallsCount == "1")
                {
                    q += " ORDER BY rand() FETCH FIRST 5 ROWS ONLY";
                    
                }
                else if (randomCallsCount == "2")
                {
                    q += " ORDER BY rand() FETCH FIRST 10 ROWS ONLY";
                }
                else if(randomCallsCount == "3")
                {
                    q += " FETCH FIRST 100 ROWS ONLY";
                }
                else if(randomCallsCount == "0")
                {
                    q += " FETCH FIRST 200 ROWS ONLY";
                }

                return obj.GetCallRec(q);
            }
            else if(direction == "2")
            {
                string q = "SELECT cdr.UNIQUEID, cdr.CALLDATE, cdr.SRC, cdr.DST, cdr.DURATION, cdr.USERFIELD, cdr.CLID, cdr.DISPOSITION, cdr.AUDIOFILENAME, cdr.UID, u.USERCODE AS USERCODE FROM TBLCCCDR AS cdr LEFT JOIN TBLUSERS AS u ON cdr.UID = u.UID WHERE DISPOSITION = 'ANSWERED' AND USERFIELD = 'OUTBOUND' AND CALLDATE BETWEEN '" + callStart + "' AND '" + callEnd + "' AND cdr.CAMPAIGNID IS NULL";

                if (telefonNo != "")
                {
                    q += " AND (cdr.SRC = '" + telefonNo + "' OR cdr.DST = '" + telefonNo + "')";
                }
                if (durationFrom != -1)
                {
                    q += " AND cdr.DURATION >='" + durationFrom + "'";
                }
                if (durationTo != -1)
                {
                    q += " AND cdr.DURATION <='" + durationTo + "'";
                }
                if (agentID != "")
                {
                    q += " AND cdr.UID = '" + agentID + "'";
                }
                if (agentList != "0")
                {
                    q += " AND cdr.UID IN (" + agentList + ")";
                }
                if (agentList == "0")
                {
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
                    DataTable dt = new DataTable();
                    try
                    {
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter("SELECT LOGID FROM AGENTS", conn);
                        da.Fill(dt);
                        conn.Close();
                        string UID = string.Empty;
                        foreach (DataRow dr in dt.Rows)
                        {
                            UID += "'" + dr["LOGID"] + "',";
                        }
                        UID = UID.Remove(UID.Length - 1, 1);

                        q += " AND cdr.UID IN (" + UID + ")";
                    }
                    catch (Exception Ex)
                    {

                    }
                }
                if (randomCallsCount == "1")
                {
                    q += " ORDER BY rand() FETCH FIRST 5 ROWS ONLY";

                }
                else if (randomCallsCount == "2")
                {
                    q += " ORDER BY rand() FETCH FIRST 10 ROWS ONLY";
                }
                else if (randomCallsCount == "3")
                {
                    q += " FETCH FIRST 100 ROWS ONLY";
                }
                else if(randomCallsCount == "0")
                {
                    q += " FETCH FIRST 200 ROWS ONLY";
                }

                return obj.GetCallRec(q);
            }
            else if(direction == "3")
            {
                string q = "SELECT cdr.UNIQUEID, cdr.CALLDATE, cdr.SRC, cdr.DST, cdr.DURATION, cdr.USERFIELD, cdr.CLID, cdr.DISPOSITION, cdr.AUDIOFILENAME, cdr.UID, u.USERCODE AS USERCODE, cam.NAME AS SKILL FROM TBLCCCDR AS cdr LEFT JOIN TBLUSERS AS u ON cdr.UID = u.UID LEFT JOIN TBLCCCAMPAIGN AS cam ON cdr.CAMPAIGNID = cam.CAMPAIGNID  WHERE DISPOSITION = 'ANSWERED' AND USERFIELD = 'OUTBOUND' AND CALLDATE BETWEEN '" + callStart + "' AND '" + callEnd + "' AND cdr.CAMPAIGNID IS NOT NULL";

                if (telefonNo != "")
                {
                    q += " AND (cdr.SRC = '" + telefonNo + "' OR cdr.DST = '" + telefonNo + "')";
                }
                if (durationFrom != -1)
                {
                    q += " AND cdr.DURATION >='" + durationFrom + "'";
                }
                if (durationTo != -1)
                {
                    q += " AND cdr.DURATION <='" + durationTo + "'";
                }
                if (agentID != "")
                {
                    q += " AND cdr.UID = '" + agentID + "'";
                }
                if (agentList != "0")
                {
                    q += " AND cdr.UID IN (" + agentList + ")";
                }
                if (agentList == "0")
                {
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
                    DataTable dt = new DataTable();
                    try
                    {
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter("SELECT LOGID FROM AGENTS", conn);
                        da.Fill(dt);
                        conn.Close();
                        string UID = string.Empty;
                        foreach (DataRow dr in dt.Rows)
                        {
                            UID += "'" + dr["LOGID"] + "',";
                        }
                        UID = UID.Remove(UID.Length - 1, 1);

                        q += " AND cdr.UID IN (" + UID + ")";
                    }
                    catch (Exception Ex)
                    {

                    }
                }
                if (randomCallsCount == "1")
                {
                    q += " ORDER BY rand() FETCH FIRST 5 ROWS ONLY";

                }
                else if (randomCallsCount == "2")
                {
                    q += " ORDER BY rand() FETCH FIRST 10 ROWS ONLY";
                }
                else if (randomCallsCount == "3")
                {
                    q += " FETCH FIRST 100 ROWS ONLY";
                }
                else if(randomCallsCount == "0")
                {
                    q += " FETCH FIRST 200 ROWS ONLY";
                }

                return obj.GetCallRec(q);
            }
            return null;
        }
    }
}