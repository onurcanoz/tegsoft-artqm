﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmptyMaster.Master" AutoEventWireup="true" CodeBehind="ViewEval.aspx.cs" Inherits="ARTQM.ViewEval" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <style>

        tr.kriter_satir, tr.kriter_satir td, td.kriter_satir
{
    white-space:normal;
    text-indent:0px;
    text-align:left;
	padding-top:2px;
	padding-bottom:2px;
	font-weight: bold;
	background-color:#fe0000;
	border-bottom: 1px solid #c0c0c0;
}

        .row1, .row1 td {

    background-color:#d9534f;
    color:#fefefe;
    border: solid 1px #d43f3a;

}

        
kriter_baslik tr, td.kriter_baslik, .kriter_baslik, .kriter_baslik td
{
    background-color: #eeeeee;
    font-size: 12px;
    text-align: center;
    color: #363636;
    border-bottom: solid 1px #909090;
    font-weight: bold;
    padding-top: 2px;
    padding-bottom: 2px;
}

.kriter_baslik2 tr, td.kriter_baslik2, .kriter_baslik2, .kriter_baslik2 td
{
    background-color: #eeeeee;
    font-size: 12px;
    text-align: center;
    color: #363636;
    border-bottom: solid 1px #909090;
    font-weight: bold;
    padding-top: 2px;
    padding-bottom: 2px;
}

.kriter_secenek , .kriter_secenek td
{
    text-align: left;
    font-weight: normal;
    text-indent:0px;
    white-space: normal;
    border-bottom: 1px solid #c0c0c0;
    
}
.yorum td
{
    background-color: #eeeeee;
    
    text-align: left;
    font-weight:normal;
    
    border-top: solid 2px #464646;
    border-bottom: solid 5px #464646;
}


 tr.kriter_satir, tr.kriter_satir td, td.kriter_satir
{
    white-space:normal;
    text-indent:0px;
    text-align:left;
	padding-top:2px;
	padding-bottom:2px;
	font-weight: bold;
	background-color:#fefefe;
	border-bottom: 1px solid #c0c0c0;
}

.highlightcol , td.highlightcol , .highlightcol td, tr.kriter_satir .highlightcol{
            background-color: #eeeeee; 
            text-align:center; 
            color:#333333;
            border-bottom: 1px solid #c0c0c0;
        }
tr.kriter_baslik .highlightcol{
            border-bottom: 1px solid #909090;
        }
.kriter_satir {
    display:none;
}
.kriter_baslik{
    cursor: pointer;
}


    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
                    
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" runat="server" id="hResult"></h4>
                        </div>
                        <div class="modal-body" runat="server" id="bResult">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                        </div>
                    </div>
                </div>
            </div>

    <div runat="server" id="divResult"></div>

       <div class="panel panel-default">
        <div class="panel-heading">       <asp:Label ID="lblProje" runat="server"></asp:Label>
            </div>
       </div>
    <div class="panel panel-default">
        <div class="panel-heading">SES KAYITLARI</div>
        <div class="panel-body">

   <div class="row">
<div class="col-sm-6">
    <div id="divPlayer" runat="server" style="background-color:#FFFFFF"><audio controls="controls" runat="server" id="Player">          
                        <source src="" type="audio/x-wav" />
                </audio></div>
          <%--<iframe id="frmPlayer" runat=server  frameborder=0 width=100% height=80 src="Player.aspx"></iframe>--%>
    </div>
         <div class="col-sm-6">
    <asp:GridView ID="grdSegments" runat="server" AutoGenerateColumns="false"
        Width="100%" HeaderStyle-HorizontalAlign="Left" BorderColor="Black" 
        BorderStyle="Solid" BorderWidth="1px" CssClass="table table-bordered table-hover" 
        onselectedindexchanged="GridView1_SelectedIndexChanged">
        <Columns>
            <asp:CommandField SelectText="Dinle" ShowSelectButton="True" />
            <asp:BoundField HeaderText="Ses ID" DataField="inum"  HeaderStyle-HorizontalAlign="Center"/>
            <asp:BoundField HeaderText="Başlangıç" DataField="startedat"  DataFormatString="{0:HH\:mm\:ss}" HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField HeaderText="Süre"  HeaderStyle-HorizontalAlign="Left">
            <ItemTemplate>
            <%# Seconds2Span(Eval("duration2").ToString()) %>
            </ItemTemplate>
             </asp:TemplateField>

            
        </Columns>
    </asp:GridView>
   
             
             </div>
         <div class="col-sm-12">   <asp:Label ID="lblMsg" runat="server" CssClass="label label-warning"></asp:Label> 
         </div>


      </div>


        </div>
    </div>
    
    <table id="karne" class="table table-condensed">
        <thead>
              <tr><th><asp:LinkButton ID="lnkDownload" runat="server" onclick="lnkDownload_Click" CssClass="btn btn-default"><i class="glyphicon glyphicon-floppy-save"></i> İndir</asp:LinkButton></th><th>&nbsp;</th><th class="highlightcol">TAM PUAN</th><th runat="server" id="EditButton" style="text-align:right;"><asp:LinkButton ID="lnkEdit" runat="server" onclick="lnkEdit_Click" Visible="true" CssClass="btn btn-default"><i class="glyphicon glyphicon-edit"></i> Düzenle</asp:LinkButton></th><th runat="server" id="ItirazButton" Visible="false" style="text-align:right;"><asp:LinkButton ID="LinkItiraz" runat="server" onclick="lnkItiraz_Click" CssClass="btn btn-default"><i class="glyphicon glyphicon-exclamation-sign"></i> İtiraz Et</asp:LinkButton></th></tr>
        </thead><tbody>
        <tr class="row1"><td>&nbsp;</td>
        <td>BAŞARI YÜZDESİ</td>
        <td style="text-align:center" >100%</td>
        <td style="text-align:center"><asp:Label runat="server" ID="lblYuzde"></asp:Label></td>
        </tr>
        <tr class="row1"><td>&nbsp;</td>
        <td>TOPLAM PUAN</td>
        <td style="text-align:center" >100</td>
        <td style="text-align:center"><asp:Label runat="server" ID="lblToplam"></asp:Label></td>
        </tr>

            <asp:ListView ID="lstKarne" runat="server">               
            <LayoutTemplate>

            <asp:PlaceHolder id="itemPlaceholder" runat="server"></asp:PlaceHolder></LayoutTemplate>
            <ItemTemplate>   
        <tr id='<%#"tr"+Container.DataItemIndex.ToString() %>' class='<%# GetRowClass(Eval("SORUNO").ToString() , Eval("SORUSIRA").ToString() , Eval("EVETHAYIR").ToString()) %>'><td><%# Eval("SORUSIRA").ToString().Replace("_",".") %></td>
            <td ><%# Eval("ETIKET") %></td>
            <td class="highlightcol" ><%# Eval("MAKSSKOR") %>&nbsp;</td>
            <td style="text-align:center"><%# Eval("YANIT") %></td>
        </tr>

            </ItemTemplate> </asp:ListView>     
            </tbody> 
            </table>
    <br />
    <div class="col-sm-1">
        <label class="col-sm-1 control-label" for="txtNot">NOT</label>        
    </div>
     
     
     <div class="col-sm-11">
         <asp:TextBox runat="server" id="txtNot" ClientIDMode="Static" name="txtNot" class="form-control" TextMode="MultiLine" Rows="4" MaxLength="500" />      
    </div>

    <div id="divScript" runat="server"></div>
    <script>
        $(document).ready(function () {
            $(".kriter_baslik").click(function () { $(this).nextUntil(".kriter_baslik").toggle(); });
        });
    </script>       
           
</asp:Content>
