﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="EvalHistory.aspx.cs" Inherits="ARTQM.EvalHistory" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="form-horizontal">
        <fieldset>

            <legend>Arama Kriterleri</legend>

            <div id="divScript" runat="server"></div>

            <div class="row">

                <div class="form-group">

                    <label class="col-sm-1 control-label" for="drpGroup">Operatör</label>

                    <div class="col-sm-2">
                        <asp:DropDownList runat="server" ID="drpGroup" ClientIDMode="Static" name="drpGroup" class="form-control input-sm" />


                    </div>



<%--                    <label class="col-sm-1 control-label" for="dtStart">Görüşme Tarihi</label>
                    <div class="col-sm-2">
                        <asp:TextBox runat="server" ID="txtRange" ClientIDMode="Static" name="txtRange" placeholder="" class="form-control input-sm" />

                    </div>--%>

                    <label class="col-sm-1 control-label" for="txtEvalRange">Değ. Tarihi</label>
                    <div class="col-sm-2">
                        <asp:TextBox runat="server" ID="txtEvalRange" ClientIDMode="Static" name="txtEvalRange" placeholder="" class="form-control input-sm" />
                    </div>

                    <label class="col-sm-1 control-label" for="drpUsers">Değerlendiren</label>

                    <div class="col-sm-2">
                        <asp:DropDownList runat="server" ID="drpUsers" ClientIDMode="Static" name="drpUsers" class="form-control input-sm">
                        </asp:DropDownList>


                    </div>
                </div>

            </div>
            <div class="row">

                <div class="form-group">
                    <label class="col-sm-1 control-label" for="drpSkill">Split/Skill</label>

                    <div class="col-sm-2">
                        <asp:DropDownList runat="server" ID="drpSkill" ClientIDMode="Static" name="drpSkill" class="form-control input-sm">
                        </asp:DropDownList>
                    </div>
                    <label class="col-sm-1 control-label" for="drpDirection">Yön</label>

                    <div class="col-sm-2">
                        <asp:DropDownList runat="server" ID="drpDirection" ClientIDMode="Static" name="drpDirection" class="form-control input-sm">
                            <asp:ListItem Value="0" Text="Tümü" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Gelen"></asp:ListItem>
                            <asp:ListItem Value="2" Text="Giden"></asp:ListItem>
                        </asp:DropDownList>


                    </div>

                    <label class="col-sm-1 control-label" for="dtStart">Süre (sn) </label>
                    <div class="col-sm-1">
                        <asp:TextBox runat="server" ID="txtStart" ClientIDMode="Static" name="txtStart" placeholder="" class="form-control input-sm" Text="-1" />

                    </div>

                    <div class="col-sm-1">
                        <asp:TextBox runat="server" ID="txtEnd" ClientIDMode="Static" name="txtEnd" placeholder="" class="form-control input-sm" Text="-1" />

                    </div>






                </div>


                <div class="form-group">
                    <label class="col-sm-1 control-label" for="drpForm">Değ. Formu</label>

                    <div class="col-sm-2">
                        <asp:DropDownList runat="server" ID="drpForm" ClientIDMode="Static" name="drpForm" class="form-control input-sm">
                        </asp:DropDownList>


                    </div>
                    <label class="col-sm-1 control-label" for="txtTelefon">Telefon No</label>

                    <div class="col-sm-2">
                        <asp:TextBox runat="server" ID="txtTelefon" ClientIDMode="Static" name="txtTelefon" class="form-control input-sm" Text="" placeholder="5XXXXXXXXX" />


                    </div>
                   <%-- <div class="col-sm-4">
                        <label class="label label-info">Doğru kayıt eşleşmesi için telefon numarasını 5XXXXXXXXX şeklinde 10 haneli olarak giriniz.</label>
                    </div>--%>
                    <div class="col-sm-1" style="margin-left:322px;">
                        <asp:Button ID="btnSearch" runat="server" class="btn btn-success" Text="Ara" OnClick="btnSearch_Click" />
                    </div>
                </div>
            </div>
            <br />

            <legend>Tüm Değerlendirmeler</legend>
            <div class="form-group">
                <div class="pull-left">
                    <asp:LinkButton ID="Button1" runat="server" CssClass="btn btn-success" OnClick="Button1_Click" data-toggle="tooltip" data-placement="bottom" title="Excel' e Aktar"><i class="fa fa-file-excel-o" aria-hidden="true"></i></asp:LinkButton>
                </div>
                <div class="pull-left">
                    <h3>
                        <span class="label label-danger" id="spanInfo" runat="server"></span></h3>
                </div>
            </div>
            <div class="form-group">

                <div class="col-sm-12">
                    <asp:GridView ID="grdData" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-responsive table-striped "
                        RowStyle-HorizontalAlign="Center"
                        HeaderStyle-HorizontalAlign="Center"
                        AllowPaging="False" OnRowCommand="grdData_RowCommand">
                        <Columns>
                            <asp:BoundField HeaderText="ID" DataField="ANKETID" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                <ItemStyle></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Operatör" DataField="AGENTS" ItemStyle-CssClass="col-sm-1" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                <ItemStyle CssClass="col-sm-1"></ItemStyle>
                            </asp:BoundField>


                            <asp:BoundField HeaderText="Çağrı Kuyruğu / Kampanya" DataField="ACDGROUP" ItemStyle-CssClass="col-sm-1" HeaderStyle-HorizontalAlign="Left">
                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>

                                <ItemStyle CssClass="col-sm-1"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Değ. Tarihi" DataField="DEGERLENDIRMETARIHI" ItemStyle-CssClass="col-sm-2" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                <ItemStyle CssClass="col-sm-2"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Değerlendiren" DataField="DEGERLENDIREN" ItemStyle-CssClass="col-sm-1" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                <ItemStyle CssClass="col-sm-1"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Form" DataField="FORM" ItemStyle-CssClass="col-sm-1" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                <ItemStyle CssClass="col-sm-1"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Topl. Puan" DataField="TOPLAMPUAN" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                <ItemStyle></ItemStyle>
                            </asp:BoundField>

                            <asp:BoundField HeaderText="Puan" DataField="PUAN" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                <ItemStyle></ItemStyle>
                            </asp:BoundField>

                            <asp:BoundField HeaderText="Geçer Puan" DataField="GECERPUAN" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                <ItemStyle></ItemStyle>
                            </asp:BoundField>

                            <asp:BoundField HeaderText="Görüşme Tarihi" DataField="GORUSMETARIHI" ItemStyle-CssClass="col-sm-1" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                <ItemStyle CssClass="col-sm-2"></ItemStyle>
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="Yön">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>

                                <ItemTemplate>
                                    <%# GetCallDirection(Eval("YON").ToString()) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Süre">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>

                                <ItemTemplate>
                                    <%# Seconds2Span(Eval("SURE").ToString()) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Katılımcılar" DataField="KATILIMCILAR" ItemStyle-CssClass="col-sm-2" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

                                <ItemStyle CssClass="col-sm-2"></ItemStyle>
                            </asp:BoundField>



                            <asp:TemplateField HeaderText="İşlemler">
                                <ItemStyle CssClass="col-sm-1" HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton runat="server" ID="btnView" CommandName="view" CommandArgument=<%#Eval("ANKETNO").ToString() +"?"+ Eval("DATAYIL").ToString() + Eval("DATAAY").ToString().PadLeft(2,'0') +"?"+ Eval("PROJENO").ToString() + "?" + Eval("GORUSMETARIHI").ToString()  %> Text="<span data-toggle='tooltip' title='Görüntüle' ><span class='glyphicon glyphicon-zoom-in'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />

                                    <asp:LinkButton runat="server" ID="btnCall" CommandName="details" CommandArgument='<%#Eval("AGENTS").ToString()+"?"+Eval("ACDGROUP").ToString()+"?"+Eval("GORUSMETARIHI").ToString()+"?"+Eval("YON").ToString()+"?"+Eval("KATILIMCILAR").ToString()+"?"+Eval("SURE").ToString()+"?"+Eval("ANKETNO").ToString() %>' Text="<span data-toggle='tooltip' title='Çağrı Kartı' ><span class='glyphicon glyphicon-headphones'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />


                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                </div>


            </div>
        </fieldset>

        <script>
            $(function () {
                $('#txtRange').daterangepicker(
                    {
                        locale: {
                            "format": "DD.MM.YYYY",
                            "separator": " - ",
                            "applyLabel": "Tamam",
                            "cancelLabel": "Temizle",
                            "fromLabel": "Başlangıç",
                            "toLabel": "Bitiş",
                            "customRangeLabel": "Custom",
                            "weekLabel": "H",
                            "daysOfWeek": [
                                "Pz",
                                "Pt",
                                "Sa",
                                "Ça",
                                "Pe",
                                "Cu",
                                "Ct"
                            ],
                            "monthNames": [
                                "Ocak",
                                "Şubat",
                                "Mart",
                                "Nisan",
                                "Mayıs",
                                "Haziran",
                                "Temmuz",
                                "Ağustos",
                                "Eylül",
                                "Ekim",
                                "Kasım",
                                "Aralık"
                            ],
                            "firstDay": 1

                        }
                    });

                $('#txtEvalRange').daterangepicker(
                  {
                      locale: {
                          "format": "DD.MM.YYYY",
                          "separator": " - ",
                          "applyLabel": "Tamam",
                          "cancelLabel": "Temizle",
                          "fromLabel": "Başlangıç",
                          "toLabel": "Bitiş",
                          "customRangeLabel": "Custom",
                          "weekLabel": "H",
                          "daysOfWeek": [
                              "Pz",
                              "Pt",
                              "Sa",
                              "Ça",
                              "Pe",
                              "Cu",
                              "Ct"
                          ],
                          "monthNames": [
                              "Ocak",
                              "Şubat",
                              "Mart",
                              "Nisan",
                              "Mayıs",
                              "Haziran",
                              "Temmuz",
                              "Ağustos",
                              "Eylül",
                              "Ekim",
                              "Kasım",
                              "Aralık"
                          ],
                          "firstDay": 1

                      }
                  });
            });
        </script>
    </div>

</asp:Content>
