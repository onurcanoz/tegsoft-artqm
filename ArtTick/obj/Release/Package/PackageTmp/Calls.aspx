﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="Calls.aspx.cs" Inherits="ARTQM.Calls" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="form-horizontal">
<fieldset>


       <div class="modal fade" id="divUpdate" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" runat="server" id="hResult2">Form Seçiniz</h4>
        </div>
        <div class="modal-body" runat="server" id="bResult2">
    <div class="form-group">
  
  <div class="col-sm-12">     
    <div class="form-group">
    
     <label class="col-sm-3 control-label" for="drpTitle">Form</label>
    
  <div class="col-sm-9">
  	<asp:DropDownList runat="server" id="drpForms" ClientIDMode="Static" name="drpTitle" class="form-control input-sm"/>
  
      
  </div>
        </div>
        

    <div class="form-group col-sm-12">
  <label class="col-sm-10 control-label" for="btnContinue"></label>
  <div class="col-sm-2 pull-right">
    <asp:Button id="btnContinue" runat="server" class="btn btn-success" Text="Değerlendir" OnClick="btnContinue_Click" />
  </div>
</div>
        </div>
        </div></div>
      
      </div>
      
    </div>
  </div>
  

<legend>Arama Kriterleri</legend>

   <div id="divScript" runat="server"></div>
    
      <div class="row"> 
          
          <div class="form-group">
    
     <label class="col-sm-1 control-label" for="drpGroup">Operatör</label>
    
  <div class="col-sm-2">
  	<asp:DropDownList runat="server" id="drpGroup" ClientIDMode="Static" name="drpGroup" class="form-control input-sm"/>
  
      
  </div>
  
 
    
     <label class="col-sm-1 control-label" for="dtStart">Görüşme Tarihi</label>
  <div class="col-sm-2">
  	<asp:TextBox runat="server" id="txtRange" ClientIDMode="Static" name="txtRange" placeholder="" class="form-control input-sm"/>
      
  </div>
        <label class="col-sm-1 control-label" for="txtTelefon">Telefon No </label>
  <div class="col-sm-1">
  	<asp:TextBox runat="server" id="txtTelefon" ClientIDMode="Static" name="txtTelefon"  class="form-control input-sm" Text="" placeholder="5XXXXXXXXX"/>
      
  </div>
    <div class="col-sm-1">
  	<asp:DropDownList runat="server" id="drpRandomCall" ClientIDMode="Static" name="drpRandomCall" class="form-control input-sm">
          <asp:ListItem Value="0" Text="Tümü" Selected="True"></asp:ListItem>
          <asp:ListItem Value="1" Text="5"></asp:ListItem>
          <asp:ListItem Value="2" Text="10"></asp:ListItem>
          <asp:ListItem Value="3" Text="100"></asp:ListItem>
          </asp:DropDownList>
  </div>


<%--  <div class="col-sm-1">
  <label class="label label-info">Doğru kayıt eşleşmesi için telefon numarasını 5XXXXXXXXX şeklinde 10 haneli olarak giriniz.</label>
  </div>--%>
       
      
  </div>

</div>
    <div class="row"> 

         <div class="form-group">
      <label class="col-sm-1 control-label" for="drpSkill">Split/Skill</label>
    
  <div class="col-sm-2">
  	<asp:DropDownList runat="server" id="drpSkill" ClientIDMode="Static" name="drpSkill" class="form-control input-sm">
          
          </asp:DropDownList>
  </div>
     <label class="col-sm-1 control-label" for="drpDirection">Yön</label>
    
  <div class="col-sm-2">
  	<asp:DropDownList runat="server" id="drpDirection" ClientIDMode="Static" name="drpDirection" class="form-control input-sm">
          <asp:ListItem Value="0" Text="Tümü" Selected="True"></asp:ListItem>
          <asp:ListItem Value="1" Text="Gelen"></asp:ListItem>
          <asp:ListItem Value="2" Text="Giden"></asp:ListItem>
          <asp:ListItem Value="3" Text="Kampanya"></asp:ListItem>
          </asp:DropDownList>
  </div>
             <label class="col-sm-1 control-label" for="dtStart">Süre (sn) </label>
  <div class="col-sm-1">
  	<asp:TextBox runat="server" id="txtStart" ClientIDMode="Static" name="txtStart" placeholder="" class="form-control input-sm" Text="-1"/>
      
  </div>

  <div class="col-sm-1">
  	<asp:TextBox runat="server" id="txtEnd" ClientIDMode="Static" name="txtEnd" placeholder="" class="form-control input-sm" Text="-1"/>
      
  </div>
             <label class="col-sm-1 control-label" for="drpUsers">&nbsp;</label>
  <div class="col-sm-1">
    <asp:Button id="btnSearch" runat="server" class="btn btn-success" Text="Ara" OnClick="btnSearch_Click" />
  </div>
</div>
</div>
      <br />
   
    <legend>Tüm Çağrılar</legend>
     <div class="form-group" style="margin-left:0px;">    
    <div class="pull-left">
    <asp:LinkButton ID="Button1" runat="server" CssClass="btn btn-success"  OnClick="Button1_Click" data-toggle="tooltip" data-placement="bottom" title="Excel' e Aktar"><i class="fa fa-file-excel-o" aria-hidden="true"></i></asp:LinkButton>
            </div>
         <div class="pull-left"><h3> 
        <span class="label label-danger" id="spanInfo" runat="server"></span></h3></div>
      </div>
      <div class="form-group">
  
  <div class="col-sm-12">
 <asp:GridView ID="grdData" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-responsive table-striped " 
     RowStyle-HorizontalAlign="Center"
     HeaderStyle-HorizontalAlign="Center"
     AllowPaging="True"  PageSize="50" OnRowCommand="grdData_RowCommand" OnPageIndexChanging="grdData_PageIndexChanging">
     <Columns>

           <asp:BoundField HeaderText="ID"  DataField="Callid" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="col-sm-1">
            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>

            <ItemStyle CssClass="col-sm-1"></ItemStyle>
         </asp:BoundField>

         <asp:BoundField HeaderText="Müşteri Temsilcisi"  DataField="Partyagentnames" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="col-sm-2">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
         </asp:BoundField>
          <asp:BoundField HeaderText="Çağrı Kuyruğu / Kampanya"  DataField="Skillset" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="col-sm-2">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="Left"></ItemStyle>
         </asp:BoundField>
          <asp:BoundField HeaderText="Telefon"  DataField="Partynames" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="col-sm-2">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle HorizontalAlign="left"></ItemStyle>
         </asp:BoundField>

            <asp:TemplateField HeaderText="Yön">
             <ItemStyle CssClass="col-sm-1" HorizontalAlign="Center"></ItemStyle>
       
               <ItemTemplate>
                   <%# GetCallDirection(Eval("Direction").ToString()) %>

               </ItemTemplate>
               </asp:TemplateField>
           <asp:BoundField HeaderText="Başlangıç"  DataField="Startdate" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="col-sm-2">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle></ItemStyle>
         </asp:BoundField>
           <asp:TemplateField HeaderText="Süre">
             <ItemStyle CssClass="col-sm-1" HorizontalAlign="Center"></ItemStyle>
       
               <ItemTemplate>
                   <%# Seconds2Span(Eval("Duration").ToString()) %>

               </ItemTemplate>
               </asp:TemplateField>
               
         <asp:TemplateField HeaderText="İşlemler">
             <ItemStyle CssClass="col-sm-1" HorizontalAlign="Left"></ItemStyle>
             <ItemTemplate>
                 <asp:LinkButton  runat="server" ID="btnView" CommandName="details" CommandArgument='<%#Eval("Partyagentnames").ToString()+"?"+Eval("Skillset").ToString()+"?"+Eval("Startdate").ToString()+"?"+Eval("Direction").ToString()+"?"+Eval("Partynames").ToString()+"?"+Eval("Duration").ToString()+"?"+Eval("Callid").ToString() %>'  Text="<span data-toggle='tooltip' title='Çağrı Kartı' ><span class='glyphicon glyphicon-headphones'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
                 <asp:LinkButton  runat="server" ID="btnEval" CommandName="eval" CommandArgument='<%#Eval("Callid").ToString() + "?" + Eval("DONEM").ToString() + "?" + Eval("UID").ToString() + "?" + Eval("Direction").ToString() + "?" + Eval("Duration") + "?" + Eval("Skill").ToString() + "?" + Eval("Partynames").ToString() + "?" + Eval("Startdate").ToString() + "?" + Eval("Skillset") + "?" + Eval("Partyagentnames") %>'  Text="<span data-toggle='tooltip' title='Değerlendir' ><span class='glyphicon glyphicon-check'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
                 <asp:LinkButton  runat="server" ID="btnOK" CommandName="kontrol" Visible='<%# EditEval(Eval("EditEval").ToString())  %>' Text="<span data-toggle='tooltip' title='Değerlendirildi' ><span class='glyphicon glyphicon-ok'></span></span>" ControlStyle-CssClass="btn btn-xs btn-success" />
             </ItemTemplate>
         </asp:TemplateField>

         </Columns>
      </asp:GridView>
  </div>

 
</div>
    </fieldset>
  
    <script>
        $(function () {
            $('#txtRange').daterangepicker(
                {
                    locale: {
                        "format": "DD.MM.YYYY",
                        "separator": " - ",
                        "applyLabel": "Tamam",
                        "cancelLabel": "Temizle",
                        "fromLabel": "Başlangıç",
                        "toLabel": "Bitiş",
                        "customRangeLabel": "Custom",
                        "weekLabel": "H",
                        "daysOfWeek": [
                            "Pz",
                            "Pt",
                            "Sa",
                            "Ça",
                            "Pe",
                            "Cu",
                            "Ct"
                        ],
                        "monthNames": [
                            "Ocak",
                            "Şubat",
                            "Mart",
                            "Nisan",
                            "Mayıs",
                            "Haziran",
                            "Temmuz",
                            "Ağustos",
                            "Eylül",
                            "Ekim",
                            "Kasım",
                            "Aralık"
                        ],
                        "firstDay": 1

                    }
                });

            $('#txtEvalRange').daterangepicker(
              {
                  locale: {
                      "format": "DD.MM.YYYY",
                      "separator": " - ",
                      "applyLabel": "Tamam",
                      "cancelLabel": "Temizle",
                      "fromLabel": "Başlangıç",
                      "toLabel": "Bitiş",
                      "customRangeLabel": "Custom",
                      "weekLabel": "H",
                      "daysOfWeek": [
                          "Pz",
                          "Pt",
                          "Sa",
                          "Ça",
                          "Pe",
                          "Cu",
                          "Ct"
                      ],
                      "monthNames": [
                          "Ocak",
                          "Şubat",
                          "Mart",
                          "Nisan",
                          "Mayıs",
                          "Haziran",
                          "Temmuz",
                          "Ağustos",
                          "Eylül",
                          "Ekim",
                          "Kasım",
                          "Aralık"
                      ],
                      "firstDay": 1

                  }
              });
        });
</script>
      </div>

</asp:Content>
