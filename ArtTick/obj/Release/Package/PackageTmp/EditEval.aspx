﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmptyMaster.Master" AutoEventWireup="true" CodeBehind="EditEval.aspx.cs" Inherits="ARTQM.EditEval" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  <style>

              .row1, .row1 td {

    background-color:#d9534f;
    color:#fefefe;
    border: solid 1px #d43f3a;

}


      ul li {
          font-family:Arial;

      }
        
kriter_baslik tr, td.kriter_baslik, .kriter_baslik, .kriter_baslik td
{
    background-color: #eeeeee;
    font-size: 12px;
    text-align: center;
    color: #363636;
    border-bottom: solid 1px #909090;
    font-weight: bold;
    padding-top: 2px;
    padding-bottom: 2px;
}

.kriter_baslik2 tr, td.kriter_baslik2, .kriter_baslik2, .kriter_baslik2 td
{
    background-color: #eeeeee;
    font-size: 12px;
    text-align: center;
    color: #363636;
    border-bottom: solid 1px #909090;
    font-weight: bold;
    padding-top: 2px;
    padding-bottom: 2px;
}

.kriter_secenek , .kriter_secenek td
{
    text-align: left;
    font-weight: normal;
    text-indent:0px;
    white-space: normal;
    border-bottom: 1px solid #c0c0c0;
    
}
.yorum td
{
    background-color: #eeeeee;
    
    text-align: left;
    font-weight:normal;
    
    border-top: solid 2px #464646;
    border-bottom: solid 5px #464646;
}


 tr.kriter_satir, tr.kriter_satir td, td.kriter_satir
{
    white-space:normal;
    text-indent:0px;
    text-align:left;
	padding-top:2px;
	padding-bottom:2px;
	font-weight: bold;
	background-color:#fefefe;
	border-bottom: 1px solid #c0c0c0;
}

 
#ContentPlaceHolder1_grdSorular th
{
 background-color:#d9534f;
    color:#fefefe;
    border: solid 1px #d43f3a;

}
input[type=checkbox] + label {
  color: #ccc;
  font-style: italic;
} 
input[type=checkbox]:checked + label {
  color: #f00;
  font-style: normal;
} 
.sorualtrow {
    display:none;
}
.kriter_baslik2{
    cursor: pointer;
}
textarea {
    resize: none;
    
}



  </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="panel panel-default">
        <div class="panel-heading">SES KAYITLARI</div>
        <div class="panel-body">

   <div class="row">
<div class="col-sm-6">
    <div id="divPlayer" runat="server" style="background-color:#FFFFFF"><audio controls="controls" runat="server" id="Player">          
                        <source src="" type="audio/x-wav" />
                </audio></div>
          <%--<iframe id="frmPlayer" runat=server  frameborder=0 width=100% height=80 src="Player.aspx"></iframe>--%>
    </div>
         <div class="col-sm-6">
    <asp:GridView ID="grdSegments" runat="server" AutoGenerateColumns="false"
        Width="100%" HeaderStyle-HorizontalAlign="Left" BorderColor="Black" 
        BorderStyle="Solid" BorderWidth="1px" CssClass="table table-bordered table-hover" 
        onselectedindexchanged="GridView1_SelectedIndexChanged">
        <Columns>
            <asp:CommandField SelectText="Dinle" ShowSelectButton="True" />
            <asp:BoundField HeaderText="Tarih" DataField="startedat" DataFormatString="{0:HH\:mm\:ss}" HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundField HeaderText="MT ID" DataField="usercode" HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField HeaderText="Yön"  HeaderStyle-HorizontalAlign="Left">
            <ItemTemplate>
            <%# GetCallDirection(Eval("userfield").ToString()) %>
            </ItemTemplate>
             </asp:TemplateField>
            <asp:BoundField HeaderText="Telefon" DataField="src" HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField HeaderText="Süre"  HeaderStyle-HorizontalAlign="Left">
            <ItemTemplate>
            <%# Seconds2Span(Eval("duration2").ToString()) %>
            </ItemTemplate>
             </asp:TemplateField>

            
        </Columns>
    </asp:GridView>
   
             
             </div>
         <div class="col-sm-12">   <asp:Label ID="lblMsg" runat="server" CssClass="label label-warning"></asp:Label> 
         </div>


      </div>


        </div>
    </div>
  
     <div class="panel panel-default">
        <div class="panel-heading">       <asp:Label ID="lblProje" runat="server" Visible="false"></asp:Label></div>
        <div class="panel-body">
  
    
    <div class="row">

            <asp:GridView ID="grdSorular" runat="server" 
                AutoGenerateColumns="False" 
                ondatabound="grdSorular_DataBound" EnableModelValidation="True" 
                Width="100%" onrowdatabound="grdSorular_RowDataBound" CellPadding="0" CssClass="table table-condensed" OnSelectedIndexChanged="SelectedIndexChanged">               
                
            <Columns>
                
             
                <asp:BoundField HeaderText="Soru" DataField="ETIKET">
                <ItemStyle Width="60%" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Özel Not" ItemStyle-HorizontalAlign="Left">
                 <ItemTemplate>
                <%-- <%# EtiketBul2(Eval("SECENEKLER").ToString(), Eval("DEGER").ToString(), Eval("EVETHAYIR").ToString()) %>--%>
                     <asp:TextBox Rows="2" maxlength= "10" TextMode="MultiLine" runat="server" ID="TxtOzelNot" class="form-control input-sm"  BorderStyle="None" onkeypress="if (this.value.length > 63) { return false; }" Visible='<%# EtiketBul3(Eval("SECENEKLER").ToString(), Eval("DEGER").ToString(), Eval("EVETHAYIR").ToString()) %>' Text= '<%#Eval("OZELNOT").ToString()%>' ></asp:TextBox>
                 </ItemTemplate>
                    <ItemStyle Width="20%" HorizontalAlign="left" VerticalAlign="Bottom" />
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="Düzenlenmiş Cevap" ItemStyle-HorizontalAlign="Left">
                 <ItemTemplate>
                 <asp:TextBox runat="server" id ="txtYANIT" Width="100%" TextMode="SingleLine"  Text='<%# EtiketBul(Eval("SECENEKLER").ToString(), Eval("YANIT").ToString(), Eval("EVETHAYIR").ToString()) %>' CssClass="input-sm"></asp:TextBox>
                 
                 <asp:DropDownList runat="server" id="cbYANIT" Width="99%" CssClass="input-sm"></asp:DropDownList>
                 </ItemTemplate>
                    <ItemStyle HorizontalAlign="left" VerticalAlign="Top"  Width="20%"/>
                </asp:TemplateField>
          
            </Columns>
                
            </asp:GridView>
        </div>

    
     <div class="col-sm-1">
        <label class="col-sm-1 control-label" for="txtNot">NOT</label>        
    </div>
     <div class="col-sm-11">
         <asp:TextBox runat="server" id="txtNot" ClientIDMode="Static" name="txtNot" class="form-control" TextMode="MultiLine" Rows="4" MaxLength="500" onkeypress="if (this.value.length > 499) { return false; }" />      
    </div>



</div>   
         </div>  

         <div class="form-group">    
         <div class="col-sm-5">
         <input type="checkbox" runat="server" id="sifirlama"/>
         <label for="sifirlama">Değerlendirmeyi Sıfırla</label>
             
       

    </div>
    <div class="col-sm-5">
         <input type="checkbox" runat="server" id="mailKontrol"/>
         <label for="sifirlama">Mail Gönder</label>
             
       

    </div>
    <div class="pull-right" style="margin-top:-5px;">

            <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-success"  OnClick="btnSave_Click" data-toggle="tooltip" data-placement="bottom" title="Kaydet">Kaydet</asp:LinkButton>
            </div>
         <div class="pull-left"><h3> 
        <span class="label label-danger" id="spanInfo" runat="server"></span></h3></div>

      </div>

<div id="divScript" runat="server"></div>    
    <script>
        $(document).ready(function () {
            $(".kriter_baslik2").click(function () { $(this).nextUntil(".kriter_baslik2").toggle(); });
        });
    </script>
</asp:Content>
