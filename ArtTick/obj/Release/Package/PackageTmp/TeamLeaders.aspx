﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="TeamLeaders.aspx.cs" Inherits="ARTQM.TeamLeaders" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/i18n/defaults-tr_TR.min.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="form-horizontal">
        <fieldset>
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" runat="server" id="hResult"></h4>
                        </div>
                        <div class="modal-body" runat="server" id="bResult">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divUpdate" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" runat="server" id="hResult2">Yeni Takım Lideri Kartı</h4>
                        </div>
                        <div class="modal-body" runat="server" id="bResult2">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="txtUsercode">Kullanıcı Adı</label>
                                        <div class="col-sm-9">
  	                                        <asp:TextBox runat="server" id="txtUsercode" ClientIDMode="Static" name="txtUsercode" placeholder="Kullanıcı Adı" class="form-control input-sm"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="txtEposta">E-Posta</label>
                                        <div class="col-sm-9">
  	                                        <asp:TextBox runat="server" id="txtEposta" TextMode="Email" ClientIDMode="Static" name="txtEposta" placeholder="E-Posta" class="form-control input-sm"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label class="col-sm-10 control-label" for="btnSave"></label>
                                        <div class="col-sm-2 pull-right" style="margin-right:-36px;">
                                            <asp:Button id="btnSave" runat="server" class="btn btn-success" Text="Kaydet" OnClick="btnSave_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divAddAgent" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" runat="server" id="h1">Müşteri Temsilcisi Ekle</h4>
                        </div>
                        <div class="modal-body" runat="server" id="bResult3">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="TxtUser">Kullanıcı Adı</label>
                                        <div class="col-sm-9">
  	                                        <asp:TextBox runat="server" id="TxtUser" ClientIDMode="Static" name="TxtUser" placeholder="Kullanıcı Adı" class="form-control input-sm"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="SelectAgent">Müşteri Temsilcisi</label>
                                        <div class="col-sm-9">
  	                                        <select id="SelectAgent" class="selectpicker form-control" data-live-search="true" multiple="true" runat="server"></select>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="form-group col-sm-12">
                                        <label class="col-sm-10 control-label" for="BtnAdd"></label>
                                        <div class="col-sm-2 pull-right" style="margin-right:-36px;">
                                            <asp:Button id="BtnAdd" runat="server" class="btn btn-success" Text="Kaydet" OnClick="BtnAdd_Click" />
                                        </div>
                                    </div>
                                    <br /><h4>Tanımlı Müşteri Temsilcileri</h4>
                                    <asp:GridView ID="grdAgent" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-responsive table-striped" RowStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" OnRowCommand="grdData_RowCommand2">
                                        <Columns>
                                            <asp:BoundField HeaderText="Kullanıcı Adı"  DataField="USERCODE" ItemStyle-CssClass="col-sm-1" HeaderStyle-HorizontalAlign="Left">
                                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                <ItemStyle CssClass="col-sm-1" HorizontalAlign="Left"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="İsim"  DataField="NAME" ItemStyle-CssClass="col-sm-1" ItemStyle-HorizontalAlign="Left">
                                                <ItemStyle HorizontalAlign="Left" CssClass="col-sm-1"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="İşlemler" ItemStyle-Width="1%">
                                                <ItemStyle CssClass="col-sm-1" HorizontalAlign="Center"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:LinkButton runat="server" ID="btnDelete2" CommandName="Delete_MT2" CommandArgument='<%#Eval("LOGID").ToString()  %>'  Text="<span data-toggle='tooltip' title='Sil' ><span class='glyphicon glyphicon-floppy-remove'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divAddSkill" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" runat="server" id="h2">Çağrı Kuyruğu Ekle</h4>
                        </div>
                        <div class="modal-body" runat="server" id="Div1">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="TxtUserSkill">Kullanıcı Adı</label>
                                        <div class="col-sm-9">
  	                                        <asp:TextBox runat="server" id="TxtUserSkill" ClientIDMode="Static" name="TxtUserSkill" placeholder="Kullanıcı Adı" class="form-control input-sm"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="SelectSkill">Çağrı Kuyruğu</label>
                                        <div class="col-sm-9">
  	                                        <select id="SelectSkill" class="selectpicker form-control" data-live-search="true" multiple="true" runat="server"></select>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="form-group col-sm-12">
                                        <label class="col-sm-10 control-label" for="BtnAddSkill"></label>
                                        <div class="col-sm-2 pull-right" style="margin-right:-36px;">
                                            <asp:Button id="BtnAddSkill" runat="server" class="btn btn-success" Text="Kaydet" OnClick="BtnAddSkill_Click" />
                                        </div>
                                    </div>
                                    <br /><h4>Tanımlı Çağrı Kuyrukları</h4>
                                    <asp:GridView ID="grdSkill" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-responsive table-striped" RowStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" OnRowCommand="grdData_RowCommand3">
                                        <Columns>
                                            <asp:BoundField HeaderText="Çağrı Kuyruk ID"  DataField="SkillID" ItemStyle-CssClass="col-sm-1" HeaderStyle-HorizontalAlign="Left">
                                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                <ItemStyle CssClass="col-sm-1" HorizontalAlign="Left"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Çağrı Kuyruk Adı"  DataField="Skill" ItemStyle-CssClass="col-sm-1" ItemStyle-HorizontalAlign="Left">
                                                <ItemStyle HorizontalAlign="Left" CssClass="col-sm-1"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="İşlemler" ItemStyle-Width="1%">
                                                <ItemStyle CssClass="col-sm-1" HorizontalAlign="Center"></ItemStyle>
                                                <ItemTemplate>
                                                    <asp:LinkButton runat="server" ID="btnDeleteSkill" CommandName="Delete_MT3" CommandArgument='<%#Eval("SkillID").ToString()  %>'  Text="<span data-toggle='tooltip' title='Sil' ><span class='glyphicon glyphicon-floppy-remove'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divEdit" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" runat="server" id="h3">Takım Lideri Kartı Güncelle</h4>
                        </div>
                        <div class="modal-body" runat="server" id="Div2">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="TxtEditName">İsim</label>
                                        <div class="col-sm-9">
  	                                        <asp:TextBox runat="server" id="TxtEditName" ClientIDMode="Static" name="TxtEditName" placeholder="İsim" class="form-control input-sm"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="TxtEditUsercode">Kullanıcı Adı</label>
                                        <div class="col-sm-9">
  	                                        <asp:TextBox runat="server" id="TxtEditUsercode" ClientIDMode="Static" name="TxtEditUsercode" placeholder="Kullanıcı Adı" class="form-control input-sm"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="TxtEditEposta">E-Posta</label>
                                        <div class="col-sm-9">
  	                                        <asp:TextBox runat="server" id="TxtEditEposta" TextMode="Email" ClientIDMode="Static" name="TxtEditEposta" placeholder="E-Posta" class="form-control input-sm"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label class="col-sm-10 control-label" for="BtnEditSave"></label>
                                        <div class="col-sm-2 pull-right" style="margin-right:-36px;">
                                            <asp:Button id="BtnEditSave" runat="server" class="btn btn-success" Text="Kaydet" OnClick="BtnEditSave_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>

            <div runat="server" id="divResult"></div>

            <legend>Tanımlı Takım Liderleri</legend>
            <div class="form-group col-sm-12">
                <div class="col-sm-3 btn-group" style="margin-left:-15px;">
                    <asp:LinkButton ID="lnkNew" runat="server" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Yeni Takım Lideri"  OnClick="lnkNew_Click"><i class="glyphicon glyphicon-plus" aria-hidden="true"></i> Yeni</asp:LinkButton>
                </div>
                <div class="col-sm-9">
                    <span class="label label-default" id="spanInfo" runat="server"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <br />
                    <asp:GridView ID="grdData" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-responsive table-striped" RowStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" OnRowCommand="grdData_RowCommand">
                        <Columns>
                            <asp:BoundField HeaderText="Sıra"  DataField="Sıra" ItemStyle-CssClass="col-sm-1" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle CssClass="col-sm-1"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="ID"  DataField="LOGID" ItemStyle-CssClass="col-sm-2" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Left" CssClass="col-sm-2"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="İsim"  DataField="NAME" ItemStyle-CssClass="col-sm-2" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Left" CssClass="col-sm-2"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Kullanıcı Adı"  DataField="USERCODE" ItemStyle-CssClass="col-sm-2" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Left" CssClass="col-sm-2"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="E-Posta"  DataField="EPOSTA" ItemStyle-CssClass="col-sm-2" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Left" CssClass="col-sm-2"></ItemStyle>
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="İşlemler">
                                <ItemStyle CssClass="col-sm-1" HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton  runat="server" ID="btnUpdate" CommandName="Refresh_Pwd" CommandArgument='<%#Eval("LOGID").ToString() + ":" + Eval("USERCODE").ToString()  %>'  Text="<span data-toggle='tooltip' title='Güncelle' ><span class='glyphicon glyphicon-refresh'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
                                    <asp:LinkButton  runat="server" ID="btnDelete" CommandName="Delete_TK" CommandArgument='<%#Eval("LOGID").ToString() + ":" + Eval("USERCODE").ToString() + ":" + Eval("INDEX").ToString() %>' Text="<span data-toggle='tooltip' title='Sil' ><span class='glyphicon glyphicon-floppy-remove'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
                                    <asp:LinkButton  runat="server" ID="btnEdit" CommandName="Edit_TK" CommandArgument='<%#Eval("LOGID").ToString() + ":" + Eval("USERCODE").ToString() + ":" + Eval("EPOSTA").ToString() + ":" + Eval("NAME").ToString()  %>'  Text="<span data-toggle='tooltip' title='Değiştir' ><span class='glyphicon glyphicon-edit'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
                                    <asp:LinkButton  runat="server" ID="btnAdd" CommandName="Add_Agent" CommandArgument='<%#Eval("INDEX").ToString() + ":" + Eval("USERCODE").ToString() %>'  Text="<span data-toggle='tooltip' title='MT Ekle' ><span class='glyphicon glyphicon-user'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
                                    <asp:LinkButton  runat="server" ID="btnSkill" CommandName="Add_Skill" CommandArgument='<%#Eval("INDEX").ToString() + ":" + Eval("USERCODE").ToString() %>'  Text="<span data-toggle='tooltip' title='Skill Ekle' ><span class='glyphicon glyphicon-phone-alt'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <RowStyle HorizontalAlign="Center"></RowStyle>
                    </asp:GridView>
                </div>
            </div>

        </fieldset>
    </div>
</asp:Content>
