﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="AgentReport.aspx.cs" Inherits="ARTQM.AgentReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>

 tr.kriter_satir, tr.kriter_satir td, td.kriter_satir
{
    white-space:normal;
    text-indent:0px;
    text-align:left;
	padding-top:2px;
	padding-bottom:2px;
	font-weight: bold;
	background-color:#fe0000;
	border-bottom: 1px solid #c0c0c0;
}

        .row1, .row1 th {

    background-color:#363636;
    color:#f3f3f3;
    border: solid 1px #d43f3a;
    font-size:10px;

}

        .genel_csi, .genel_csi td {
             background-color:#d9534f;
    color:#fefefe;
    border: solid 1px #d43f3a;
        }
        
kriter_baslik tr, td.kriter_baslik, .kriter_baslik, .kriter_baslik td
{
    background-color: #eeeeee;
    font-size: 12px;
    text-align: center;
    color: #363636;
    border-bottom: solid 1px #909090;
    font-weight: bold;
    padding-top: 2px;
    padding-bottom: 2px;
}

.kriter_baslik2 tr, td.kriter_baslik2, .kriter_baslik2, .kriter_baslik2 td
{
    background-color: #eeeeee;
    font-size: 12px;
    text-align: center;
    color: #363636;
    border-bottom: solid 1px #909090;
    font-weight: bold;
    padding-top: 2px;
    padding-bottom: 2px;
}

.kriter_secenek , .kriter_secenek td
{
    text-align: left;
    font-weight: normal;
    text-indent:0px;
    white-space: normal;
    border-bottom: 1px solid #c0c0c0;
    
}
.yorum td
{
    background-color: #eeeeee;
    
    text-align: left;
    font-weight:normal;
    
    border-top: solid 2px #464646;
    border-bottom: solid 5px #464646;
}


 tr.kriter_satir, tr.kriter_satir td, td.kriter_satir
{
    white-space:normal;
    text-indent:0px;
    text-align:left;
	padding-top:2px;
	padding-bottom:2px;
	font-weight: bold;
	background-color:#fefefe;
	border-bottom: 1px solid #c0c0c0;
}

.highlightcol , td.highlightcol , .highlightcol td, tr.kriter_satir .highlightcol{
            background-color: #eeeeee; 
            text-align:center; 
            color:#333333;
            border-bottom: 1px solid #c0c0c0;
        }
tr.kriter_baslik .highlightcol{
            border-bottom: 1px solid #909090;
        }



.yatay_baslik, #ctl00_ContentPlaceHolder1_grdData2 .yatay_baslik, #ctl00_ContentPlaceHolder1_grdData2 .yatay_baslik th,  .yatay_baslik th, #ctl00_ContentPlaceHolder1_grdData2 th.yatay_baslik 
{
	text-align:center;
    padding: 0px 0px 0px 0px; 
    margin: 0px 0px 0px 0px;
   color: #fff;
	/*border-right:solid 1px #a0a0a0;
	border-bottom:solid 1px #a0a0a0;
	 border-top: 4px solid #a0a0a0;border-bottom: 1px ;
	 */
	 border: none 0px;
	 border-right: solid 1px #f0f0f0;
	 border-top: solid 1px #f0f0f0;
    font-family:Tahoma, Geneva, sans-serif;
    font-weight:normal;
    background-color:#d16562;
    font-size: 10px;
	font-weight: bold;
	
}


.gozlemsatir, .gozlemsatir td
{
    font-size: 13px;
    text-align: center;
    color: #363636;
    font-family: verdana;
    border-bottom: solid 2px #333;
    font-weight: bold;
    padding-top: 0px;
    padding-bottom: 0px;
    
    background-color:#ddd;
}
.row1{
    font-size:18px !important;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="form-horizontal">
<fieldset>

<legend>Rapor Kriterleri</legend>

   <div id="divScript" runat="server"></div>
    
      <div class="row"> 
          
          <div class="form-group">
    
     <label class="col-sm-1 control-label" for="drpYIL">Dönem Aralığı</label>
    
  <div class="col-sm-1">
  	<asp:DropDownList runat="server" id="drpYIL" ClientIDMode="Static" name="drpYIL" class="form-control input-sm"/>
  </div>
    
 <div class="col-sm-1">
  <asp:DropDownList runat="server" id="drpAY" ClientIDMode="Static" name="drpAY" class="form-control input-sm"/>
  	
  </div>
              <div class="col-sm-1">
  	<asp:DropDownList runat="server" id="drpYIL2" ClientIDMode="Static" name="drpYIL2" class="form-control input-sm"/>
  </div>
    
 <div class="col-sm-1">
  <asp:DropDownList runat="server" id="drpAY2" ClientIDMode="Static" name="drpAY2" class="form-control input-sm"/>
  	
  </div>
</div>

          <div class="form-group">
               <label class="col-sm-1 control-label" for="drpForm">Değ. Formu</label>
    
  <div class="col-sm-4">
  	<asp:DropDownList runat="server" id="drpForm" ClientIDMode="Static" name="drpForm" class="form-control input-sm">
          </asp:DropDownList>
  
      
  </div>

              <div class="col-sm-1">
    <asp:Button id="btnSearch" runat="server" class="btn btn-success" Text="Ara" OnClick="btnSearch_Click" />
  </div>
          </div>

          <div class="form-group" style="display:none">

                  <label class="col-sm-1 control-label" for="drpSkill">Split/Skill</label>
    
  <div class="col-sm-4">
  	<asp:DropDownList runat="server" id="drpSkill" ClientIDMode="Static" name="drpSkill" class="form-control input-sm">
          
          </asp:DropDownList>
      </div>


          </div>

          <div class="panel-group">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" href="#collapse1">OPERATÖR SEÇİMİ</a>
      </h4>
    </div>
    <div id="collapse1" class="panel-collapse collapse">
      <div class="panel-body">
          <asp:CheckBoxList ID="chkAgents" runat="server" RepeatDirection="Vertical" RepeatColumns="5" RepeatLayout="Table" CssClass="col-sm-12"></asp:CheckBoxList>
      </div>
      <div class="panel-footer">Raporda görüntülemek istediğiniz MT'leri seçiniz. Seçim yapmazsanız tüm MT'ler raporlanacaktır.</div>
    </div>
  </div>
</div>
</div>
      <br />
   
    <legend>Agent Skor Tablosu</legend>
     <div class="form-group" style="margin-left:0px;">    
    <div class="pull-left">
    <asp:LinkButton ID="Button1" runat="server" CssClass="btn btn-success"  OnClick="Button1_Click" data-toggle="tooltip" data-placement="bottom" title="Excel' e Aktar"><i class="fa fa-file-excel-o" aria-hidden="true"></i></asp:LinkButton>
            </div>
         <div class="pull-left"><h3> 
        <span class="label label-danger" id="spanInfo" runat="server"></span></h3></div>
      </div>
      <div class="form-group">
  
  <div class="col-sm-12" style="overflow:auto; height:60vh;">
 <asp:GridView ID="grdData" runat="server" AutoGenerateColumns="True" CssClass="table table-bordered table-responsive table-striped" RowStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderStyle-CssClass="row1" AllowPaging="False">
     <Columns/>
          
      </asp:GridView>
    
  </div>

 
</div>
    </fieldset>
  
    <script>
        $(function () {
            $('#txtRange').daterangepicker(
                {
                    locale: {
                        "format": "DD.MM.YYYY",
                        "separator": " - ",
                        "applyLabel": "Tamam",
                        "cancelLabel": "Temizle",
                        "fromLabel": "Başlangıç",
                        "toLabel": "Bitiş",
                        "customRangeLabel": "Custom",
                        "weekLabel": "H",
                        "daysOfWeek": [
                            "Pz",
                            "Pt",
                            "Sa",
                            "Ça",
                            "Pe",
                            "Cu",
                            "Ct"
                        ],
                        "monthNames": [
                            "Ocak",
                            "Şubat",
                            "Mart",
                            "Nisan",
                            "Mayıs",
                            "Haziran",
                            "Temmuz",
                            "Ağustos",
                            "Eylül",
                            "Ekim",
                            "Kasım",
                            "Aralık"
                        ],
                        "firstDay": 1

                    }
                });

            $('#txtEvalRange').daterangepicker(
              {
                  locale: {
                      "format": "DD.MM.YYYY",
                      "separator": " - ",
                      "applyLabel": "Tamam",
                      "cancelLabel": "Temizle",
                      "fromLabel": "Başlangıç",
                      "toLabel": "Bitiş",
                      "customRangeLabel": "Custom",
                      "weekLabel": "H",
                      "daysOfWeek": [
                          "Pz",
                          "Pt",
                          "Sa",
                          "Ça",
                          "Pe",
                          "Cu",
                          "Ct"
                      ],
                      "monthNames": [
                          "Ocak",
                          "Şubat",
                          "Mart",
                          "Nisan",
                          "Mayıs",
                          "Haziran",
                          "Temmuz",
                          "Ağustos",
                          "Eylül",
                          "Ekim",
                          "Kasım",
                          "Aralık"
                      ],
                      "firstDay": 1

                  }
              });
        });
</script>
      </div>

</asp:Content>
