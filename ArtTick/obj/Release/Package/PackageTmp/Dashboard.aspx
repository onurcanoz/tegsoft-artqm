﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="ARTQM.Dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 	
    <script language="javascript" type="text/javascript" src="flot/jquery.flot.js"></script>
 <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="flot/excanvas.min.js"></script><![endif]-->   
<script type="text/javascript" src="flot/jquery.flot.js"></script>

    <script type="text/javascript" src="flot/jquery.flot.categories.js"></script>
    <script type="text/javascript" src="flot/jquery.flot.symbol.js"></script>
    
    <script type="text/javascript" src="/js/flot/jquery.flot.axislabels.js"></script>
<script type="text/javascript" src="/js/flot/jquery.flot.symbol.js"></script>
	<script>

	    function labelFormatter(label, series) {
	        if (Math.round(series.data[0][1]) == -1)
	            return "<div style='font-size:24px; text-align:center; padding:5px; color:white;'>0</div>";
	        else
	            return "<div style='font-size:24px; text-align:center; padding:5px; color:white;'>" + Math.round(series.data[0][1]) + "</div>";
	    }
        </script>
       <style>
           .flot-x-axis .flot-tick-label {
    white-space: nowrap;
    transform: translate(-9px, 0) rotate(-60deg);
    text-indent: -100%;
    transform-origin: top right;
    text-align: right !important;

}
.placeholder-lg {
	width: 80vw;
	height: 80vh;
	font-size: 18px;
	line-height: 1.2em;
    
}
.walltable { 
    
      border:0px #333333 solid;
 
}
#placeholder {
	width: 80vw;
	height: 60vh;
	font-size: 15px;
	line-height: 1.2em;
      border:0px #ffffff solid;
    /* Shadow */
    /* Background */
   
    color: #333;
   
}
 tr.kriter_satir, tr.kriter_satir td, td.kriter_satir
{
    white-space:normal;
    text-indent:0px;
    text-align:left;
	padding-top:2px;
	padding-bottom:2px;
	font-weight: bold;
	background-color:#fe0000;
	border-bottom: 1px solid #c0c0c0;
}

        .row1, .row1 th {

    background-color:#363636;
    color:#f3f3f3;
    border: solid 1px #d43f3a;
    font-size:10px;

}

        .genel_csi, .genel_csi td {
             background-color:#d9534f;
    color:#fefefe;
    border: solid 1px #d43f3a;
        }
        
kriter_baslik tr, td.kriter_baslik, .kriter_baslik, .kriter_baslik td
{
    background-color: #eeeeee;
    font-size: 12px;
    text-align: center;
    color: #363636;
    border-bottom: solid 1px #909090;
    font-weight: bold;
    padding-top: 2px;
    padding-bottom: 2px;
}

.kriter_baslik2 tr, td.kriter_baslik2, .kriter_baslik2, .kriter_baslik2 td
{
    background-color: #eeeeee;
    font-size: 12px;
    text-align: center;
    color: #363636;
    border-bottom: solid 1px #909090;
    font-weight: bold;
    padding-top: 2px;
    padding-bottom: 2px;
}

.kriter_secenek , .kriter_secenek td
{
    text-align: left;
    font-weight: normal;
    text-indent:0px;
    white-space: normal;
    border-bottom: 1px solid #c0c0c0;
    
}
.yorum td
{
    background-color: #eeeeee;
    
    text-align: left;
    font-weight:normal;
    
    border-top: solid 2px #464646;
    border-bottom: solid 5px #464646;
}


 tr.kriter_satir, tr.kriter_satir td, td.kriter_satir
{
    white-space:normal;
    text-indent:0px;
    text-align:left;
	padding-top:2px;
	padding-bottom:2px;
	font-weight: bold;
	background-color:#fefefe;
	border-bottom: 1px solid #c0c0c0;
}

.highlightcol , td.highlightcol , .highlightcol td, tr.kriter_satir .highlightcol{
            background-color: #eeeeee; 
            text-align:center; 
            color:#333333;
            border-bottom: 1px solid #c0c0c0;
        }
tr.kriter_baslik .highlightcol{
            border-bottom: 1px solid #909090;
        }



.yatay_baslik, #ctl00_ContentPlaceHolder1_grdData2 .yatay_baslik, #ctl00_ContentPlaceHolder1_grdData2 .yatay_baslik th,  .yatay_baslik th, #ctl00_ContentPlaceHolder1_grdData2 th.yatay_baslik 
{
	text-align:center;
    padding: 0px 0px 0px 0px; 
    margin: 0px 0px 0px 0px;
   color: #fff;
	/*border-right:solid 1px #a0a0a0;
	border-bottom:solid 1px #a0a0a0;
	 border-top: 4px solid #a0a0a0;border-bottom: 1px ;
	 */
	 border: none 0px;
	 border-right: solid 1px #f0f0f0;
	 border-top: solid 1px #f0f0f0;
    font-family:Tahoma, Geneva, sans-serif;
    font-weight:normal;
    background-color:#d16562;
    font-size: 10px;
	font-weight: bold;
	
}


.gozlemsatir, .gozlemsatir td
{
    font-size: 13px;
    text-align: center;
    color: #363636;
    font-family: verdana;
    border-bottom: solid 2px #333;
    font-weight: bold;
    padding-top: 0px;
    padding-bottom: 0px;
    
    background-color:#ddd;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
        <div class="form-horizontal">
<fieldset>
   <div id="divScript" runat="server"></div>
    
      <div class="row"> 
          
          <div class="form-group">
    
     <label class="col-sm-1 control-label" for="drpYIL">Dönem Aralığı</label>
    
  <div class="col-sm-1">
  	<asp:DropDownList runat="server" id="drpYIL" ClientIDMode="Static" name="drpYIL" class="form-control input-sm"/>
  </div>
    
 <div class="col-sm-1">
  <asp:DropDownList runat="server" id="drpAY" ClientIDMode="Static" name="drpAY" class="form-control input-sm"/>
  	
  </div>
              <div class="col-sm-1">
  	<asp:DropDownList runat="server" id="drpYIL2" ClientIDMode="Static" name="drpYIL2" class="form-control input-sm"/>
  </div>
    
 <div class="col-sm-1">
  <asp:DropDownList runat="server" id="drpAY2" ClientIDMode="Static" name="drpAY2" class="form-control input-sm"/>
  	
  </div>
</div>

          <div class="form-group">
               <label class="col-sm-1 control-label" for="drpForm">Değ. Formu</label>
    
  <div class="col-sm-6">
  	<asp:DropDownList runat="server" id="drpForm" ClientIDMode="Static" name="drpForm" class="form-control input-sm">
          </asp:DropDownList>
  
      
  </div>

              <div class="col-sm-1">
    <asp:Button id="btnSearch" runat="server" class="btn btn-success" Text="Ara" OnClick="btnSearch_Click" />
  </div>
          </div>
</div>
      <br />
   
    <legend>Agent Skor Tablosu</legend>
     <div id="placeholder" class="placeholder-lg"></div>
    <div id="divScriptLeft" runat="server"> </div>


    
        </fieldset>
  
    <script>
        $(function () {
            $('#txtRange').daterangepicker(
                {
                    locale: {
                        "format": "DD.MM.YYYY",
                        "separator": " - ",
                        "applyLabel": "Tamam",
                        "cancelLabel": "Temizle",
                        "fromLabel": "Başlangıç",
                        "toLabel": "Bitiş",
                        "customRangeLabel": "Custom",
                        "weekLabel": "H",
                        "daysOfWeek": [
                            "Pz",
                            "Pt",
                            "Sa",
                            "Ça",
                            "Pe",
                            "Cu",
                            "Ct"
                        ],
                        "monthNames": [
                            "Ocak",
                            "Şubat",
                            "Mart",
                            "Nisan",
                            "Mayıs",
                            "Haziran",
                            "Temmuz",
                            "Ağustos",
                            "Eylül",
                            "Ekim",
                            "Kasım",
                            "Aralık"
                        ],
                        "firstDay": 1

                    }
                });

            $('#txtEvalRange').daterangepicker(
              {
                  locale: {
                      "format": "DD.MM.YYYY",
                      "separator": " - ",
                      "applyLabel": "Tamam",
                      "cancelLabel": "Temizle",
                      "fromLabel": "Başlangıç",
                      "toLabel": "Bitiş",
                      "customRangeLabel": "Custom",
                      "weekLabel": "H",
                      "daysOfWeek": [
                          "Pz",
                          "Pt",
                          "Sa",
                          "Ça",
                          "Pe",
                          "Cu",
                          "Ct"
                      ],
                      "monthNames": [
                          "Ocak",
                          "Şubat",
                          "Mart",
                          "Nisan",
                          "Mayıs",
                          "Haziran",
                          "Temmuz",
                          "Ağustos",
                          "Eylül",
                          "Ekim",
                          "Kasım",
                          "Aralık"
                      ],
                      "firstDay": 1

                  }
              });
        });
</script>
      </div>

</asp:Content>
