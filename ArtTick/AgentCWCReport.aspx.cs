﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
namespace ARTQM
{
    public partial class AgentCWCReport : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateTime date = DateTime.Today;
                var firstDayOfMonth = new DateTime(date.Year, date.Month, 1);
                var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                txtRange.Text = firstDayOfMonth.ToString("dd.MM.yyyy") + " - " + lastDayOfMonth.ToString("dd.MM.yyyy") ;
                LoadGroups();
                
                btnSearch_Click(null, null);
            }
            
        }
        public void LoadGroups()
        {
            Session["CWCKEYS"] = Lists.GetCWCKeys();

            drpGroup.DataSource = (DataTable)Session["CWCKEYS"];
            drpGroup.DataTextField = "ReportKey";
            drpGroup.DataValueField = "ReportCodes";
            drpGroup.DataBind();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {

            DataTable dt = Reports.GetAgentCWCReport(txtRange.Text.Split('-')[0].Trim() + " 00:00:00", txtRange.Text.Split('-')[1].Trim() + " 23:59:59", drpGroup.SelectedValue);
            DataTable report = new DataTable();
            report.Columns.Add("AGENT");
            report.Columns["AGENT"].Caption = "Operatör";
            DataView vi = new DataView(dt);
            DataTable states = vi.ToTable(true, new string[] { "CWCNAME" });
            DataTable depts = vi.ToTable(true, new string[] { "AGENTID", "AGENTNAME" });
            vi = new DataView(states);
            vi.Sort="CWCNAME";
            states = vi.ToTable();
            foreach (DataRow state in states.Rows)
            {
                report.Columns.Add(state[0].ToString());
                report.Columns[state[0].ToString()].Caption = state[0].ToString();
            }

            report.Columns.Add("TOPLAM");

            int final_total = 0;
            DataRow totalrow = report.NewRow();
            totalrow[0] = "TOPLAM";
            foreach (DataRow dr in depts.Rows)
            {
                DataRow nrow = report.NewRow();
                nrow["AGENT"] = dr["AGENTID"].ToString() +" " + dr["AGENTNAME"].ToString(); ;
                int depttotal = 0;
                foreach (DataColumn dc in report.Columns)
                {
                    if (totalrow[dc.ColumnName] == null || totalrow[dc.ColumnName].ToString() == "") 
                        totalrow[dc.ColumnName] = "0";
                    if (dc.ColumnName == "AGENT") continue;
                    if (dc.ColumnName == "TOPLAM") continue;
                    try
                    {
                        nrow[dc.ColumnName] = dt.Select("AGENTID='" + dr["AGENTID"].ToString() + "' and CWCNAME='" + dc.ColumnName + "'")[0]["TOPLAM"].ToString();
                        depttotal += int.Parse(nrow[dc.ColumnName].ToString());
                        final_total+= int.Parse(nrow[dc.ColumnName].ToString());

                        totalrow[dc.ColumnName] = int.Parse(totalrow[dc.ColumnName].ToString()) + int.Parse(nrow[dc.ColumnName].ToString());
                    }
                    catch (Exception ex)
                    {
                        nrow[dc.ColumnName] = "0";
                    }
                }
                nrow["TOPLAM"] = depttotal;
                report.Rows.Add(nrow);

            }

            totalrow["TOPLAM"] = final_total;
            report.Rows.Add(totalrow);

            grdData.DataSource = report;
            grdData.DataBind();
            grdData.Rows[grdData.Rows.Count - 1].CssClass = "table-active";
           
        
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            ExportToExcel(grdData, "AGENTCWCRAPORU_" + txtRange.Text.Replace(" ", "").Replace("-", "_").Replace(".", "_")+"_"+ drpGroup.SelectedItem.Text.Replace(" ","_").Replace("-","_"));
        }
       

       
    }
}