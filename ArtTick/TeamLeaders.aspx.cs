﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ARTQM
{
    public partial class TeamLeaders : PageBase
    {
        DataProvider obj = new DataProvider();
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            string yonetim = Session["YONETIM"].ToString();

            if (yonetim.Equals("0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                        "alert('Yetkiniz Yok'); window.location='Login.aspx';", true);
                return;
            }

            if (!IsPostBack)
            {
                LoadData();
            }
        }
        protected void LoadData()
        {
            try
            {
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM TEAMLEADERS ORDER BY NAME DESC", conn);
                DataTable TotalTL = new DataTable();
                da.Fill(TotalTL);
                TotalTL.Columns.Add("Sıra");
                int index = 1;
                foreach (DataRow dr in TotalTL.Rows)
                {
                    dr["Sıra"] = index;
                    index++;
                }
                grdData.DataSource = TotalTL;
                grdData.DataBind();

                conn.Close();

                spanInfo.InnerHtml = "Toplam " + grdData.Rows.Count.ToString() + " kayıt bulundu.";
            }
            catch (Exception Ex)
            {
                hResult.InnerHtml = "Hata Oluştu!";
                bResult.InnerHtml = "Sistemsel bir hata oluşmuştur. Lütfen bağlantılarınızı kontrol edeniz.";
                divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                conn.Close();
            }
        }
        protected void grdData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Refresh_Pwd":
                    try
                    {
                        string[] id = e.CommandArgument.ToString().Trim().Split(':');
                        DataTable AgentInfo = new DataTable();
                        AgentInfo = obj.GetAgentData("SELECT UID, USERNAME, SURNAME, USERCODE, PASS FROM TBLUSERS WHERE UID = '" + id[0] + "'");
                        if (AgentInfo.Rows.Count == 1)
                        {
                            SqlCommand cmd = new SqlCommand("UPDATE TEAMLEADERS SET NAME = '" + AgentInfo.Rows[0]["Name"] + "', USERCODE = '" + AgentInfo.Rows[0]["Usercode"] + "' WHERE LOGID = '" + id[0] + "'", conn);
                            conn.Open();
                            cmd.ExecuteNonQuery();

                            cmd = new SqlCommand("UPDATE KULLANICILAR SET KULLANICIKOD = '" + AgentInfo.Rows[0]["Usercode"] + "', SIFRE = '" + HashPassword(AgentInfo.Rows[0]["Pass"].ToString().Trim()) + "' WHERE KULLANICIKOD = '" + id[1] + "'", conn);
                            cmd.ExecuteNonQuery();

                            conn.Close();

                            Response.Redirect("TeamLeaders.aspx", false);
                        }
                        else if (AgentInfo.Rows.Count == 0)
                        {
                            hResult.InnerHtml = "Hata Oluştu!";
                            bResult.InnerHtml = "Belirtilen kullanıcı adına ait müşteri temsilcisi bulunamamıştır.";
                            divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                        }
                    }
                    catch(Exception Ex)
                    {
                        hResult.InnerHtml = "Hata Oluştu!";
                        bResult.InnerHtml = "Sistemsel bir hata oluştu. Lütfen bağlantınızı kontrol ediniz.";
                        divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                    }
                    break;
                case "Delete_TK":
                    try
                    {
                        string[] Params = e.CommandArgument.ToString().Trim().Split(':');

                        conn.Open();
                        SqlCommand cmd = new SqlCommand("DELETE FROM TEAMLEADERS WHERE LOGID = '"+Params[0]+"'", conn);
                        cmd.ExecuteNonQuery();

                        cmd = new SqlCommand("DELETE FROM KULLANICILAR WHERE KULLANICIKOD = '" + Params[1] + "'", conn);
                        cmd.ExecuteNonQuery();

                        cmd = new SqlCommand("DELETE FROM KULLANICISEVIYE WHERE SEVIYEKOD = '" + Params[2] + "'", conn);
                        cmd.ExecuteNonQuery();

                        conn.Close();

                        Response.Redirect("TeamLeaders.aspx", false);
                    }
                    catch(Exception Ex)
                    {
                        hResult.InnerHtml = "Hata Oluştu!";
                        bResult.InnerHtml = "Sistemsel bir hata oluştu. Lütfen bağlantınızı kontrol ediniz.";
                        divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                    }
                    break;
                case "Edit_TK":
                    try
                    {
                        string[] Params = e.CommandArgument.ToString().Trim().Split(':');
                        TxtEditUsercode.Text = Params[1];
                        TxtEditUsercode.Attributes.Add("readonly", "readonly");
                        TxtEditEposta.Text = Params[2];
                        TxtEditName.Text = Params[3];
                        TxtEditName.Attributes.Add("readonly", "readonly");
                        divResult.InnerHtml = "<script>$('#divEdit').modal(); </script>";
                    }
                    catch(Exception Ex)
                    {
                        hResult.InnerHtml = "Hata Oluştu!";
                        bResult.InnerHtml = "Sistemsel bir hata oluştu. Lütfen bağlantınızı kontrol ediniz.";
                        divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                    }
                    break;
                case "Add_Agent":
                    try
                    {
                        string[] Params = e.CommandArgument.ToString().Split(':');
                        TxtUser.Text = Params[1];
                        TxtUser.Attributes.Add("readonly", "readonly");
                        SelectAgent.Items.Clear();
                        SelectAgent.Items.AddRange(AgentList());
                        Session["SEVIYEKOD5"] = Params[0];
                        GetAgentData();
                        divResult.InnerHtml = "<script>$('#divAddAgent').modal(); </script>";
                    }
                    catch(Exception Ex)
                    {
                        hResult.InnerHtml = "Hata Oluştu!";
                        bResult.InnerHtml = "Sistemsel bir hata oluştu. Lütfen bağlantınızı kontrol ediniz.";
                        divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                    }
                    break;
                case "Add_Skill":
                    try
                    {
                        string[] Params = e.CommandArgument.ToString().Split(':');
                        TxtUserSkill.Text = Params[1];
                        TxtUserSkill.Attributes.Add("readonly", "readonly");
                        SelectSkill.Items.Clear();
                        SelectSkill.Items.AddRange(SkillList());
                        Session["SEVIYEKOD5"] = Params[0];
                        GetSkillData();
                        divResult.InnerHtml = "<script>$('#divAddSkill').modal(); </script>";
                    }
                    catch(Exception Ex)
                    {

                    }
                    break;
            }

        }
        protected void grdData_RowCommand2(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Delete_MT2":
                    try
                    {
                        string id = e.CommandArgument.ToString().Trim();
                        string userCode = Session["SEVIYEKOD5"].ToString().Trim();
                        SqlCommand cmd = new SqlCommand("SELECT AGENTS FROM KULLANICISEVIYE WHERE SEVIYEKOD = '" + userCode + "'", conn);
                        conn.Open();
                        string Result = cmd.ExecuteScalar().ToString().Trim();
                        string[] splitResult = Result.Split(',');
                        DataTable dt = new DataTable();
                        dt.Columns.Add("AGENT");
                        int count = splitResult.Length;
                        for(int i = 0; i < count; i++)
                        {
                            dt.Rows.Add(splitResult[i]);

                        }
                        dt.AcceptChanges();
                        foreach (DataRow dr in dt.Rows)
                        {
                            if(dr["AGENT"].ToString() == id)
                            {
                                dr.Delete();
                            } 
                        }
                        dt.AcceptChanges();
                        Result = string.Empty;
                        foreach(DataRow dr in dt.Rows)
                        {
                            Result += dr["AGENT"] + ",";
                        }
                        if(Result != string.Empty)
                        {
                            Result = Result.Remove(Result.Length - 1, 1);
                        }
                        cmd = new SqlCommand("UPDATE KULLANICISEVIYE SET AGENTS = '" + Result.Trim() + "' WHERE SEVIYEKOD = '" + userCode + "'", conn);
                        cmd.ExecuteNonQuery();

                        hResult.InnerHtml = "Başarılı İşlem!";
                        bResult.InnerHtml = "Belirtilen müşteri temsilcisi çıkartılmıştır. Seçilen takım lideri çıkış giriş yaptıktan sonra güncellenecektir.";
                        divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                    }
                    catch(Exception Ex)
                    {
                        hResult.InnerHtml = "Hata Oluştu!";
                        bResult.InnerHtml = "Sistemsel bir hata oluştu. Lütfen bağlantınızı kontrol ediniz.";
                        divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                    }
                    break;
            }
        }
        protected void grdData_RowCommand3(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Delete_MT3":
                    try
                    {
                        string id = e.CommandArgument.ToString().Trim();
                        string userCode = Session["SEVIYEKOD5"].ToString().Trim();
                        SqlCommand cmd = new SqlCommand("SELECT SKILLS FROM KULLANICISEVIYE WHERE SEVIYEKOD = '" + userCode + "'", conn);
                        conn.Open();
                        string Result = cmd.ExecuteScalar().ToString().Trim();
                        string[] splitResult = Result.Split(',');
                        DataTable dt = new DataTable();
                        dt.Columns.Add("SKILL");
                        int count = splitResult.Length;
                        for (int i = 0; i < count; i++)
                        {
                            dt.Rows.Add(splitResult[i]);

                        }
                        dt.AcceptChanges();
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (dr["SKILL"].ToString() == id)
                            {
                                dr.Delete();
                            }
                        }
                        dt.AcceptChanges();
                        Result = string.Empty;
                        foreach (DataRow dr in dt.Rows)
                        {
                            Result += dr["SKILL"] + ",";
                        }
                        if(Result != string.Empty)
                        {
                            Result = Result.Remove(Result.Length - 1, 1);
                        }
                        cmd = new SqlCommand("UPDATE KULLANICISEVIYE SET SKILLS = '" + Result.Trim() + "' WHERE SEVIYEKOD = '" + userCode + "'", conn);
                        cmd.ExecuteNonQuery();

                        hResult.InnerHtml = "Başarılı İşlem!";
                        bResult.InnerHtml = "Belirtilen çağrı kuyruğu çıkartılmıştır. Seçilen takım lideri çıkış giriş yaptıktan sonra güncellenecektir.";
                        divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                    }
                    catch (Exception Ex)
                    {
                        hResult.InnerHtml = "Hata Oluştu!";
                        bResult.InnerHtml = "Sistemsel bir hata oluştu. Lütfen bağlantınızı kontrol ediniz.";
                        divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                    }
                    break;
            }
        }
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            string Agents = string.Empty;

            var AgentList = SelectAgent.Items.Cast<ListItem>()
            .Where(item => item.Selected)
            .Select(item => item.Value)
            .ToList();

            if (AgentList.Count > 0)
            {
                foreach (var item in AgentList)
                    Agents += item.Trim() + ",";
                Agents = Agents.Remove(Agents.Length - 1, 1);
            }
            else
                Agents = string.Empty;

            if(Agents != string.Empty)
            {
                SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM KULLANICISEVIYE WHERE SEVIYEKOD = '" + Session["SEVIYEKOD5"].ToString().Trim() + "'", conn);
                conn.Open();
                int Result = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                if(Result == 1)
                {
                    cmd = new SqlCommand("SELECT AGENTS FROM KULLANICISEVIYE WHERE SEVIYEKOD = '" + Session["SEVIYEKOD5"].ToString().Trim() + "'", conn);
                    string UID = cmd.ExecuteScalar().ToString();
                    if(UID != string.Empty)
                    {
                        Agents = Agents + "," + UID;
                    }
                    cmd = new SqlCommand("UPDATE KULLANICISEVIYE SET AGENTS = '" + Agents + "' WHERE SEVIYEKOD = '" + Session["SEVIYEKOD5"].ToString().Trim() + "'", conn);
                    cmd.ExecuteScalar();

                    hResult.InnerHtml = "Başarılı İşlem!";
                    bResult.InnerHtml = "Belirtilen müşteri temsilcisi eklenmiştir. Seçilen takım lideri çıkış giriş yaptıktan sonra güncellenecektir.";
                    divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                }
                else if(Result == 0)
                {
                    cmd = new SqlCommand("INSERT INTO KULLANICISEVIYE(SEVIYEKOD, SEVIYEAD, AGENTS, PREFIXES) VALUES ('" + Session["SEVIYEKOD5"].ToString().Trim() + "', 'AGENTS', '" + Agents + "', '0')", conn);
                    cmd.ExecuteNonQuery();

                    hResult.InnerHtml = "Başarılı İşlem!";
                    bResult.InnerHtml = "Belirtilen müşteri temsilcisi eklenmiştir. Seçilen takım lideri çıkış giriş yaptıktan sonra güncellenecektir.";
                    divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                }
                else
                {
                    hResult.InnerHtml = "Hata Oluştu!";
                    bResult.InnerHtml = "Sistemsel bir hata oluşmuştur. Lütfen bağlantılarınızı kontrol edeniz.";
                    divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                    conn.Close();
                }
            }
        }
        protected void BtnAddSkill_Click(object sender, EventArgs e)
        {
            string Skills = string.Empty;

            var SkillList = SelectSkill.Items.Cast<ListItem>()
            .Where(item => item.Selected)
            .Select(item => item.Value)
            .ToList();

            if (SkillList.Count > 0)
            {
                foreach (var item in SkillList)
                    Skills += item.Trim() + ",";
                Skills = Skills.Remove(Skills.Length - 1, 1);
            }
            else
                Skills = string.Empty;

            if (Skills != string.Empty)
            {
                SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM KULLANICISEVIYE WHERE SEVIYEKOD = '" + Session["SEVIYEKOD5"].ToString().Trim() + "'", conn);
                conn.Open();
                int Result = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                if (Result == 1)
                {
                    cmd = new SqlCommand("SELECT SKILLS FROM KULLANICISEVIYE WHERE SEVIYEKOD = '" + Session["SEVIYEKOD5"].ToString().Trim() + "'", conn);
                    string SkillName = cmd.ExecuteScalar().ToString();
                    if(SkillName != string.Empty)
                    {
                        Skills = Skills + "," + SkillName;
                    }
                    cmd = new SqlCommand("UPDATE KULLANICISEVIYE SET SKILLS = '" + Skills + "' WHERE SEVIYEKOD = '" + Session["SEVIYEKOD5"].ToString().Trim() + "'", conn);
                    cmd.ExecuteScalar();

                    hResult.InnerHtml = "Başarılı İşlem!";
                    bResult.InnerHtml = "Belirtilen çağrı kuyruğu eklenmiştir. Seçilen takım lideri çıkış giriş yaptıktan sonra güncellenecektir.";
                    divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                }
                else if (Result == 0)
                {
                    cmd = new SqlCommand("INSERT INTO KULLANICISEVIYE(SEVIYEKOD, SEVIYEAD, SKILLS, PREFIXES) VALUES ('" + Session["SEVIYEKOD5"].ToString().Trim() + "', 'AGENTS', '" + Skills + "', '0')", conn);
                    cmd.ExecuteNonQuery();

                    hResult.InnerHtml = "Başarılı İşlem!";
                    bResult.InnerHtml = "Belirtilen çağrı kuyruğu eklenmiştir. Seçilen takım lideri çıkış giriş yaptıktan sonra güncellenecektir.";
                    divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                }
                else
                {
                    hResult.InnerHtml = "Hata Oluştu!";
                    bResult.InnerHtml = "Sistemsel bir hata oluşmuştur. Lütfen bağlantılarınızı kontrol edeniz.";
                    divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                    conn.Close();
                }
            }
        }
        protected void BtnEditSave_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("UPDATE TEAMLEADERS SET EPOSTA = '" + TxtEditEposta.Text.Trim() + "' WHERE USERCODE = '" + TxtEditUsercode.Text.Trim() + "'", conn);
                int Result = cmd.ExecuteNonQuery();
                if (Result == 1)
                {
                    Response.Redirect("TeamLeaders.aspx");
                }
            }
            catch (Exception Ex)
            {
                hResult.InnerHtml = "Hata Oluştu!";
                bResult.InnerHtml = "Sistemsel bir hata oluşmuştur. Lütfen bağlantılarınızı kontrol edeniz.";
                divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                conn.Close();
            }
        }
        public ListItem[] AgentList()
        {
            List<ListItem> items = new List<ListItem>();
            SqlDataAdapter da = new SqlDataAdapter("SELECT LOGID, NAME, USERCODE FROM AGENTS", conn);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
                foreach (DataRow dr in dt.Rows)
                {
                    items.Add(new ListItem { Text = "(" + dr["USERCODE"].ToString().Trim() + ") " + dr["NAME"].ToString(), Value = dr["LOGID"].ToString() });
                }
            return items.ToArray();
        }
        public ListItem[] SkillList()
        {
            List<ListItem> items = new List<ListItem>();
            DataTable dt = new DataTable();
            dt = (DataTable)Session["SKILLS"];
            if (dt.Rows.Count > 0)
                foreach (DataRow dr in dt.Rows)
                {
                    items.Add(new ListItem { Text = "(" + dr["SkillID"].ToString().Trim() + ") " + dr["Skill"].ToString(), Value = dr["SkillID"].ToString() });
                }
            return items.ToArray();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                conn.Open();
                DataTable TeamLeaders = new DataTable();
                TeamLeaders = obj.GetAgentData("SELECT UID, USERNAME, SURNAME, USERCODE, PASS FROM TBLUSERS WHERE USERCODE = '" + txtUsercode.Text.Trim() + "'");
                if(TeamLeaders.Rows.Count == 1)
                {
                    TeamLeaders.Rows[0]["Eposta"] = txtEposta.Text.Trim();
                    SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM TEAMLEADERS WHERE LOGID = '" + TeamLeaders.Rows[0]["UID"] + "'", conn);
                    int Result = Convert.ToInt32(cmd.ExecuteScalar().ToString());
                    if(Result == 0)
                    {
                        cmd = new SqlCommand("INSERT INTO TEAMLEADERS(LOGID, NAME, USERCODE, EPOSTA) VALUES(@LOGID, @NAME, @USERCODE, @EPOSTA)", conn);
                        cmd.Parameters.AddWithValue("@LOGID", TeamLeaders.Rows[0]["UID"]);
                        cmd.Parameters.AddWithValue("@NAME", TeamLeaders.Rows[0]["Name"]);
                        cmd.Parameters.AddWithValue("@USERCODE", TeamLeaders.Rows[0]["Usercode"]);
                        cmd.Parameters.AddWithValue("@EPOSTA", TeamLeaders.Rows[0]["Eposta"]);
                        cmd.ExecuteNonQuery();

                        cmd = new SqlCommand("SELECT [INDEX] FROM TEAMLEADERS WHERE LOGID = '" + TeamLeaders.Rows[0]["UID"] + "'", conn);
                        string Index = cmd.ExecuteScalar().ToString();

                        if (!string.IsNullOrEmpty(Index))
                        {
                            cmd = new SqlCommand("INSERT INTO KULLANICILAR VALUES(@KULLANICIKOD, @SIFRE, 'TL', 'TL', '" + Convert.ToInt32(Index) + "', '5', '1', '1')", conn);
                            cmd.Parameters.AddWithValue("@KULLANICIKOD", TeamLeaders.Rows[0]["Usercode"]);
                            cmd.Parameters.AddWithValue("@SIFRE", HashPassword(TeamLeaders.Rows[0]["Pass"].ToString()));
                            cmd.ExecuteNonQuery();
                        }
                        else
                        {
                            hResult.InnerHtml = "Hata Oluştu!";
                            bResult.InnerHtml = "Sistemsel bir hata oluştu. Lütfen bağlantınızı kontrol ediniz.";
                            divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                        }
                        conn.Close();

                        Response.Redirect("TeamLeaders.aspx", false);
                    }
                    else
                    {
                        hResult.InnerHtml = "Hata Oluştu!";
                        bResult.InnerHtml = "Aynı kullanıcı adlı bir takım lideri sistemde bulunmaktadır.";
                        divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                        conn.Close();
                    }
                }
                else if (TeamLeaders.Rows.Count == 0)
                {
                    hResult.InnerHtml = "Hata Oluştu!";
                    bResult.InnerHtml = "Belirtilen kullanıcı adına ait takım lideri bulunamamıştır.";
                    divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                    conn.Close();
                }
                else
                {
                    hResult.InnerHtml = "Hata Oluştu!";
                    bResult.InnerHtml = "Belirtilen kullanıcı adına ait Tegsoft'da birden fazla takım lideri bulunmaktadır.";
                    divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                    conn.Close();  
                }
            }
            catch(Exception Ex)
            {
                hResult.InnerHtml = "Hata Oluştu!";
                bResult.InnerHtml = "Sistemsel bir hata oluşmuştur. Lütfen bağlantılarınızı kontrol edeniz.";
                divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                conn.Close();
            }

        }
        protected void lnkNew_Click(object sender, EventArgs e)
        {
            try
            {
                txtUsercode.Text = "";
                divResult.InnerHtml = "<script>$('#divUpdate').modal(); </script>";
            }
            catch
            {
            }
        }
        protected void GetAgentData()
        {
            try
            {
                DataTable dt = new DataTable();
                string userLevel = Session["SEVIYEKOD5"].ToString().Trim();
                SqlCommand cmd = new SqlCommand("SELECT AGENTS FROM KULLANICISEVIYE WHERE SEVIYEKOD = '" + userLevel + "'", conn);
                conn.Open();
                string AgentList = string.Empty;
                try
                {
                    AgentList = cmd.ExecuteScalar().ToString();
                }
                catch(Exception Ex) { }
                

                string[] SplitList = AgentList.Split(',');

                string whereClause = string.Empty;
                foreach(string s in SplitList)
                {
                    whereClause += "'" + s + "'" + ",";
                }
                whereClause = whereClause.Remove(whereClause.Length - 1, 1);

                SqlDataAdapter da = new SqlDataAdapter("SELECT LOGID, USERCODE, NAME FROM AGENTS WHERE LOGID IN(" + whereClause + ") ", conn);
                da.Fill(dt);
                conn.Close();
                grdAgent.DataSource = dt;
                grdAgent.DataBind();


            }
            catch(Exception Ex)
            {
                hResult.InnerHtml = "Hata Oluştu!";
                bResult.InnerHtml = "Sistemsel bir hata oluşmuştur. Lütfen bağlantılarınızı kontrol edeniz.";
                divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
            }
        }
        protected void GetSkillData()
        {
            try
            {
                DataTable dt = new DataTable();
                string userLevel = Session["SEVIYEKOD5"].ToString().Trim();
                SqlCommand cmd = new SqlCommand("SELECT SKILLS FROM KULLANICISEVIYE WHERE SEVIYEKOD = '" + userLevel + "'", conn);
                conn.Open();
                string SkillList = string.Empty;
                try
                {
                    SkillList = cmd.ExecuteScalar().ToString();
                }
                catch(Exception Ex) { }
                
                if (SkillList != string.Empty)
                {


                    string[] SplitList = SkillList.Split(',');

                    string whereClause = string.Empty;
                    foreach (string s in SplitList)
                    {
                        whereClause += "'" + s + "'" + ",";
                    }
                    whereClause = whereClause.Remove(whereClause.Length - 1, 1);

                    dt = obj.GetList("SELECT SKILL AS SkillID, NAME AS Skill FROM TBLCCSKILLS WHERE SKILL IN(" + whereClause + ")", "SKILLID", "SKILL", "SkillID", "Skill");
                    conn.Close();
                    grdSkill.DataSource = dt;
                    grdSkill.DataBind();
                }
                else
                {
                    grdSkill.DataSource = dt;
                    grdSkill.DataBind();
                }
            }
            catch (Exception Ex)
            {
                hResult.InnerHtml = "Hata Oluştu!";
                bResult.InnerHtml = "Sistemsel bir hata oluşmuştur. Lütfen bağlantılarınızı kontrol edeniz.";
                divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
            }
        }
        public static string HashPassword(string password)
        {
            var bytes = new UTF8Encoding().GetBytes(password);
            var hashBytes = System.Security.Cryptography.MD5.Create().ComputeHash(bytes);
            return Convert.ToBase64String(hashBytes);
        }
    }
}