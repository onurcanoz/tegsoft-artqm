﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ARTQM
{
    public partial class Download : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["FILENAME"] != null)
            {
                Response.Clear();
                Response.AddHeader("content-disposition", "attachment;filename=" + Session["FILENAME"].ToString().Replace("temp/", ""));

                if (Session["FILENAME"].ToString().Contains(".xls"))
                    Response.ContentType = "application/ms-excel";
                if (Request["dir"] == null)
                    Response.WriteFile(Server.MapPath("~/" + Session["FILENAME"].ToString()));
                else
                    Response.WriteFile(Server.MapPath(Session["FILENAME"].ToString()));

                Response.End();
            }
        
        }
    }
}