﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmptyMaster.Master" AutoEventWireup="true" CodeBehind="CallDetail.aspx.cs" Inherits="ARTQM.CallDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#home">GENEL</a></li>
  <li><a data-toggle="tab" href="#menu1">NOTLAR</a></li>
  <li><a data-toggle="tab" href="#menu2">İŞLEM GEÇMİŞİ</a></li>
</ul>

<div class="tab-content">
    <br />
  <div id="home" class="tab-pane fade in active">
   
      
          <h3>ÇAĞRI DETAYLARI</h3><br />
        <div class="row"> 
          
          <div class="form-group">
    
     <label class="col-sm-2 control-label" for="lblAgent">Operatör</label>
    
  <div class="col-sm-10">
  	<asp:Label runat="server" id="lblAgent" ClientIDMode="Static" name="lblAgent" class="input-sm"/>
  
      
  </div>
              </div>
         </div>
        <div class="row"> 
          
          <div class="form-group">
    
     <label class="col-sm-2 control-label" for="lblGroup">Çağrı Kuyruğu</label>
    
  <div class="col-sm-10">
  	<asp:Label runat="server" id="lblGroup" ClientIDMode="Static" name="lblGroup" class="input-sm"/>
  
      
  </div>
              </div>
         </div>


       <div class="row"> 
          
          <div class="form-group">
    
     <label class="col-sm-2 control-label" for="lblGroup">Başlangıç</label>
    
  <div class="col-sm-4">
  	<asp:Label runat="server" id="lblStartDate" ClientIDMode="Static" name="lblGroup" class="input-sm"/>
  
      
  </div>


              </div>
         </div>
      <div class="row">
          <div class="form-group">
                             <label class="col-sm-2 control-label" for="lblDuration">Süre</label>
    
  <div class="col-sm-4">
  	<asp:Label runat="server" id="lblDuration" ClientIDMode="Static" name="lblDuration" class="input-sm"/>
  
      
  </div>
          </div>
      </div>
      <div class="row">
          <div class="form-group">
                <label class="col-sm-2 control-label" for="lblDirection">Yön</label>
    
  <div class="col-sm-4">
  	<asp:Label runat="server" id="lblDirection" ClientIDMode="Static" name="lblDirection" class="input-sm"/>
  
      
  </div>
          </div>
      </div>
       <div class="row"> 
          
          <div class="form-group">
    
     <label class="col-sm-2 control-label" for="lblParties">Katılımcılar</label>
    
  <div class="col-sm-10">
  	<asp:Label runat="server" id="lblParties" ClientIDMode="Static" name="lblParties" class="input-sm"/>
  
      
  </div>

              </div>
         </div>
<div><br /></div>
          <div id="divPlayer" runat="server" style="background-color:#FFFFFF"><audio controls="controls" runat="server" id="Player">          
                        <source src="" type="audio/mp3" />
                </audio></div>
      <br />
      <div class="row" style="overflow:auto; height:45vh;">
          <%--<iframe id="frmPlayer" runat=server  frameborder=0 width=100% height=80 src="Player.aspx"></iframe>--%>
    <asp:GridView ID="grdSegments" runat="server" AutoGenerateColumns="false" 
        Width="100%" HeaderStyle-HorizontalAlign="Left" BorderColor="Black" 
        BorderStyle="Solid" BorderWidth="1px" CssClass="table table-bordered table-hover"
        onselectedindexchanged="GridView1_SelectedIndexChanged">
        <Columns>
            <asp:CommandField SelectText="Dinle" ShowSelectButton="True" />
            <asp:BoundField HeaderText="Ses ID" DataField="inum"  HeaderStyle-HorizontalAlign="Center"/>
            <asp:BoundField HeaderText="Başlangıç" DataField="startedat"  DataFormatString="{0:HH\:mm\:ss}" HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField HeaderText="Süre"  HeaderStyle-HorizontalAlign="Left">
            <ItemTemplate>
            <%# Seconds2Span(Eval("duration2").ToString()) %>
            </ItemTemplate>
             </asp:TemplateField>

            
        </Columns>
    </asp:GridView>
      <asp:Label ID="lblMsg" runat="server" CssClass="label label-warning"></asp:Label> 



      </div>
  </div>
    <div id="menu1" class="tab-pane fade">
              <h3>ÇAĞRI NOTLARI</h3>
    <asp:UpdatePanel runat="server" ID="pnlNotes" UpdateMode="Conditional">
        <ContentTemplate>
<%--         <div class="row">  
               <div class="form-group">
    
     <label class="col-sm-1 control-label" for="txtNot">Not</label>
    
  <div class="col-sm-9">
  	<asp:TextBox runat="server" id="txtNot" ClientIDMode="Static" name="txtNot" class="form-control input-sm" TextMode="MultiLine" Rows="2"/>
  
      
  </div>
                <div class="col-sm-2">
    <asp:Button id="btnSave" runat="server" class="btn btn-success" Text="Kaydet" OnClick="btnSave_Click" />
  </div>

              </div>
       

      


</div>--%>
      <br />
        <div class="row"> 
        <div class="form-group">
  
  <div class="col-sm-12">
 <asp:GridView ID="grdData" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-responsive table-striped " 
     RowStyle-HorizontalAlign="Left"
     HeaderStyle-HorizontalAlign="Center"
     AllowPaging="False">
     <Columns>
           <asp:BoundField HeaderText="Not"  DataField="NOTE" HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="col-sm-8">
<HeaderStyle HorizontalAlign="Left"></HeaderStyle>

<ItemStyle></ItemStyle>
         </asp:BoundField>
           <asp:BoundField HeaderText="Kullanıcı"  DataField="USERNAME" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="col-sm-2">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" CssClass="col-sm-2"></ItemStyle>
         </asp:BoundField>
           <asp:BoundField HeaderText="Tarih"  DataField="CREATEDATETIME" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="col-sm-2">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" CssClass="col-sm-2"></ItemStyle>

         </asp:BoundField>

         </Columns>
         </asp:GridView></div>
  </div>
          </div>
  </ContentTemplate>
<%--        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
        </Triggers>--%>
        </asp:UpdatePanel>
         
</div>   
      <div id="menu2" class="tab-pane fade">
    <h3>İŞLEMLER</h3>
      <div class="row"> 
        <div class="form-group">
  
  <div class="col-sm-12">
 <asp:GridView ID="grdLog" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-responsive table-striped " 
     RowStyle-HorizontalAlign="Center"
     HeaderStyle-HorizontalAlign="Center"
     AllowPaging="False">
     <Columns>
           <asp:BoundField HeaderText="Kullanıcı"  DataField="UserName" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="col-sm-2">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle></ItemStyle>
         </asp:BoundField>
           <asp:BoundField HeaderText="Tarih"  DataField="CREATEDATETIME" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="col-sm-3">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle></ItemStyle>
         </asp:BoundField>
         <asp:BoundField HeaderText="İşlem Detayı"  DataField="ACTIONDESC" HeaderStyle-HorizontalAlign="Center" ItemStyle-CssClass="col-sm-7">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle></ItemStyle>
         </asp:BoundField>
           
         </Columns>
         </asp:GridView></div>
  </div>
            </div>
  </div>
</div>
</asp:Content>
