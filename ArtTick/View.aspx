﻿<%@ Page Title="" Language="C#" MasterPageFile="~/EmptyMaster.Master" AutoEventWireup="true" CodeBehind="View.aspx.cs" Inherits="ARTQM.View" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <style>

        tr.kriter_satir, tr.kriter_satir td, td.kriter_satir
{
    white-space:normal;
    text-indent:0px;
    text-align:left;
	padding-top:2px;
	padding-bottom:2px;
	font-weight: bold;
	background-color:#fe0000;
	border-bottom: 1px solid #c0c0c0;
}

        .row1, .row1 td {

    background-color:#d9534f;
    color:#fefefe;
    border: solid 1px #d43f3a;

}

        
kriter_baslik tr, td.kriter_baslik, .kriter_baslik, .kriter_baslik td
{
    background-color: #eeeeee;
    font-size: 12px;
    text-align: center;
    color: #363636;
    border-bottom: solid 1px #909090;
    font-weight: bold;
    padding-top: 2px;
    padding-bottom: 2px;
}

.kriter_baslik2 tr, td.kriter_baslik2, .kriter_baslik2, .kriter_baslik2 td
{
    background-color: #eeeeee;
    font-size: 12px;
    text-align: center;
    color: #363636;
    border-bottom: solid 1px #909090;
    font-weight: bold;
    padding-top: 2px;
    padding-bottom: 2px;
}

.kriter_secenek , .kriter_secenek td
{
    text-align: left;
    font-weight: normal;
    text-indent:0px;
    white-space: normal;
    border-bottom: 1px solid #c0c0c0;
    
}
.yorum td
{
    background-color: #eeeeee;
    
    text-align: left;
    font-weight:normal;
    
    border-top: solid 2px #464646;
    border-bottom: solid 5px #464646;
}


 tr.kriter_satir, tr.kriter_satir td, td.kriter_satir
{
    white-space:normal;
    text-indent:0px;
    text-align:left;
	padding-top:2px;
	padding-bottom:2px;
	font-weight: bold;
	background-color:#fefefe;
	border-bottom: 1px solid #c0c0c0;
}

.highlightcol , td.highlightcol , .highlightcol td, tr.kriter_satir .highlightcol{
            background-color: #eeeeee; 
            text-align:center; 
            color:#333333;
            border-bottom: 1px solid #c0c0c0;
        }
tr.kriter_baslik .highlightcol{
            border-bottom: 1px solid #909090;
        }



    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

       <div class="panel panel-default">
        <div class="panel-heading">       <asp:Label ID="lblProje" runat="server"></asp:Label>
            </div>
       </div>
    
    <table id="karne" class="table table-condensed">
        <thead>
              <tr>  <th><asp:LinkButton ID="lnkDownload" runat="server" onclick="lnkDownload_Click" CssClass="btn btn-default"><i class="glyphicon glyphicon-floppy-save"></i>Kaydet</asp:LinkButton></th><th>&nbsp;</th><th class="highlightcol">TAM PUAN</th><th style="text-align:right;"><asp:LinkButton ID="lnkEdit" runat="server" onclick="lnkEdit_Click" CssClass="btn btn-default"><i class="glyphicon glyphicon-edit"></i>Düzenle</asp:LinkButton></th></tr>
        </thead><tbody>
        <tr class="row1"><td>&nbsp;</td>
        <td>BAŞARI YÜZDESİ</td>
        <td style="text-align:center" >100%</td>
        <td style="text-align:center"><asp:Label runat=server ID="lblYuzde"></asp:Label></td>
        </tr>
        <tr class="row1"><td>&nbsp;</td>
        <td>TOPLAM PUAN</td>
        <td style="text-align:center" >100</td>
        <td style="text-align:center"><asp:Label runat=server ID="lblToplam"></asp:Label></td>
        </tr>

            <asp:ListView ID="lstKarne" runat="server">               
            <LayoutTemplate>

            <asp:PlaceHolder id="itemPlaceholder" runat=server></asp:PlaceHolder></LayoutTemplate>
            <ItemTemplate>   
        <tr id='<%#"tr"+Container.DataItemIndex.ToString() %>' class=<%# GetRowClass(Eval("SORUNO").ToString() , Eval("SORUSIRA").ToString() , Eval("EVETHAYIR").ToString()) %>><td><%# Eval("SORUSIRA").ToString().Replace("_",".") %></td>
            <td ><%# Eval("ETIKET") %></td>
            <td class="highlightcol" ><%# Eval("MAKSSKOR") %>&nbsp;</td>
            <td style="text-align:center"><%# Eval("PUAN") %></td>
        </tr>

            </ItemTemplate> </asp:ListView>     
            </tbody> 
            </table>

    <div id="divScript" runat="server"></div>
            
           
</asp:Content>