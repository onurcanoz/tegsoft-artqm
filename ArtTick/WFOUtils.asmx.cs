﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ARTQM
{
    /// <summary>
    /// Summary description for WFOUtils
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class WFOUtils : System.Web.Services.WebService
    {
        [WebMethod]
        public DataTable QueryCalls(string Extension, string Agent, string ACDGroup, DateTime StartDateTime, DateTime EndDateTime, int DurationFrom, int DurationTo,
            string CallerID, string CalledID, string Direction, string CallID, string Status, string Label)
        {
            NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["acrDBString"].ConnectionString);
            try
            {
                conn.Open();

            }
            catch (Exception ex)
            {
                //  AppendLog("ERROR", "CANNOT ACCESS ACR DATABASE (PGSQL)" + ex.ToString());
                return null;
            }

            string sql = " SELECT  distinct  calls.nativecallid, callsegments.callid, callsegments.startedat, sum(distinct callsegments.duration::int) as duration2, recordings.inum, agents(callsegments.callid, callsegments.segmentnum) AS partyagentnames, skills(callsegments.callid, callsegments.segmentnum) AS skillset, otherparties_qmi(callsegments.callid, callsegments.segmentnum) AS partynames, callset_names(callsegments.callid, callsegments.segmentnum) AS csnames, FIRST(parties.dirn) as direction"
            + " FROM callsegments NATURAL JOIN calls NATURAL JOIN parties NATURAL JOIN recordings  where parties.owner = true ";
            sql += " and callsegments.startedat >= '" + StartDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "'::timestamp ";
            if (EndDateTime != DateTime.MaxValue)
                sql += " and callsegments.startedat <= '" + EndDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "'::timestamp ";
            else
                sql += " and callsegments.startedat <= '" + StartDateTime.ToString("yyyy-MM-dd") + " 23:59:59'::timestamp ";

            if (DurationFrom != -1)
                sql += " and callsegments.duration >= " + DurationFrom + "";
            if (DurationTo != -1)
                sql += " and callsegments.duration <= " + DurationTo + "";
            if (Direction == "Gelen")
                sql += " and parties.dirn = 1";
            if (Direction == "Giden")
                sql += " and parties.dirn = 2";
            if (CallID != "")
                sql += " and callsegments.callid ='" + CallID + "'";
            /* if (CallerID != "")
                 sql += " and otherparties_qmi(callsegments.callid, callsegments.segmentnum)  like '%" + CallerID + "%' ";*/

            sql += " group by calls.nativecallid, callsegments.callid, callsegments.startedat, recordings.inum, agents(callsegments.callid, callsegments.segmentnum), skills(callsegments.callid, callsegments.segmentnum) , otherparties_qmi(callsegments.callid, callsegments.segmentnum) , callset_names(callsegments.callid, callsegments.segmentnum)  order by callsegments.callid ";
            sql += " limit 500 ";



            Npgsql.NpgsqlDataAdapter da = new NpgsqlDataAdapter(sql, conn);
            try
            {
                da.SelectCommand.CommandTimeout = 300000;
                DataTable temp = new DataTable("CALLS");
                da.Fill(temp);
                //kalan alanlar filtrelenecek
                DataView vi = new DataView(temp);
                vi.RowFilter = " (1 = 1)";
                if (Extension != "")
                    vi.RowFilter += " and (partynames like '" + Extension + ",%' or partynames likes '%, " + Extension + "')";
                if (Agent != "")
                    vi.RowFilter += " and (partyagentnames='" + Agent + "')";
                if (ACDGroup != "")
                    vi.RowFilter += " and (skillset='" + ACDGroup + "')";

                DataTable calls = new DataTable("CALLS");
                calls = vi.ToTable();
                return calls;
            }
            catch
            {
                //log
                return null;
            }
            finally
            {
                conn.Close();
            }

        }


        [WebMethod]
        public DataTable QueryCalls2(string Extension, string Agent, string ACDGroup, DateTime StartDateTime, DateTime EndDateTime, int DurationFrom, int DurationTo,
            DateTime EvalStartDateTime, DateTime EvalEndDateTime, string EvalUser, string EvalForm,
            string CallerID, string CalledID, string Direction, string CallID, string Status, string Label)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);


            try
            {
                conn.Open();

            }
            catch (Exception ex)
            {
                //  AppendLog("ERROR", "CANNOT ACCESS ACR DATABASE (PGSQL)" + ex.ToString());
                return null;
            }

            string sql = " SELECT  distinct  Nativecallid, Callid, Startdate, Duration,  ARTCOMMONS.dbo.FN_AGENTNAME(Partyagentnames) as Partyagentnames, ARTCOMMONS.dbo.FN_SKILLNAME(Skillset) as Skillset, Partynames , Csnames, Direction, convert(nvarchar, Startdate, 112) as DONEM "
            + " FROM CallData where 1=1";
            sql += " and Startdate >= '" + StartDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "'";
            if (EndDateTime != DateTime.MaxValue)
                sql += " and  Startdate <= '" + EndDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "'";
            else
                sql += " and Startdate  <= '" + StartDateTime.ToString("yyyy-MM-dd") + " 23:59:59' ";

            if (CallID != "")
                sql += " and Callid = '" + CallID + "'";
            
            if (DurationFrom != -1)
                sql += " and Duration >= " + DurationFrom + "";
            if (DurationTo != -1)
                sql += " and Duration <= " + DurationTo + "";
            if (Direction == "1")
                sql += " and Direction = 1";
            if (Direction == "2")
                sql += " and Direction = 2";
            /* if (CallerID != "")
                 sql += " and otherparties_qmi(callsegments.callid, callsegments.segmentnum)  like '%" + CallerID + "%' ";*/
            if (Extension != "")
                sql += " and (Partynames like '" + Extension + ",%' or Partynames likes '%, " + Extension + "')";
            if (CallerID != "")
                sql += " and (Partynames like '%" + CallerID + "%')";
            else
            {
                if (!string.IsNullOrEmpty(Session["UserPrefixes"].ToString()) ||
                    !Session["UserPrefixes"].ToString().Equals("0"))
                {
                    ICUser user = (ICUser)Session["CurrentUser"];

                    string tmp = "";
                    string[] prefixes = Session["UserPrefixes"].ToString().Split(',');
                    foreach (string pref in prefixes)
                    {
                        tmp += "or (Partynames like '%," + pref + "%') ";
                    }

                    if (user.userLevel != 0)
                    {
                        if (ACDGroup != "")
                            sql += " and (" + tmp.Substring(2, tmp.Length - 3) + " or (Skillset IN (" + ACDGroup + ")))";
                        else
                            sql += " and (" + tmp.Substring(2, tmp.Length - 3) + ")";
                    }
                    else
                    {
                        if (ACDGroup != "")
                        {

                            sql += " and (" + tmp.Substring(2, tmp.Length - 3) + " or (Skillset IN (" + ACDGroup + ")))";
                        }
                        else
                        {

                        }
                    }
                }

            }

            if (Agent != "")
                sql += " and (Partyagentnames like '%" + Agent + "%')";
            /*if (ACDGroup != "")
                sql += " and (Skillset IN (" + ACDGroup + "))";*/

            string sql2 = "";
            if (EvalStartDateTime != DateTime.MinValue)
            {
                sql2 += " DEGERLENDIRMETARIHI >= '" + EvalStartDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                if (EvalEndDateTime != DateTime.MaxValue)
                    sql2 += " and  DEGERLENDIRMETARIHI <= '" + EndDateTime.ToString("yyyy-MM-dd HH:mm:ss") + "'";
            }
            if (EvalUser != "" && EvalUser != "0")
            {
                if (sql2 != "")
                    sql2 += " AND ";
                sql2 += " DEGERLENDIREN='" + EvalUser + "'";
            }

            if (EvalForm != "" && EvalForm != "0")
            {
                if (sql2 != "")
                    sql2 += " AND ";
                sql2 += " PROJENO='" + EvalForm+ "'";
            }

            if (sql2 != "")
                sql += " AND Callid in (select ANKETNO from ANKETLER where " + sql2 + ")";

            sql += " order by Startdate,Callid";
            sql += "";



            SqlDataAdapter da = new SqlDataAdapter (sql, conn);
            try
            {
                da.SelectCommand.CommandTimeout = 30000;
                DataTable temp = new DataTable("CALLS");
                da.Fill(temp);
                //kalan alanlar filtrelenecek
                return temp;
            }
            catch
            {
                //log
                return null;
            }
            finally
            {
                conn.Close();
            }

        }
        
        
        [WebMethod]
        public DataTable GetCallSegments(string CallID)
        {
            NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["acrDBString"].ConnectionString);
            try
            {
                conn.Open();

            }
            catch (Exception ex)
            {
                //  AppendLog("ERROR", "CANNOT ACCESS ACR DATABASE (PGSQL)" + ex.ToString());
                return null;
            }

            string sql = " select distinct  cs.duration::int as duration2, cs.startedat, rec.inum from callsegments cs inner join recordings rec  "
                          + " on cs.callid = rec.callid and cs.segmentnum=rec.segmentnum  where cs.callid =" + CallID
                          + " order by cs.startedat ";


            Npgsql.NpgsqlDataAdapter da = new NpgsqlDataAdapter(sql, conn);
            try
            {
                da.SelectCommand.CommandTimeout = 300;
                DataTable temp = new DataTable("SEGMENTS");
                da.Fill(temp);
                //kalan alanlar filtrelenecek
                return temp;
            }
            catch
            {
                //log
                return null;
            }
            finally
            {
                conn.Close();
            }

        }

        [WebMethod]
        public DataTable GetCallSets()
        {
            NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["acrDBString"].ConnectionString);
            try
            {
                conn.Open();

            }
            catch (Exception ex)
            {
                //  AppendLog("ERROR", "CANNOT ACCESS ACR DATABASE (PGSQL)" + ex.ToString());
                return null;
            }

            string sql = " select * from callsets where callsetname not like '%LOCK%' order by callsetname";


            Npgsql.NpgsqlDataAdapter da = new NpgsqlDataAdapter(sql, conn);
            try
            {
                da.SelectCommand.CommandTimeout = 300;
                DataTable temp = new DataTable("CALLSETS");
                da.Fill(temp);
                //kalan alanlar filtrelenecek
                return temp;
            }
            catch
            {
                //log
                return null;
            }
            finally
            {
                conn.Close();
            }

        }

        [WebMethod]
        public bool Insert2CallSet(string callid, string callsetid)
        {
            NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["acrDBString"].ConnectionString);
            try
            {
                conn.Open();

            }
            catch (Exception ex)
            {
                //  AppendLog("ERROR", "CANNOT ACCESS ACR DATABASE (PGSQL)" + ex.ToString());
                return false;
            }

            string sql = "insert into cssegments (callsetid, segmentnum, callid) values (" + callsetid + ",1,'" + callid + "') ";

            Npgsql.NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
            try
            {
                cmd.CommandTimeout = 300;
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex2)
            {
                if (ex2.Message.ToLower().Contains("duplicate"))
                {
                    //Violation of primary key. Handle Exception
                    return true;
                }
                else
                    //log
                    return false;
            }
            finally
            {
                conn.Close();
            }

        }
        [WebMethod]
        public bool RemoveFromCallSet(string callid, string callsetid)
        {
            NpgsqlConnection conn = new NpgsqlConnection(ConfigurationManager.ConnectionStrings["acrDBString"].ConnectionString);
            try
            {
                conn.Open();

            }
            catch (Exception ex)
            {
                //  AppendLog("ERROR", "CANNOT ACCESS ACR DATABASE (PGSQL)" + ex.ToString());
                return false;
            }

            string sql = "delete from cssegments callsetid=" + callsetid + " and segmentnum=1 and callid='" + callid + "'";

            Npgsql.NpgsqlCommand cmd = new NpgsqlCommand(sql, conn);
            try
            {
                cmd.CommandTimeout = 300;
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                conn.Close();
            }

        }
        [WebMethod]
        public DataTable QueryEvalSummary(string direction, string callStart, string callEnd, string agent, int durationFrom, int durationTo)
        {
            DataProvider obj = new DataProvider();
            Utils utils = new Utils();
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            DataTable evalSummary = new DataTable();

            string userCode = string.Empty;
        
            if (direction == "0")
            {
                DataTable Inbound = new DataTable();
                DataTable Outbound = new DataTable();

                string q1 = "SELECT VARCHAR_FORMAT(cdr.CALLDATE, 'YYYY-MM-DD') AS REPORTDATE, COUNT(*) AS CAGRI, u.USERCODE AS AGENTS FROM TBLCCCDR AS cdr LEFT JOIN TBLCCSKILLS AS s ON cdr.SKILL = s.SKILL LEFT JOIN TBLUSERS AS u ON cdr.UID = u.UID WHERE cdr.DISPOSITION = 'ANSWERED' AND (cdr.SKILLEVENT = 'COMPLETECALLER' OR cdr.SKILLEVENT = 'COMPLETEAGENT') AND cdr.USERFIELD = 'INBOUND' AND cdr.CALLDATE BETWEEN '" + callStart + "' AND '" + callEnd + "' AND AUDIOFILENAME != ''";

                if (agent != "0")
                {
                    q1 += " AND cdr.UID IN (" + agent + ")";
                }
                if (agent == "0")
                {
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
                    DataTable dt = new DataTable();
                    try
                    {
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter("SELECT LOGID FROM AGENTS", conn);
                        da.Fill(dt);
                        conn.Close();
                        string UID = string.Empty;
                        foreach (DataRow dr in dt.Rows)
                        {
                            UID += "'" + dr["LOGID"] + "',";
                        }
                        UID = UID.Remove(UID.Length - 1, 1);

                        q1 += " AND cdr.UID IN (" + UID + ")";
                    }
                    catch (Exception Ex)
                    {

                    }
                }

                q1 += " GROUP BY VARCHAR_FORMAT(cdr.CALLDATE, 'YYYY-MM-DD'), u.USERCODE ORDER BY REPORTDATE ASC";
                Inbound = obj.QueryEvalSummary(q1);

                if (Inbound.Rows.Count > 0)
                {
                    foreach (DataRow dr in Inbound.Rows)
                    {
                        userCode += "'" + dr["AGENTS"].ToString() + "',";
                    }
                    userCode = userCode.Remove(userCode.Length - 1);

                    string sql = "SELECT convert(nvarchar(10), CALLDATE, 120) AS REPORTDATE, PARTYAGENTNAMES AS AGENTS, COUNT(*) AS DEGERLENDIRME FROM[ANKETLER] WHERE PARTYAGENTNAMES IN(" + userCode + ") AND CALLDATE BETWEEN '" + callStart + "' AND '" + callEnd + "' AND YON = '1' GROUP BY PARTYAGENTNAMES, convert(nvarchar(10), CALLDATE, 120)";
                    Inbound = getEvalSummary(sql, Inbound);
                }
                string q2 = "SELECT VARCHAR_FORMAT(cdr.CALLDATE, 'YYYY-MM-DD') AS REPORTDATE, COUNT(*) AS CAGRI, u.USERCODE AS AGENTS FROM TBLCCCDR AS cdr LEFT JOIN TBLUSERS AS u ON cdr.UID = u.UID WHERE DISPOSITION = 'ANSWERED' AND USERFIELD = 'OUTBOUND' AND cdr.CALLDATE BETWEEN '" + callStart + "' AND '" + callEnd + "' AND AUDIOFILENAME != ''";

                if (agent != "0")
                {
                    q2 += " AND cdr.UID IN (" + agent + ")";
                }
                if (agent == "0")
                {
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
                    DataTable dt = new DataTable();
                    try
                    {
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter("SELECT LOGID FROM AGENTS", conn);
                        da.Fill(dt);
                        conn.Close();
                        string UID = string.Empty;
                        foreach (DataRow dr in dt.Rows)
                        {
                            UID += "'" + dr["LOGID"] + "',";
                        }
                        UID = UID.Remove(UID.Length - 1, 1);

                        q2 += " AND cdr.UID IN (" + UID + ")";
                    }
                    catch (Exception Ex)
                    {

                    }
                }

                q2 += " GROUP BY VARCHAR_FORMAT(cdr.CALLDATE, 'YYYY-MM-DD'), u.USERCODE ORDER BY REPORTDATE ASC";
                Outbound = obj.QueryEvalSummary(q2);

                if (Outbound.Rows.Count > 0)
                {
                    userCode = string.Empty;
                    foreach (DataRow dr in Outbound.Rows)
                    {
                        userCode += "'" + dr["AGENTS"].ToString() + "',";
                    }
                    userCode = userCode.Remove(userCode.Length - 1);

                    string sql = "SELECT convert(nvarchar(10), CALLDATE, 120) AS REPORTDATE, PARTYAGENTNAMES AS AGENTS, COUNT(*) AS DEGERLENDIRME FROM[ANKETLER] WHERE PARTYAGENTNAMES IN(" + userCode + ") AND CALLDATE BETWEEN '" + callStart + "' AND '" + callEnd + "' AND YON = '2' GROUP BY PARTYAGENTNAMES, convert(nvarchar(10), CALLDATE, 120)";
                    Outbound = getEvalSummary(sql, Outbound);
                }

                foreach(DataRow dr in Inbound.Rows)
                {
                    if(dr["DEGERLENDIRME"].ToString() == "")
                    {
                        dr["DEGERLENDIRME"] = "0";
                    }
                }
                foreach(DataRow dr in Outbound.Rows)
                {
                    if(dr["DEGERLENDIRME"].ToString() == "")
                    {
                        dr["DEGERLENDIRME"] = 0;
                    }
                }
                int inboundLength = Inbound.Rows.Count;
                int outboundLength = Outbound.Rows.Count;

                for(int i = 0; i < inboundLength; i++)
                {
                    for(int j = 0; j < outboundLength; j++)
                    {
                        if(Inbound.Rows[i]["AGENTS"].ToString() == Outbound.Rows[j]["AGENTS"].ToString() && Inbound.Rows[i]["REPORTDATE"].ToString() == Outbound.Rows[j]["REPORTDATE"].ToString())
                        {
                            Inbound.Rows[i]["DEGERLENDIRME"] = (Convert.ToInt32(Inbound.Rows[i]["DEGERLENDIRME"].ToString()) + Convert.ToInt32(Outbound.Rows[j]["DEGERLENDIRME"].ToString())).ToString();
                            Inbound.Rows[i]["CAGRI"] = (Int32.Parse(Inbound.Rows[i]["CAGRI"].ToString()) + Int32.Parse(Outbound.Rows[j]["CAGRI"].ToString())).ToString();
                            Outbound.Rows[j].Delete();
                            Outbound.AcceptChanges();
                            j--;
                            outboundLength = Outbound.Rows.Count;
                        }
                    }
                    
                    
                }
                
                if(Outbound.Rows.Count > 0)
                {
                    Inbound.Merge(Outbound);
                    Inbound.DefaultView.Sort = "REPORTDATE ASC";
                }
                return Inbound;
            }
            else if(direction == "1")
            {
                string q = "SELECT VARCHAR_FORMAT(cdr.CALLDATE, 'YYYY-MM-DD') AS REPORTDATE, COUNT(*) AS CAGRI, u.USERCODE AS AGENTS FROM TBLCCCDR AS cdr LEFT JOIN TBLCCSKILLS AS s ON cdr.SKILL = s.SKILL LEFT JOIN TBLUSERS AS u ON cdr.UID = u.UID WHERE cdr.DISPOSITION = 'ANSWERED' AND (cdr.SKILLEVENT = 'COMPLETECALLER' OR cdr.SKILLEVENT = 'COMPLETEAGENT') AND cdr.USERFIELD = 'INBOUND' AND cdr.CALLDATE BETWEEN '" + callStart + "' AND '" + callEnd + "' AND AUDIOFILENAME != ''";

                if (agent != "0")
                {
                    q += " AND cdr.UID IN (" + agent + ")";
                }
                if(agent == "0")
                {
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
                    DataTable dt = new DataTable();
                    try
                    {
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter("SELECT LOGID FROM AGENTS", conn);
                        da.Fill(dt);
                        conn.Close();
                        string UID = string.Empty;
                        foreach (DataRow dr in dt.Rows)
                        {
                            UID += "'" + dr["LOGID"] + "',";
                        }
                        UID = UID.Remove(UID.Length - 1, 1);

                        q += " AND cdr.UID IN (" + UID + ")";
                    }
                    catch (Exception Ex)
                    {

                    }
                }

                q += " GROUP BY VARCHAR_FORMAT(cdr.CALLDATE, 'YYYY-MM-DD'), u.USERCODE ORDER BY REPORTDATE ASC";
                evalSummary = obj.QueryEvalSummary(q);

                if (evalSummary.Rows.Count > 0)
                {
                    foreach (DataRow dr in evalSummary.Rows)
                    {
                        userCode += "'" + dr["AGENTS"].ToString() + "',";
                    }
                    userCode = userCode.Remove(userCode.Length - 1);

                    string sql = "SELECT convert(nvarchar(10), CALLDATE, 120) AS REPORTDATE, PARTYAGENTNAMES AS AGENTS, COUNT(*) AS DEGERLENDIRME FROM[ANKETLER] WHERE PARTYAGENTNAMES IN(" + userCode + ") AND CALLDATE BETWEEN '" + callStart + "' AND '" + callEnd + "' AND YON = '1' GROUP BY PARTYAGENTNAMES, convert(nvarchar(10), CALLDATE, 120)";
                    //string sifirlama = "  SELECT convert(nvarchar(10), s.CALLDATE, 120) AS CALLDATE, a.PARTYAGENTNAMES, COUNT(*) AS SIFIRLAMA FROM SIFIRLAMAGUNCEL AS s LEFT JOIN ANKETLER AS a ON a.ANKETNO = s.ANKETNO WHERE a.CALLDATE BETWEEN '"+ callStart + "' AND '"+ callEnd + "' AND a.YON = '1' AND a.PARTYAGENTNAMES IN(" + userCode + ") GROUP BY a.PARTYAGENTNAMES, convert(nvarchar(10), s.CALLDATE, 120)";
                    evalSummary = getEvalSummary(sql, evalSummary);
                }
                return evalSummary;
            }
            else
            {
                string q = "SELECT VARCHAR_FORMAT(cdr.CALLDATE, 'YYYY-MM-DD') AS REPORTDATE, COUNT(*) AS CAGRI, u.USERCODE AS AGENTS FROM TBLCCCDR AS cdr LEFT JOIN TBLUSERS AS u ON cdr.UID = u.UID WHERE DISPOSITION = 'ANSWERED' AND USERFIELD = 'OUTBOUND' AND cdr.CALLDATE BETWEEN '" + callStart + "' AND '" + callEnd + "' AND AUDIOFILENAME != ''";

                if(agent != "0")
                {
                    q += " AND cdr.UID IN (" + agent + ")";
                }
                if(agent == "0")
                {
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
                    DataTable dt = new DataTable();
                    try
                    {
                        conn.Open();
                        SqlDataAdapter da = new SqlDataAdapter("SELECT LOGID FROM AGENTS", conn);
                        da.Fill(dt);
                        conn.Close();
                        string UID = string.Empty;
                        foreach (DataRow dr in dt.Rows)
                        {
                            UID += "'" + dr["LOGID"] + "',";
                        }
                        UID = UID.Remove(UID.Length - 1, 1);

                        q += " AND cdr.UID IN (" + UID + ")";
                    }
                    catch (Exception Ex)
                    {

                    }
                }
                
                q += " GROUP BY VARCHAR_FORMAT(cdr.CALLDATE, 'YYYY-MM-DD'), u.USERCODE ORDER BY REPORTDATE ASC";
                evalSummary = obj.QueryEvalSummary(q);

                if (evalSummary.Rows.Count > 0)
                {
                    foreach (DataRow dr in evalSummary.Rows)
                    {
                        userCode += "'" + dr["AGENTS"].ToString() + "',";
                    }
                    userCode = userCode.Remove(userCode.Length - 1);

                    string sql = "SELECT convert(nvarchar(10), CALLDATE, 120) AS REPORTDATE, PARTYAGENTNAMES AS AGENTS, COUNT(*) AS DEGERLENDIRME FROM[ANKETLER] WHERE PARTYAGENTNAMES IN(" + userCode + ") AND CALLDATE BETWEEN '" + callStart + "' AND '" + callEnd + "' AND YON = '2' GROUP BY PARTYAGENTNAMES, convert(nvarchar(10), CALLDATE, 120)";
                    evalSummary = getEvalSummary(sql, evalSummary);
                }
                return evalSummary;
            }
        }
        public DataTable getEvalSummary(string q, DataTable evalSummary)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);

            try
            {
                con.Open();
                SqlCommand comm = new SqlCommand(q, con);
                comm.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(comm);
                DataTable summ = new DataTable("SUMMARY");
                da.Fill(summ);

                int lengthEval = evalSummary.Rows.Count;
                int lengthSumm = summ.Rows.Count;
                for (int i = 0; i < lengthEval; i++)
                {
                    for (int j = 0; j < lengthSumm; j++)
                    {
                        try
                        {
                            if (evalSummary.Rows[i]["AGENTS"].ToString() == summ.Rows[j]["AGENTS"].ToString() && evalSummary.Rows[i]["REPORTDATE"].ToString() == summ.Rows[j]["REPORTDATE"].ToString())
                            {
                                evalSummary.Rows[i]["DEGERLENDIRME"] = summ.Rows[j]["DEGERLENDIRME"];
                                j = lengthSumm;
                            }
                            else
                            {
                                evalSummary.Rows[i]["DEGERLENDIRME"] = "0";
                            }
                        }
                        catch (Exception Ex)
                        {
                            evalSummary.Rows[i]["DEGERLENDIRME"] = "0";
                        }
                    }
                }
                return evalSummary;
            }
            catch (Exception Ex)
            {
                return new DataTable("SUMMARY");

            }
            finally
            {
                con.Close();
            }
        }

        [WebMethod]
        public DataTable QueryEvals(string evalDateStart, string evalDateEnd, string direction, int durationFrom, int durationTo, string phoneNumber, string skill, string agent, string form, string user)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);

            string sql = "SELECT a.ANKETID, a.ANKETNO, a.SKILLSET as ACDGROUP, a.PARTYAGENTNAMES as AGENTS, a.DEGERLENDIRMETARIHI, a.DEGERLENDIREN, P.PROJEADI as FORM, P.TOPLAMPUAN, a.GENELCSI as PUAN, P.GECERPUAN, a.CALLDATE as GORUSMETARIHI, a.YON, a.SURE, PARTYNAMES as KATILIMCILAR, a.DATAYIL, a.DATAAY, P.PROJENO from ANKETLER as a inner join PROJELER P on P.PROJENO = a.PROJENO WHERE a.DEGERLENDIRMETARIHI BETWEEN '" + evalDateStart +"' AND '" + evalDateEnd + "'";
            
            if(direction != "0")
            {
                sql += " AND a.YON = '"+direction+"' ";
            }
            if(durationFrom != -1)
            {
                sql += " AND a.SURE >= " + durationFrom + "";
            }
            if(durationTo != -1)
            {
                sql += " AND a. SURE <= " + durationTo + "";
            }
            if(phoneNumber != string.Empty)
            {
                sql += " AND a.PARTYNAMES LIKE '%" + phoneNumber + "%'  ";
            }
            if(skill != "" && direction != "2" )
            {
                sql += " AND a.SKILLS IN (" + skill + ")";
            }
            if(agent != "0")
            {
                sql += " AND a.LOGID IN (" + agent + ")";
            }
            if(form != "0")
            {
                sql += " AND a.PROJENO = '" + form + "'";
            }
            if(user != "")
            {
                sql += " AND a.DEGERLENDIREN = '" + user + "' ";
            }
            try
            {
                con.Open();
                SqlCommand comm = new SqlCommand(sql, con);
                comm.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.SelectCommand.CommandTimeout = 600000;
                DataTable summ = new DataTable("EVALS");
                da.Fill(summ);
                return summ;

            }
            catch (Exception ex)
            {
                return new DataTable("EVALS");

            }
            finally
            {
                con.Close();

            }
        }

    }
}
