﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ARTQM
{
    public partial class MTemplates : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string tanimlamalar = Session["TANIMLAMALAR"].ToString();

            if (tanimlamalar.Equals("0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                        "alert('Yetkiniz Yok'); window.location='" + Request.UrlReferrer.ToString() + "';", true);
                return;
            }
            if (!IsPostBack)
            {
                Session["SelectedID"] = null;

                LoadData();
            }
        }

        public void LoadData()
        {
            Session["Templates"] = Lists.GetTemplatesList();

            grdData.DataSource = (DataTable)Session["Templates"];
            grdData.DataKeyNames = new string[] { "TASLAKID" };
            grdData.DataBind();


            spanInfo.InnerHtml = "Toplam " + grdData.Rows.Count.ToString() + " kayıt bulundu.";


        }

        protected void btnSave_Click(object sender, EventArgs e)
        {


            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Bildirim Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            string secenekler = txtSecenekler.Text.Trim().Replace(", ", ",");
            string puanlama = txtPuanlama.Text.Trim().Replace(", ", ",");
            try
            {
                SqlCommand cmd = new SqlCommand("", conn);
                int result = 0;
                if (Session["SelectedID"] == null)
                {
                    cmd.CommandText = "INSERT INTO [dbo].[PUANTASLAKLAR] ([SECENEKLER] ,[SECENEKLERPUAN]) VALUES (@p1, @p2)";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add("@p1", SqlDbType.NVarChar, 250).Value = secenekler;
                    cmd.Parameters.Add("@p2", SqlDbType.NVarChar, 250).Value = puanlama;

                    result = cmd.ExecuteNonQuery();
                }
                else
                {
                    cmd.CommandText = "update [dbo].[PUANTASLAKLAR]  set [SECENEKLER]=@p1 ,[SECENEKLERPUAN] =@p2 where TASLAKID=@p3";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add("@p1", SqlDbType.NVarChar, 250).Value = secenekler;
                    cmd.Parameters.Add("@p2", SqlDbType.NVarChar, 250).Value = puanlama;
                    cmd.Parameters.Add("@p3", SqlDbType.Int).Value = Session["SelectedID"].ToString();

                    result = cmd.ExecuteNonQuery();
                }

                hResult.InnerHtml = "Kayıt Başarılı";
                bResult.InnerHtml = "Şablon kaydı başarıyla alınmıştır.";
                divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";

                LoadData();

            }
            catch (Exception ex)
            {
                hResult.InnerHtml = "Hata Oluştu!";
                bResult.InnerHtml = "Şablon kaydı gerçekleştirilemedi. Hata Mesajı: " + ex.Message;
                divResult.InnerHtml = "<script>$('#myModal').modal();  </script>";


            }
            finally
            {
                conn.Close();
                Session["SelectedID"] = null;
            }
        }


        protected void Button1_Click(object sender, EventArgs e)
        {
            Session["SelectedID"] = null;
            ExportToExcel(grdData, "Sablonlar");
        }

        protected void lnkNew_Click(object sender, EventArgs e)
        {
            try
            {
                Session["SelectedID"] = null;
                txtPuanlama.Text = "";
                txtSecenekler.Text = "";
                divResult.InnerHtml = "<script>$('#divUpdate').modal(); </script>";
            }
            catch
            {
            }
        }

        protected void grdData_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                string id = grdData.DataKeys[e.RowIndex].Value.ToString();
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
                try
                {
                    conn.Open();
                }
                catch (Exception ex)
                {
                    throw new Exception("Bildirim Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

                }
                SqlCommand cmd = new SqlCommand("", conn);
                int result = 0;
                cmd.CommandText = "delete from TemplateItems where TemplateID=@id";
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                result = cmd.ExecuteNonQuery();
                cmd.CommandText = "delete from Templates where TemplateID=@id";
                result = cmd.ExecuteNonQuery();
                hResult.InnerHtml = "Kayıt Silindi";
                bResult.InnerHtml = "Şablon kaydı başarıyla silindi.";
                divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";

                LoadData();
            }
            catch (Exception ex)
            {
                hResult.InnerHtml = "Kayıt Silinemedi";
                bResult.InnerHtml = "Şablon kaydı silme başarısız oldu. Hata: " + ex.ToString();
                divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
            }
        }


        protected void grdData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "editt":
                    try
                    {
                        Session["SelectedID"] = null;
                        DataTable groups = (DataTable)Session["Templates"];
                        string id = e.CommandArgument.ToString();
                        Session["SelectedID"] = id;
                        DataRow dr = groups.Select("TASLAKID=" + id)[0];
                        txtSecenekler.Text = dr["SECENEKLER"].ToString();
                        txtPuanlama.Text = dr["PUANLAMA"].ToString();
                        divResult.InnerHtml = "<script>$('#divUpdate').modal(); </script>";
                    }
                    catch
                    {
                    }
                    break;

                case "remove":
                    try
                    {
                        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
                        try
                        {
                            conn.Open();
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Taslak Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

                        }
                        SqlCommand cmd = new SqlCommand("", conn);
                        int result = 0;
                        cmd.CommandText = "delete from PUANTASLAKLAR where TASLAKID=@id";
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = e.CommandArgument;
                        result = cmd.ExecuteNonQuery();
                        hResult.InnerHtml = "Kayıt Silindi";
                        bResult.InnerHtml = "Şablon kaydı başarıyla silindi.";
                        divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                        LoadData();
                    }
                    catch (Exception ex)
                    {
                        hResult.InnerHtml = "Kayıt Silinemedi";
                        bResult.InnerHtml = "Şablon kaydı silme başarısız oldu. Hata: " + ex.ToString();
                        divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";
                    }
                    break;
                default: break;

            }
        }
    }
}