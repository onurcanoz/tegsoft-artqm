﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="EvalSummary.aspx.cs" Inherits="ARTQM.EvalSummary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="form-horizontal">
<fieldset>

<legend>Arama Kriterleri</legend>

   
    
      <div class="row"> 
          
          <div class="form-group">
    
     <label class="col-sm-1 control-label" for="drpGroup">Operatör</label>
    
  <div class="col-sm-2">
  	<asp:DropDownList runat="server" id="drpGroup" ClientIDMode="Static" name="drpGroup" class="form-control input-sm"/>
  
      
  </div>
  
 
    
     <label class="col-sm-1 control-label" for="dtStart">Görüşme Tarihi</label>
  <div class="col-sm-3">
  	<asp:TextBox runat="server" id="txtRange" ClientIDMode="Static" name="txtRange" placeholder="" class="form-control input-sm"/>
      
  </div>
  
     <label class="col-sm-1 control-label" for="dtStart">Süre (sn) </label>
  <div class="col-sm-1">
  	<asp:TextBox runat="server" id="txtStart" ClientIDMode="Static" name="txtStart" placeholder="" class="form-control input-sm" Text="-1"/>
      
  </div>

  <div class="col-sm-1">
  	<asp:TextBox runat="server" id="txtEnd" ClientIDMode="Static" name="txtStart" placeholder="" class="form-control input-sm" Text="-1"/>
      
  </div>

</div>

</div>
    <div class="row"> 

         <div class="form-group">
      <label class="col-sm-1 control-label" for="drpSkill">Split/Skill</label>
    
  <div class="col-sm-2">
  	<asp:DropDownList runat="server" id="drpSkill" ClientIDMode="Static" name="drpSkill" class="form-control input-sm">
          
          </asp:DropDownList>
      </div>
     <label class="col-sm-1 control-label" for="drpDirection">Yön</label>
    
  <div class="col-sm-2">
  	<asp:DropDownList runat="server" id="drpDirection" ClientIDMode="Static" name="drpDirection" class="form-control input-sm">
          <asp:ListItem Value="0" Text="Tümü" Selected="True"></asp:ListItem>
          <asp:ListItem Value="1" Text="Gelen"></asp:ListItem>
          <asp:ListItem Value="2" Text="Giden"></asp:ListItem>
          </asp:DropDownList>
  
      
  </div>
  <label class="col-sm-1 control-label" for="drpForm">Değ. Formu</label>
    
  <div class="col-sm-3">
  	<asp:DropDownList runat="server" id="drpForm" ClientIDMode="Static" name="drpForm" class="form-control input-sm">
          </asp:DropDownList>
  
      
  </div>
            <div class="form-group">
  <label class="col-sm-2 control-label" for="btnSave"></label>
  <div class="col-sm-1" style="float:right;margin-top:-12px;">
    <asp:Button id="Button2" runat="server" class="btn btn-success" Text="Ara" OnClick="btnSearch_Click" />
  </div>
</div>
 
</div>
 
</div>
      

   
    <legend>Değerlendirmeler Özeti</legend>
     <div class="form-group" style="margin-left:0px;">    
    <div class="pull-left">
    <asp:LinkButton ID="Button1" runat="server" CssClass="btn btn-success"  OnClick="Button1_Click" data-toggle="tooltip" data-placement="bottom" title="Excel' e Aktar"><i class="fa fa-file-excel-o" aria-hidden="true"></i></asp:LinkButton>
            </div>
         <div class="pull-left"><h3> 
        <span class="label label-danger" id="spanInfo" runat="server"></span></h3></div>
      </div>
      <div class="form-group">
  
  <div class="col-sm-12">
 <asp:GridView ID="grdData" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-responsive table-striped table-totals" 
     RowStyle-HorizontalAlign="Center"
     HeaderStyle-HorizontalAlign="Center"
     AllowPaging="False">
     <Columns>
          <asp:BoundField HeaderText="Görüşme Tarihi"  DataField="REPORTDATE" ItemStyle-CssClass="col-sm-1" HeaderStyle-HorizontalAlign="Center">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle CssClass="col-sm-1"></ItemStyle>
         </asp:BoundField>


         <asp:BoundField HeaderText="Skill"  DataField="ACDGROUP" ItemStyle-CssClass="col-sm-2" HeaderStyle-HorizontalAlign="Center">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle CssClass="col-sm-2"></ItemStyle>
         </asp:BoundField>
         <asp:BoundField HeaderText="Operatörler"  DataField="AGENTS" ItemStyle-CssClass="col-sm-2" HeaderStyle-HorizontalAlign="Center">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle CssClass="col-sm-2"></ItemStyle>
         </asp:BoundField>
         <asp:BoundField HeaderText="Çağrı Adedi"  DataField="CAGRI" ItemStyle-CssClass="col-sm-1" HeaderStyle-HorizontalAlign="Center">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle CssClass="col-sm-1"></ItemStyle>
         </asp:BoundField>
         <asp:BoundField HeaderText="Değ. Adedi"  DataField="DEGERLENDIRME" ItemStyle-CssClass="col-sm-1" HeaderStyle-HorizontalAlign="Center">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle CssClass="col-sm-1"></ItemStyle>
         </asp:BoundField>
     </Columns>
      </asp:GridView>
    
  </div>



</div>
    </fieldset>
  
    <script>
        $(function () {
            $('#txtRange').daterangepicker(
                {
                    locale: {
                        "format": "DD.MM.YYYY",
                        "separator": " - ",
                        "applyLabel": "Tamam",
                        "cancelLabel": "Temizle",
                        "fromLabel": "Başlangıç",
                        "toLabel": "Bitiş",
                        "customRangeLabel": "Custom",
                        "weekLabel": "H",
                        "daysOfWeek": [
                            "Pz",
                            "Pt",
                            "Sa",
                            "Ça",
                            "Pe",
                            "Cu",
                            "Ct"
                        ],
                        "monthNames": [
                            "Ocak",
                            "Şubat",
                            "Mart",
                            "Nisan",
                            "Mayıs",
                            "Haziran",
                            "Temmuz",
                            "Ağustos",
                            "Eylül",
                            "Ekim",
                            "Kasım",
                            "Aralık"
                        ],
                        "firstDay": 1

                    }
                });
        });
</script>
      </div>

</asp:Content>
