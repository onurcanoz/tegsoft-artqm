﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Configuration;

namespace ARTQM
{
    public class Reports
    {
        public static DataTable GetCWCReport(string start, string end, string Groups)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand("select convert(nvarchar, CREATEDATETIME, 104) as CWCDATE, dbo.[FN_GETFULLCWCNAME](CWCNAME) as CWCNAME, COUNT(CWCNAME) as TOPLAM from VDNCWCData "
                                    +" where CREATEDATETIME between convert(datetime, '"+start+"', 104) and convert(datetime, '"+end+"', 104) "
                                    + (Groups== ""? "": " and CWCNAME in (select CWCID from VDNCWCList where VDN in (select VDN from VDNCWC where CWCVDN in ("+Groups+")))")
                                    + " group by convert(nvarchar, CREATEDATETIME, 104),  dbo.[FN_GETFULLCWCNAME](CWCNAME) "
                                    + " order by convert(nvarchar, CREATEDATETIME, 104), dbo.[FN_GETFULLCWCNAME](CWCNAME)", conn);
                cmd.CommandType = CommandType.Text;
                

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable GetAgentCWCReport(string start, string end, string Groups)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand("select AGENTID,  dbo.FN_GETAGENTNAME(AGENTID) as AGENTNAME, dbo.[FN_GETFULLCWCNAME](CWCNAME) as CWCNAME, COUNT(CWCNAME) as TOPLAM from VDNCWCData "
                                    + " where CREATEDATETIME between convert(datetime, '" + start + "', 104) and convert(datetime, '" + end + "', 104) "
                                    + (Groups == "" ? "" : " and CWCNAME in (select CWCID from VDNCWCList where VDN in (" + Groups + "))")
                                    + " group by AGENTID, dbo.FN_GETAGENTNAME(AGENTID) , dbo.[FN_GETFULLCWCNAME](CWCNAME)  order by  AGENTID, dbo.[FN_GETFULLCWCNAME](CWCNAME)", conn);
                cmd.CommandType = CommandType.Text;


                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }


        public static DataSet DashboardTotals(int yil1, int donem1, int yil2, int donem2, string PROJENO)
        {
            return null; 

            DataTable ioCalls = new DataTable("IOCALLS");
            #region Sonuc tablosu hazırlanıyor

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            if (con.State == ConnectionState.Closed)
                con.Open();
            DataTable evals = new DataTable("EVALS");


            try
            {
                SqlDataAdapter da = new SqlDataAdapter("select Partyagentnames, COUNT(distinct Callid) as CALLS from CallData where Startdate >= '" + new DateTime(yil1, donem1, 1).ToString("yyyy.MM.dd HH:mm:ss") + "' and Startdate <= '" + new DateTime(yil2, donem2, 1).AddMonths(1).AddSeconds(-1).ToString("yyyy.MM.dd HH:mm:ss") + "' group by Partyagentnames", con);
                da.SelectCommand.CommandType = CommandType.Text;

                da.Fill(ioCalls);
            
                ICUser user = (ICUser)System.Web.HttpContext.Current.Session["CurrentUser"];


                da.SelectCommand.CommandText = "select Partyagentnames as LOGID, count(ANKETNO) as EVALS from ANKETLER inner join CallData on ANKETLER.ANKETNO = CallData.Callid where (DATAYIL*100+DATAAY between " + yil1 + donem1.ToString().PadLeft(2, '0') + " and " + yil2 + donem2.ToString().PadLeft(2, '0') + ") "
                    +(PROJENO != "0" ? " AND PROJENO="+PROJENO : "" ) +

                    (user.userLevel != 0 ? " and LOGID IN (" + System.Web.HttpContext.Current.Session["userAgents"] + ") " : "") +

                    " group by Partyagentnames";
                da.Fill(evals);
            }
            catch
            { }
            finally
            {
                con.Close();
            }
            #endregion
            DataSet sonuc = new DataSet();
            sonuc.Tables.Add(ioCalls);
            sonuc.Tables.Add(evals);
            return sonuc;
        }
        public static DataTable AylikToplamRaporAlfabetik(int proje, string skill, string agents, int yil1, int donem1, int yil2, int donem2, string soruStr)
        {
            float __toplam = 0;
            int __sayac = 0;
            DataTable sonuc = new DataTable();
            string alan = "YANITR", skor_alan = "GENELCSI";

            #region Sonuc tablosu hazırlanıyor
            sonuc.Columns.Add("SORU");
            // sonuc.Columns.Add("Gozlem");
            sonuc.Columns.Add("Ulusal");
            sonuc.Columns["Ulusal"].Caption = "Genel";


            string sureSql = "";
            DataTable kriterler = Lists.GetTitles(proje);
            sureSql = " (DATAYIL*100+CAST(DATAAY AS INT) between " + yil1 + donem1.ToString().PadLeft(2, '0') + " AND " + yil2 + donem2.ToString().PadLeft(2, '0') + ")";

            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            DataTable olcek_degerler = new DataTable();
            DataTable kriter_degerler = new DataTable();
            DataTable etiketler = new DataTable();
            if (con.State == ConnectionState.Closed)
                con.Open();
            SqlDataAdapter da = new SqlDataAdapter("", con);


            DataTable genel_skorlar = new DataTable();
            da.SelectCommand.CommandText = "SELECT ANKETLER.PARTYAGENTNAMES , AVG(" + skor_alan + ") AS SKOR, COUNT(*) AS ZIYARET FROM ANKETLER WHERE PROJENO=" + proje + " AND IPTAL  <> 1 and " + sureSql + " " + (agents == "" ? "" : " and ANKETLER.LOGID IN (" + agents + ") ") + "  group by ANKETLER.PARTYAGENTNAMES ORDER BY ANKETLER.PARTYAGENTNAMES";
            da.Fill(genel_skorlar);




            da.SelectCommand.CommandText = "select A.PARTYAGENTNAMES, AD.SORUNO, round(avg(" + alan + "), 8) as VERI from ANKETDETAY  AD INNER JOIN ANKETLER A ON AD.ANKETID = A.ANKETID WHERE A.IPTAL = 0  AND " + sureSql + (agents == "" ? "" : " and A.LOGID  IN (" + agents + ") ") + " AND AD.SORUNO IN (SELECT SORUNO FROM SORULAR WHERE PROJENO = " + proje + "  " + (soruStr == "" ? "" : " AND SORUNO IN (" + soruStr + ")") + ")"
                      + " group by A.PARTYAGENTNAMES, AD.SORUNO";
            da.Fill(olcek_degerler);



            da.SelectCommand.CommandText = "select DISTINCT CK.KRITERNO, A.PARTYAGENTNAMES,  ROUND(AVG(VERI), 8) as SKOR from CARSAF_KRITER CK INNER JOIN ANKETLER A ON CK.ANKETNO = A.ANKETID WHERE A.IPTAL <> 1 AND A.PROJENO = " + proje + " AND " + sureSql.Replace("DATAYIL", "A.DATAYIL").Replace("DATAAY", "A.DATAAY") + "  "
            + (agents == "" ? "" : " AND A.LOGID IN (" + agents + ") ")
            + " GROUP BY CK.KRITERNO, A.PARTYAGENTNAMES ORDER BY CK.KRITERNO, A.PARTYAGENTNAMES";
            da.Fill(kriter_degerler);


            /*turkiye geneli degerleri */

            DataTable ulusal_skorlar = new DataTable();
            da.SelectCommand.CommandText = "SELECT SORUNO, ROUND(AVG(" + alan + "),10) as SKOR"
            + " FROM ANKETDETAY"
            + " WHERE ANKETID IN (SELECT ANKETID FROM ANKETLER WHERE PROJENO=" + proje + " AND IPTAL = 0 AND  " + sureSql + " " + (agents == "" ? "" : " and LOGID IN (" + agents + ")")
            + ") GROUP BY SORUNO ORDER BY SORUNO";
            da.Fill(ulusal_skorlar);

            DataTable ulusal_kriterler = new DataTable();


            da.SelectCommand.CommandText = "select KRITERNO, round( AVG(VERI),10) as SKOR from CARSAF_KRITER WHERE  ANKETNO IN (SELECT ANKETID FROM ANKETLER A WHERE IPTAL = 0 and PROJENO = " + proje + " AND " + sureSql + " "
            + (agents == "" ? "" : " AND A.LOGID IN (" + agents + ") ") + " )"
            + " GROUP BY KRITERNO ORDER BY KRITERNO ";
            da.Fill(ulusal_kriterler);



            DataTable genel_skorlar_tg = new DataTable();
            da.SelectCommand.CommandText = "SELECT  COUNT(*) as CSI, "
            + "  round(AVG(" + skor_alan + "), 10) AS  [Genel CSI]  "
            + " FROM ANKETLER WHERE PROJENO=" + proje + " AND IPTAL <> 1 AND  " + sureSql;
            da.Fill(genel_skorlar_tg);

            DataProvider obj = new DataProvider();
            
            DataTable kisiler = obj.getUserList("SELECT UID AS LOGID, USERCODE AS AD FROM TBLUSERS WHERE UID IN ("+agents+")");
            


            string tip = "";
            foreach (DataRow row in kisiler.Rows)
            {
                try
                {
                    //modification is here
                    sonuc.Columns.Add(row["AD"].ToString().Replace("-","''"));
                    sonuc.Columns[row["AD"].ToString()].Caption = "<table border=0 style=\"width:100px; \"><tr><td style=\"text-align:center;border:none;font-size:18px; border-bottom: solid 1px #303030; height:20px;\">" + row["AD"].ToString() + "</td></tr></table>";
                }
                catch
                { }
            }


            foreach (DataRow k in kriterler.Rows)
            {

                DataRow dr = sonuc.NewRow();


                DataRow[] _ustskor = kriter_degerler.Select("KRITERNO='" + k["KRITERNO"].ToString() + "'");
                dr[0] = "***" + k["KRITER"].ToString(); // gorselde renk degısımı ıcın *** lara bakıyoruz

                //if (int.Parse(k["AGIRLIK"].ToString()) > 0)
                //{
                try
                {
                    dr["Ulusal"] = String.Format("{0:0.00}", Math.Round(PageBase.parse2Double(ulusal_kriterler.Select("KRITERNO=" + k["KRITERNO"].ToString() + "")[0]["SKOR"].ToString()), 2, MidpointRounding.AwayFromZero)).Replace(",", ".");
                }
                catch
                { }

                foreach (DataRow _skor in _ustskor)
                {
                    try
                    {
                        dr[_skor["PARTYAGENTNAMES"].ToString()] = String.Format("{0:0.00}", Math.Round(PageBase.parse2Double(_skor["SKOR"].ToString()), 2, MidpointRounding.AwayFromZero));
                    }
                    catch
                    {
                    }
                }
                //}
                DataTable sorular = Lists.GetQuestions2(proje, k["KRITERNO"].ToString());
                if (sorular.Rows.Count != 0)
                {
                    sonuc.Rows.Add(dr);
                    string sorusira = "";

                    //foreach (DataRow S in sorular.Rows)
                    //{
                    //    //Ulusal sutunu degerlerı cek
                    //    if (int.Parse(S["EVETHAYIR"].ToString()) < 0) continue;
                    //    DataRow TEMP = sonuc.NewRow();
                    //    if (S["SORUSIRA"].ToString() != sorusira)
                    //    {
                    //        if (!S["SORUSIRA"].ToString().Contains("."))
                    //            TEMP[0] = "**" + S["KISAETIKET"].ToString(); // + (S["SKORADAHIL"].ToString() == "0" ? "(Skora dahil değildir.)" : "");
                    //        else
                    //            TEMP[0] = S["KISAETIKET"].ToString();// +(S["SKORADAHIL"].ToString() == "0" ? "(Skora dahil değildir.)" : "");
                    //    }
                    //    else if (S["SORUSIRA"].ToString().Contains("."))
                    //    {
                    //        continue;
                    //    }
                    //    else
                    //    {
                    //        TEMP[0] = S["KISAETIKET"].ToString();
                    //    }

                    //    sorusira = S["SORUSIRA"].ToString();


                    //    {
                    //        if (olcek_degerler.Select("SORUNO=" + S["SORUNO"].ToString()).Length == 0)
                    //        {

                    //            sonuc.Rows.Add(TEMP);
                    //            continue;
                    //        }
                    //        __toplam = 0;
                    //        __sayac = 0;

                    //        //modification
                    //        foreach (DataColumn col in sonuc.Columns)
                    //        {
                    //            if (col.Ordinal <= 1) continue;
                    //            //sonra aylık veriler cekiliyor
                    //            try
                    //            {
                    //                DataRow bayi_deger = olcek_degerler.Select("PARTYAGENTNAMES = '"+col.ColumnName+ "' AND SORUNO=" + S["SORUNO"].ToString())[0];
                    //                //   DataRow bayi_sayac = olcek_sayaclar.Select("BAYI='" + col.ColumnName + "' AND SORUNO=" + S.SORUNO)[0];
                    //                //  __sayac += int.Parse(bayi_sayac["SAYAC"].ToString());
                    //                if (bayi_deger["VERI"] == null || bayi_deger["VERI"].ToString() == "")
                    //                    TEMP[col.ColumnName] = "";
                    //                else
                    //                    TEMP[col.ColumnName] = String.Format("{0:0.00}", Math.Round((PageBase.parse2Float(bayi_deger["VERI"].ToString())), 2, MidpointRounding.AwayFromZero));
                    //            }
                    //            catch(Exception Ex)
                    //            {
                    //            }
                    //        }
                    //        try
                    //        {
                    //            TEMP["Ulusal"] = String.Format("{0:0.00}", Math.Round(PageBase.parse2Double(ulusal_skorlar.Select("SORUNO=" + S["SORUNO"].ToString() + "")[0]["SKOR"].ToString()), 2, MidpointRounding.AwayFromZero)).Replace(",", ".");

                    //            string TTEMP = "";
                    //            foreach (DataRow k0 in kriterler.Rows)
                    //            {
                    //                if (k0["KRITERNO"] == S["KRITERNO"])
                    //                {
                    //                    TTEMP = "***" + k0["KRITER"];
                    //                    break;
                    //                }
                    //            }
                    //        }
                    //        catch
                    //        { }
                    //        sonuc.Rows.Add(TEMP);
                    //    }

                    //}
                }
            }


            DataRow genelcsi = sonuc.NewRow();
            DataRow ziyaret = sonuc.NewRow();
            ziyaret[0] = "Değ. Adedi";
            genelcsi[0] = "GENEL";

            foreach (DataRow skor in genel_skorlar.Rows)
            {
                try
                {
                    genelcsi[skor["PARTYAGENTNAMES"].ToString()] = String.Format("{0:0.000000}", Math.Round(PageBase.parse2Double(skor["SKOR"].ToString()), 6, MidpointRounding.AwayFromZero)).Replace(",", ".");
                }
                catch
                { }
                try
                {
                    ziyaret[skor["PARTYAGENTNAMES"].ToString()] = int.Parse(skor["ZIYARET"].ToString());
                }
                catch
                { }
            }
            try
            {
                ziyaret["Ulusal"] = genel_skorlar_tg.Rows[0][0].ToString();
                genelcsi["Ulusal"] = genel_skorlar_tg.Rows[0][1].ToString();
            }
            catch
            {
            }

            #endregion

            sonuc.Rows.InsertAt(genelcsi, 0);
            sonuc.Rows.InsertAt(ziyaret, 0);
            sonuc.Columns["SORU"].Caption = "<table border=0 style=\"width:100px;font-family: arial; font-size:9px;\"><tr><td style=\"text-align:center;border:none; border-bottom: solid 1px #303030; \">&nbsp;</td></tr> <tr><td style=text-align:center;border:none;height:26px;'>&nbsp;</td></tr></table>";


            sonuc.Columns[1].Caption = "<table border=0 style=\"width:100px; font-family: arial; font-size:18px;\"><tr rowspan=2><td style=\"text-align:center;border:none; border-bottom: solid 1px #303030; height:26px;direction:rtl;mso-rotate:90;\">GENEL</td></tr></table>";
            return sonuc;
        }



        public static DataTable AylikToplamRaporSkill(int proje, string skill, string agents, int yil1, int donem1, int yil2, int donem2, string soruStr)
            {
                float __toplam = 0;
                int __sayac = 0;
                DataTable sonuc = new DataTable();
                string alan = "YANITR", skor_alan = "GENELCSI";

                #region Sonuc tablosu hazırlanıyor
                sonuc.Columns.Add("SORU");
                // sonuc.Columns.Add("Gozlem");
                sonuc.Columns.Add("Ulusal");
                sonuc.Columns["Ulusal"].Caption = "Genel";


                string sureSql = "";
                DataTable kriterler = Lists.GetTitles(proje);
                sureSql = " (DATAYIL*100+CAST(DATAAY AS INT) between " + yil1 + donem1.ToString().PadLeft(2, '0') + " AND " + yil2 + donem2.ToString().PadLeft(2, '0') + ")";

                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
                DataTable olcek_degerler = new DataTable();
                DataTable kriter_degerler = new DataTable();
                DataTable etiketler = new DataTable();
                if (con.State == ConnectionState.Closed)
                    con.Open();
                SqlDataAdapter da = new SqlDataAdapter("", con);


                DataTable genel_skorlar = new DataTable();
                da.SelectCommand.CommandText = "SELECT ANKETLER.LOGID , AVG(" + skor_alan + ") AS SKOR, COUNT(*) AS ZIYARET FROM ANKETLER WHERE PROJENO=" + proje + " AND IPTAL  <> 1 and " + sureSql + " " + (agents == "" ? "" : " and ANKETLER.LOGID IN (" + agents + ") ") + "  group by ANKETLER.LOGID ORDER BY ANKETLER.LOGID";
                da.Fill(genel_skorlar);




                da.SelectCommand.CommandText = "select A.LOGID, AD.SORUNO, round(avg(" + alan + "), 8) as VERI from ANKETDETAY  AD INNER JOIN ANKETLER A ON AD.ANKETID = A.ANKETID WHERE A.IPTAL = 0  AND " + sureSql + (agents == "" ? "" : " and A.LOGID  IN (" + agents + ") ") + " AND AD.SORUNO IN (SELECT SORUNO FROM SORULAR WHERE PROJENO = " + proje + "  " + (soruStr == "" ? "" : " AND SORUNO IN (" + soruStr + ")") + ")"
                          + " group by A.LOGID , AD.SORUNO";
                da.Fill(olcek_degerler);



                da.SelectCommand.CommandText = "select DISTINCT CK.KRITERNO, A.LOGID,  ROUND(AVG(VERI), 8) as SKOR from CARSAF_KRITER CK INNER JOIN ANKETLER A ON CK.ANKETNO = A.ANKETID WHERE A.IPTAL <> 1 AND A.PROJENO = " + proje + " AND " + sureSql.Replace("DATAYIL", "A.DATAYIL").Replace("DATAAY", "A.DATAAY") + "  "
                + (agents == "" ? "" : " AND A.LOGID IN (" + agents + ") ")
                + " GROUP BY CK.KRITERNO, A.LOGID ORDER BY CK.KRITERNO, A.LOGID";
                da.Fill(kriter_degerler);


                /*turkiye geneli degerleri */

                DataTable ulusal_skorlar = new DataTable();
                da.SelectCommand.CommandText = "SELECT SORUNO, ROUND(AVG(" + alan + "),10) as SKOR"
                + " FROM ANKETDETAY"
                + " WHERE ANKETID IN (SELECT ANKETID FROM ANKETLER WHERE PROJENO=" + proje + " AND IPTAL = 0 AND  " + sureSql + " " + (agents == "" ? "" : " and LOGID IN (" + agents + ")")
                + ") GROUP BY SORUNO ORDER BY SORUNO";
                da.Fill(ulusal_skorlar);

                DataTable ulusal_kriterler = new DataTable();


                da.SelectCommand.CommandText = "select KRITERNO, round( AVG(VERI),10) as SKOR from CARSAF_KRITER WHERE  ANKETNO IN (SELECT ANKETID FROM ANKETLER A WHERE IPTAL = 0 and PROJENO = " + proje + " AND " + sureSql + " "
                + (agents == "" ? "" : " AND A.LOGID IN (" + agents + ") ") + " )"
                + " GROUP BY KRITERNO ORDER BY KRITERNO ";
                da.Fill(ulusal_kriterler);



                DataTable genel_skorlar_tg = new DataTable();
                da.SelectCommand.CommandText = "SELECT  COUNT(*) as CSI, "
                + "  round(AVG(" + skor_alan + "), 10) AS  [Genel CSI]  "
                + " FROM ANKETLER WHERE PROJENO=" + proje + " AND IPTAL <> 1 AND  " + sureSql;
                da.Fill(genel_skorlar_tg);


                DataTable kisiler = new DataTable();
                da.SelectCommand.CommandText = "select ItemCode as LOGID, ItemName as AD from ARTCOMMONS.dbo.AGENTS " + (agents == "" ? "" : " where ItemCode in (" + agents + ") ") + " ORDER BY ItemName";/**/
                da.Fill(kisiler);


                string tip = "";
                foreach (DataRow row in kisiler.Rows)
                {
                    try
                    {
                        //modification is here
                        sonuc.Columns.Add(row["LOGID"].ToString());
                        sonuc.Columns[row["LOGID"].ToString()].Caption = "<table border=0 style=\"width:100px; \"><tr><td style=\"text-align:center;border:none; border-bottom: solid 1px #303030; height:20px;\">" + row["AD"].ToString() + "</td></tr> <tr><td style=text-align:center;border:none;height:26px;direction:rtl;mso-rotate:90;'>" + row["LOGID"].ToString() + "</td></tr></table>";
                    }
                    catch
                    { }
                }


                foreach (DataRow k in kriterler.Rows)
                {

                    DataRow dr = sonuc.NewRow();


                    DataRow[] _ustskor = kriter_degerler.Select("KRITERNO='" + k["KRITERNO"].ToString() + "'");
                    dr[0] = "***" + k["KRITER"].ToString(); // gorselde renk degısımı ıcın *** lara bakıyoruz

                    //if (int.Parse(k["AGIRLIK"].ToString()) > 0)
                    //{
                    try
                    {
                        dr["Ulusal"] = String.Format("{0:0.00}", Math.Round(PageBase.parse2Double(ulusal_kriterler.Select("KRITERNO=" + k["KRITERNO"].ToString() + "")[0]["SKOR"].ToString()), 2, MidpointRounding.AwayFromZero)).Replace(",", ".");
                    }
                    catch
                    { }

                    foreach (DataRow _skor in _ustskor)
                    {
                        try
                        {
                            dr[_skor["PARTYAGENTNAMES"].ToString()] = String.Format("{0:0.00}", Math.Round(PageBase.parse2Double(_skor["SKOR"].ToString()), 2, MidpointRounding.AwayFromZero));
                        }
                        catch
                        {
                        }
                    }
                    //}
                    DataTable sorular = Lists.GetQuestions2(proje, k["KRITERNO"].ToString());
                    if (sorular.Rows.Count != 0)
                    {
                        sonuc.Rows.Add(dr);
                        string sorusira = "";

                        foreach (DataRow S in sorular.Rows)
                        {
                            //Ulusal sutunu degerlerı cek
                            if (int.Parse(S["EVETHAYIR"].ToString()) < 0) continue;
                            DataRow TEMP = sonuc.NewRow();
                            if (S["SORUSIRA"].ToString() != sorusira)
                            {
                                if (!S["SORUSIRA"].ToString().Contains("."))
                                    TEMP[0] = "**" + S["KISAETIKET"].ToString(); // + (S["SKORADAHIL"].ToString() == "0" ? "(Skora dahil değildir.)" : "");
                                else
                                    TEMP[0] = S["KISAETIKET"].ToString();// +(S["SKORADAHIL"].ToString() == "0" ? "(Skora dahil değildir.)" : "");
                            }
                            else if (S["SORUSIRA"].ToString().Contains("."))
                            {
                                continue;
                            }
                            else
                            {
                                TEMP[0] = S["KISAETIKET"].ToString();
                            }

                            sorusira = S["SORUSIRA"].ToString();


                            {
                                if (olcek_degerler.Select("SORUNO=" + S["SORUNO"].ToString()).Length == 0)
                                {

                                    sonuc.Rows.Add(TEMP);
                                    continue;
                                }
                                __toplam = 0;
                                __sayac = 0;

                                //modification
                                foreach (DataColumn col in sonuc.Columns)
                                {
                                    if (col.Ordinal <= 1) continue;
                                    //sonra aylık veriler cekiliyor
                                    try
                                    {
                                        DataRow bayi_deger = olcek_degerler.Select("LOGID=" + col.ColumnName + " AND SORUNO=" + S["SORUNO"].ToString())[0];
                                        //   DataRow bayi_sayac = olcek_sayaclar.Select("BAYI='" + col.ColumnName + "' AND SORUNO=" + S.SORUNO)[0];
                                        //  __sayac += int.Parse(bayi_sayac["SAYAC"].ToString());
                                        if (bayi_deger["VERI"] == null || bayi_deger["VERI"].ToString() == "")
                                            TEMP[col.ColumnName] = "";
                                        else
                                            TEMP[col.ColumnName] = String.Format("{0:0.00}", Math.Round((PageBase.parse2Float(bayi_deger["VERI"].ToString())), 2, MidpointRounding.AwayFromZero));
                                    }
                                    catch
                                    {
                                    }
                                }
                                try
                                {
                                    TEMP["Ulusal"] = String.Format("{0:0.00}", Math.Round(PageBase.parse2Double(ulusal_skorlar.Select("SORUNO=" + S["SORUNO"].ToString() + "")[0]["SKOR"].ToString()), 2, MidpointRounding.AwayFromZero)).Replace(",", ".");

                                    string TTEMP = "";
                                    foreach (DataRow k0 in kriterler.Rows)
                                    {
                                        if (k0["KRITERNO"] == S["KRITERNO"])
                                        {
                                            TTEMP = "***" + k0["KRITER"];
                                            break;
                                        }
                                    }
                                }
                                catch
                                { }
                                sonuc.Rows.Add(TEMP);
                            }

                        }
                    }
                }


                DataRow genelcsi = sonuc.NewRow();
                DataRow ziyaret = sonuc.NewRow();
                ziyaret[0] = "Değ. Adedi";
                genelcsi[0] = "GENEL";

                foreach (DataRow skor in genel_skorlar.Rows)
                {
                    try
                    {
                        genelcsi[skor["LOGID"].ToString()] = String.Format("{0:0.000000}", Math.Round(PageBase.parse2Double(skor["SKOR"].ToString()), 6, MidpointRounding.AwayFromZero)).Replace(",", ".");
                    }
                    catch
                    { }
                    try
                    {
                        ziyaret[skor["LOGID"].ToString()] = int.Parse(skor["ZIYARET"].ToString());
                    }
                    catch
                    { }
                }
                try
                {
                    ziyaret["Ulusal"] = genel_skorlar_tg.Rows[0][0].ToString();
                    genelcsi["Ulusal"] = genel_skorlar_tg.Rows[0][1].ToString();
                }
                catch
                {
                }

                #endregion

                sonuc.Rows.InsertAt(genelcsi, 0);
                sonuc.Rows.InsertAt(ziyaret, 0);
                sonuc.Columns["SORU"].Caption = "<table border=0 style=\"width:100px;font-family: arial; font-size:9px;\"><tr><td style=\"text-align:center;border:none; border-bottom: solid 1px #303030; \">&nbsp;</td></tr> <tr><td style=text-align:center;border:none;height:26px;'>&nbsp;</td></tr></table>";


                sonuc.Columns[1].Caption = "<table border=0 style=\"width:100px; font-family: arial; font-size:9px;\"><tr rowspan=2><td style=\"text-align:center;border:none; border-bottom: solid 1px #303030; height:26px;direction:rtl;mso-rotate:90;\">GENEL</td></tr></table>";
                return sonuc;
            }
    }
}