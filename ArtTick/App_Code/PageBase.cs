﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ARTQM
{
    public partial class PageBase : System.Web.UI.Page
    {
        Tools T = new Tools();
        protected override void OnPreLoad(EventArgs e)
        {
            if (CurrentUser == null)
            {
                var returnUrl = Server.UrlEncode(Request.Url.PathAndQuery);
                Response.Redirect("~/Login.aspx?ReturnURL=" + returnUrl, true);
                //Response.Redirect("Login.aspx", true);
            }
            base.OnPreLoad(e);
        }
        public string ANI
        {
            get
            {
                if (String.IsNullOrEmpty(Request.QueryString["ani"])) return "";
                else return ProcessANI( Request.QueryString["ani"]);
            }
        }
        public string CALLID
        {
            get
            {
                if (String.IsNullOrEmpty(Request.QueryString["callid"])) return "";
                else return Request.QueryString["callid"];
            }
        }

        public string UCID
        {
            get
            {
                if (String.IsNullOrEmpty(Request.QueryString["ucid"])) return "";
                else return Request.QueryString["ucid"];
            }
        }
        public string AGENT
        {
            get
            {
                if (String.IsNullOrEmpty(Request.QueryString["agent"])) return "";
                else return Request.QueryString["agent"];
            }
        }
        public string ProcessANI(string ani)
        {
            if (ani.Length > 10)
                ani = ani.Substring(ani.Length - 10);

            return ani;

        }
        public ICUser CurrentUser
        {
            get {
                try
                {
                    return (ICUser)Session["CurrentUser"];
                }
                catch
                {
                    Response.Redirect("Login.aspx", true);
                    return null;
                }
            }
            set
            {
                Session["CurrentUser"] = value; 
            }
        }
         public DataTable TicketSources
         {
             get
             {
                 return (DataTable)Session["TicketSources"];
             }
         }
         public  void LoadList(System.Web.UI.WebControls.DropDownList dest, string Sql, string ValueField, string DisplayField, bool EmptySelection)
         {

             SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
             try
             {
                 conn.Open();
             }
             catch (Exception ex)
             {
                 throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

             }
             DataTable dt = new DataTable();

             try
             {
                 dest.Items.Clear();
                 SqlCommand cmd = new SqlCommand(Sql, conn);
                 cmd.CommandType = CommandType.Text;
                 SqlDataAdapter da = new SqlDataAdapter(cmd);
                 da.Fill(dt);

                 

                 dest.DataSource = dt;
                 dest.DataValueField = ValueField;
                 dest.DataTextField = DisplayField;
                 dest.DataBind();

                 if (EmptySelection)
                 {
                     dest.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Seçiniz--", "0"));
                 }

             }
             catch (Exception ex)
             {
                 throw new Exception("Liste Hata: " + ex.ToString());
             }
             finally
             {
                 conn.Close();
             }
         }

       
    
         public void LoadProjects(System.Web.UI.WebControls.DropDownList dest, bool EmptySelection)
         {

             DataTable projects = (DataTable)Application["Kampanyalar"];
             dest.Items.Clear();
             
             try
             {

                 
                 
                 for (int i = 0; i < projects.Rows.Count; i++)

                     dest.Items.Add(new System.Web.UI.WebControls.ListItem(projects.Rows[i]["GORUNENAD"].ToString(), projects.Rows[i]["KODU"].ToString()));

                 
                 if (EmptySelection)
                 {
                     dest.Items.Insert(0, new System.Web.UI.WebControls.ListItem("--Seçiniz--", "0"));
                 }


             }
             catch (Exception ex)
             {
                 throw new Exception("Liste Hata: " + ex.ToString());
             }
             finally
             {

             }
         }

         public string GetTicketDuration(DateTime start, string State)
         {
             TimeSpan duration = ((TimeSpan)(DateTime.Today - start));
             if (State == TicketStatus.ASSIGNED.ToString() || State == TicketStatus.PENDING.ToString())
             {
                 if (duration.Days < 5) {
                     if (duration.Days < 1) 
                         return "<span class='label'><"+duration.Days.ToString() + "gün</span>";
                     else return "<span class='label'><" + duration.Days.ToString() + "gün</span>";
                     }
                 else
                     if (duration.Days < 15) 
                        return "<span class='label label-warning'>" + duration.Days.ToString() + "gün</span>";
                    else
                         return "<span class='label label-danger'>" + duration.Days.ToString() + "gün</span>";
             }
             return "";
         }
      
         public string GetCallDirection(string Direction)
         {
             string icon = "";
             if (Direction == "1" || Direction=="Gelen")
                 icon = "<span class='btn btn-xs btn-success' data-toggle='tooltip' title='Gelen'><span class='glyphicon glyphicon glyphicon-arrow-left'></span>Gelen</span>";
             else if (Direction == "2" || Direction == "Giden")
                 icon = "<span class='btn btn-xs btn-danger'  data-toggle='tooltip' title='Giden'><span class='glyphicon glyphicon-arrow-right'></span>Giden</span>";
             else 
                 icon = "<span class='btn btn-xs btn-primary'  data-toggle='tooltip' title='Internal'><span class='glyphicon glyphicon-arrow-up'></span>Internal</span>";
             return icon;
         }
        public string GetFormDisposition(string disposition)
        {
            string icon = "";
            if (disposition == "1")
                icon = "<span class='btn btn-xs btn-success' data-toggle='tooltip' title='Aktif'><span class='glyphicon glyphicon-circle-arrow-up'></span> Aktif</span>";
            else
                icon = "<span class='btn btn-xs btn-danger'  data-toggle='tooltip' title='Pasif'><span class='glyphicon glyphicon-circle-arrow-down'></span> Pasif</span>";
            
            return icon;
        }
        public string GetRecordStatus(string status)
        {
             string icon = "";
             if (status == "True" || status == "1")

                 icon = "<span class='glyphicon glyphicon-ok-circle' data-toggle='tooltip' title='Kullanımda'></span>";
             else
                 icon = "<span class='glyphicon glyphicon-remove-circle' data-toggle='tooltip' title='Kullanım Dışı'></span>";

             return icon;

        }

        public static string Seconds2Span(string seconds)
         {
             try
             {
                 return TimeSpan.FromSeconds(int.Parse(seconds)).ToString();
             }
             catch
             {
                 return "00:00:00";
             }
        }
        public static bool EditEval(string args)
        {
            try
            {
                if (args == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        protected void ExportToExcel(System.Web.UI.WebControls.GridView grdSource, string FileName)
         {
             Response.Clear();
             Response.Buffer = true;
             Response.AddHeader("content-disposition", "attachment;filename="+FileName+".xls");
             Response.ContentEncoding = System.Text.Encoding.Unicode;
             Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());

             Response.Charset = "UTF-8";
             Response.ContentType = "application/vnd.ms-excel";
             using (StringWriter sw = new StringWriter())
             {
                 HtmlTextWriter hw = new HtmlTextWriter(sw);

                 
                 

                 //To Export all pages

                 grdSource.HeaderRow.BackColor = Color.White;
                 foreach (TableCell cell in grdSource.HeaderRow.Cells)
                 {
                     cell.BackColor = grdSource.HeaderStyle.BackColor;
                 }
                 foreach (GridViewRow row in grdSource.Rows)
                 {
                     row.BackColor = Color.White;
                     foreach (TableCell cell in row.Cells)
                     {
                         if (row.RowIndex % 2 == 0)
                         {
                             cell.BackColor = grdSource.AlternatingRowStyle.BackColor;
                         }
                         else
                         {
                             cell.BackColor = grdSource.RowStyle.BackColor;
                         }
                         cell.CssClass = "textmode";
                     }
                 }

                 grdSource.RenderControl(hw);

                 //style to format numbers to string
                 string style = @"<style> .textmode { } </style>";
                 Response.Write(style);
                 Response.Output.Write(sw.ToString());
                 Response.Flush();
                 Response.End();
             }
         }

         public override void VerifyRenderingInServerForm(Control control)
         {
             /* Verifies that the control is rendered */
         }

         public string EtiketBul(string secenekler, string yanit, string evethayir)
         {
             try
             {
                if (evethayir == "")
                    return yanit;

                 if (int.Parse(evethayir) < 0)
                     return yanit;
                 /* if (int.Parse(evethayir) == 1)
                  {
                      if (yanit == "1") return "Evet";
                      else return "Hayır";
                  }
                  else*/
                 {
                     if (secenekler == "" || yanit.Trim() == "") return yanit;
                     try
                     {
                         return secenekler.Split(Properties.Settings.Default.FIELDSEP1)[int.Parse(yanit) - 1];
                     }
                     catch
                     {
                         return yanit;
                     }
                 }
             }
             catch
             {
                 return yanit;
             }
         }
         public string EtiketBul2(string secenekler, string deger, string evethayir)
         {
             try
             {
                 if (int.Parse(evethayir) < 0)
                     return deger;

                 if (deger != "" && deger != null && !String.IsNullOrEmpty(deger))
                 {
                     string[] s_ = secenekler.Split(Properties.Settings.Default.FIELDSEP1);
                     for (int i = 0; i < s_.Length; i++)
                     {
                         if (s_[i].Split(Properties.Settings.Default.FIELDSEP2)[0] == deger)
                             return s_[i].Split(Properties.Settings.Default.FIELDSEP2)[1];
                     }
                 }
                 return "";
             }
             catch
             {
                 return "";
             }
         }
        public bool EtiketBul3(string secenekler, string deger, string evethayir)
        {
            try
            {
                if (evethayir == "")
                    return false;

                if (int.Parse(evethayir) < 0)
                    return false;

                if (deger != "" && deger != null && !String.IsNullOrEmpty(deger))
                {
                    string[] s_ = secenekler.Split(Properties.Settings.Default.FIELDSEP1);
                    for (int i = 0; i < s_.Length; i++)
                    {
                        if (s_[i].Split(Properties.Settings.Default.FIELDSEP2)[0] == deger)
                            return true;
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static  double parse2Double(string val)
         {
             try
             {

                 return double.Parse(val.Replace(",", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator).Replace(".", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator));
             }
             catch
             {
                 return 0;
             }
         }
         public static float parse2Float(string val)
         {
             try
             {
                 return float.Parse(val.Replace(",", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator).Replace(".", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator));
             }
             catch
             {
                 return 0;
             }
         }
    }
}