﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARTQM.App_Code
{
    public class TicketSources
    {
        public int SourceTypeID { get; set; }

        public string SourceType { get; set; }

        public string SourceTypeDesc { get; set; }

        public bool? IsDeleted { get; set; }

    }
}