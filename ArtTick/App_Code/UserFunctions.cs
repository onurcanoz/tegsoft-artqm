﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace ARTQM
{
    public class UserFunctions
    {
        public static string HashPassword(string password)
        {
            var bytes = new UTF8Encoding().GetBytes(password);
            var hashBytes = System.Security.Cryptography.MD5.Create().ComputeHash(bytes);
            return Convert.ToBase64String(hashBytes);
        }
        public static ICUser GetUserObject(int UserID)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Kullanıcı Hata: Veri sunucusuna bağlanılamıyor. " + ex.Message);

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand("SP_GETUSER", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = UserID;
                SqlDataReader rdr = cmd.ExecuteReader();


                if (rdr.Read())
                {
                    ICUser userInfo = new ICUser();
                    userInfo.UserID = rdr.GetInt32(rdr.GetOrdinal("DUMMYID"));
                    userInfo.UserName = rdr.GetString(rdr.GetOrdinal("KULLANICIKOD"));
                    userInfo.UserType = UserTypes.SUPERVISOR;
                    userInfo.AgentID = 0;
                    userInfo.Displayname = rdr.GetString(rdr.GetOrdinal("ADSOYAD"));
                    userInfo.userLevel = rdr.GetInt32(rdr.GetOrdinal("SEVIYEKOD"));
                    rdr.Close();
                    return userInfo;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Kullanıcı Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }

        }

        public static string GetGroupIDs(string UserName)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("GroupAudit Hata: Veri sunucusuna bağlanılamıyor. " + ex.Message);

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand("select GroupID from GroupAudit where UserName='" + UserName + "'", conn);
                cmd.CommandType = CommandType.Text;

                SqlDataReader rdr = cmd.ExecuteReader();

                string ids = "";
                while (rdr.Read())
                {
                    ids += rdr["GroupID"].ToString() + ",";
                }
                if (ids != "")
                    ids = ids.Remove(ids.Length - 1);
                return ids;
            }
            catch (Exception ex)
            {
                throw new Exception("GroupAudit Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }

        }

        public static bool LoginUser(string UserName, string UserPasswd, out int UserID, out string UserLevel, out string UserSkills,out string UserPrefixes, out string SEVIYEKOD)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand("SELECT SEVIYEKOD FROM KULLANICILAR WHERE KULLANICIKOD = '" + UserName + "' AND SIFRE = '"+ HashPassword(UserPasswd) + "'", conn);
                SEVIYEKOD = cmd.ExecuteScalar().ToString();
                
            }
            catch (Exception ex)
            {
                UserID = 0;
                UserLevel = "0";
                UserSkills = "0";
                UserPrefixes = "0";
                throw new Exception("Login Hata: Hatalı Kullanıcı İsmi Ya da Şifresi.");
            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand("SP_USERLOGIN_V2", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@UserName", SqlDbType.NVarChar, 50)).Value = UserName;
                cmd.Parameters.Add(new SqlParameter("@UserPasswd", SqlDbType.NVarChar, 50)).Value = HashPassword(UserPasswd);
                cmd.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Direction = ParameterDirection.Output;
                cmd.Parameters.Add(new SqlParameter("@UserAgents", SqlDbType.NVarChar, 5000)).Direction = ParameterDirection.Output;
                cmd.Parameters.Add(new SqlParameter("@UserSkills", SqlDbType.NVarChar, 100)).Direction = ParameterDirection.Output;
                cmd.Parameters.Add(new SqlParameter("@UserPrefixes", SqlDbType.NVarChar, 150)).Direction = ParameterDirection.Output;
                cmd.Parameters.Add(new SqlParameter("@RESULT", SqlDbType.Int)).Direction = ParameterDirection.ReturnValue;

                cmd.ExecuteNonQuery();
                if ((int)cmd.Parameters["@RESULT"].Value == 0)
                {
                    UserID = (int)cmd.Parameters["@UserID"].Value;
                    UserLevel = (string)cmd.Parameters["@UserAgents"].Value;
                    UserSkills = (string)cmd.Parameters["@UserSkills"].Value;
                    UserPrefixes = (string)cmd.Parameters["@UserPrefixes"].Value;
                    return true;
                    //throw new Exception("Kullanıcı bilgileri hatalı.");
                }
                else if ((int)cmd.Parameters["@RESULT"].Value == -1) //kullanıcı silinmiş veya aktif değil
                {
                    UserID = 0;
                    throw new Exception("Kullanıcı silinmiş veya aktif değil.");
                }
                else if ((int)cmd.Parameters["@RESULT"].Value == 1) //kullanıcı dogrulandı
                {
                    UserID = (int)cmd.Parameters["@UserID"].Value;
                    UserLevel = (string)cmd.Parameters["@UserAgents"].Value;
                    UserSkills = (string)cmd.Parameters["@UserSkills"].Value;
                    UserPrefixes = (string)cmd.Parameters["@UserPrefixes"].Value;
                    return true;
                }
                UserID = 0;
                UserLevel = "0";
                UserSkills = "0";
                UserPrefixes = "0";
                return false;
            }
            catch (Exception ex)
            {
                UserID = 0;
                throw new Exception("Login Hata: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }

        }
        public static void getPermissions(int UserId)
        {
            try
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
                SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM KULLANICISAYFAYETKI WHERE DUMMYID = " + UserId, conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        System.Web.HttpContext.Current.Session["CAGRIKAYITLARI"] = dr[1].ToString();
                        System.Web.HttpContext.Current.Session["DEGERLENDIRMELER"] = dr[2].ToString();
                        System.Web.HttpContext.Current.Session["RAPORLAR"] = dr[3].ToString();
                        System.Web.HttpContext.Current.Session["TANIMLAMALAR"] = dr[4].ToString();
                        System.Web.HttpContext.Current.Session["YONETIM"] = dr[5].ToString();
                    }
                }
                else
                {
                    System.Web.HttpContext.Current.Session["CAGRIKAYITLARI"] = "0";
                    System.Web.HttpContext.Current.Session["DEGERLENDIRMELER"] = "0";
                    System.Web.HttpContext.Current.Session["RAPORLAR"] = "0";
                    System.Web.HttpContext.Current.Session["TANIMLAMALAR"] = "0";
                    System.Web.HttpContext.Current.Session["YONETIM"] = "0";
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Login Hata: " + ex.Message);
            }
        }
        public static string Concat(string response)
        {
            string retVal = null;
            List<string> tmpList = new List<string>();

            try
            {
                string[] tmp = response.Split(',');
                foreach (string t in tmp)
                {
                    //if (t.Contains("-")) //yorum satırına alındı
                    //{
                    //    for (int i = Convert.ToInt32(t.Split('-')[0]); i <= Convert.ToInt32(t.Split('-')[1]); i++)
                    //    {
                    //        tmpList.Add(i.ToString());
                    //    }
                    //}
                    //else
                    //{
                    if(t != "0" && t != "")
                    {
                        tmpList.Add("'" + t + "'");
                    }
                    else
                    {
                        tmpList.Add(t);
                    }
                    //}
                }
                if (tmpList.Count > 0)
                    retVal = string.Join(",", tmpList);
            }
            catch (Exception ex)
            {
            }
            return retVal;
        }

    }
}