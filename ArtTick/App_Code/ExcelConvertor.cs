﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace ARTQM
{
    public class ExcelConvertor
    {


      
        public string Convert(DataTable oDataTable, string directoryPath, string fileName)
        {

            //// İSMAİL /////
            /// BU FONKSİYONA BAKACAGIZ, HTML TAGLERI İCİNE BİZİM RAPOR GİBİ GORUNMESİ İCİN STİL YERLESTİRECEGİZ BACKGROUND BORDER ALİGNMENT VS
            string fullpath = "";

            if (directoryPath.Substring(directoryPath.Length - 1, 1) == @"\" || directoryPath.Substring(directoryPath.Length - 1, 1) == "/")
            {
                fullpath = directoryPath + fileName;
            }
            else
            {
                fullpath = directoryPath + @"\" + fileName;
            }

            StreamWriter SW;
            SW = File.CreateText(fullpath);


            StringBuilder oStringBuilder = new StringBuilder();

            /********************************************************
             * Start, check for border width
             * ******************************************************/
            int borderWidth = 1;

            /********************************************************
             * End, Check for border width
             * ******************************************************/

            /********************************************************
             * Start, Check for bold heading
             * ******************************************************/
            string boldTagStart = "";
            string boldTagEnd = "";
            boldTagStart = "<B>";
            boldTagEnd = "</B>";

            /********************************************************
             * End,Check for bold heading
             * ******************************************************/

            oStringBuilder.Append("<Table border=" + borderWidth + " style=\"font-family: arial; font-size:10px;\">");

            string generalCell = "border: 1px solid #000000;font-family:Arial, Tahoma, Geneva, sans-serif; font-size:10px";

            /*******************************************************************
             * Start, Creating table header
             * *****************************************************************/


            oStringBuilder.Append("<TR>");

            foreach (DataColumn oDataColumn in oDataTable.Columns)
            {
                oStringBuilder.Append("<TD style=\"" + generalCell + ";" + (oDataColumn.Ordinal != 0 ? "direction:rtl;mso-rotate:90;" : "") + "white-space:nowrap;color: #16365c; background-color:#ffffff;\">" + boldTagStart + oDataColumn.Caption.Replace("****", "") + boldTagEnd + "</TD>");
            }

            oStringBuilder.Append("</TR>");

            /*******************************************************************
             * End, Creating table header
             * *****************************************************************/

            /*******************************************************************
             * Start, Creating rows
             * *****************************************************************/


            //************************************

            string firstCellStyle = "";
            string rowStyle = "";
            int rowIndex = 0;
            int maxrow = 1;

            if (oDataTable.Rows[0][0].ToString() == "Bölge")
                maxrow = 2;
            foreach (DataRow oDataRow in oDataTable.Rows)
            {

                if (rowIndex <= maxrow) //ilk iki satir farklı gorunecek
                {
                    if (rowIndex == 1)
                    {
                        rowStyle = generalCell + ";" + " text-align: center; color: #ffffff; font-family: verdana; border-bottom: solid 1px #c0c0c0; font-weight: bold; padding-top: 2px; padding-bottom: 2px;background-color: #16365c;";
                    }
                    else if (rowIndex == 0)
                    {
                        rowStyle = generalCell + ";" + " text-align: center; color: #16365c; font-family: verdana; border-bottom: solid 1px #c0c0c0; font-weight: bold; padding-top: 2px; padding-bottom: 2px;background-color: #fcd5b4;";
                    }
                    else
                    {
                        rowStyle = generalCell + ";" + " text-align: center; color: #16365c; font-family: verdana; border-bottom: solid 1px #c0c0c0; font-weight: bold; padding-top: 0px; padding-bottom: 0px;background-color: #FFffff;";
                    }
                    firstCellStyle = "text-align: left";
                }
                else
                    if (oDataRow[0].ToString().StartsWith("***"))
                    {
                        rowStyle = generalCell + ";" + "background-color: #16365c; color:#fff; text-align: center; border-bottom: solid 1px #909090; font-weight:bold; padding-top:2px; padding-bottom:2px;";
                        firstCellStyle = "text-align: left";
                    }
                    else if (oDataRow[0].ToString().Contains("(Skora dahil değildir.)"))
                    {
                        rowStyle = generalCell + ";" + "white-space:nowrap;  background-color: #cecece;   text-align:left; 	padding-top:2px; padding-bottom:2px; color:#000000;";
                        firstCellStyle = "text-align: left";
                    }
                    else if (oDataRow[0].ToString().StartsWith("**"))
                    {
                        rowStyle = generalCell + ";" + "white-space:nowrap;     text-align:left; padding-top:2px; padding-bottom:2px;color:#000000;";
                        firstCellStyle = "text-align: left";
                    }
                    else
                    {
                        rowStyle = generalCell + ";" + "font-weight: normal;color:#000000;";
                        firstCellStyle = "text-align: right;";
                    }


                //oStringBuilder.Append("<TR style=\""+rowStyle+"\">");
                oStringBuilder.Append("<TR>");
                foreach (DataColumn oDataColumn in oDataTable.Columns)
                {
                    if (oDataColumn.Ordinal == 0) //ilk sutunsa index 0 
                        oStringBuilder.Append("<td style=\"" + rowStyle + generalCell + ";" + firstCellStyle + "\">");
                    else if (oDataColumn.Caption.Replace("****", "") == "")
                        oStringBuilder.Append("<td style=\"border-bottom: 0px solid;border-top: 0px solid;text-align:center; background-color:#ffffff; \">");
                    else if (rowIndex == 1 || rowIndex == 2 || oDataRow["SORU"].ToString().StartsWith("****"))
                    {
                        oStringBuilder.Append("<td style=\"" + rowStyle + generalCell + ";" + "text-align:center;mso-number-format:'0';\">");
                    }
                    else if (oDataColumn.Caption.ToLower().Contains("bölge"))
                    {
                        oStringBuilder.Append("<td style=\"" + rowStyle + generalCell + ";" + "text-align:center;mso-number-format:'0'; border-left: solid 10px #646464;\">");
                    }
                    else
                    {
                        if (oDataColumn.ColumnName.EndsWith("."))
                            oStringBuilder.Append("<td style=\"" + rowStyle + generalCell + ";text-align:center;font-weight:bold;border-left:2px solid #393939;mso-number-format:'0';\">");
                        else
                            oStringBuilder.Append("<td style=\"" + rowStyle + generalCell + ";" + "text-align:center;mso-number-format:'0'; border-left:solid 0px; border-top: solid 0px; border-bottom:solid 1px #000000; border-right:solid 1px #000000;\">");
                    }
                    if (oDataColumn.Ordinal == 0)
                        oStringBuilder.Append(oDataRow[oDataColumn.ColumnName].ToString().Replace("*", "") + "</TD>");
                    else
                        oStringBuilder.Append(oDataRow[oDataColumn.ColumnName].ToString().Replace("*", "").Replace(",", ".") + "</TD>");
                }

                oStringBuilder.Append("</TR>");
                rowIndex++;
            }


            /*******************************************************************
             * End, Creating rows
             * *****************************************************************/



            oStringBuilder.Append("</Table>");

            SW.WriteLine(oStringBuilder.ToString());
            SW.Close();

            return fullpath;
        }
        public string ConvertList(DataTable oDataTable, string directoryPath, string fileName)
        {

            string fullpath = "";

            if (directoryPath.Substring(directoryPath.Length - 1, 1) == @"\" || directoryPath.Substring(directoryPath.Length - 1, 1) == "/")
            {
                fullpath = directoryPath + fileName;
            }
            else
            {
                fullpath = directoryPath + @"\" + fileName;
            }

            StreamWriter SW;
            SW = new StreamWriter(File.Create(fullpath), System.Text.Encoding.UTF8);


            StringBuilder oStringBuilder = new StringBuilder();

            int borderWidth = 0;

            if (_ShowExcelTableBorder)
            {
                borderWidth = 1;
            }

            string boldTagStart = "";
            string boldTagEnd = "";
            if (_ExcelHeaderBold)
            {
                boldTagStart = "<B>";
                boldTagEnd = "</B>";
            }


            oStringBuilder.Append("<Table border=" + borderWidth + ">");


            oStringBuilder.Append("<TR>");

            foreach (DataColumn oDataColumn in oDataTable.Columns)
            {
                oStringBuilder.Append("<TD style='border: solid 1px #303030;background-color: #16365c; color:#ffffff; font-family: arial; font-size:11px;'>" + boldTagStart + oDataColumn.Caption + boldTagEnd + "</TD>");
            }

            oStringBuilder.Append("</TR>");

            string rowColor = "";
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                oStringBuilder.Append("<TR>");
                if (oDataRow[1].ToString().ToLower().Contains("ulusal"))//üstbaşlık
                {
                    rowColor = "#ffffff";
                }
                else
                {
                    rowColor = "#ffffff";
                }
                foreach (DataColumn oDataColumn in oDataTable.Columns)
                {

                    if (oDataColumn.ColumnName.ToLower().Contains("skor"))
                    {
                        oStringBuilder.Append("<TD style='background-color:#cecece; text-align: center;border: solid 1px #303030;'> " + oDataRow[oDataColumn.ColumnName] + " </TD>");
                    }
                    else
                    {
                        oStringBuilder.Append("<TD style='background-color:" + rowColor + "; text-align: left;border: solid 1px #303030;'>" + oDataRow[oDataColumn.ColumnName] + " </TD>");
                    }
                }
                oStringBuilder.Append("</TR>");
            }
            /*******************************************************************
             * End, Creating rows
             * *****************************************************************/
            oStringBuilder.Append("</Table>");

            SW.WriteLine(oStringBuilder.ToString());
            SW.Close();

            return fullpath;
        }
        public string ConvertGizliMusteri(DataTable oDataTable, string directoryPath, string fileName)
        {

            string fullpath = "";

            if (directoryPath.Substring(directoryPath.Length - 1, 1) == @"\" || directoryPath.Substring(directoryPath.Length - 1, 1) == "/")
            {
                fullpath = directoryPath + fileName;
            }
            else
            {
                fullpath = directoryPath + @"\" + fileName;
            }

            StreamWriter SW;
            SW = new StreamWriter(File.Create(fullpath), System.Text.Encoding.UTF8);


            StringBuilder oStringBuilder = new StringBuilder();

            /********************************************************
             * Start, check for border width
             * ******************************************************/
            int borderWidth = 0;

            if (_ShowExcelTableBorder)
            {
                borderWidth = 1;
            }
            /********************************************************
             * End, Check for border width
             * ******************************************************/

            /********************************************************
             * Start, Check for bold heading
             * ******************************************************/
            string boldTagStart = "";
            string boldTagEnd = "";
            if (_ExcelHeaderBold)
            {
                boldTagStart = "<B>";
                boldTagEnd = "</B>";
            }

            /********************************************************
             * End,Check for bold heading
             * ******************************************************/

            oStringBuilder.Append("<Table border=" + borderWidth + ">");


            /*******************************************************************
             * Start, Creating table header
             * *****************************************************************/

            oStringBuilder.Append("<TR>");
            foreach (DataColumn oDataColumn in oDataTable.Columns)
            {
                oStringBuilder.Append("<TD style='border: solid 1px #898989;background-color: #ffffff; color:#000;font-family: arial; font-size:11px;'>" + boldTagStart + oDataColumn.Caption + boldTagEnd + "</TD>");
            }
            oStringBuilder.Append("</TR>");

            /*******************************************************************
             * End, Creating table header
             * *****************************************************************/

            /*******************************************************************
             * Start, Creating rows
             * *****************************************************************/
            string rowColor = "";
            foreach (DataRow oDataRow in oDataTable.Rows)
            {
                oStringBuilder.Append("<TR>");
                foreach (DataColumn oDataColumn in oDataTable.Columns)
                {

                    if (oDataColumn.ColumnName.ToLower().Contains("skor"))
                    {
                        oStringBuilder.Append("<TD style='background-color:#99CCFF; text-align: center;border: solid 1px #303030;'> " + oDataRow[oDataColumn.ColumnName] + " </TD>");
                    }
                    else
                    {
                        oStringBuilder.Append("<TD style='" + rowColor + "; text-align: left;border: solid 1px #303030;'>" + oDataRow[oDataColumn.ColumnName] + " </TD>");
                    }


                }

                oStringBuilder.Append("</TR>");
            }


            /*******************************************************************
             * End, Creating rows
             * *****************************************************************/



            oStringBuilder.Append("</Table>");

            SW.WriteLine(oStringBuilder.ToString());
            SW.Close();

            return fullpath;
        }


        private bool _ShowExcelTableBorder = false;

        /// <summary>
        /// To show or hide the excel table border
        /// </summary>
        public bool ShowExcelTableBorder
        {
            get
            {
                return _ShowExcelTableBorder;
            }
            set
            {
                _ShowExcelTableBorder = value;
            }
        }

        private bool _ExcelHeaderBold = true;


        /// <summary>
        /// To make header bold or normal
        /// </summary>
        public bool ExcelHeaderBold
        {
            get
            {
                return _ExcelHeaderBold;
            }
            set
            {
                _ExcelHeaderBold = value;
            }
        }

        public string ConvertKarne(DataTable oDataTable, string directoryPath, string fileName, string bayiAdi, string gorusulenkisi, string il, string katilimTarih)
        {

            string fullpath = "";

            if (directoryPath.Substring(directoryPath.Length - 1, 1) == @"\" || directoryPath.Substring(directoryPath.Length - 1, 1) == "/")
            {
                fullpath = directoryPath + fileName;
            }
            else
            {
                fullpath = directoryPath + @"\" + fileName;
            }

            StreamWriter SW;
            SW = new StreamWriter(fullpath, false, Encoding.Unicode);
            string empty_row = "<tr><td colspan=3>&nbsp;</td></tr>";


            StringBuilder oStringBuilder = new StringBuilder();

            oStringBuilder.Append("<table cellpadding=0 cellspacing=0 style='border:solid 1px #333333; font-family: verdana, arial, tahoma, verdana; font-size:10px;'>");
            oStringBuilder.Append("<tr><td style='border: solid 1px #333333;background-color: #eeeeee'></td><td style='border: solid 1px #333333;background-color: #eeeeee'>Proje/Çağrı</td><td style='border: solid 1px #333333;background-color: #eeeeee' colspan=3>" + bayiAdi + "</td></tr>");
            oStringBuilder.Append("<tr><td style='border: solid 1px #333333;background-color: #eeeeee'></td><td style='border: solid 1px #333333;background-color: #eeeeee'>Operatör</td><td style='border: solid 1px #333333;background-color: #eeeeee' colspan=3>" + gorusulenkisi+ "</td></tr>");
            oStringBuilder.Append("<tr><td style='border: solid 1px #333333;background-color: #eeeeee'></td><td style='border: solid 1px #333333;background-color: #eeeeee'>Görüşme Süresi</td><td style='border: solid 1px #333333;background-color: #eeeeee' colspan=3>" + il + "</td></tr>");
            oStringBuilder.Append("<tr><td style='border: solid 1px #333333;background-color: #eeeeee'></td><td style='border: solid 1px #333333;background-color: #eeeeee'>Değerlendirme Tarihi</td><td style='border: solid 1px #333333;background-color: #eeeeee' colspan=3>" + katilimTarih + "</td></tr>");


            oStringBuilder.Append("<TR><Td style='width:40px;color: #fff; background-color: #800000; border-bottom: solid 1px #909090; font-weight: bold; padding-top: 4px; padding-bottom: 4px;'></td><Td style='color: #fff; background-color: #800000; border-bottom: solid 1px #909090; font-weight: bold; padding-top: 4px; padding-bottom: 4px;'></td><Td style='color: #fff; background-color: #800000; border-bottom: solid 1px #909090; font-weight: bold; padding-top: 4px; padding-bottom: 4px;width:120px;'>Gelen Cevap</td><Td style='text-align:center;color: #fff; background-color: #800000; border-bottom: solid 1px #909090; font-weight: bold; padding-top: 4px; padding-bottom: 4px;width:40px;'>Ağırlık</td><Td style='text-align:center;color: #fff; background-color: #800000; border-bottom: solid 1px #909090; font-weight: bold; padding-top: 4px; padding-bottom: 4px;width:40px;'>Puan</td></TR>");
            int cnt = 0;
            foreach (DataRow _dr in oDataTable.Rows)
            {
                if (cnt == 0)
                {
                    oStringBuilder.Append("<TR><Td style='color: #0000; background-color: #eeeeee; border-bottom: solid 1px #909090; font-weight: bold; padding-top: 4px; padding-bottom: 4px;width:40px;'></td>"
                      + "<Td style='color: #000; background-color: #eeeeee; border-bottom: solid 1px #909090; font-weight: bold; padding-top: 4px; padding-bottom: 4px;width:300px'>GENEL SKOR</td>"
                      + "<td style='width:120px; color: #000; background-color: #eeeeee; border-bottom: solid 1px #909090; font-weight: bold; padding-top: 4px; padding-bottom: 4px;'></td>"
                      + "<td style='text-align:center;width:40px; color: #000; background-color: #eeeeee; border-bottom: solid 1px #909090; font-weight: bold; padding-top: 4px; padding-bottom: 4px;'>" + 100 + "</td>"
                      + "<td style='text-align:center;width:40px; color: #00; background-color: #eeeeee; border-bottom: solid 1px #909090; font-weight: bold; padding-top: 4px; padding-bottom: 4px;'>" + _dr["PUAN"].ToString() + "</td>"
                      + "</TR>");
                    cnt++;
                }
                
                if (_dr["SORUNO"].ToString() == "0" && _dr["ETIKET"].ToString().StartsWith("+")) //kriter
                {
                    oStringBuilder.Append("<TR><Td style='color: #fff; background-color: #800000; border-bottom: solid 1px #909090; font-weight: bold; padding-top: 4px; padding-bottom: 4px;width:40px;'></td>"
                        + "<Td style='color: #fff; background-color: #800000; border-bottom: solid 1px #909090; font-weight: bold; padding-top: 4px; padding-bottom: 4px;width:300px'>" + _dr["ETIKET"].ToString().Replace("+", "") + "</td>"
                        + "<td style='width:120px; color: #fff; background-color: #800000; border-bottom: solid 1px #909090; font-weight: bold; padding-top: 4px; padding-bottom: 4px;'></td>"
                        + "<td style='text-align:center;width:40px; color: #fff; background-color: #800000; border-bottom: solid 1px #909090; font-weight: bold; padding-top: 4px; padding-bottom: 4px;'>" + _dr["MAKSSKOR"].ToString() + "</td>"
                        + "<td style='text-align:center;width:40px; color: #fff; background-color: #800000; border-bottom: solid 1px #909090; font-weight: bold; padding-top: 4px; padding-bottom: 4px;'>" + _dr["PUAN"].ToString() + "</td>"
                        + "</TR>");
                }
                else if (_dr["EVETHAYIR"].ToString() == "-2") //kalitatifler / yorum 
                {
                    oStringBuilder.Append("<TR><Td style='background-color: #cdcdcd;  text-align: left; font-weight:bold; color: #000080; border-top: solid 4px #606060; border-bottom: solid 2px #606060; width:40px;'></td>"
                    + "<Td style='background-color: #cdcdcd; text-align: left; font-weight:bold; color: #000080; border-top: solid 4px #606060; border-bottom: solid 2px #606060;width:300px; '>" + _dr["ETIKET"].ToString().Replace("+", "") + "</td>"
                    + "<td style='background-color: #cdcdcd; text-align: left; font-weight:bold; color: #000080; border-top: solid 4px #606060; border-bottom: solid 2px #606060;width:240px;' colspan=3>" + _dr["YANIT"].ToString().Replace("**", "") + "</td>"
                    + "</TR>");
                    //oStringBuilder.Append(empty_row);

                }
                else if (!_dr["ETIKET"].ToString().StartsWith("*")) // soru baslıgı degil saga yasla
                {
                    oStringBuilder.Append("<TR><Td style='border-right: solid 1px #333333;border-bottom: solid 1px #333333;width:40px;'></td>"
                        + "<Td style='border-right: solid 1px #333333;border-bottom: solid 1px #333333;width:300px;'>" + _dr["ETIKET"].ToString() + "</td>"
                        + "<td style='width:120px; border-right: solid 1px #333333;border-bottom: solid 1px #333333;width:140px;'>" + _dr["YANIT"].ToString() + "</td>"
                    + "<td style='text-align:center;width:200px; border-right: solid 1px #333333;border-bottom: solid 1px #333333;width:40px;'>" + _dr["MAKSSKOR"].ToString() + "</td>"
                    + "<td style='text-align:center;width:200px; border-right: solid 1px #333333;border-bottom: solid 1px #333333;width:40px;'>" + _dr["PUAN"].ToString() + "</td></tr>");
                }
                else if (_dr["SKORADAHIL"].ToString() == "0") // SKORA DAHİL DEĞİL SARI İŞARETLE
                {
                    oStringBuilder.Append("<TR><Td style='background-color:#cdcdcd; border-right: solid 1px #333333;border-bottom: solid 1px #333333;width:40px;'></td>"
                        + "<Td style='background-color:#cdcdcd; border-right: solid 1px #333333;border-bottom: solid 1px #333333;width:300px;'>" + _dr["ETIKET"].ToString().Replace("*", "") + " (Skora dahil değildir.)" + "</td>"
                        + "<td style='border-right: solid 1px #333333;border-bottom: solid 1px #333333;background-color:#cdcdcd; width:140px; '>" + _dr["YANIT"].ToString() + "</td>"
                        + "<td style='text-align:center;border-right: solid 1px #333333;border-bottom: solid 1px #333333;background-color:#cdcdcd; width:40px; '>" + _dr["MAKSSKOR"].ToString() + "</td>"
                        + "<td style='text-align:center;border-right: solid 1px #333333;border-bottom: solid 1px #333333;background-color:#cdcdcd; width:40px; '>" + _dr["PUAN"].ToString() + "</td>"
                        + "</TR>");
                }
                else
                {
                    oStringBuilder.Append("<tr><Td style='border-right: solid 1px #333333;border-bottom: solid 1px #333333;width:40px;'>S" + _dr["SORUSIRA"].ToString() + ".</td>"
                        + "<Td style='border-right: solid 1px #333333;border-bottom: solid 1px #333333;width:300px;'>" + _dr["ETIKET"].ToString().Replace("*", "").Replace("S" + _dr["SORUSIRA"].ToString() + ".", "") + "</td>"
                        + "<td style='border-right: solid 1px #333333;border-bottom: solid 1px #333333;width:120px;'>" + _dr["YANIT"].ToString() + "</td>"
                        + "<td style='text-align:center;border-right: solid 1px #333333;border-bottom: solid 1px #333333;width:40px;'>" + _dr["MAKSSKOR"].ToString() + "</td>"
                        + "<td style='text-align:center;border-right: solid 1px #333333;border-bottom: solid 1px #333333;width:40px;'>" + _dr["PUAN"].ToString() + "</td>"
                        + "</tr>");
                }
            }



            /*******************************************************************
             * End, Creating rows
             * *****************************************************************/



            oStringBuilder.Append("</table>");

            SW.WriteLine(oStringBuilder.ToString());
            SW.Close();

            return fullpath;
        }


    }
}