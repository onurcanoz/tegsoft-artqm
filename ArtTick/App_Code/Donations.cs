﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


namespace ARTQM
{
    public class Donations
    {
        public static bool GetIVRData(string ani, out string CCNO, out string CCM, out string CCY, out string CVC, out int STATUS)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                CCNO = CCM = CCY = CVC = "";
                STATUS = 0;
                throw new Exception("Login Hata: Veri sunucusuna bağlanılamıyor. " + ex.Message);
            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand("SP_GETIVRDATA", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ANI", SqlDbType.NVarChar, 50)).Value = ani;
                cmd.Parameters.Add(new SqlParameter("@CCNO", SqlDbType.NVarChar, 150)).Direction = ParameterDirection.Output;
                cmd.Parameters.Add(new SqlParameter("@CCM", SqlDbType.VarChar, 2)).Direction = ParameterDirection.Output;
                cmd.Parameters.Add(new SqlParameter("@CCY", SqlDbType.VarChar, 4)).Direction = ParameterDirection.Output;
                cmd.Parameters.Add(new SqlParameter("@CVC", SqlDbType.VarChar, 3)).Direction = ParameterDirection.Output;
                cmd.Parameters.Add(new SqlParameter("@STATUS", SqlDbType.Int)).Direction = ParameterDirection.Output;
                cmd.Parameters.Add(new SqlParameter("@RESULT", SqlDbType.Int)).Direction = ParameterDirection.ReturnValue;

                cmd.ExecuteNonQuery();

                if ((int)cmd.Parameters["@RESULT"].Value == 1) //kullanıcı dogrulandı
                {
                    CCNO = cmd.Parameters["@CCNO"].Value.ToString();
                    CCM = cmd.Parameters["@CCM"].Value.ToString();
                    CCY = cmd.Parameters["@CCY"].Value.ToString();
                    CVC = cmd.Parameters["@CVC"].Value.ToString();
                    STATUS = (int)cmd.Parameters["@STATUS"].Value;
                    return true;
                }
                else
                {
                    CCNO = CCM = CCY = CVC = "";
                    STATUS = 0;
                    return true;
                }
                

            }
            catch (Exception ex)
            {
                CCNO = CCM = CCY = CVC = "";
                STATUS = 0;
                return false;
            }
            finally
            {
                conn.Close();
            }




        }
        public static bool ClearIVRData(string ani)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                
                throw new Exception("IVRDATA Hata: Veri sunucusuna bağlanılamıyor. " + ex.Message);
            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand("delete from IVRDATA where ANI = '"+ani+"'", conn);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                conn.Close();
            }

        }

        public static bool LogDonationWS(string info, string wsresult)
        {
            wsresult = wsresult.Replace("'", "`");
            info = info.Replace("'", "`");

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {

                throw new Exception("LogDonations Hata: Veri sunucusuna bağlanılamıyor. " + ex.Message);
            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand("insert into WSLOG (CCINFO, WSRESULT) values ('" + info + "','"+wsresult+"')", conn);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                conn.Close();
            }

        }

        public static DataTable FindWSLogs(string start, string end)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Bağış Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand("select * from WSLOG where ROWDATETIME between  cONVERT(datetime, '" + start + "', 104)  and cONVERT(datetime, '" + end + "', 104) order by ROWDATETIME desc", conn);

                cmd.CommandType = CommandType.Text;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
  

    }
}