﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

namespace ARTQM
{
    public class ICUser
    {
        public int UserID { get; set; }

        public string UserName { get; set; }

        public string Displayname { get; set; }


        public int UserType { get; set; }

        public int AgentID { get; set; }

        public string GroupIDs{ get; set; }

        public int userLevel { get; set; }
    }

 
    public class UserTypes
    {
        public static  int ADMIN= 1;
        public static  int SUPERVISOR = 2;
        public static int AGENT = 3;
        public static int TAKIMLIDERI = 4;
        

        public static string GetMemberName<T, TValue>(Expression<Func<T, TValue>> memberAccess)
        {
            return ((MemberExpression)memberAccess.Body).Member.Name;
        }
        public static string GetUserTypeName(int value)
        { 
            FieldInfo  info = null;
            if (value == UserTypes.TAKIMLIDERI)
            {
                info = typeof(UserTypes).GetField("TAKIMLIDERI");

                return info.Name;
            }
            else if (value == UserTypes.ADMIN)
            {
                info = typeof(UserTypes).GetField("ADMIN");

                return info.Name;
            }
            else if (value == UserTypes.SUPERVISOR)
            {
                info = typeof(UserTypes).GetField("SUPERVISOR");

                return info.Name;
            }
            else if (value == UserTypes.AGENT)
            {
                info = typeof(UserTypes).GetField("AGENT");

                return info.Name;
            }
          
            else
            {
                return "N/A";
            }
        }
    }

}