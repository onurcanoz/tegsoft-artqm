﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARTQM
{
    public class TicketStatus
    {
        public static int CREATED = 0;
        public static int CLOSED = 1;
        public static int UPDATED = 2;
        public static int ASSIGNED = 3;
        public static int PENDING = 4;
        
    }
}