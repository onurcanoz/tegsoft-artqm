﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace ARTQM
{
    public class Lists
    {     
        public static DataTable GetForms()
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand comm = new SqlCommand("select * from PROJELER order by PROJENO", conn);
                comm.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(comm);
                DataTable forms = new DataTable();
                da.Fill(forms); 
                
                return forms;
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
        public static DataTable GetActiveForms()
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand comm = new SqlCommand("select * from PROJELER where AKTIF = '1' order by PROJENO", conn);
                comm.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(comm);
                DataTable forms = new DataTable();
                da.Fill(forms);

                return forms;
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
        public static string GetFormName(string PROJENO)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            try
            {
                SqlCommand comm = new SqlCommand("select PROJEADI from PROJELER where PROJENO="+ PROJENO, conn);
                comm.CommandType = CommandType.Text;
                object obj = comm.ExecuteScalar();
                if (obj == null) return "";
                else return obj.ToString();

                
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
        public static string  GetEvalInfo(string callid, string PROJENO, out string AGENT)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            try
            {
                SqlCommand comm = new SqlCommand("select (select PROJEADI from PROJELER WHERE PROJENO = '"+PROJENO+ "') as PROJEADI, isnull((select convert(nvarchar, DEGERLENDIRMETARIHI, 104) from ANKETLER where ANKETNO='"+callid+"' AND PROJENO="+PROJENO+"),'') as DEGERLENDIRMETARIHI,  "
                + " sum(CONVERT(int, ct.SURE)) as DURATION,MIN( ct.Partyagentnames ) as AGENT from ANKETLER ct where ct.ANKETNO= '" + callid + "'"
                + " group by ct.ANKETNO", conn);
                comm.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(comm);
                DataTable forms = new DataTable();
                da.Fill(forms);
                if (forms.Rows.Count == 0)
                {
                    AGENT = "";
                    return "";
                }
                else
                {
                    try
                    {
                        AGENT = forms.Rows[0]["AGENT"].ToString().Split(' ')[0].Trim();
                    }
                    catch
                    {
                        AGENT = forms.Rows[0]["AGENT"].ToString().Trim();
                    }
                    return forms.Rows[0]["PROJEADI"].ToString() + "  |  Çağrı: " + callid + ", Agent: " + forms.Rows[0]["AGENT"].ToString() + ", Süre: " + ARTQM.PageBase.Seconds2Span(forms.Rows[0]["DURATION"].ToString()) + ", Değerlendirme Tarihi: " + forms.Rows[0]["DEGERLENDIRMETARIHI"].ToString();
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }

        public static string[] GetEvalInfo2(string callid, string PROJENO, out string AGENT)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            try
            {
                SqlCommand comm = new SqlCommand("select (select PROJEADI from PROJELER WHERE PROJENO = '" + PROJENO + "') as PROJEADI, isnull((select convert(nvarchar, DEGERLENDIRMETARIHI, 104) from ANKETLER where ANKETNO='" + callid + "' AND PROJENO=" + PROJENO + "),'') as DEGERLENDIRMETARIHI,  "
                + " sum(CONVERT(INT, SURE)) as DURATION,MIN(PARTYAGENTNAMES) as AGENT from ANKETLER where ANKETNO= '" + callid + "'"
                + " group by ANKETNO", conn);
                comm.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(comm);
                DataTable forms = new DataTable();
                da.Fill(forms);
                if (forms.Rows.Count == 0)
                {
                    AGENT = "";
                    return new string[0];
                }
                else
                {
                    try
                    {
                        AGENT = forms.Rows[0]["AGENT"].ToString().Split(' ')[0].Trim();
                    }
                    catch
                    {
                        AGENT = forms.Rows[0]["AGENT"].ToString().Trim();
                    }
                    return new string[]{ forms.Rows[0]["PROJEADI"].ToString() , callid , forms.Rows[0]["AGENT"].ToString() , ARTQM.PageBase.Seconds2Span(forms.Rows[0]["DURATION"].ToString()) , forms.Rows[0]["DEGERLENDIRMETARIHI"].ToString() };
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
   
  
        public static DataTable GetTitles(int FORMNO)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand comm = new SqlCommand("select DUMMYID, KRITERNO, KRITER, PROJENO, MAKSSKOR from KRITERLER where PROJENO="+FORMNO+" order by KRITERNO", conn);
                comm.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(comm);
                DataTable forms = new DataTable();
                da.Fill(forms);

                return forms;
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }

        public static void AddNote(string CallId, string Note, string UserName)
        {
             SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand comm = new SqlCommand("insert into CAGRINOTLARI (CALLID, NOTE, USERNAME) values ('" + CallId + "', '" + Note.Replace("'", "`") + "','" + UserName + "')",  conn);
                comm.CommandType = CommandType.Text;
                comm.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
            
        }
       
        public static DataTable GetQuestions(int FORMNO)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand comm = new SqlCommand("select s.PROJENO, k.KRITER, k.KRITERNO, k.DUMMYID,  s.SORUNO, s.SORUSIRA, s.SORUMETNI, EVETHAYIR as SORUTIPI, st.SoruTipi as SORUTIPI2,  SECENEKLER, SECENEKLERPUAN , s.ACIKLAMA, s.KISAETIKET  from KRITERLER k inner join SORULAR s on k.KRITERNO = s.KRITERNO and k.PROJENO = s.PROJENO inner join SORUTIPLERI st on st.TipID = s.EVETHAYIR  where k.PROJENO = " + FORMNO + " order by s.SORUNO", conn);
                comm.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(comm);
                DataTable forms = new DataTable();
                da.Fill(forms);

                return forms;
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
        public static DataTable GetQuestions2(int FORMNO, string KRITERNO)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand comm = new SqlCommand("select s.*, k.KRITER, k.KRITERNO, k.DUMMYID  from KRITERLER k inner join SORULAR s on k.KRITERNO = s.KRITERNO and k.PROJENO = s.PROJENO inner join SORUTIPLERI st on st.TipID = s.EVETHAYIR  where k.PROJENO = " + FORMNO + (KRITERNO!= "0" && KRITERNO!= "" ? " AND s.KRITERNO='"+KRITERNO+"'" : "")+" order by k.KRITERNO, s.SORUNO", conn);
                comm.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(comm);
                DataTable forms = new DataTable();
                da.Fill(forms);

                return forms;
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
          public static DataTable GetTemplates()
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand comm = new SqlCommand(" select TASLAKID, SECENEKLER +' | '+ SECENEKLERPUAN as TASLAK from PUANTASLAKLAR order by TASLAKID", conn);
                comm.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(comm);
                DataTable forms = new DataTable();
                da.Fill(forms);

                return forms;
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }

          public static DataTable GetTemplatesList()
          {

              SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
              try
              {
                  conn.Open();
              }
              catch (Exception ex)
              {
                  throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

              }
              DataTable dt = new DataTable();

              try
              {
                  SqlCommand comm = new SqlCommand(" select TASLAKID, SECENEKLER, SECENEKLERPUAN as PUANLAMA from PUANTASLAKLAR order by TASLAKID", conn);
                  comm.CommandType = CommandType.Text;
                  SqlDataAdapter da = new SqlDataAdapter(comm);
                  DataTable forms = new DataTable();
                  da.Fill(forms);

                  return forms;
              }
              catch (Exception ex)
              {
                  throw new Exception("Liste Hata: " + ex.ToString());
              }
              finally
              {
                  conn.Close();
              }
          }
        public static DataTable GetQuestionsTypes()
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand comm = new SqlCommand("select * from SORUTIPLERI order by TipOrder", conn);
                comm.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(comm);
                DataTable forms = new DataTable();
                da.Fill(forms);

                return forms;
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
        public static DataTable GetProjects2()
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand("select * from Projects order by KODU", conn);

                cmd.CommandType = CommandType.Text;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
        public static DataTable GetCWCList(int GroupID)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
             //   LogClass.AddLog("GetCWCList Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString(), true);
                return null;
            }

            try
            {
                SqlCommand cmd = new SqlCommand("select *, dbo.FN_GETCWCNAME(ParentID) as PARENTNAME  from CWCList where GroupID = " + GroupID + " and IsActive = 1 and ParentID> 0 order by ParentID", conn);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable cwcs = new DataTable();
                da.Fill(cwcs);

                return cwcs;
            }
            catch (Exception ex)
            {
              //  LogClass.AddLog("GetCWCList : " + ex.ToString(), true);

                return null;
            }
            finally
            {
                conn.Close();
            }
        }
        public static DataTable GetCWCList2(int GroupID)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                //   LogClass.AddLog("GetCWCList Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString(), true);
                return null;
            }

            try
            {
                SqlCommand cmd = new SqlCommand("select *, dbo.FN_GETCWCNAME(ParentID) as PARENTNAME  from CWCList where GroupID = " + GroupID + " and IsActive = 1 order by ParentID", conn);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable cwcs = new DataTable();
                da.Fill(cwcs);

                return cwcs;
            }
            catch (Exception ex)
            {
                //  LogClass.AddLog("GetCWCList : " + ex.ToString(), true);

                return null;
            }
            finally
            {
                conn.Close();
            }
        }
        public static DataTable GetAUXList()
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                //   LogClass.AddLog("GetCWCList Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString(), true);
                return null;
            }

            try
            {
                SqlCommand cmd = new SqlCommand("select * from Settings where SettingName like 'AUX%' and IsActive = 1 order by SettingName", conn);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable cwcs = new DataTable();
                da.Fill(cwcs);

                return cwcs;
            }
            catch (Exception ex)
            {
                //  LogClass.AddLog("GetCWCList : " + ex.ToString(), true);

                return null;
            }
            finally
            {
                conn.Close();
            }
        }
        public static DataTable GetSchedules(string GroupIDs, string startDate, string endDate)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                //   LogClass.AddLog("GetCWCList Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString(), true);
                return null;
            }

            try
            {
                SqlCommand cmd = new SqlCommand("select ScheduleID, ScheduleDate,  (select TemplateName from Templates  where Templates.TemplateID=Schedules.TemplateID) as TemplateName, "
                + " (select GroupName from Groups where Groups.GroupID=Schedules.GroupID) as GroupName,  StatusCode, GroupID, CreatedBy, CreateDateTime from Schedules where ScheduleDate between convert(datetime,'"+startDate+"', 104) and convert(datetime,'"+endDate+"', 104) " + (GroupIDs==""?"": " ad GroupID in ("+GroupIDs+")"), conn);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable cwcs = new DataTable();
                da.Fill(cwcs);

                return cwcs;
            }
            catch (Exception ex)
            {
                //  LogClass.AddLog("GetCWCList : " + ex.ToString(), true);

                return null;
            }
            finally
            {
                conn.Close();
            }
        }
        public static DataTable GetAgentsList()
        {
            //DataProvider obj = new DataProvider();


            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            //try
            //{
            //    conn.Open();
            //}
            //catch (Exception ex)
            //{
            //    //   LogClass.AddLog("GetCWCList Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString(), true);
            //    return null;
            //}

            try
            {
                SqlDataAdapter da = new SqlDataAdapter("SELECT USERCODE AS AGENT, LOGID AS AGENTID FROM AGENTS", conn);
                DataTable data = new DataTable();
                da.Fill(data);
                //SqlCommand cmd = new SqlCommand("select ItemCode as AgentID, ItemName + '('+ ltrim(rtrim(str(ItemCode))) + ')' as AGENT from ARTCOMMONS.dbo.AGENTS order by ItemName", conn);
                //cmd.CommandType = CommandType.Text;
                //SqlDataAdapter da = new SqlDataAdapter(cmd);

                //DataTable data = obj.GetList("SELECT USERCODE AS AGENT, UID AS AGENTID FROM TBLUSERS", "AGENT", "AGENTID", "AGENT", "AgentID");

                return data;
            }
            catch (Exception ex)
            {
                //  LogClass.AddLog("GetCWCList : " + ex.ToString(), true);

                return null;
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable GetSkillList()
        {
            DataProvider obj = new DataProvider();
            //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            //try
            //{
            //    conn.Open();
            //}
            //catch (Exception ex)
            //{
            //    //   LogClass.AddLog("GetCWCList Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString(), true);
            //    return null;
            //}

            try
            {
                //SqlCommand cmd = new SqlCommand("select ItemCode as SkillID, ItemName + '('+ ltrim(rtrim(str(ItemCode))) + ')' as Skill from ARTCOMMONS.dbo.SKILLS order by ItemName", conn);
                //cmd.CommandType = CommandType.Text;
                //SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable data = obj.GetList("SELECT SKILL AS SkillID, NAME AS Skill FROM TBLCCSKILLS", "SKILLID", "SKILL", "SkillID", "Skill");
                //da.Fill(data);

                return data;
            }
            catch (Exception ex)
            {
                //  LogClass.AddLog("GetCWCList : " + ex.ToString(), true);

                return null;
            }
            finally
            {
                //conn.Close();
            }
        }
  

        public static DataTable GetCallNotes(string CALLID)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                //   LogClass.AddLog("GetCWCList Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString(), true);
                return null;
            }

            try
            {
                SqlCommand cmd = new SqlCommand("select * from CAGRINOTLARI where CALLID='" + CALLID + "' order by CREATEDATETIME", conn);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable data = new DataTable();
                da.Fill(data);

                return data;
            }
            catch (Exception ex)
            {
                //  LogClass.AddLog("GetCWCList : " + ex.ToString(), true);

                return null;
            }
            finally
            {
                conn.Close();
            }
        }
        public static DataTable GetFirstCallNotes(string CALLID)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                //   LogClass.AddLog("GetCWCList Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString(), true);
                return null;
            }

            try
            {
                SqlCommand cmd = new SqlCommand("select TOP 1 * from CAGRINOTLARI where CALLID='" + CALLID + "' order by CREATEDATETIME DESC", conn);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable data = new DataTable();
                da.Fill(data);

                return data;
            }
            catch (Exception ex)
            {
                //  LogClass.AddLog("GetCWCList : " + ex.ToString(), true);

                return null;
            }
            finally
            {
                conn.Close();
            }
        }
        public static bool GetSifirlamaFlag(string ANKETNO)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM SIFIRLAMAGUNCEL WHERE ANKETNO = '" + ANKETNO + "'", conn);
                DataTable dt = new DataTable();
                da.Fill(dt);
                if(dt.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public static void GetCallDetails(string CALLID)
        {
            //DataTable callRec = new DataTable();
            //try
            //{
            //    DataProvider obj = new DataProvider();
            //}
            //catch(Exception Ex)
            //{

            //}
            //return callRec;
        }

        public static DataTable GetCallLog(string CALLID)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                //   LogClass.AddLog("GetCWCList Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString(), true);
                return null;
            }

            try
            {
                SqlCommand cmd = new SqlCommand("select * from ISLEMGECMISI where CALLID='" + CALLID + "' order by CREATEDATETIME", conn);
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                DataTable data = new DataTable();
                da.Fill(data);

                return data;
            }
            catch (Exception ex)
            {
                //  LogClass.AddLog("GetCWCList : " + ex.ToString(), true);

                return null;
            }
            finally
            {
                conn.Close();
            }
        }
        public static void AddCallLog(string CALLID, string UserName, string ACTIONDESC, string REFID)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                //   LogClass.AddLog("GetCWCList Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString(), true);
                return;
            }

            try
            {
                SqlCommand cmd = new SqlCommand("INSERT INTO [ISLEMGECMISI] ([CALLID] ,[USERNAME] ,[CREATEDATETIME],[ACTIONDESC] ,[REFID]) VALUES ('"+CALLID+"','"+UserName+"', getdate(),'"+ACTIONDESC + "','"+ REFID + "')", conn);
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();
                return ;
            }
            catch (Exception ex)
            {
                //  LogClass.AddLog("GetCWCList : " + ex.ToString(), true);

                return;
            }
            finally
            {
                conn.Close();
            }
        }
        public static DataTable GetGroups(string GroupIDs)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand("select * , (Select COUNT(AgentID) from Agents where Agents.GroupID = Groups.GroupID AND IsActive=1) as AgentCount, (Select COUNT(*) from CWCList where CWCList.GroupID = Groups.GroupID and CWCList.IsActive=1 and CWCList.ParentID>0) as CWCCount from Groups "+(GroupIDs==""?"":" where GroupID in (" + GroupIDs + ")")+" order by ParentGroupID, GroupName", conn);

                cmd.CommandType = CommandType.Text;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }


        public static DataTable GetCWCKeys()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("GetCWCKeys Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand("Select distinct Main.ReportKey, Left(Main.ReportCodes,Len(Main.ReportCodes)-1) As ReportCodes "
+ " From     (        Select distinct ST2.ReportKey,              (                 Select ST1.ItemCode + ',' AS [text()]                 From dbo.Dictionary ST1                 Where ST1.ReportKey = ST2.ReportKey "
 +" ORDER BY ST1.ItemCode For XML PATH ('') ) [ReportCodes] From dbo.Dictionary ST2 ) [Main]", conn);

                cmd.CommandType = CommandType.Text;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception("GetCWCKeys Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            } 
        }
       public static DataTable GetUnits()
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand("select *, dbo.FN_GETUNITNAME(ParentUnitID) as ParentName, dbo.FN_GETDEPTNAME(DepartmentID) as DepartmentName from Units order by ViewOrder", conn);

                cmd.CommandType = CommandType.Text;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
        public static DataTable GetUsers()
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand("select KULLANICIKOD as KULLANICI from KULLANICILAR WHERE DEGERLENDIREBILIR = 1 order by KULLANICIKOD", conn);

                cmd.CommandType = CommandType.Text;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
        public static DataTable GetWallboard()
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand("select * from ArtRT.dbo.ReportRealTimeData where Column1 in ('41','42','43') order by Column1", conn);

                cmd.CommandType = CommandType.Text;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
        public static DataTable GetUserTypes()
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand("select * from UserTypes order by UserLevel", conn);

                cmd.CommandType = CommandType.Text;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }

        public static DataTable GetEval(string EVALID, int PROJENO)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand("select * from ANKETLER where ANKETNO='"+EVALID +"' and PROJENO='"+PROJENO+"'", conn); //değiştirdim

                cmd.CommandType = CommandType.Text;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
        public static DataTable GetEvalDetails(string EVALID)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand("select * from ANKETDETAY where ANKETID=" + EVALID, conn);

                cmd.CommandType = CommandType.Text;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                throw new Exception("Liste Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }


        public static int ExecuteNonQuery(string sql)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("ExecuteNonQuery: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand(sql, conn);

                cmd.CommandType = CommandType.Text;

                return cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception("ExecuteNonQuery Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }
        public static string ExecuteScalar(string sql)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("ExecuteNonQuery: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand(sql, conn);

                cmd.CommandType = CommandType.Text;

                object obj = cmd.ExecuteScalar();
                if (obj== null) return "";
                return obj.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("ExecuteNonQuery Hata: " + ex.ToString());
            }
            finally
            {
                conn.Close();
            }
        }

     
    }
}