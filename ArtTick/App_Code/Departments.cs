﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ARTQM
{
    public class Departments
    {
        public int DepartmentID { get; set; }

        public string DepartmentName { get; set; }

        public string DepartmentDesc { get; set; }

        public string DepartmentEmail { get; set; }

        public string DepartmentPhone { get; set; }

        public string DepartmentCity { get; set; }

        public bool? IsDeleted { get; set; }

    }

}