﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ARTQM
{
    public partial class MQuestions : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string tanimlamalar = Session["TANIMLAMALAR"].ToString();

            if (tanimlamalar.Equals("0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                        "alert('Yetkiniz Yok'); window.location='" + Request.UrlReferrer.ToString() + "';", true);
                return;
            }
            if (!IsPostBack)
            {
                Session["SelectedID"] = null;
             
                LoadForms();
                if (string.IsNullOrEmpty(Request.QueryString["formno"]))
                {
                    drpForm.SelectedIndex = 0;
                    LoadData();
                }
                else 
                {
                    drpForm.SelectedValue = Request.QueryString["formno"];
                    LoadData();
                }
            }
        }

        public void LoadForms()
        {
            Session["Forms"] = Lists.GetForms();     
            drpForm.DataSource = (DataTable)Session["Forms"];
            drpForm.DataValueField = "PROJENO";
            drpForm.DataTextField= "PROJEADI";
            drpForm.DataBind();


            Session["Types"] = Lists.GetQuestionsTypes();
            drpSORUTIPI.DataSource = (DataTable)Session["Types"];
            drpSORUTIPI.DataValueField = "TipID";
            drpSORUTIPI.DataTextField = "SoruTipi";
            drpSORUTIPI.DataBind();

            drpTemplate.DataSource = Lists.GetTemplates();


            drpTemplate.DataValueField = "TASLAKID";
            drpTemplate.DataTextField= "TASLAK";
            drpTemplate.DataBind();
            drpTemplate.SelectedIndex = 0;
            drpTemplate_SelectedIndexChanged(null, null);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
            try
            {
                conn.Open();
            }
            catch (Exception ex)
            {
                throw new Exception("Bildirim Hata: Veri sunucusuna bağlanılamıyor. " + ex.ToString());

            }
        
            try
            {
                SqlCommand cmd = new SqlCommand("INSERT INTO SORULAR ([SORUSIRA] ,[SORUMETNI] ,[KISAETIKET],[PROJENO],[KRITERNO] ,[EVETHAYIR] ,[SECENEKLER] ,[SECENEKLERPUAN] ,[MAKSSKOR] ,[TOPLANIR],[ACIKLAMA]) "
                        + " VALUES (@SORUSIRA, @SORUMETNI,@KISAETIKET,@PROJENO,@KRITERNO,@SORUTIPI,@SECENEKLER,@PUANLAMA, NULL, 1,@ACIKLAMA)", conn);

                int result = 0;
                if (Session["SelectedID"] == null)
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add("@SORUMETNI", SqlDbType.NVarChar, 500).Value = txtSORUMETNI.Text;
                    cmd.Parameters.Add("@KISAETIKET", SqlDbType.NVarChar, 100).Value = txtKISAETIKET.Text;
                    cmd.Parameters.Add("@SORUSIRA", SqlDbType.NVarChar, 50).Value = int.Parse(txtOrder.Text.Trim());
                    cmd.Parameters.Add("@PROJENO", SqlDbType.Int).Value = int.Parse(drpForm.SelectedValue);
                    cmd.Parameters.Add("@KRITERNO", SqlDbType.Int).Value = drpTitle.SelectedValue;
                    cmd.Parameters.Add("@SORUTIPI", SqlDbType.Int).Value = drpSORUTIPI.SelectedValue;
                    cmd.Parameters.Add("@SECENEKLER", SqlDbType.NVarChar, 2000).Value = txtSECENEKLER.Text;
                    cmd.Parameters.Add("@PUANLAMA", SqlDbType.NVarChar, 255).Value = txtPUANLAMA.Text;
                    cmd.Parameters.Add("@ACIKLAMA", SqlDbType.NVarChar, 1000).Value = txtAciklama.Text.Trim().Replace("'","`");
                    result = cmd.ExecuteNonQuery();
                }
                else
                {
                    cmd.CommandText = "UPDATE SORULAR SET [SORUSIRA]=@SORUSIRA ,[SORUMETNI]=@SORUMETNI,[KISAETIKET]=@KISAETIKET, [KRITERNO]=@KRITERNO,[EVETHAYIR]=@SORUTIPI,[SECENEKLER]=@SECENEKLER,[SECENEKLERPUAN]=@PUANLAMA,[ACIKLAMA]=@ACIKLAMA where SORUNO= @SORUNO";
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.Add("@SORUMETNI", SqlDbType.NVarChar, 500).Value = txtSORUMETNI.Text;
                    cmd.Parameters.Add("@KISAETIKET", SqlDbType.NVarChar, 100).Value = txtKISAETIKET.Text;
                    cmd.Parameters.Add("@SORUSIRA", SqlDbType.NVarChar, 50).Value = int.Parse(txtOrder.Text.Trim());
                    cmd.Parameters.Add("@PROJENO", SqlDbType.Int).Value = int.Parse(drpForm.SelectedValue);
                    cmd.Parameters.Add("@KRITERNO", SqlDbType.Int).Value = drpTitle.SelectedValue;
                    cmd.Parameters.Add("@SORUTIPI", SqlDbType.Int).Value = drpSORUTIPI.SelectedValue;
                    cmd.Parameters.Add("@SECENEKLER", SqlDbType.NVarChar, 2000).Value = txtSECENEKLER.Text;
                    cmd.Parameters.Add("@PUANLAMA", SqlDbType.NVarChar, 255).Value = txtPUANLAMA.Text;
                    cmd.Parameters.Add("@ACIKLAMA", SqlDbType.NVarChar, 1000).Value = txtAciklama.Text.Trim().Replace("'", "`");
                    cmd.Parameters.Add("@SORUNO", SqlDbType.Int).Value = Session["SelectedID"].ToString();
                    
                    result = cmd.ExecuteNonQuery();
                }

                hResult.InnerHtml = "Kayıt Başarılı";
                bResult.InnerHtml = "Soru kaydı başarıyla alınmıştır.";
                divResult.InnerHtml = "<script>$('#myModal').modal(); </script>";

                LoadData();

            }
            catch (Exception ex)
            {
                hResult.InnerHtml = "Hata Oluştu!";
                bResult.InnerHtml = "Soru  kaydı gerçekleştirilemedi. Hata Mesajı: " + ex.Message;
                divResult.InnerHtml = "<script>$('#myModal').modal();  </script>";


            }
            finally
            {
                conn.Close();
                Session["SelectedID"] = null;
            }
        }
        
     
        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    Session["SelectedID"] = null;
        //    ExportToExcel(grdData, drpForm.SelectedItem.Text.Replace(" ", "_").Replace("-", "_").Replace(".", "_").Replace("/", "_").Replace("\\", "_") + "_KIRILIMLAR");
        //}

        protected void lnkNew_Click(object sender, EventArgs e)
        {
            try
            {
                Session["SelectedID"] = null;
                txtAciklama.Text = txtKISAETIKET.Text =  txtPUANLAMA.Text = txtSECENEKLER.Text = txtSORUMETNI.Text = "";
                drpSORUTIPI.SelectedIndex = 0;
                drpTemplate.SelectedIndex = 0;
                txtOrder.Text = "0";
                divResult.InnerHtml = "<script>$('#divUpdate').modal(); </script>";
            }
            catch(Exception Ex)
            {
            }
        }

        protected void grdData_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "editt":
                    try
                    {
                        Session["SelectedID"] = null;
                        DataTable groups = (DataTable)Session["Questions"];
                        string id = e.CommandArgument.ToString();
                        Session["SelectedID"] = id;
                        DataRow dr = groups.Select("SORUNO=" + id)[0];
                        txtSORUMETNI.Text = dr["SORUMETNI"].ToString();
                        txtKISAETIKET.Text = dr["KISAETIKET"].ToString();
                        txtAciklama.Text = dr["ACIKLAMA"].ToString();
                        txtPUANLAMA.Text = dr["SECENEKLERPUAN"].ToString();
                        txtSECENEKLER.Text = dr["SECENEKLER"].ToString();
                        txtOrder.Text = dr["SORUSIRA"].ToString();
                        drpSORUTIPI.SelectedValue = dr["SORUTIPI"].ToString();


                       //drpTitle.SelectedValue = dr["DUMMYID"].ToString();
                        divResult.InnerHtml = "<script>$('#divUpdate').modal(); </script>";

                    }
                    catch(Exception ex)
                    {
                    }
                    break;

               
                default: break;
              
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect("MQuestions.aspx?formno=" + drpForm.SelectedValue, true);
                
        }
        public void LoadData()
        {
            try
            {
                int formno = int.Parse(drpForm.SelectedValue);
                Session["Questions"] = Lists.GetQuestions(formno);
                grdData.DataSource = (DataTable)Session["Questions"];
                grdData.DataKeyNames = new string[] { "SORUNO" };
                grdData.DataBind();

                Session["Titles"] = Lists.GetTitles(formno);
                drpTitle.DataSource = (DataTable)Session["Titles"];
                drpTitle.DataValueField = "KRITERNO";
                drpTitle.DataTextField = "KRITER";

                drpTitle.DataBind();
            }
            catch(Exception ex)
            {

            }
           
        }

        protected void drpTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string sec = drpTemplate.SelectedItem.Text.Split('|')[0].Trim();
                string puan = drpTemplate.SelectedItem.Text.Split('|')[1].Trim();

                txtSECENEKLER.Text = sec;
                txtPUANLAMA.Text = puan;
            }
            catch(Exception ex)
            {

            }
        }
    }
}