﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ARTQM
{
    public partial class EditEval : PageBase
    {
        public int PROJENO= 0;
        public string AnketNo = "0";
        DataProvider obj = new DataProvider();
        public static bool sifirlamaFlag = false;
        DataTable carsafKriter;
        DataTable dt;

        protected void Page_Load(object sender, EventArgs e)
        {
            if(!(Convert.ToInt32(Session["YETKİ"].ToString()) > 1))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Yetkiniz Yok'); window.location='" + Request.UrlReferrer.ToString() + "';", true);
                return;
            }

            if (!IsPostBack)
            {
                if (String.IsNullOrEmpty(Request.QueryString["projeno"]))
                    Response.Redirect("Calls.aspx", true);
                this.PROJENO = int.Parse(Request.QueryString["projeno"]);

                LoadDetails();
                loadTextBox();
                loadSifirlama();

                

                if (Request.QueryString["anket"] == "")
                {

                    LoadEmpty();

                    
                }
                else
                {
                    AnketNo = "0"; //  int.Parse(Request.QueryString["anket"]);
                    LoadAnket();
                }

            }

        }

        protected void loadTextBox()
        {
            DataTable dt = Lists.GetFirstCallNotes(Request.QueryString["callid"]);
            if(dt.Rows.Count > 0)
            {
                txtNot.Text = dt.Rows[0]["NOTE"].ToString();
            }
            
        }
        protected void loadSifirlama()
        {
            sifirlamaFlag = Lists.GetSifirlamaFlag(Request.QueryString["callid"]);
            if (sifirlamaFlag)
            {
                sifirlama.Checked = true;
            }
            else
            {
                sifirlama.Checked = false;
            }
        }
        public void LoadDetails()
        {
            //    try
            //    {
            //        DataTable dt = Lists.GetCallDetails(Request.QueryString["callid"]);
            //        lblAgent.Text = dt.Rows[0]["AGENTS"].ToString();
            //        lblDuration.Text = dt.Rows[0]["DURATION"].ToString();
            //        lblDirection.Text = dt.Rows[0]["DIRECTION"].ToString();
            //        lblParties.Text = dt.Rows[0]["PARTIES"].ToString();
            //        lblGroup.Text = dt.Rows[0]["SKILLSET"].ToString();
            //        lblStartDate.Text = dt.Rows[0]["STARTDATE"].ToString();
            //    }
            //    catch
            //    {
            //    }


            //WFOUtils ws = new WFOUtils();
            //DataTable segments = obj.EditEvalGetList("SELECT cdr.UNIQUEID, cdr.CALLDATE, cdr.SRC, cdr.DST, cdr.DURATION, cdr.USERFIELD, u.USERCODE AS USERCODE FROM TBLCCCDR AS cdr LEFT JOIN TBLUSERS AS u ON cdr.UID = u.UID WHERE UNIQUEID = " + Request.QueryString["callid"]);
            //DataTable segments = ws.GetCallSegments(Request.QueryString["callid"]);
            DataTable segments = new DataTable();
            segments.Columns.Add("inum");
            segments.Columns.Add("startedat");
            segments.Columns.Add("duration2");
            segments.Columns.Add("src");
            segments.Columns.Add("dst");
            segments.Columns.Add("userfield");
            segments.Columns.Add("usercode");

            segments.Rows.Add(Request.QueryString["callid"], Request.QueryString["startdate"], Request.QueryString["duration"], Request.QueryString["partynames"], "", Request.QueryString["direction"], Request.QueryString["partyagentnames"]);

            if (segments != null)
            {
                grdSegments.DataSource = segments;
                grdSegments.DataKeyNames = new string[] { "inum" };
                grdSegments.DataBind();
            }
        }
        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

            string inum = grdSegments.SelectedDataKey.Value.ToString();
            //frmPlayer.Attributes["src"] = "Player.aspx?inum=" + inum;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://176.88.216.226/Tobe/app/ApplicationServlet?externalService=downloadRecording&usercode=artpro.admin&clearpassword=Artpro12!&uniqueid=" + inum);
            //string authInfo = ARTQM.Properties.Settings.Default.ACRAPIUSER + ":" + ARTQM.Properties.Settings.Default.ACRAPIPWD;
            //authInfo = Convert.ToBase64String(Encoding.Default.GetBytes(authInfo));
            //request.Headers.Add("Authorization", "Basic " + authInfo);
            //request.Credentials = new NetworkCredential(ARTQM.Properties.Settings.Default.ACRAPIUSER, ARTQM.Properties.Settings.Default.ACRAPIPWD);
            request.AllowAutoRedirect = true;
            request.Proxy = null;
            request.MaximumAutomaticRedirections = 1;

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            if (response.ContentType == "audio/mpeg;charset=UTF-8")
            {
                MemoryStream ms = new MemoryStream();
                response.GetResponseStream().CopyTo(ms);
                ms.Position = 0;
                FileStream sw = new FileStream(Server.MapPath("temp") + "\\" + inum + ".mp3", FileMode.OpenOrCreate);
                ms.WriteTo(sw);
                ms.Close();
                sw.Close();

                Player.Attributes["src"] = "temp\\" + inum + ".mp3";
            }
            else if (response.ContentType == "application/zip;charset=UTF-8")
            {
                DataProvider obj = new DataProvider();

                DataTable dt = obj.GetCallRec("SELECT AUDIOFILENAME, CALLDATE FROM TBLCCCDR WHERE UNIQUEID = '" + inum + "'");

                if (!File.Exists(Server.MapPath("temp") + "\\" + dt.Rows[0]["audioFileName"].ToString().Trim() + ".mp3"))
                {
                    MemoryStream ms = new MemoryStream();
                    response.GetResponseStream().CopyTo(ms);
                    ms.Position = 0;

                    //FileStream sw = new FileStream(@"C:\Users\lorta\Desktop\TegsoftSesKayit\TegsoftSesKayit\zip\20190503-1556892009.300.zip", FileMode.OpenOrCreate);
                    FileStream sw = new FileStream(Server.MapPath("zip") + "\\" + inum + ".zip", FileMode.OpenOrCreate);
                    ms.WriteTo(sw);
                    ms.Close();
                    sw.Close();

                    //ZipFile.ExtractToDirectory(@"C:\Users\lorta\Desktop\TegsoftSesKayit\TegsoftSesKayit\zip\20190503-1556892009.300.zip", @"C:\Users\lorta\Desktop\TegsoftSesKayit\TegsoftSesKayit\temp");



                    ZipFile.ExtractToDirectory(Server.MapPath("zip") + "\\" + inum + ".zip", Server.MapPath("temp"));
                }
                Player.Attributes["src"] = "temp\\" + dt.Rows[0]["audioFileName"].ToString().Trim() + ".mp3";
            }

        }
        public void LoadEmpty()
        {
            try
            {
                DataTable kriterler = Lists.GetTitles(PROJENO);
                DataTable sorular = Lists.GetQuestions2(PROJENO,"0");
                string agent = "";
                lblProje.Text = Lists.GetEvalInfo(Request.QueryString["callid"], PROJENO.ToString(), out agent);

                DataTable dt = new DataTable();
                dt.Columns.Add("SORUNO");
                dt.Columns.Add("SORUSIRA");
                dt.Columns.Add("EVETHAYIR");
                dt.Columns.Add("SECENEKLER");
                dt.Columns.Add("ETIKET");
                dt.Columns.Add("YANIT");
                dt.Columns.Add("YANITNO");
                dt.Columns.Add("MAKSSKOR");
                dt.Columns.Add("PUAN");
                dt.Columns.Add("TOPLANIR");
                dt.Columns.Add("DEGER");
                dt.Columns.Add("ONCELIK");
                dt.Columns.Add("OZELNOT");

                Session["AGENT"] = agent;
                Session["CALLID"] = Request.QueryString["callid"];

                // lblSaat.Text = anket.BASLANGICSAAT;
                // lblGizliMusteri.Text = anket.GIZLI;

                //  lblTarih.Text = anket.KATILIMTARIH.ToString("dd.MM.yyyy");
                string prevkriter = "";
                string ustbaslik = "";
                string prevsira = "";
                DataRow dr;
                DataTable ustkisim = new DataTable("");
                ustkisim.Columns.Add("YANITNO");
                ustkisim.Columns.Add("SORUNO");
                ustkisim.Columns.Add("YANIT");


                Dictionary<string, string> _ustkisim = new Dictionary<string, string>();
                foreach (DataRow soru in sorular.Rows)
                {

                    if (prevkriter != soru["KRITERNO"].ToString())
                    {

                        dr = dt.NewRow();
                        dr["SORUNO"] = 0;
                        dr["SORUSIRA"] = "";
                        dr["ETIKET"] = "+" + kriterler.Select("KRITERNO=" +soru["KRITERNO"].ToString())[0]["KRITER"].ToString();
                        dr["MAKSSKOR"] = "";
                        dr["PUAN"] = "";
                        prevkriter = soru["KRITERNO"].ToString();
                        dt.Rows.Add(dr);

                    }
                    dr = dt.NewRow();
                    dr["SORUNO"] = soru["SORUNO"].ToString();
                    dr["SORUSIRA"] = soru["SORUSIRA"].ToString();
                  //  dr["TOPLANIR"] = soru["TOPLANIR"].ToString();
                    if (prevsira == "")
                    {
                        prevsira = soru["SORUSIRA"].ToString();
                        dr["ETIKET"] = "*S" + soru["SORUSIRA"].ToString() + "." + soru["SORUMETNI"].ToString() + (soru["ACIKLAMA"].ToString()==""? "":  "<br/>" + soru["ACIKLAMA"].ToString());
                        
                    }
                    else
                        if (prevsira == soru["SORUSIRA"].ToString())
                            dr["ETIKET"] = soru["SORUMETNI"].ToString();
                        else
                        {
                            dr["ETIKET"] = "*S" + soru["SORUSIRA"].ToString() + "." + soru["SORUMETNI"].ToString() + (soru["ACIKLAMA"].ToString() == "" ? "" : "<br/>" + soru["ACIKLAMA"].ToString());
                            prevsira = soru["SORUSIRA"].ToString();
                        }
                    dr["SECENEKLER"] = soru["SECENEKLER"];
                    dr["EVETHAYIR"] = soru["EVETHAYIR"].ToString();
                    dr["MAKSSKOR"] = soru["MAKSSKOR"];
                    dr["ONCELIK"] = soru["SORUSIRA"];

                    //try
                    //{
                    //    string[] _y = FindYanit(detaylar, soru.SORUNO);
                    //    dr["YANITNO"] = _y[0];
                    //    dr["YANIT"] = _y[1];
                    //    dr["DEGER"] = _y[2];
                    //}
                    //catch
                    //{
                    //}

                    //TOPLANAN /OR ILE ISLENEN SUTUNLARIN PUANI BURAYA
                    try
                    {
                        dt.Rows.Add(dr);
                    }
                    catch
                    {
                    }
                }


                grdSorular.DataSource = dt;

                grdSorular.DataKeyNames = new string[] { "YANITNO", "SORUNO", "YANIT", "SECENEKLER", "EVETHAYIR", "DEGER" };
                grdSorular.DataBind();
                Session["grdSorular1"] = dt;
                int r = 0;
                foreach (DataRow _row in dt.Rows)
                {
                    DropDownList cb = (DropDownList)grdSorular.Rows[r].FindControl("cbYANIT");
                    TextBox txt = (TextBox)grdSorular.Rows[r].FindControl("txtYANIT");
                    if (!_row["EVETHAYIR"].ToString().StartsWith("-"))
                    {
                        txt.Visible = false;
                        cb.Visible = true;
                        cb.Items.Clear();
                        string[] secenekler = _row["SECENEKLER"].ToString().Split(Properties.Settings.Default.FIELDSEP1);
                        int _s = 1;
                        //cb.Items.Add(new ListItem("---",""));
                        try
                        {
                            foreach (string s in secenekler)
                            {
                                if(s != "")
                                {
                                    //değiştirdim
                                    //cb.Items.Add(new ListItem(s.Trim(), _s.ToString()));
                                    cb.Items.Add(new ListItem(s.ToString().Split(Properties.Settings.Default.FIELDSEP2)[1], s.ToString().Split(Properties.Settings.Default.FIELDSEP2)[0]));
                                    _s++;
                                }
                            }
                        }
                        catch
                        {
                        }
                        try
                        {
                            if (cb.Items.FindByValue(_row["DEGER"].ToString().Trim()) == null)
                                cb.SelectedValue = cb.Items.FindByValue(_row["DEGER"].ToString().Trim()).Value;
                            else
                                cb.SelectedValue = _row["DEGER"].ToString();
                            txt.Text = _row["DEGER"].ToString();

                        }
                        catch
                        {
                            cb.SelectedIndex = -1;
                        }
                    }
                    else
                    {
                        txt.Visible = true;
                        cb.Visible = false;
                    }
                    r++;
                    //buraya ekle dropdownbox bu fonksiyonda setleniyor
                    cb.SelectedIndex = 0;
                    
                }


                int row = 0;
                int bolum = 0;
                foreach (DataRow _dr in dt.Rows)
                {
                    if (_dr["SORUNO"].ToString() == "0" && !_dr["ETIKET"].ToString().StartsWith("+")) //ustbaslik
                    {
                        bolum++;
                        grdSorular.Rows[row].CssClass = "ust_baslik" + (bolum == 1 ? "" : "2");
                        grdSorular.Rows[row].FindControl("txtYANIT").Visible = false;
                        grdSorular.Rows[row].FindControl("cbYANIT").Visible = false;

                    }
                    else if (_dr["SORUNO"].ToString() == "0" && _dr["ETIKET"].ToString().StartsWith("+")) //kriter
                    {
                        grdSorular.Rows[row].CssClass = "kriter_baslik" + (bolum == 1 ? "" : "2");
                        grdSorular.Rows[row].Cells[0].Text = grdSorular.Rows[row].Cells[0].Text.Replace("+", "");
                        grdSorular.Rows[row].FindControl("txtYANIT").Visible = false;
                        grdSorular.Rows[row].FindControl("cbYANIT").Visible = false;
                    }
                    else
                    {
                        if (bolum != 1) grdSorular.Rows[row].CssClass = "sorualtrow";
                        if (_dr["EVETHAYIR"].ToString().StartsWith("-")) //kalitatifler / yorum 
                        {
                            //grdSorular.Rows[row].CssClass = "yorum";
                            // grdSorular.Rows[row].Cells[3].Text = "";
                            ((TextBox)grdSorular.Rows[row].FindControl("txtYANIT")).TextMode = TextBoxMode.MultiLine;
                            ((TextBox)grdSorular.Rows[row].FindControl("txtYANIT")).Rows = 3;
                        }
                        else if (!_dr["ETIKET"].ToString().StartsWith("*")) // soru baslıgı degil saga yasla
                        {
                            grdSorular.Rows[row].Cells[0].CssClass = "kriter_secenek";
                            grdSorular.Rows[row].Cells[0].HorizontalAlign = HorizontalAlign.Right;
                        }

                        if (!_dr["EVETHAYIR"].ToString().StartsWith("-"))
                        {
                            grdSorular.Rows[row].FindControl("txtYANIT").Visible = false;
                            grdSorular.Rows[row].FindControl("cbYANIT").Visible = true;
                        }
                        else
                        {
                            grdSorular.Rows[row].FindControl("txtYANIT").Visible = true;
                            grdSorular.Rows[row].FindControl("cbYANIT").Visible = false;
                        }
                    }
                    grdSorular.Rows[row].Cells[0].Text = grdSorular.Rows[row].Cells[0].Text.Replace("*", "");
                    row++;
                }




                grdSorular.Columns[2].Visible = true;

            }
            catch (Exception ex)
            {
                Response.Write(ex.ToString());

            }

        }

        public void LoadAnket()
        {
            try
            {
                DataTable anket = Lists.GetEval(Request.QueryString["callid"], PROJENO);
                if (anket == null || anket.Rows.Count == 0)
                {
                    LoadEmpty();
                    return;
                }
                Session["ANKET"] = anket;
                AnketNo = anket.Rows[0]["ANKETID"].ToString();
                DataTable detaylar = Lists.GetEvalDetails(AnketNo);
                DataTable kriterler = Lists.GetTitles(PROJENO);
                DataTable sorular = Lists.GetQuestions2(PROJENO,"0");
                string agent = "";
                lblProje.Text = Lists.GetEvalInfo(Request.QueryString["callid"], PROJENO.ToString(), out agent);

                Session["CALLID"] = Request.QueryString["callid"];
                Session["AGENT"] = agent;

                dt = new DataTable();
                dt.Columns.Add("SORUNO");
                dt.Columns.Add("SORUSIRA");
                dt.Columns.Add("EVETHAYIR");
                dt.Columns.Add("SKORADAHIL");
                dt.Columns.Add("SECENEKLER");
                dt.Columns.Add("ETIKET");
                dt.Columns.Add("YANIT");
                dt.Columns.Add("YANITNO");
                dt.Columns.Add("MAKSSKOR");
                dt.Columns.Add("PUAN");
                dt.Columns.Add("TOPLANIR");
                dt.Columns.Add("ONCELIK");
                dt.Columns.Add("DEGER");
                dt.Columns.Add("OZELNOT");




                // lblSaat.Text = anket.BASLANGICSAAT;
                // lblGizliMusteri.Text = anket.GIZLI;

                //  lblTarih.Text = anket.KATILIMTARIH.ToString("dd.MM.yyyy");
                string prevkriter = "";
                string prevsira = "";
                DataRow dr;

                Dictionary<string, string> _ustkisim = new Dictionary<string, string>();
                foreach (DataRow soru in sorular.Rows)
                {

                    if (prevkriter != soru["KRITERNO"].ToString())
                    {

                        dr = dt.NewRow();
                        dr["SORUNO"] = 0;
                        dr["SORUSIRA"] = "";
                        dr["ETIKET"] = "+" + kriterler.Select("KRITERNO=" + soru["KRITERNO"].ToString())[0]["KRITER"].ToString();
                        dr["MAKSSKOR"] = "";
                        dr["PUAN"] = "";
                        prevkriter = soru["KRITERNO"].ToString();
                        dt.Rows.Add(dr);

                    }
                    dr = dt.NewRow();
                    dr["SORUNO"] = soru["SORUNO"].ToString();
                    dr["SORUSIRA"] = soru["SORUSIRA"].ToString();
                    dr["TOPLANIR"] = soru["TOPLANIR"].ToString();
                    if (prevsira == "")
                    {
                        prevsira = soru["SORUSIRA"].ToString();
                        dr["ETIKET"] = "*S" + soru["SORUSIRA"].ToString() + "." + soru["SORUMETNI"].ToString() + (soru["ACIKLAMA"].ToString() == "" ? "" : "<br/>" + soru["ACIKLAMA"].ToString()); 
                    }
                    else
                        if (prevsira == soru["SORUSIRA"].ToString())
                            dr["ETIKET"] = soru["SORUMETNI"].ToString();
                        else
                        {
                            dr["ETIKET"] = "*S" + soru["SORUSIRA"].ToString() + "." + soru["SORUMETNI"].ToString() + (soru["ACIKLAMA"].ToString() == "" ? "" : "<br/>" + soru["ACIKLAMA"].ToString());
                            prevsira = soru["SORUSIRA"].ToString();
                        }
                    dr["SECENEKLER"] = soru["SECENEKLER"];
                    dr["EVETHAYIR"] = soru["EVETHAYIR"].ToString();
                    dr["MAKSSKOR"] = soru["MAKSSKOR"];
                    dr["ONCELIK"] = soru["SORUSIRA"];


                    try
                    {
                        dr["YANITNO"] = detaylar.Select("SORUNO=" + soru["SORUNO"].ToString())[0]["YANITNO"].ToString();
                        dr["YANIT"] = detaylar.Select("SORUNO=" + soru["SORUNO"].ToString())[0]["YANIT"];
                        dr["DEGER"] = (int)parse2Double(detaylar.Select("SORUNO=" + soru["SORUNO"].ToString())[0]["DEGER"].ToString());
                    }
                    catch
                    {
                    }

                    //TOPLANAN /OR ILE ISLENEN SUTUNLARIN PUANI BURAYA
                    try
                    {
                        dt.Rows.Add(dr);
                    }
                    catch
                    {
                    }
                }
                DataRow[] hesaplanacaklar = dt.Select("ETIKET like '%<br/>%' and SORUSIRA like '%.%' and SORUNO > 0");
                foreach (DataRow _row in hesaplanacaklar)
                {
                    DataRow[] _kk = dt.Select("SORUSIRA='" + _row["SORUSIRA"].ToString() + "'");
                    float sum = 0;
                    foreach (DataRow _k in _kk)
                    {
                        if (_k["TOPLANIR"].ToString() == "1")
                            sum += parse2Float(_k["PUAN"].ToString());
                        else
                            if (parse2Float(_k["PUAN"].ToString()) > 0)
                            { sum = 1; break; }
                    }
                    _row["PUAN"] = String.Format("{0:0}", (int)sum);
                }
                hesaplanacaklar = dt.Select("ETIKET like '%<br/>%' and SORUSIRA not like '%.%' and SORUNO > 0");
                foreach (DataRow _row in hesaplanacaklar)
                {
                    DataRow[] _kk = dt.Select("SORUSIRA='" + _row["SORUSIRA"].ToString() + "'");
                    float sum = 0;
                    if (_row["TOPLANIR"].ToString() != "0")
                    {
                        foreach (DataRow _k in _kk)
                        {
                            if (_k["TOPLANIR"].ToString() == "1")
                                sum += parse2Float(_k["PUAN"].ToString());
                            else
                            {
                                if (parse2Float(_k["PUAN"].ToString()) > 0)
                                { sum = 1; break; }
                            }
                        }
                        _row["PUAN"] = String.Format("{0:0}", (int)sum);
                    }
                    else _row["PUAN"] = "";
                }

                /* grdKisiler.DataSource = ustkisim;
                 grdKisiler.DataKeyNames = new string[] { "YANITNO", "SORUNO", "YANIT" };
                 grdKisiler.DataBind();
                 */

                foreach(DataRow item in dt.Rows)
                {
                    foreach(DataRow detay in detaylar.Rows)
                    {
                        if(item["SORUNO"].ToString() == detay["SORUNO"].ToString())
                        {
                            item["OZELNOT"] = detay["OZELNOT"];
                        }
                    }
                }

                grdSorular.DataSource = dt;
                // ----->   DEĞİŞİKLİK : KEYLERE DEGER SUTUNU EKLENDİ

                grdSorular.DataKeyNames = new string[] { "YANITNO", "SORUNO", "YANIT", "SECENEKLER", "EVETHAYIR", "DEGER" };
                grdSorular.DataBind();
                Session["grdSorular1"] = dt;
                int r = 0;
                foreach (DataRow _row in dt.Rows)
                {
                    DropDownList cb = (DropDownList)grdSorular.Rows[r].FindControl("cbYANIT");
                    TextBox txt = (TextBox)grdSorular.Rows[r].FindControl("txtYANIT");
                    if (!_row["EVETHAYIR"].ToString().StartsWith("-"))
                    {
                        txt.Visible = false;
                        cb.Visible = true;
                        cb.Items.Clear();
                        string[] secenekler = _row["SECENEKLER"].ToString().Split(Properties.Settings.Default.FIELDSEP1);
                        int _s = 1;
                        //--- kaldırmak için burayı yoruma al
                        //cb.Items.Add(new ListItem("---", ""));
                        try
                        {
                            foreach (string s in secenekler)
                            {
                                if(s != "")
                                {
                                    //değiştirdim
                                    //cb.Items.Add(new ListItem(s.Trim(), _s.ToString()));
                                    cb.Items.Add(new ListItem(s.ToString().Split(Properties.Settings.Default.FIELDSEP2)[1], s.ToString().Split(Properties.Settings.Default.FIELDSEP2)[0]));
                                    _s++;
                                }

                            }
                        }
                        catch
                        {
                        }
                        try
                        {
                            if (cb.Items.FindByValue(_row["DEGER"].ToString().Trim()) == null)
                                cb.SelectedValue = cb.Items.FindByValue(_row["DEGER"].ToString().Trim()).Value;
                            else
                                cb.SelectedValue = _row["DEGER"].ToString();
                            txt.Text = _row["DEGER"].ToString();

                        }
                        catch
                        {
                            cb.SelectedIndex = -1;
                        }
                    }
                    else
                    {
                        txt.Visible = true;
                        cb.Visible = false;
                    }
                    r++;
                }


                int row = 0;

                foreach (DataRow _dr in dt.Rows)
                {
                    if (_dr["SORUNO"].ToString() == "0" && _dr["ETIKET"].ToString().StartsWith("+")) //kriter
                    {
                        grdSorular.Rows[row].CssClass = "kriter_baslik2";
                        grdSorular.Rows[row].Cells[0].Text = grdSorular.Rows[row].Cells[0].Text.Replace("+", "");
                        grdSorular.Rows[row].FindControl("txtYANIT").Visible = false;
                        grdSorular.Rows[row].FindControl("cbYANIT").Visible = false;
                    }
                    else
                    {
                        grdSorular.Rows[row].CssClass = "sorualtrow";
                        if (_dr["EVETHAYIR"].ToString().StartsWith("-")) //kalitatifler / yorum 
                        {
                            //grdSorular.Rows[row].CssClass = "yorum";
                            // grdSorular.Rows[row].Cells[3].Text = "";
                            ((TextBox)grdSorular.Rows[row].FindControl("txtYANIT")).TextMode = TextBoxMode.MultiLine;
                            ((TextBox)grdSorular.Rows[row].FindControl("txtYANIT")).Rows = 3;
                        }
                        else if (!_dr["ETIKET"].ToString().StartsWith("*")) // soru baslıgı degil saga yasla
                        {
                            grdSorular.Rows[row].Cells[0].CssClass = "kriter_secenek";
                            grdSorular.Rows[row].Cells[0].HorizontalAlign = HorizontalAlign.Right;
                        }

                        if (!_dr["EVETHAYIR"].ToString().StartsWith("-"))
                        {
                            grdSorular.Rows[row].FindControl("txtYANIT").Visible = false;
                            grdSorular.Rows[row].FindControl("cbYANIT").Visible = true;
                        }
                        else
                        {
                            grdSorular.Rows[row].FindControl("txtYANIT").Visible = true;
                            grdSorular.Rows[row].FindControl("cbYANIT").Visible = false;
                        }
                    }
                    grdSorular.Rows[row].Cells[0].Text = grdSorular.Rows[row].Cells[0].Text.Replace("*", "");
                    row++;
                }


                string filename = anket.Rows[0]["LOGID"].ToString().Replace(" ", "_").Replace("-", "_") + "_" + anket.Rows[0]["ANKETNO"].ToString() + ".xls";

                //string temp = new ExportDemo.Libraries.ExcelConvertor().ConvertKarne(dt, Server.MapPath("temp"), filename,anket.BAYI,anket.GORUSULENKISI,anket.MARKA,anket.MODEL,(anket.KATILIMTARIH).ToString("dd.MM.yyyy"), sumtel.ToString(), sumbayi.ToString (), genelcsi.ToString());

                //Session["FILENAME"] = "temp/" + filename;


                grdSorular.Columns[2].Visible = true;

                YetkiKontrol();

            }
            catch
            {

            }
        }
     


        protected void lnkSesKaydi_Click1(object sender, ImageClickEventArgs e)
        {
            divScript.InnerHtml = ("<script>playerwin('Player.aspx?mode=1');</script>");
        }


        protected void btnOnayla_Click(object sender, EventArgs e)
        {
            //divScript.InnerHtml = "";
            //int result;
            //AnketNo = int.Parse(Session["ANKETNO"].ToString());
            //result = DataAccess.Execute.ExecuteNonQuery("Update ANKETLER set DURUM=3 WHERE ANKETNO=" + AnketNo + "");
            //if (result > 0)
            //{
            //    LoadAnket();
            //    divScript.InnerHtml = "<script></script>";

            //    String ip = HttpContext.Current.Request.UserHostAddress;
            //    String str = "INSERT INTO UPDATELOG (KULLANICIKOD,ANKETNO,USERIP,TARIH, SAAT, ACIKLAMA) VALUES ('" + AktifKullanici.KULLANICIKOD +
            //        "'," + AnketNo + ", '" + ip + "','" + DateTime.Now.ToString("yyyy.MM.dd") + "','" + DateTime.Now.ToString("HH:mm:ss") + "','ONAYLANDI' )";
            //    DataAccess.Execute.ExecuteNonQuery(str);
            //}


        }
       

        protected void grdSorular_DataBound(object sender, EventArgs e)
        {

        }
        

        protected void lnkVideoKaydi_Click(object sender, ImageClickEventArgs e)
        {
            divScript.InnerHtml = ("<script>playerwin('Player.aspx?mode=2');</script>");
        }
        public void YetkiKontrol()
        {


        }
        protected void grdSorular_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                e.Row.Cells[0].Text = Server.HtmlDecode(e.Row.Cells[0].Text);
                
            }
            catch
            {
            }
        }
        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            divScript.InnerHtml = "<script>playerwin('Download.aspx?karne=true');</script>";
        }
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            divScript.InnerHtml = "<script>playerwin('Download.aspx?karne=true');</script>";
        }


        public void KarneIsaret(string soruno, System.Drawing.Color clr)
        {
            //int row = 0;
            //for (row = 0; row < grdSorular.Rows.Count; row++)
            //{

            //    if (grdSorular.DataKeys[row].Values[1].ToString() == soruno)
            //    {
            //        grdSorular.Rows[row].BackColor = clr;
            //        grdSorular.Rows[row].FindControl("imgIkaz").Visible = true;
            //    }
            //}

        }
        protected void SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow gvr = (GridViewRow)(((Control)sender).NamingContainer);
            DropDownList duty = (DropDownList)gvr.FindControl("cbYANIT");
            duty.SelectedValue = "Evet";
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            DataTable datas = (DataTable)Session["grdSorular1"];
            divScript.InnerHtml = "";
            carsafKriter = new DataTable();
            carsafKriter.Columns.Add("Kriter");
            carsafKriter.Columns.Add("Yanit");
            int yanitIndex = 0;
            int sonuc = -1;
            string yil = Request.QueryString["donem"].Substring(0, 4);
            string ay = Request.QueryString["donem"].Substring(4, 2);
            string callid = Request.QueryString["callid"];
            //string agent = Session["AGENT"].ToString();
            string agent = Request.QueryString["agent"];
            PROJENO = int.Parse(Request.QueryString["projeno"]);

            if(agent == string.Empty)
            {
                agent = "''";
            }

            if (Request.QueryString["anket"] == "")
            {
                //yeni kayıt
                AnketNo = "0";

                string duration = Request.QueryString["duration"].ToString();
                string direction = Request.QueryString["direction"].ToString();
                string skill = Request.QueryString["skill"].ToString();
                string partynames = Request.QueryString["partynames"].ToString();
                string startdate = DateTime.Parse(Request.QueryString["startdate"]).ToString("yyyy-MM-dd HH:mm:ss");
                string skillset = Request.QueryString["skillset"].ToString();
                string partyagentnames = Request.QueryString["partyagentnames"].ToString();

                if (skill == string.Empty)
                    skill = "''";
                if (skillset == string.Empty)
                    skillset = "";

                string SQL = "INSERT INTO ANKETLER (ANKETNO, PROJENO, DATAYIL, DATAAY, LOGID, DEGERLENDIRMETARIHI, ANALIZDISI, SKORADAHIL, IPTAL,SURE, YON, SKILLS, DEGERLENDIREN, CALLDATE, PARTYNAMES, SKILLSET, PARTYAGENTNAMES) VALUES ('" + callid + "', " + PROJENO + ", " + yil + ", " + ay + ", '" + agent + "', getdate(),0,1,0, "+duration+", "+direction+", "+skill+", '"+CurrentUser.UserName+"', '"+startdate+"', '"+partynames+"', '"+skillset+"', '"+partyagentnames+"')";

                sonuc = Lists.ExecuteNonQuery(SQL);
                AnketNo = Lists.ExecuteScalar("select top 1 ANKETID FROM ANKETLER WHERE ANKETNO='" + callid + "' and PROJENO="+PROJENO+"").ToString();
                

            }
            else
            {
                /*  AnketNo = int.Parse(Request
                      .QueryString["anket"].ToString());
                  */
                string strAnket = Request.QueryString["anket"];
                //Session["ANKET"] = anket;
                if (strAnket == "")
                    AnketNo = "0";
                else
                    AnketNo = strAnket;

                string SQL = "UPDATE ANKETLER SET DATAYIL=" + yil + ", DATAAY=" + ay + ", GUNCELLEYEN='"+CurrentUser.UserName+"', GUNCELLEME=getdate(), DURUM=2 WHERE ANKETID=" + AnketNo;

                sonuc = Lists.ExecuteNonQuery(SQL);

            }
            foreach (GridViewRow gr in grdSorular.Rows)
            {
                try
                {

                    TextBox txt = (TextBox)gr.FindControl("txtYANIT");
                    DropDownList cb = (DropDownList)gr.FindControl("cbYANIT");
                    // ----->   DEĞİŞİKLİK BURADAN
                    string soruno = grdSorular.DataKeys[gr.RowIndex].Values[1].ToString();
                    string evethayir = grdSorular.DataKeys[gr.RowIndex].Values[4].ToString();
                    string yanitSoru = string.Empty;

                    TextBox txt1 = null;
                    string ozelNot = string.Empty;
                    if(soruno != "0")
                    {
                        txt1 = (TextBox)gr.FindControl("TxtOzelNot");
                        ozelNot = txt1.Text.Trim();
                        yanitSoru = cb.SelectedItem.Text.Trim();
                    }
                    bool flag = true;

                    if (soruno == "0")
                    {
                        yanitIndex++;
                    }

                    if (soruno != "0")
                    {
                        carsafKriter.Rows.Add(yanitIndex, yanitSoru);
                    }

                    if(soruno != "0")
                    {
                        foreach(DataRow dr in datas.Rows)
                        {
                            if(dr["SORUNO"].ToString() == soruno)
                            {
                                if(dr["OZELNOT"].ToString() != ozelNot)
                                {
                                    flag = false;
                                }
                                break;
                            }
                        }
                    }
                    if (flag)
                    {
                        if (grdSorular.DataKeys[gr.RowIndex].Values[2].ToString() == txt.Text && txt.Visible) //degisiklik yok
                            continue;
                        if (grdSorular.DataKeys[gr.RowIndex].Values[5].ToString() == cb.SelectedValue && !txt.Visible) //degisiklik yok
                            continue;
                    }


                   
                    


                    if (int.Parse(evethayir) >= 0)
                    {
                        // -----> DEĞİŞİKLİK BURADE BİTİYOR

                        if (cb.SelectedIndex < 0) continue;
                        string secenek = grdSorular.DataKeys[gr.RowIndex].Values[3].ToString();

                        string yanit = cb.SelectedItem.Text.Trim(); //DegerBul(secenek, txt.Text.Trim(), evethayir);

                        if (flag)
                        {
                            if (grdSorular.DataKeys[gr.RowIndex].Values[2].ToString() == yanit) //degisiklik yok
                                continue;
                        }


                        try
                        {
                            //ilk defa değerlendirme yapılırken insert atıyor
                            if (grdSorular.DataKeys[gr.RowIndex].Values[0].ToString() == "")
                            {
                                if (cb.SelectedItem.Text == "---")
                                    sonuc = Lists.ExecuteNonQuery("INSERT INTO ANKETDETAY (ANKETID, SORUNO, YANIT) values (" + AnketNo + ", " + soruno + ",NULL)");
                                else
                                    sonuc = Lists.ExecuteNonQuery("INSERT INTO ANKETDETAY (ANKETID, SORUNO, YANIT, DEGER, OZELNOT) values (" + AnketNo + ", " + soruno + ",'" + yanit + "', " + parse2Double(cb.SelectedValue) + ", '" + ozelNot+ "')");
                            }
                            //değerlendirme yapıldıktan sonra tekrar düzenleme yapılırsa buraya giriyor
                            else
                            {
                                if (cb.SelectedItem.Text == "---")
                                    sonuc = Lists.ExecuteNonQuery("UPDATE ANKETDETAY SET YANIT=NULL WHERE YANITNO=" + grdSorular.DataKeys[gr.RowIndex].Values[0].ToString());
                                else
                                    sonuc = Lists.ExecuteNonQuery("UPDATE ANKETDETAY SET YANIT='" + yanit + "', DEGER=" + parse2Double(cb.SelectedValue) + ", OZELNOT = '" + ozelNot + "' WHERE YANITNO=" + grdSorular.DataKeys[gr.RowIndex].Values[0].ToString());
                            }
                        }
                        catch (Exception ex1)
                        {


                            throw new Exception("1: " + ex1.ToString() + ":" + AnketNo + ":" + soruno + ":" + yanit + ":" + cb.SelectedValue);
                        }
                    }

                    else
                    {
                        try
                        {
                            if (grdSorular.DataKeys[gr.RowIndex].Values[0].ToString() == "")
                                sonuc = Lists.ExecuteNonQuery("INSERT INTO ANKETDETAY (ANKETID, SORUNO, YANIT, DEGER) values (" + AnketNo + ", " + soruno + ",'" + txt.Text.Trim() + "', NULL)");
                            else
                                sonuc = Lists.ExecuteNonQuery("UPDATE ANKETDETAY SET YANIT='" + txt.Text.Trim() + "', DEGER=NULL WHERE YANITNO=" + grdSorular.DataKeys[gr.RowIndex].Values[0].ToString());

                        }
                        catch (Exception ex1)
                        {


                            throw new Exception("2: " + ex1.ToString() + ":" + AnketNo + ":" + soruno + ":" + txt.Text.Trim() + ":");
                        }
                    }
                }
                catch(Exception Ex)
                {
                }
            }

            /* foreach (GridViewRow gr in grdKisiler.Rows)
             {
                 TextBox txt = (TextBox)gr.FindControl("txtYANIT");
                 if (grdKisiler.DataKeys[gr.RowIndex].Values[2].ToString() == txt.Text) //degisiklik yok
                     continue;
                 else
                 {
                     string yanit = txt.Text;
                     if (grdKisiler.DataKeys[gr.RowIndex].Values[2].ToString() == yanit) //degisiklik yok
                         continue;

                     sonuc = DataAccess.Execute.ExecuteNonQuery("UPDATE ANKETDETAY SET YANIT='" + yanit + "' WHERE YANITNO=" + grdKisiler.DataKeys[gr.RowIndex].Values[0].ToString());
                 }
             }*/

            Lists.ExecuteNonQuery("EXEC [dbo].[SP_RECODE_SINGLE] " + PROJENO + "," + yil + "," + int.Parse(ay).ToString() + "," + AnketNo);
            Lists.AddCallLog(Request.QueryString["callid"], CurrentUser.UserName, "DEĞERLENDİRİLDİ", AnketNo);

            //puanlamaların tekrar düzenlendiği bölüm hayır varsa 0 yoksa maxskoru yazdırıyoruz
            try
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
                DataTable anketData = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter("SELECT ck.ID, ck.ANKETNO, ck.DATAYIL, ck.DATAAY, ck.KRITERNO, ck.VERI, k.MAKSSKOR FROM CARSAF_KRITER AS ck INNER JOIN KRITERLER AS k ON ck.KRITERNO = k.KRITERNO WHERE ANKETNO = '" + AnketNo + "' AND k.PROJENO = '" + PROJENO + "' ORDER BY ck.KRITERNO ASC", conn);
                conn.Open();
                da.Fill(anketData);
                int anketDataCount = anketData.Rows.Count;
                bool flag = true;

                for(int i=1;i<=anketDataCount;i++)
                {
                    flag = true;
                    foreach(DataRow dr in carsafKriter.Rows)
                    {
                        if(dr["Kriter"].ToString() == i.ToString())
                        {
                            if(dr["Yanit"].ToString() == "Hayır")
                            {
                                // hayır yanıtı alındı ise
                                SqlCommand cmd = new SqlCommand("UPDATE CARSAF_KRITER SET VERI = '0' WHERE ANKETNO = '" + AnketNo + "' AND KRITERNO = '" + i.ToString() + "'", conn);
                                cmd.ExecuteNonQuery();
                                flag = false;
                            }
                        }
                    }
                    if (flag)
                    {
                        SqlCommand cmd = new SqlCommand("UPDATE CARSAF_KRITER SET VERI = '" + Convert.ToInt32(anketData.Rows[i-1]["MAKSSKOR"].ToString()) + "' WHERE ANKETNO = '" + AnketNo + "' AND KRITERNO = '" + i.ToString() + "'", conn);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch(Exception Ex)
            {

            }

            //anketlerin değerlendirme puanlarının düzenleneceği bölüm
            try
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
                conn.Open();
                SqlCommand cmd = new SqlCommand("UPDATE ANKETLER SET GENELCSI = (SELECT SUM(VERI) FROM CARSAF_KRITER WHERE ANKETNO = '" + AnketNo + "') WHERE ANKETID = '" + AnketNo + "'", conn);
                cmd.ExecuteNonQuery();
            }
            catch(Exception Ex)
            {

            }

            if(txtNot.Text != string.Empty)
            {
                Lists.AddNote(Request.QueryString["callid"], txtNot.Text, CurrentUser.UserName);
            }
            

            if (!sifirlamaFlag)
            {
                try
                {
                    if (sifirlama.Checked)
                    {
                        //sıfırlama
                        string CALLDATE = DateTime.Parse(Request.QueryString["startdate"]).ToString("yyyy-MM-dd HH:mm:ss");
                        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
                        conn.Open();
                        SqlCommand cmd = new SqlCommand("INSERT INTO SIFIRLAMA VALUES('" + Request.QueryString["callid"] + "', '" + CALLDATE + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "')", conn);
                        cmd.ExecuteNonQuery();
                        cmd = new SqlCommand("INSERT INTO SIFIRLAMAGUNCEL VALUES('" + Request.QueryString["callid"] + "', '" + CALLDATE + "', '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "')", conn);
                        cmd.ExecuteNonQuery();
                        conn.Close();


                    }
                }
                catch (Exception Ex)
                {

                }
            }

            try
            {
                if(sifirlamaFlag == true && sifirlama.Checked == false)
                {
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("DELETE FROM SIFIRLAMAGUNCEL WHERE ANKETNO = '"+ Request.QueryString["callid"] + "'", conn);
                    cmd.ExecuteNonQuery();
                    conn.Close();
                }
            }
            catch(Exception Ex)
            {


            }

            if (mailKontrol.Checked)
            {
                try
                {

                    //mail gönder
                    string link = "http://178.210.179.34/ARTQM/" + "ViewEval.aspx?anket=" + Request.QueryString["anket"] + "&callid=" + Request.QueryString["callid"] + "&projeno=" + Request.QueryString["projeno"] + "&agent=" + Request.QueryString["agent"] + "&donem=" + Request.QueryString["donem"];
                    string anketNo = Request.QueryString["callid"];

                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["dbString"].ConnectionString);
                    conn.Open();

                    SqlDataAdapter da = new SqlDataAdapter("SELECT LOGID, CALLDATE FROM ANKETLER WHERE ANKETNO = '" + anketNo + "'", conn);
                    DataTable dt = new DataTable();
                    da.Fill(dt);

                    string logid = dt.Rows[0]["LOGID"].ToString().Trim();
                    string callDate = dt.Rows[0]["CALLDATE"].ToString().Trim();

                    string[] date = callDate.Split(' ');

                    da = new SqlDataAdapter("SELECT NAME, EPOSTA FROM AGENTS WHERE LOGID = '" + logid + "'", conn);
                    dt.Clear();
                    da.Fill(dt);
                    string agentName = dt.Rows[0]["NAME"].ToString();
                    string agentEmail = dt.Rows[0]["EPOSTA"].ToString();

                    //da = new SqlDataAdapter("SELECT PARTYAGENTNAMES, SKILLSET, CONVERT(varchar(20), CALLDATE, 103)+' '+CONVERT(varchar(20),convert(time, CALLDATE),108) AS CALLDATE, YON, PARTYNAMES, SURE, ANKETNO FROM ANKETLER WHERE ANKETNO = '" + anketNo + "'", conn);
                    //DataTable audioMail = new DataTable();
                    //da.Fill(audioMail);

                    //string audioLink = "http://178.210.179.34/ARTQM/" + "CallDetail.aspx?callid=" + audioMail.Rows[0]["PARTYAGENTNAMES"] + "?" + audioMail.Rows[0]["SKILLSET"] + "?" + audioMail.Rows[0]["CALLDATE"] + "?" + audioMail.Rows[0]["YON"] + "?" + audioMail.Rows[0]["PARTYNAMES"] + "?" + audioMail.Rows[0]["SURE"] + "?" + audioMail.Rows[0]["ANKETNO"];

                    //audioLink = audioLink.Replace(" ", "%20");

                    conn.Close();
                    Tools T = new ARTQM.Tools();


                    T.SendMail("Merhabalar Sayın " + agentName + ",\n\n" + date[0] + " tarihinde saat " + date[1] + "'de yapılmış olan görüşmenin değerlendirme kartına aşağıdaki linkten ulaşabilirsiniz. \n\n" + link + "\n\nİyi Çalışmalar", "ARTQM Bilgilendirme Maili", agentEmail);
                }
                catch (Exception Ex) { }
            }
            Response.Redirect("ViewEval.aspx?anket=" + Request.QueryString["anket"] + "&callid=" + Request.QueryString["callid"] + "&projeno=" + Request.QueryString["projeno"] + "&agent=" + Request.QueryString["agent"] + "&donem=" + Request.QueryString["donem"] + "&startdate=" + Request.QueryString["startdate"], true);



            //if (sonuc > 0)
            //{
            //    lblMesaj.Text = "Agent karnesi güncellendi.";
            //    String ip = HttpContext.Current.Request.UserHostAddress;
            //    String str = "INSERT INTO UPDATELOG (KULLANICIKOD,ANKETNO,USERIP,TARIH, SAAT, ACIKLAMA) VALUES ('" + AktifKullanici.KULLANICIKOD +
            //        "'," + AnketNo + ", '" + ip + "','" + DateTime.Now.ToString("yyyy.MM.dd") + "','" + DateTime.Now.ToString("HH:mm:ss") + "','GUNCELLENDI' )";
            //    DataAccess.Execute.ExecuteNonQuery(str);
            //}
            //else
            //{
            //    lblMesaj.Text = "Güncelleme başarısız.";
            //    String ip = HttpContext.Current.Request.UserHostAddress;
            //    String str = "INSERT INTO UPDATELOG (KULLANICIKOD,ANKETNO,USERIP,TARIH, SAAT, ACIKLAMA) VALUES ('" + AktifKullanici.KULLANICIKOD +
            //        "'," + AnketNo + ", '" + ip + "','" + DateTime.Now.ToString("yyyy.MM.dd") + "','" + DateTime.Now.ToString("HH:mm:ss") + "','GUNCELLEME BASARISIZ' )";
            //    DataAccess.Execute.ExecuteNonQuery(str);
            //}

            // Response.Redirect(Request.Url.ToString(), true);

        }

    }
}