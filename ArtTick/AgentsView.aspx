﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="AgentsView.aspx.cs" Inherits="ARTQM.AgentsView" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>

        
.card {
    position: relative;
    display: block;
    margin-bottom: 0px;
    background-color: #fff;
    border-radius: .25rem;
    border: 1px solid rgba(0,0,0,.125);

    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
}

    .card-title {
    margin-bottom: .75rem;
    border-bottom: 1px;
    border-color: #fff;
}


    .card-primary {
    background-color: #0275d8;
    border-color: #0275d8;
}


.card-inverse {
color:#fff;

}


.bg-info {
    background-color: #5bc0de!important;
}

.bg-danger {
    background-color: #d9534f!important;

}

.bg-success {
    background-color: #5cb85c!important;
}

.bg-primary {
    background-color: #0275d8!important;
}
.bg-gray {
    background-color: #999999!important;
}
.bg-lgray {
        background-repeat: repeat-x;
    background-image: linear-gradient(to bottom, #f5f5f5 0%, #e8e8e8 100%);
    color: #333;
    background-color: #f5f5f5;
    border-color: #ddd;
    padding: 10px 15px;
    border-bottom: 1px solid transparent;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
    
}


    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="form-horizontal">
<fieldset>
     <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" runat="server" id="hResult"></h4>
        </div>
        <div class="modal-body" runat="server" id="bResult">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
        </div>
      </div>
      
    </div>
  </div>

     <div runat="server" id="divResult"></div>
    

<legend>Operatör İzleme</legend>

  <div class="row"><div class="form-group col-sm-10">
    
     <label class="col-sm-4 control-label" for="drpGroup">Lütfen Grup Seçiniz </label>
    
      <div class="col-sm-8">
  	    	<asp:DropDownList id="drpGroup" runat="server" name="drpGroup" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="drpGroup_SelectedIndexChanged">
  </asp:DropDownList>
    </div>
  
</div>
 
   </div>  
    <div class="row">
 <div class="form-group">
  
  <div class="col-sm-12">

      <asp:UpdatePanel ID="pnlView" runat="server" UpdateMode="Conditional">
          <ContentTemplate>
      
                      <asp:Timer ID="tmView" runat="server" Interval="5000" OnTick="tmView_Tick"></asp:Timer>

              <asp:DataList ID="lstSummary" runat="server"  CellPadding="0" CellSpacing="0"  CssClass="table table-condensed" RepeatDirection="Horizontal" RepeatColumns="12">

                  <ItemStyle CssClass="col-sm-1" />
                  <ItemTemplate>
                      <div class='<%# GetCardStyle(Eval("Status").ToString()) %>'><small><%#Eval("StatusName")%>&nbsp;</small>
        <div class='col-sm-12'> 
      <%#Eval("StatusCount")%>&nbsp;
  </div>
   
</div>
                  </ItemTemplate>
              </asp:DataList>
 <asp:DataList ID="grdData" runat="server"  CellPadding="0" CellSpacing="0"  CssClass="table table-condensed"  RowStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" RepeatDirection="Horizontal" RepeatColumns="6">
     <ItemTemplate>
<div class='<%# GetCardStyle(Eval("Status").ToString()) %>'> 
    
     <div class="row">
        <div class="col-sm-12">
        <span class='card-title pull-left'><%#Eval("DisplayName").ToString () +"("+Eval("AgentID").ToString () +")"%>&nbsp;</span>    </div></div>
    <div class="form-group">
        <div class="col-sm-12">
            <div class="col-sm-3"><span class='fa-stack fa-md'><i class='fa fa-user fa-stack-2x'></i></span></div>
            <div class="col-sm-9">
                  <div class="col-sm-12"><%# Seconds2Span(Eval("Duration").ToString()) %></div>
                <div class="col-sm-12"><%#GetCardStatus(Eval("Status").ToString(), Eval("StatusCode").ToString())%>&nbsp;
                    </div>
            </div>

        </div>
        </div>

 
      <div class="row">
        <div class="col-sm-12">
    
     <span class="label label-default"><%#Eval("StatusData")%></span> &nbsp;
  </div>
   
    </div>
         
    </div>
         
                 

     </ItemTemplate>
     <ItemStyle CssClass="col-sm-2"/>
         
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>
      </asp:DataList>
    </ContentTemplate>
      </asp:UpdatePanel>
  </div>

     </div>
     </div>

    </fieldset>
 
      </div>



</asp:Content>
