﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="MForms.aspx.cs" Inherits="ARTQM.MForms" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="form-horizontal">
<fieldset>
     <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" runat="server" id="hResult"></h4>
        </div>
        <div class="modal-body" runat="server" id="bResult">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
        </div>
      </div>
      
    </div>
  </div>
     <div class="modal fade" id="divUpdate" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" runat="server" id="hResult2">Yeni Form Kartı</h4>
        </div>
        <div class="modal-body" runat="server" id="bResult2">
    <div class="form-group">
  
  <div class="col-sm-12">     
    <div class="form-group">
    
     <label class="col-sm-3 control-label" for="txtFormName">Form Adı</label>
    
      <div class="col-sm-9">
  	    <asp:TextBox runat="server" id="txtFormName" ClientIDMode="Static" name="txtFormName" placeholder="Form Adı" class="form-control input-sm"/>
    </div>
  
 
</div>
<div class="form-group">
       <label class="col-sm-3 control-label" for="txtToplam">Toplam Puan</label>
    
      <div class="col-sm-9">
  	    <asp:TextBox runat="server" id="txtToplam" ClientIDMode="Static" name="txtToplam" placeholder="Toplam Puan" class="form-control input-sm" TextMode="Number"/>
    </div>
  

</div>

<div class="form-group">
       <label class="col-sm-3 control-label" for="txtGecer">Geçer Puan</label>
    
      <div class="col-sm-9">
  	    <asp:TextBox runat="server" id="txtGecer" ClientIDMode="Static" name="txtGecer" placeholder="Geçer Puan" class="form-control input-sm" TextMode="Number"/>
    </div>
  

</div>


    <div class="form-group col-sm-12">
  <label class="col-sm-10 control-label" for="btnSave"></label>
  <div class="col-sm-2 pull-right">
    <asp:Button id="btnSave" runat="server" class="btn btn-success" Text="Kaydet" OnClick="btnSave_Click" />
  </div>
</div>
        </div>
        </div></div>
      
      </div>
      
    </div>
  </div>
   


    <div runat="server" id="divResult"></div>
    


   

    


   
    <legend>TANIMLI FORMLAR</legend>
  <div class="form-group col-sm-12">   <div class="col-sm-3 btn-group" style="margin-left:-15px;">
     <asp:LinkButton ID="lnkNew" runat="server" class="btn btn-danger"  data-toggle="tooltip" data-placement="bottom" title="Yeni Form" OnClick="lnkNew_Click" ><i class="glyphicon glyphicon-plus" aria-hidden="true"></i> Yeni Form</asp:LinkButton>
            </div>
    <div class="col-sm-9">
        
        <span class="label label-default" id="spanInfo" runat="server"></span>
        </div>
      </div>
  <div class="form-group">
  
  <div class="col-sm-12">
      <br />
 <asp:GridView ID="grdData" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-responsive table-striped" 
     RowStyle-HorizontalAlign="Center"
     HeaderStyle-HorizontalAlign="Center" OnRowCommand="grdData_RowCommand">
     <Columns>

      <asp:BoundField HeaderText="Kod"  DataField="PROJENO" ItemStyle-CssClass="col-sm-1" HeaderStyle-HorizontalAlign="Center">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>


<ItemStyle CssClass="col-sm-1"></ItemStyle>
         </asp:BoundField>
         <asp:BoundField HeaderText="Form Adı"  DataField="PROJEADI" ItemStyle-CssClass="col-sm-3" ItemStyle-HorizontalAlign="Left">
<ItemStyle HorizontalAlign="Left" CssClass="col-sm-3"></ItemStyle>
         </asp:BoundField>
         
         <asp:BoundField HeaderText="Toplam Puan"  DataField="TOPLAMPUAN" ItemStyle-CssClass="col-sm-2" ItemStyle-HorizontalAlign="Left">
<ItemStyle HorizontalAlign="Left" CssClass="col-sm-2"></ItemStyle>
         </asp:BoundField>

          <asp:BoundField HeaderText="Geçer Puan"  DataField="GECERPUAN" ItemStyle-CssClass="col-sm-2" ItemStyle-HorizontalAlign="Left">
    <ItemStyle HorizontalAlign="Left" CssClass="col-sm-2"></ItemStyle>
         </asp:BoundField>
        
         <asp:TemplateField HeaderText="Durum">
             <ItemStyle CssClass="col-sm-1" HorizontalAlign="Center"></ItemStyle>
             <ItemTemplate>
                   <%# GetFormDisposition(Eval("AKTIF").ToString()) %>
                 
             </ItemTemplate>
         </asp:TemplateField>

     
         <asp:TemplateField HeaderText="İşlemler">
             <ItemStyle CssClass="col-sm-1" HorizontalAlign="Left"></ItemStyle>
             <ItemTemplate>
                <asp:LinkButton  runat="server" ID="btnEdit" CommandName="editt" CommandArgument='<%#Eval("PROJENO").ToString() %>'  Text="<span data-toggle='tooltip' title='Düzenle' ><span class='glyphicon glyphicon-edit'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
                <asp:LinkButton  runat="server" ID="btnSwitch" CommandName="switch" CommandArgument='<%#Eval("PROJENO").ToString() %>'  Text="<span data-toggle='tooltip' title='Aktif/Pasif' ><span class='glyphicon glyphicon-retweet'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
                <asp:LinkButton  runat="server" ID="btnTitles" CommandName="titles" CommandArgument='<%#Eval("PROJENO").ToString() %>'  Text="<span data-toggle='tooltip' title='Kırılımlar' ><span class='glyphicon glyphicon-th-list'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
                <asp:LinkButton  runat="server" ID="btnQuestions" CommandName="questions" CommandArgument='<%#Eval("PROJENO").ToString() %>'  Text="<span data-toggle='tooltip' title='Sorular' ><span class='glyphicon glyphicon-question-sign'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
         
                 
                 
             </ItemTemplate>
         </asp:TemplateField>


     </Columns>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<RowStyle HorizontalAlign="Center"></RowStyle>
      </asp:GridView>
    
  </div>

</div>


    </fieldset>
          </div>
 </asp:Content>
