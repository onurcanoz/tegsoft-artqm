﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Globalization;
namespace ARTQM
{
    public partial class EvalSummary : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string degerlendirmeler = Session["DEGERLENDIRMELER"].ToString();

            if (degerlendirmeler.Equals("0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                        "alert('Yetkiniz Yok'); window.location='" + Request.UrlReferrer.ToString() + "';", true);
                return;
            }
            if (!IsPostBack)
            {
                DateTime date = DateTime.Today;
                var firstDayOfMonth = DateTime.Today.AddMonths(-1);
                var lastDayOfMonth = DateTime.Today;
                txtRange.Text = firstDayOfMonth.ToString("dd.MM.yyyy") + " - " + lastDayOfMonth.ToString("dd.MM.yyyy");
                LoadGroups();
            }
            
            
        }
        public void LoadGroups()
        {
            DataTable tmp = null;
            DataTable tmp2 = null;

            if (!Session["UserAgents"].ToString().Equals("0"))
                tmp = ((DataTable)Session["AGENTS"]).Select("AgentID IN (" + Session["UserAgents"] + ")").CopyToDataTable();
            else
                tmp = (DataTable)Session["AGENTS"];
            drpGroup.DataSource = tmp;
            drpGroup.DataTextField = "AGENT";
            drpGroup.DataValueField = "AgentID";
            drpGroup.DataBind();
            drpGroup.Items.Insert(0, new ListItem("Tümü", "0"));

            if (!Session["UserSkills"].ToString().Equals(""))
                tmp2 = ((DataTable)Session["SKILLS"]).Select("SkillID IN (" + Session["UserSkills"] + ")").CopyToDataTable();
            else
                tmp2 = (DataTable)Session["SKILLS"];

            drpSkill.DataSource = tmp2;
            drpSkill.DataTextField = "Skill";
            drpSkill.DataValueField = "SkillID";
            drpSkill.DataBind();
            drpSkill.Items.Insert(0, new ListItem("Tümü", "0"));
            drpSkill.SelectedIndex = 0;


            //drpUsers.DataSource = Lists.GetUsers();
            //drpUsers.DataTextField = "KULLANICI";
            //drpUsers.DataValueField = "KULLANICI";
            //drpUsers.DataBind();
            //drpUsers.Items.Insert(0, new ListItem("Tümü", "0"));


            drpForm.DataSource = Lists.GetForms();
            drpForm.DataValueField = "PROJENO";
            drpForm.DataTextField = "PROJEADI";
            drpForm.DataBind();
            drpForm.Items.Insert(0, new ListItem("Tümü", "0"));
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {


            WFOUtils ws = new WFOUtils();

            int durStart = -1;
            int durEnd = -1;

            try
            {
                durStart = int.Parse(txtStart.Text.Trim());
            }
            catch
            {   durStart = -1; }

            try
            {
                durEnd = int.Parse(txtEnd.Text.Trim());
            }
            catch
            {   durEnd = -1; }

            String callStart = string.Empty;
            String callEnd = string.Empty;

            try
            {

                //callStart = DateTime.ParseExact(txtRange.Text.Split('-')[0].Trim(), "yyyy-MM-dd hh:mm:ss", null);
                callStart = DateTime.Parse(txtRange.Text.Split('-')[0].Trim()).ToString("yyyy-MM-dd 00:00:00");
                //sqlCallStart = DateTime.Today.AddDays(-2).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

            }
            catch (Exception)
            {

                callStart = DateTime.MinValue.ToString("yyyy-MM-dd");
            }
            try
            {
                //callEnd = DateTime.ParseExact(txtRange.Text.Split('-')[1].Trim(), "yyyy-MM-dd", null);
                callEnd = DateTime.Parse(txtRange.Text.Split('-')[1].Trim()).ToString("yyyy-MM-dd 23:59:59");
            }
            catch (Exception)
            {

                callEnd = DateTime.MaxValue.ToString("yyyy-MM-dd");
            }

            DataTable calls = ws.QueryEvalSummary(drpDirection.SelectedValue, callStart, callEnd, drpGroup.SelectedValue == "0" ? Session["UserAgents"].ToString() : "'" + drpGroup.SelectedValue + "'", durStart, durEnd);

            
              
            grdData.DataSource = calls;
            grdData.DataBind();
           
        
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            ExportToExcel(grdData, "DEGERLENDIRME_RAPORU_" + txtRange.Text.Replace(" ", "").Replace("-", "_").Replace(".", "_")+"_"+ drpGroup.SelectedItem.Text.Replace(" ","_").Replace("-","_"));
        }
       

       
    }
}