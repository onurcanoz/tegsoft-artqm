﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="MTemplates.aspx.cs" Inherits="ARTQM.MTemplates" EnableEventValidation="false" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <div class="form-horizontal">
<fieldset>
     <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" runat="server" id="hResult"></h4>
        </div>
        <div class="modal-body" runat="server" id="bResult">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
        </div>
      </div>
      
    </div>
  </div>
     <div class="modal fade" id="divUpdate" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" runat="server" id="hResult2">Şablon Kartı</h4>
        </div>
        <div class="modal-body" runat="server" id="bResult2">
    <div class="form-group">
  
  <div class="col-sm-12">     
    <div class="form-group">
    
     <label class="col-sm-3 control-label" for="txtSecenekler">Seçenekler</label>
    
      <div class="col-sm-9">
  	    <asp:TextBox runat="server" id="txtSecenekler" ClientIDMode="Static" name="txtSecenekler" placeholder="Değer=Seçenek Listesi" class="form-control input-sm" TextMode="MultiLine" Rows="4" MaxLength="250"/>
    </div>
  
 
</div>

        <div class="form-group">
    
     <label class="col-sm-3 control-label" for="txtPuanlama">Puanlama</label>
    
      <div class="col-sm-9">
  	      <asp:TextBox runat="server" id="txtPuanlama" ClientIDMode="Static" name="txtPuanlama" placeholder="Değer=Puan Listesi" class="form-control input-sm" TextMode="MultiLine" Rows="4" MaxLength="250"/>
        </div>
      

    </div>
  
  </div>

      
    <div class="form-group col-sm-12">
  <label class="col-sm-10 control-label" for="btnSave"></label>
  <div class="col-sm-2 pull-right">
    <asp:Button id="btnSave" runat="server" class="btn btn-success" Text="Kaydet" OnClick="btnSave_Click" />
  </div>
</div>
        </div>
        </div></div>
      
      </div>
      
    </div>
    
     <div runat="server" id="divResult"></div>
    


     
          
    <legend>Tanımlı Şablonlar</legend>
  <div class="form-group col-sm-12">   <div class="col-sm-3 btn-group" style="margin-left:-15px;">
     <asp:LinkButton ID="lnkNew" runat="server" CSSclass="btn btn-danger"  data-toggle="tooltip" data-placement="bottom" title="Yeni Grup" OnClick="lnkNew_Click" ><i class="glyphicon glyphicon-plus" aria-hidden="true"></i> Yeni Şablon</asp:LinkButton>
      <%--<asp:LinkButton ID="Button1" runat="server" CssClass="btn btn-danger"  OnClick="Button1_Click" data-toggle="tooltip" data-placement="bottom" title="Excel' e Aktar"><i class="fa fa-file-excel-o" aria-hidden="true"></i>Excel'e Aktar</asp:LinkButton>--%>
            </div>
    <div class="col-sm-9">
     
        <span class="label label-default" id="spanInfo" runat="server"></span>
        </div>
      </div>
  <div class="form-group">
  
  <div class="col-sm-12">
      <br />
 <asp:GridView ID="grdData" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-responsive table-striped" 
     RowStyle-HorizontalAlign="Center"
     HeaderStyle-HorizontalAlign="Center" OnRowCommand="grdData_RowCommand">
     <Columns>

         <asp:BoundField HeaderText="Kod"  DataField="TASLAKID" ItemStyle-CssClass="col-sm-1" HeaderStyle-HorizontalAlign="Center">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle CssClass="col-sm-1"></ItemStyle>
         </asp:BoundField>
         
         <asp:TemplateField HeaderText="Seçenekler"  ItemStyle-CssClass="col-sm-3" ItemStyle-HorizontalAlign="Left" >

             <ItemTemplate>

                <%# Server.HtmlDecode(Eval("SECENEKLER").ToString().Replace(",","<br/>")) %>
             </ItemTemplate>
         </asp:TemplateField>
         <asp:TemplateField HeaderText="Puanlama"  ItemStyle-CssClass="col-sm-3" ItemStyle-HorizontalAlign="Left" >

             <ItemTemplate>

                <%# Server.HtmlDecode(Eval("PUANLAMA").ToString().Replace(",","<br/>")) %>
             </ItemTemplate>
         </asp:TemplateField>
         
          <asp:TemplateField HeaderText="İşlemler">
             <ItemStyle CssClass="col-sm-1" HorizontalAlign="Left"></ItemStyle>
             <ItemTemplate>
                   <asp:LinkButton  runat="server" ID="btnEdit" CommandName="editt" CommandArgument='<%#Eval("TASLAKID").ToString() %>'  Text="<span data-toggle='tooltip' title='Düzenle' ><span class='glyphicon glyphicon-edit'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
              <asp:LinkButton  runat="server" ID="btnRemove" CommandName="remove" CommandArgument='<%#Eval("TASLAKID").ToString() %>'  Text="<span data-toggle='tooltip' title='Sil' ><span class='glyphicon glyphicon-trash'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
           
                 
                 
             </ItemTemplate>
         </asp:TemplateField>




     </Columns>

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<RowStyle HorizontalAlign="Center"></RowStyle>
      </asp:GridView>
    
  </div>

</div>


    </fieldset>
    </div>
 </asp:Content>
