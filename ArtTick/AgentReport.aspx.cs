﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ARTQM
{
    public partial class AgentReport : PageBase 
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string raporlar = Session["RAPORLAR"].ToString();

            if (raporlar.Equals("0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                        "alert('Yetkiniz Yok'); window.location='" + Request.UrlReferrer.ToString() + "';", true);
                return;
            }
            if (!IsPostBack)
            {
                LoadGroups();
            }

        }


        public void LoadGroups()
        {
            drpYIL.Items.Clear();
            drpYIL2.Items.Clear();
            for (int i = 2016; i <= DateTime.Now.Year; i++)
            {
                drpYIL.Items.Add(i.ToString());
                drpYIL2.Items.Add(i.ToString());
            }
            

            drpAY.Items.Clear();
            drpAY2.Items.Clear();
            for (int i = 1; i <= 12; i++)
            {
                drpAY.Items.Add(i.ToString().PadLeft(2,'0'));
                drpAY2.Items.Add(i.ToString().PadLeft(2,'0'));
            }

            DataTable tmp = null;
            DataTable tmp2 = null;

            drpYIL.SelectedValue = drpYIL2.SelectedValue = DateTime.Now.Year.ToString();
            drpAY.SelectedValue = drpAY2.SelectedValue = DateTime.Now.Month.ToString();
            drpForm.DataSource = Lists.GetForms();
            drpForm.DataValueField = "PROJENO";
            drpForm.DataTextField = "PROJEADI";
            drpForm.DataBind();

            if (!Session["UserAgents"].ToString().Equals("0"))
                tmp = ((DataTable)Session["AGENTS"]).Select("AgentID IN (" + Session["UserAgents"] + ")").CopyToDataTable();
            else
                tmp = (DataTable)Session["AGENTS"];

            chkAgents.DataSource = tmp;
            chkAgents.DataTextField= "AGENT";
            chkAgents.DataValueField= "AgentID";
            chkAgents.DataBind();


            //if (!Session["UserSkills"].ToString().Equals(""))
            //    tmp2 = ((DataTable)Session["SKILLS"]).Select("SkillID IN (" + Session["UserSkills"] + ")").CopyToDataTable();
            //else
            //    tmp2 = (DataTable)Session["SKILLS"];

            drpSkill.DataSource = tmp2;
            drpSkill.DataTextField = "Skill";
            drpSkill.DataValueField = "SkillID";
            drpSkill.DataBind();
            drpSkill.Items.Insert(0, new ListItem("Tümü", "0"));

        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {

            string agents = "";
            List<string> selectedValues = chkAgents.Items.Cast<ListItem>()
           .Where(li => li.Selected)
           .Select(li => li.Value)
           .ToList();

            agents = string.Join(",", selectedValues.ToArray());

            if(agents != "")
            {
                string[] agentList = agents.Split(',');

                agents = string.Empty;
                foreach(string item in agentList)
                {
                    agents += "'" + item + "',";
                }
                agents = agents.Remove(agents.Length-1);
            }

            if (agents.Equals(""))
            {
                ICUser user = (ICUser)Session["CurrentUser"];
                if (user.userLevel != 0)
                {
                    agents =Session["userAgents"].ToString();
                }
            }
            DataTable dt = Reports.AylikToplamRaporAlfabetik(int.Parse(drpForm.SelectedValue), drpSkill.SelectedValue.ToString(), agents, int.Parse(drpYIL.SelectedValue), int.Parse(drpAY.SelectedValue), int.Parse(drpYIL2.SelectedValue), int.Parse(drpAY2.SelectedValue), "");
            grdData.DataSource = dt;
            grdData.DataBind();


            #region FORMAT GRID CSS

            int i  = 0;
            foreach (DataColumn dc in dt.Columns)
            {
                grdData.HeaderRow.Cells[i].Text = dc.Caption.Replace("height:200px;", "");
                i++;
            }

            for (i = 0; i < grdData.Rows[0].Cells.Count; i++)
            {
                grdData.Rows[0].Cells[i].CssClass = "gozlemsatir";
                if (dt.Columns[i].Caption == "****")
                {
                    grdData.Rows[0].Cells[i].CssClass = "";

                }
            }

            i = 0;

            foreach (DataRow dr in dt.Rows)
            {
                grdData.Rows[i].Cells[1].Style.Add("text-align", "center");
                if (i == 0)
                {
                    for (int j = 0; j < dt.Columns.Count; j++)
                        if (dt.Columns[j].Caption == "****")
                        {
                            grdData.Rows[i].Cells[j].CssClass = "";
                            grdData.Rows[i].Cells[j].Style.Add("background-color", "#16365c");
                            grdData.HeaderRow.Cells[j].Text = "&nbsp;";
                            grdData.HeaderRow.Cells[j].Style.Add("background-color", "#16365c");

                        }


                    i++; continue;
                }
                if (dr[0].ToString().StartsWith("****"))
                {

                    grdData.Rows[i].CssClass = "ust_baslik";
                    grdData.Rows[i].Cells[0].Wrap = false;
                    //grdData.Rows[i].BackColor = System.Drawing.Color.LightSalmon;
                    grdData.Rows[i].Font.Bold = true;
                    for (int j = 1; j < grdData.Rows[i].Cells.Count; j++)
                    {
                        grdData.Rows[i].Cells[j].Style.Add("width", "60px");

                        if (grdData.Rows[i].Cells[j].Text.Replace("&nbsp;", "").Trim() != "")
                        {
                            //if (lblBaslik.Text.Contains("Endeks"))
                            grdData.Rows[i].Cells[j].Text = String.Format("{0:0.0}", Math.Round(parse2Double(grdData.Rows[i].Cells[j].Text), 1, MidpointRounding.AwayFromZero)).Replace(",", ".");
                        }
                        else
                            grdData.Rows[i].Cells[j].Text = ",";

                        if (grdData.HeaderRow.Cells[j].Text == "****")
                        {
                            grdData.Rows[i].Cells[j].CssClass = "";
                            grdData.Rows[i].Cells[j].Style.Add("background-color", "#ffffff");
                        }
                    }
                    grdData.Rows[i].Cells[0].Text = grdData.Rows[i].Cells[0].Text.Replace("****", "");

                }
                else if (dr[0].ToString().StartsWith("***"))
                {

                    grdData.Rows[i].CssClass = "kriter_baslik";
                    grdData.Rows[i].Cells[0].Wrap = false;
                    //grdData.Rows[i].BackColor = System.Drawing.Color.LightSalmon;
                    grdData.Rows[i].Font.Bold = true;
                    for (int j = 1; j < grdData.Rows[i].Cells.Count; j++)
                    {
                        grdData.Rows[i].Cells[j].Style.Add("text-align", "center");
                        grdData.Rows[i].Cells[j].Style.Add("font-weight", "normal");
                        grdData.Rows[i].Cells[j].Style.Add("width", "60px");
                        if (grdData.Rows[i].Cells[j].Text.Replace("&nbsp;", "").Trim() != "")
                        {
                            //if (lblBaslik.Text.Contains("Endeks"))
                            grdData.Rows[i].Cells[j].Text = String.Format("{0:0.0}", Math.Round(parse2Double(grdData.Rows[i].Cells[j].Text), 1, MidpointRounding.AwayFromZero)).Replace(",", ".");
                        }
                        else
                            grdData.Rows[i].Cells[j].Text = ",";
                        if (grdData.HeaderRow.Cells[j].Text == "****")
                        {
                            grdData.Rows[i].Cells[j].CssClass = "temp";
                            grdData.Rows[i].Cells[j].BackColor = System.Drawing.Color.White;
                        }
                    }
                    grdData.Rows[i].Cells[0].Text = grdData.Rows[i].Cells[0].Text.Replace("***", "");

                }
                else if (dr[0].ToString().StartsWith("**"))
                {
                    grdData.Rows[i].Cells[0].Text = grdData.Rows[i].Cells[0].Text.Replace("**", "");
                    //grdData.Rows[i].Cells[0].CssClass = "kriter_satir";
                    grdData.Rows[i].Cells[0].Wrap = false;
                    if (dr[0].ToString().Contains("(Skora d"))
                    {
                        grdData.Rows[i].CssClass = "skor_haric";
                    }
                    for (int j = 1; j < grdData.Rows[i].Cells.Count; j++)
                    {
                        grdData.Rows[i].Cells[j].Style.Add("text-align", "center");
                        grdData.Rows[i].Cells[j].Style.Add("font-weight", "normal");
                        grdData.Rows[i].Cells[j].Style.Add("width", "60px");
                        if (grdData.Rows[i].Cells[j].Text.Replace("&nbsp;", "").Trim() != "")
                        {
                            //if (lblBaslik.Text.Contains("Endeks"))
                            grdData.Rows[i].Cells[j].Text = String.Format("{0:0.0}", Math.Round(parse2Double(grdData.Rows[i].Cells[j].Text), 1, MidpointRounding.AwayFromZero)).Replace(",", ".");
                        }
                        else
                            grdData.Rows[i].Cells[j].Text = ",";
                        if (grdData.HeaderRow.Cells[j].Text == "****")
                        {
                            grdData.Rows[i].Cells[j].CssClass = "temp";
                            grdData.Rows[i].Cells[j].BackColor = System.Drawing.Color.White;
                        }
                    }
                }
                else
                {
                    //grdData.Rows[i].Cells[0].Text = grdData.Rows[i].Cells[0].Text.Replace("**", "");
                    grdData.Rows[i].Cells[0].CssClass = "kriter_secenek";
                    if (dr[0].ToString().Contains("GENEL"))
                        grdData.Rows[i].Cells[0].CssClass = "genel_csi";
                    grdData.Rows[i].Cells[0].HorizontalAlign = HorizontalAlign.Right;
                    if (dr[0].ToString().Contains("(Skora d"))
                    {
                        grdData.Rows[i].CssClass = "kriter_secenek_haric";
                    }
                    for (int j = 1; j < grdData.Rows[i].Cells.Count; j++)
                    {
                        grdData.Rows[i].Cells[j].Style.Add("text-align", "center");
                        grdData.Rows[i].Cells[j].Style.Add("width", "60px");
                        /**/
                        if (dr[0].ToString().Contains("GENEL"))
                        {
                            grdData.Rows[i].Cells[j].CssClass = "genel_csi";
                            grdData.Rows[i].Cells[j].Text = String.Format("{0:0.0}", Math.Round(parse2Double(grdData.Rows[i].Cells[j].Text), 1, MidpointRounding.AwayFromZero)).Replace(",", ".");
                        }
                        else
                        {
                            if (grdData.Rows[i].Cells[j].Text.Replace("&nbsp;", "").Trim() != "")
                                //if (lblBaslik.Text.Contains("Endeks"))
                                //   grdData.Rows[i].Cells[j].Text = String.Format("{0:0}", Math.Round(parse2Double(grdData.Rows[i].Cells[j].Text), 0, MidpointRounding.AwayFromZero)).Replace(",", ".");
                                //else
                                grdData.Rows[i].Cells[j].Text = String.Format("{0:0.0}", Math.Round(parse2Double(grdData.Rows[i].Cells[j].Text), 1, MidpointRounding.AwayFromZero)).Replace(",", ".");
                            else
                                grdData.Rows[i].Cells[j].Text = ",";
                        }
                        if (grdData.HeaderRow.Cells[j].Text == "****")
                        {
                            grdData.Rows[i].Cells[j].CssClass = "temp";
                            grdData.Rows[i].Cells[j].BackColor = System.Drawing.Color.White;
                            grdData.Rows[i].Cells[j].Width = Unit.Pixel(60);

                        }
                    }

                }
                for (int j = 1; j < grdData.HeaderRow.Cells.Count; j++)
                {
                    grdData.HeaderRow.Cells[j].Text = grdData.HeaderRow.Cells[j].Text.Replace("*", "&nbsp;&nbsp;&nbsp;&nbsp;");
                }
                try
                {
                    grdData.Rows[i].Cells[dt.Columns.IndexOf("dummy")].CssClass = "ff";

                }
                catch
                {
                }

                for (int j = 0; j < dt.Columns.Count; j++)
                    if (dt.Columns[j].Caption == "****")
                    {
                        grdData.Rows[i].Cells[j].CssClass = "";
                        grdData.Rows[i].Cells[j].Style.Add("background-color", "#16365c");
                        grdData.Rows[i].Cells[j].Text = "&nbsp;";
                        grdData.HeaderRow.Cells[j].Text = "&nbsp;";
                    }

                i++;
            }
       //     grdData.HeaderRow.Cells[dt.Columns.IndexOf("dummy")].CssClass = "ff";
                    

            #endregion

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Session["SelectedID"] = null;
            ExportToExcel(grdData, drpForm.SelectedItem.Text.Replace(" ", "_").Replace("-", "_").Replace(".", "_").Replace("/", "_").Replace("\\", "_") + "_AGENTSKOR");
   
        }
    }
}