﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="AgentCWCReport.aspx.cs" Inherits="ARTQM.AgentCWCReport" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="form-horizontal">
<fieldset>

<legend>Arama Kriterleri</legend>

   
    
    
    <div class="row"> <div class="form-group">
    
     <label class="col-sm-2 control-label" for="dtStart">Tarih Aralığı</label>
    
  <div class="col-sm-4">
  	<asp:TextBox runat="server" id="txtRange" ClientIDMode="Static" name="txtRange" placeholder="" class="form-control input-sm"/>
  
      
  </div>
  
 
</div>

</div>
        <div class="row"> <div class="form-group">
    
     <label class="col-sm-2 control-label" for="dtStart">Proje</label>
    
  <div class="col-sm-4">
  	<asp:DropDownList runat="server" id="drpGroup" ClientIDMode="Static" name="drpGroup" class="form-control input-sm"/>
  
      
  </div>
  
 
</div>

</div>
    <div class="form-group">
  <label class="col-sm-2 control-label" for="btnSave"></label>
  <div class="col-sm-9">
    <asp:Button id="btnSearch" runat=server class="btn btn-success" Text="Ara" OnClick="btnSearch_Click" />
  </div>
</div>

   
    <legend>CWC Raporu</legend>
     <div class="form-group">    
    <div class="pull-left">
    <asp:LinkButton ID="Button1" runat="server" CssClass="btn btn-success"  OnClick="Button1_Click" data-toggle="tooltip" data-placement="bottom" title="Excel' e Aktar"><i class="fa fa-file-excel-o" aria-hidden="true"></i></asp:LinkButton>
            </div>
         <div class="pull-left"><h3> 
        <span class="label label-danger" id="spanInfo" runat="server"></span></h3></div>
      </div>
      <div class="form-group">
  
  <div class="col-sm-12">
 <asp:GridView ID="grdData" runat="server" AutoGenerateColumns="True" CssClass="table table-bordered table-responsive table-striped table-totals" 
     RowStyle-HorizontalAlign="Center"
     HeaderStyle-HorizontalAlign="Center"
     AllowPaging="False">
      </asp:GridView>
    
  </div>



</div>
    </fieldset>
  
    <script>
        $(function () {
            $('#txtRange').daterangepicker(
                {
                    locale: {
                        "format": "DD.MM.YYYY",
                        "separator": " - ",
                        "applyLabel": "Tamam",
                        "cancelLabel": "Temizle",
                        "fromLabel": "Başlangıç",
                        "toLabel": "Bitiş",
                        "customRangeLabel": "Custom",
                        "weekLabel": "H",
                        "daysOfWeek": [
                            "Pz",
                            "Pt",
                            "Sa",
                            "Ça",
                            "Pe",
                            "Cu",
                            "Ct"
                        ],
                        "monthNames": [
                            "Ocak",
                            "Şubat",
                            "Mart",
                            "Nisan",
                            "Mayıs",
                            "Haziran",
                            "Temmuz",
                            "Ağustos",
                            "Eylül",
                            "Ekim",
                            "Kasım",
                            "Aralık"
                        ],
                        "firstDay": 1

                    }
                });
        });
</script>
      </div>

</asp:Content>
