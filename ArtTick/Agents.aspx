﻿<%@ Page Title="" Language="C#" MasterPageFile="~/UserMaster.Master" AutoEventWireup="true" CodeBehind="Agents.aspx.cs" Inherits="ARTQM.Agents" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

 
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="form-horizontal">
        <fieldset>
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" runat="server" id="hResult"></h4>
                        </div>
                        <div class="modal-body" runat="server" id="bResult">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Kapat</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="divUpdate" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" runat="server" id="hResult2">Yeni Agent Kartı</h4>
                        </div>
                        <div class="modal-body" runat="server" id="bResult2">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="txtUsercode">Kullanıcı Adı</label>
                                        <div class="col-sm-9">
  	                                        <asp:TextBox runat="server" id="txtUsercode" ClientIDMode="Static" name="txtUsercode" placeholder="Kullanıcı Adı" class="form-control input-sm"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="txtEposta">E-Posta</label>
                                        <div class="col-sm-9">
  	                                        <asp:TextBox runat="server" id="txtEposta" TextMode="Email" ClientIDMode="Static" name="txtEposta" placeholder="E-Posta" class="form-control input-sm"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label class="col-sm-10 control-label" for="btnSave"></label>
                                        <div class="col-sm-2 pull-right" style="margin-right:-36px;">
                                            <asp:Button id="btnSave" runat="server" class="btn btn-success" Text="Kaydet" OnClick="btnSave_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>

                <div class="modal fade" id="divEdit" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title" runat="server" id="h1">Agent Kartı Güncelle</h4>
                        </div>
                        <div class="modal-body" runat="server" id="Div1">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="TxtEditName">İsim</label>
                                        <div class="col-sm-9">
  	                                        <asp:TextBox runat="server" id="TxtEditName" ClientIDMode="Static" name="TxtEditName" placeholder="İsim" class="form-control input-sm"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="TxtEditUsercode">Kullanıcı Adı</label>
                                        <div class="col-sm-9">
  	                                        <asp:TextBox runat="server" id="TxtEditUsercode" ClientIDMode="Static" name="TxtEditUsercode" placeholder="Kullanıcı Adı" class="form-control input-sm"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="TxtEditEposta">E-Posta</label>
                                        <div class="col-sm-9">
  	                                        <asp:TextBox runat="server" id="TxtEditEposta" TextMode="Email" ClientIDMode="Static" name="TxtEditEposta" placeholder="E-Posta" class="form-control input-sm"/>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-12">
                                        <label class="col-sm-10 control-label" for="BtnEditSave"></label>
                                        <div class="col-sm-2 pull-right" style="margin-right:-36px;">
                                            <asp:Button id="BtnEditSave" runat="server" class="btn btn-success" Text="Kaydet" OnClick="BtnEditSave_Click" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>

            <div runat="server" id="divResult"></div>

            <legend>Tanımlı Müşteri Temsilcileri</legend>
            <div class="form-group col-sm-12">
                <div class="col-sm-3 btn-group" style="margin-left:-15px;">
                    <asp:LinkButton ID="lnkNew" runat="server" class="btn btn-danger" data-toggle="tooltip" data-placement="bottom" title="Yeni Agent"  OnClick="lnkNew_Click"><i class="glyphicon glyphicon-plus" aria-hidden="true"></i> Yeni</asp:LinkButton>
                </div>
                <div class="col-sm-9">
                    <span class="label label-default" id="spanInfo" runat="server"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <br />
                    <asp:GridView ID="grdData" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-responsive table-striped" RowStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" OnRowCommand="grdData_RowCommand">
                        <Columns>
                            <asp:BoundField HeaderText="Sıra"  DataField="Index" ItemStyle-CssClass="col-sm-1" HeaderStyle-HorizontalAlign="Center">
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                                <ItemStyle CssClass="col-sm-1"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Agent ID"  DataField="LOGID" ItemStyle-CssClass="col-sm-3" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Left" CssClass="col-sm-3"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="İsim"  DataField="NAME" ItemStyle-CssClass="col-sm-2" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Left" CssClass="col-sm-2"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Kullanıcı Adı"  DataField="USERCODE" ItemStyle-CssClass="col-sm-2" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Left" CssClass="col-sm-2"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="E-Posta"  DataField="EPOSTA" ItemStyle-CssClass="col-sm-2" ItemStyle-HorizontalAlign="Left">
                                <ItemStyle HorizontalAlign="Left" CssClass="col-sm-2"></ItemStyle>
                            </asp:BoundField>
                            <asp:TemplateField HeaderText="İşlemler">
                                <ItemStyle CssClass="col-sm-1" HorizontalAlign="Left"></ItemStyle>
                                <ItemTemplate>
                                    <asp:LinkButton  runat="server" ID="btnUpdate" CommandName="Refresh_Pwd" CommandArgument='<%#Eval("LOGID").ToString() + ":" + Eval("USERCODE").ToString() %>'  Text="<span data-toggle='tooltip' title='Güncelle' ><span class='glyphicon glyphicon-refresh'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
                                    <asp:LinkButton  runat="server" ID="btnDelete" CommandName="Delete_Agent" CommandArgument='<%#Eval("LOGID").ToString() + ":" + Eval("USERCODE").ToString() %>'  Text="<span data-toggle='tooltip' title='Sil' ><span class='glyphicon glyphicon-floppy-remove'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
                                    <asp:LinkButton  runat="server" ID="btnEdit" CommandName="Edit_Agent" CommandArgument='<%#Eval("LOGID").ToString() + ":" + Eval("USERCODE").ToString() + ":" + Eval("EPOSTA").ToString() + ":" + Eval("NAME").ToString()  %>'  Text="<span data-toggle='tooltip' title='Değiştir' ><span class='glyphicon glyphicon-edit'></span></span>" ControlStyle-CssClass="btn btn-xs btn-danger" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                    <RowStyle HorizontalAlign="Center"></RowStyle>
                    </asp:GridView>
                </div>
            </div>
        </fieldset>
    </div>
</asp:Content>
